/*
 * ircd - An IRCv3 Server
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"
#include <boost/program_options.hpp>

namespace po = boost::program_options;
namespace cli = po::command_line_style;

#if BOOST_OS_WINDOWS
static
BOOL WINAPI CtrlHandler(DWORD fdwCtrlType)
{
	switch (fdwCtrlType) {
	case CTRL_C_EVENT:
	case CTRL_CLOSE_EVENT:
	case CTRL_BREAK_EVENT:
	case CTRL_LOGOFF_EVENT:
	case CTRL_SHUTDOWN_EVENT:
		ircd::shutdown();
		return TRUE;

	default:
		return FALSE;
	}
}
#else
void set_signal_handler(int signum, void(*handler)(int, siginfo_t*, void*))
{
	struct sigaction act;
	memset(&act, 0, sizeof(act));

	act.sa_sigaction = handler;
	act.sa_flags = SA_SIGINFO;

	sigaction(signum, &act, nullptr);
}

static void on_signal(int sig, siginfo_t*, void*)
{
	if (sig == SIGHUP) {
		ircd::post([]() { ircd::reload_config(nullptr); });
	} else {
		ircd::post([]() { ircd::shutdown(); });
	}
}
#endif

static
po::variables_map parse_options(int argc, char** argv)
{
	po::variables_map opt;
	po::options_description desc("Allowed options");
	desc.add_options()
		("help,h", "produce help message")
		("config-file,f", po::value<std::string>(), "use specified configuration file")
#if !BOOST_OS_WINDOWS
		("no-daemon,n", "stay in foreground (do not daemonize)")
#endif
		("lua-debug,d", "enable lua debugger")
		;
	auto const pflags = cli::default_style & ~(cli::allow_guessing);
	po::store(po::parse_command_line(argc, argv, desc, pflags), opt);
	po::notify(opt);
	if (opt.count("help")) {
		std::cout << desc << std::endl;
		exit(0);
	}
	return opt;
}

#if !BOOST_OS_WINDOWS
static void do_fork()
{
	ircd::io_ctx.notify_fork(boost::asio::io_context::fork_prepare);
	if (pid_t pid = fork()) {
		if (pid > 0) {
			ircd::io_ctx.notify_fork(boost::asio::io_context::fork_parent);
			exit(0);
		} else {
			perror("fork");
			exit(1);
		}
	}
	setsid();
	// a second fork ensures the process cannot acquire a controlling terminal
	ircd::io_ctx.notify_fork(boost::asio::io_context::fork_prepare);
	if (pid_t pid = fork()) {
		if (pid > 0) {
			ircd::io_ctx.notify_fork(boost::asio::io_context::fork_parent);
			exit(0);
		} else {
			perror("fork");
			exit(1);
		}
	}
	// close standard streams, redirect them to /dev/null
	for (int i : {0, 1, 2}) {
		close(i);
		if (open("/dev/null", O_RDONLY) < 0) perror("open");
	}
	// child fork notification
	ircd::io_ctx.notify_fork(boost::asio::io_context::fork_child);
}
#endif

int main(int argc, char** argv)
{
	try {
#if BOOST_OS_WINDOWS
		SetConsoleCP(CP_UTF8);
		SetConsoleOutputCP(CP_UTF8);
		SetConsoleCtrlHandler(CtrlHandler, TRUE);
#else
		auto lbm = boost::locale::localization_backend_manager::global();
		lbm.select("icu");
		boost::locale::localization_backend_manager::global(lbm);
#endif
		boost::locale::generator g;
		std::locale::global(g(""));
		sqlite3xx::initialize();
		std::cout << "ircd " << ircd::version_str() << std::endl;
		auto opt = parse_options(argc, argv);
		sqlite3xx::initialize();
		ircd::initialize();
		if (opt.count("lua-debug")) {
			ircd::enable_lua_debug();
			std::cerr << "Lua debugger enabled" << std::endl;
		}
		if (opt.count("config-file")) {
			ircd::load_config(opt["config-file"].as<std::string>());
		} else {
			ircd::load_config("config.lua");
		}
#if BOOST_OS_WINDOWS
		std::atexit(&ircd::shutdown);
#else
		for (int sig : {SIGINT, SIGILL, SIGFPE, SIGSEGV, SIGTERM, SIGABRT, SIGPIPE, SIGHUP, SIGQUIT}) {
			set_signal_handler(sig, on_signal);
		}
		asio::post(ircd::io_ctx, [opt, name = std::string(argv[0])]{
			if (!opt.count("no-daemon") && !opt.count("lua-debug")) {
				do_fork();
				std::atexit(&ircd::shutdown);
				std::ofstream fs{ name + ".pid" };
				if (fs.is_open()) {
					fs << getpid() << std::endl;
				}
			}
		});
#endif
		ircd::run();
	} catch (std::exception const& err) {
		ircd::write_log(ircd::LOG_DEBUG, err.what());
	}
	return 0;
}
