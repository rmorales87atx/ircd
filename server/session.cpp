/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"

using boost::system::error_code;
using namespace std::placeholders;

ircd::session_manager::session_manager()
{}

ircd::session_manager::~session_manager()
{
	listen_.clear();
	close();
}

void ircd::session_manager::close()
{
	for (auto [id, ref] : sockets_) {
		if (auto sptr = ref.lock()) {
			sptr->close();
		}
	}
	sockets_.clear();
}

void ircd::session_manager::listen(int type, std::string_view host, int port)
{
	tcp::endpoint endpoint{ asio::ip::address::from_string(std::string(host)), static_cast<unsigned short>(port) };
	listen_.emplace_back(std::make_unique<listener>(*this, type, endpoint));
}

void ircd::session_manager::accept(listener& src, tcp::socket peer)
{
	try {
		auto sptr = std::shared_ptr<session>(new session{ std::move(peer) }, std::bind(&session_manager::deleter, this, _1));
		if (process(src, sptr)) sockets_.insert_or_assign(sptr->endpoint_id(), sptr);
	} catch (std::exception const&) {}
}

void ircd::session_manager::deleter(session* sptr)
{
	if (sptr) {
		sockets_.erase(sptr->endpoint_id());
		delete sptr;
	}
}

bool ircd::session_manager::process(listener& src, session_ptr peer)
{
	auto local_port = peer->local.port();
	try {
		if (allow_connection(peer)) {
			auto conn = new_client(peer);
			log_connect_attempt(peer->remote);
			if (src.type() == LISTEN_SSL) {
				snomask_log(LOG_USER, SNO_LOCAL_USER_CONN, "Processing secure local connection from %1% on port %2%",
					peer->endpoint_id(), local_port);
				peer->enable_ssl(conn);
			} else {
				snomask_log(LOG_USER, SNO_LOCAL_USER_CONN, "Processing local connection from %1% on port %2%",
					peer->endpoint_id(), local_port);
				conn->do_host_resolve();
				peer->read();
			}
			return true;
		}
	} catch (std::exception const& err) {
		snomask_log(LOG_USER, SNO_REJECT_CONN, "Error processing connection from %1% on port %2% (%3%)",
			peer->endpoint_id(), local_port, err.what());
		peer->error(err.what());
	}
	return false;
}

void ircd::session_manager::connect(bool use_ssl, std::string_view name, std::string_view host, std::string_view port)
{
	tcp::resolver resv{ io_ctx };
	auto uptr = std::shared_ptr<session>(new session, std::bind(&session_manager::deleter, this, _1));
	boost::asio::async_connect(uptr->socket, resv.resolve({ std::string(host), std::string(port) }),
		[uptr, use_ssl, name = std::string(name), host = std::string(host), port = std::string(port)](boost::system::error_code const& err, tcp::endpoint const& endpoint) {
		if (!err) {
			try {
				uptr->local = endpoint;
				uptr->remote = uptr->socket.remote_endpoint();
				db.exec_dml("INSERT OR REPLACE INTO [link-status] ([name], [status], [last_connect], [last_error]) VALUES(?, 1, timestamp(), NULL)", name);
				auto cptr = new_client(uptr);
				cptr->set_flag(CLI_SERVER, true);
				cptr->set_nick(name);
				if (use_ssl) {
					snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Starting secure connection with %1% (%2%@%3%)", name, host, port);
					uptr->enable_ssl(cptr);
				} else {
					init_link(cptr);
				}
			} catch (std::exception const& err) {
				snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Error connecting to %3% (%1%@%2%): %4%", host, port, name, err.what());
				db.exec_dml("INSERT OR REPLACE INTO [link-status] ([name], [status], [last_connect], [last_error]) VALUES(?, 0, timestamp(), ?)", name, err.what());
				uptr->close();
			}
		} else {
			snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Error connecting to %3% (%1%@%2%): %4%", host, port, name, err.message());
			db.exec_dml("INSERT OR REPLACE INTO [link-status] ([name], [status], [last_connect], [last_error]) VALUES(?, 0, timestamp(), ?)", name, err.message());
		}
	});
}
