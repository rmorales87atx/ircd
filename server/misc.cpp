/*
 * ircd - An IRCv3 Server
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"

using boost::system::error_code;
using boost::locale::collator;
using boost::locale::collator_base;
using boost::locale::normalize;
using boost::locale::fold_case;
using boost::locale::to_upper;

namespace ircd {
	char const* utf8map_string(int m)
	{
		switch (m) {
		case UTF8MAP_RFC7700:
			return "rfc7700";
		case UTF8MAP_ASCII:
			return "ascii";
		default:
			return "??";
		}
	}

	support_info const isupport_table[] = {
		{ "AWAYLEN", []() { return format_str("%1%", ircd::info.limits[MAX_REASON]); } },
		{ "CASEMAPPING", []() { return "ascii"; } },
		{ "CHANLIMIT", []() { return format_str("#&+:%1%", ircd::info.limits[MAX_CHANNELS]); } },
		{ "CHANMODES", []() { return "be,k,l,imnrst"; } },
		{ "CHANNELLEN", []() { return format_str("#&+:%1%", ircd::info.limits[MAX_CHANLEN]); } },
		{ "CHANTYPES", []() { return "#&+"; } },
		{ "DEAF", []() { return "d"; } },
		{ "EXCEPTS", []() { return "e"; } },
		{ "KEYLEN", []() { return format_str("%1%", ircd::info.limits[MAX_KEYLEN]); } },
		{ "KICKLEN", []() { return format_str("%1%", ircd::info.limits[MAX_REASON]); } },
		{ "LINELEN", []() { return "1024"; } },
		{ "MAXCHANNELS", []() { return format_str("%1%", ircd::info.limits[MAX_CHANNELS]); } },
		{ "MAXLIST", []() { return format_str("be:%1%", ircd::info.limits[MAX_CHANLIST]); } },
		{ "MAXNICKLEN", []() { return format_str("%1%", ircd::info.limits[MAX_NICKLEN]); } },
		{ "MAXTARGETS", []() { return format_str("%1%", ircd::info.limits[MAX_TARGETS]); } },
		{ "MODES", []() { return format_str("%1%", ircd::info.limits[MAX_TARGETS]); } },
		{ "MONITOR", []() { return format_str("%1%", ircd::info.limits[MAX_MONITOR]); } },
		{ "NAMELEN", []() { return format_str("%1%", ircd::info.limits[MAX_NAMELEN]); } },
		{ "NICKLEN", []() { return format_str("%1%", ircd::info.limits[MAX_NICKLEN]); } },
		{ "PREFIX", []() { return "(ov)@+"; } },
		{ "SILENCE", []() { return format_str("%1%", ircd::info.limits[MAX_SILENCE]); } },
		{ "STATUSMSG", []() { return "@+"; } },
		{ "TARGMAX", []() { return format_str("NAMES:%1%,LIST:1,WHOIS:%1%,WHOWAS:1,KICK:%1%,PRIVMSG:%1%,NOTICE:%1%", ircd::info.limits[MAX_TARGETS]); } },
		{ "TOPICLEN", []() { return format_str("%1%", ircd::info.limits[MAX_REASON]); } },
		{ "USERLEN", []() { return format_str("%1%", 1 + ircd::info.limits[MAX_NICKLEN]); } }, // add 1 to account for possible "~"
		{ "UTF8MAPPING", []() { return utf8map_string(info.utf8mapping); } },
		{ "WHOX", nullptr },
		{ nullptr, nullptr }
	};
};

int ircd::compare_nfc(std::string_view left_, std::string_view right_)
{
	auto left{ normalize_nfc(left_) };
	auto right{ normalize_nfc(right_) };
	return std::use_facet<collator<char>>(std::locale()).compare(collator_base::identical, left, right);
}

int ircd::compare_nfkc(std::string_view left_, std::string_view right_)
{
	auto left{ normalize_nfkc(left_) };
	auto right{ normalize_nfkc(right_) };
	return std::use_facet<collator<char>>(std::locale()).compare(collator_base::identical, left, right);
}

int ircd::compare_text(std::string_view left_, std::string_view right_)
{
	auto left{ fold_case_nfkc(left_) };
	auto right{ fold_case_nfkc(right_) };
	return std::use_facet<collator<char>>(std::locale()).compare(collator_base::identical, left, right);
}

bool ircd::same_text(std::string_view left_, std::string_view right_)
{
	return 0 == compare_text(left_, right_);
}

std::string ircd::fold_case_nfkc(std::string_view str)
{
	return normalize(fold_case(std::string(str)), boost::locale::norm_nfkc);
}

std::string ircd::normalize_nfkc(std::string_view str)
{
	return normalize(std::string(str), boost::locale::norm_nfkc);
}

std::string ircd::fold_case_nfc(std::string_view str)
{
	return normalize(fold_case(std::string(str)), boost::locale::norm_nfc);
}

std::string ircd::normalize_nfc(std::string_view str)
{
	return normalize(std::string(str), boost::locale::norm_nfc);
}

std::string ircd::upper_case_nfc(std::string_view str)
{
	return normalize(to_upper(std::string(str)), boost::locale::norm_nfc);
}

std::string ircd::upper_case_nfkc(std::string_view str)
{
	return normalize(to_upper(std::string(str)), boost::locale::norm_nfkc);
}

std::string ircd::version_str()
{
	static bool init_str = false;
	static boost::format f("%1%.%2%.%3%");
	if (!init_str) {
		f% ircd::VERSION_MAJOR% ircd::VERSION_MINOR% ircd::VERSION_PATCH;
		init_str = true;
	}
	return f.str();
}

uint64_t ircd::timestamp()
{
	using boost::posix_time::ptime;
	static ptime epoch(boost::gregorian::date(1970, 1, 1));
	ptime t(boost::posix_time::microsec_clock::universal_time());
	boost::posix_time::time_duration::sec_type x = (t - epoch).total_seconds();
	return (uint64_t)x;
}

bool ircd::strscan(std::string_view input, std::string_view pattern, std::vector<std::string>& matches)
{
	auto pat = str_widen(pattern);
	auto inp = str_widen(input);
	boost::wregex expr{ pat, boost::regex::perl | boost::regex::icase };
	boost::wsmatch res;
	bool ret = boost::regex_match(inp, res, expr);
	if (ret) {
		for (unsigned int i = 0; i < res.size(); i++) {
			matches.push_back(str_narrow(res[i].first, res[i].second));
		}
	}
	return ret;
}

bool ircd::strscan(std::string_view input, std::string_view pattern)
{
	auto pat = str_widen(pattern);
	auto inp = str_widen(input);
	boost::wregex expr{ pat, boost::regex::perl | boost::regex::icase };
	boost::wsmatch res;
	return boost::regex_match(inp, res, expr);
}

int ircd::valid_mask(std::string_view what)
{
	static boost::regex host{ R"([\w\?\*\.]+$)", boost::regex::perl };
	static boost::regex user{ R"(^(?:[\w\?\*]+\!)?[\w\~\?\*]+\@[\w\?\*\.]+$)", boost::regex::perl };
	static boost::regex cidr_v4{ R"(^([0-9]{1,3}\.){3}[0-9]{1,3}\/([0-9]|[1-2][0-9]|3[0-2])$)", boost::regex::perl };
	static boost::regex cidr_v6{ R"(^s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:)))(%.+)?s*(\/([0-9]|[1-9][0-9]|1[0-1][0-9]|12[0-8]))$)", boost::regex::perl };

	std::string const input{ fold_case_nfkc(what) };
	if (boost::regex_match(input, host))
		return MASK_HOST;
	else if (boost::regex_match(input, user))
		return MASK_USER;
	else if (boost::regex_match(input, cidr_v4))
		return MASK_CIDR_V4;
	else if (boost::regex_match(input, cidr_v6))
		return MASK_CIDR_V6;
	else
		return MASK_INVALID;
}

std::string ircd::reverse_ipv4(std::string_view addr)
{
	std::vector<std::string> parts;
	boost::split(parts, addr, boost::is_any_of("."));
	std::reverse(parts.begin(), parts.end());
	return join(parts, ".");
}

bool ircd::is_utf8_multibyte(std::string_view string)
{
	if (string.empty())
		return false;

	int num = 0, max_bytes = 0;

	for (auto byte = std::begin(string); byte != std::end(string);) {
		if ((*byte & 0x80) == 0x00) {
			// U+0000 to U+007F 
			num = 1;
		} else if ((*byte & 0xE0) == 0xC0) {
			// U+0080 to U+07FF 
			num = 2;
		} else if ((*byte & 0xF0) == 0xE0) {
			// U+0800 to U+FFFF 
			num = 3;
		} else if ((*byte & 0xF8) == 0xF0) {
			// U+10000 to U+10FFFF 
			num = 4;
		} else {
			return false;
		}
		if (num > max_bytes) max_bytes = num;
		std::advance(byte, 1);
		for (int i = 1; i < num; ++i) {
			if ((*byte & 0xC0) != 0x80)
				return false;
			std::advance(byte, 1);
		}
	}

	return max_bytes > 1;
}

void ircd::split_text(std::vector<std::string>& result, std::string_view input, std::string_view delims, std::optional<boost::empty_token_policy> empty_tokens)
{
	boost::empty_token_policy empty = boost::empty_token_policy::drop_empty_tokens;
	if (empty_tokens) empty = *empty_tokens;
	if (is_utf8_multibyte(input) || is_utf8_multibyte(delims)) {
		std::u16string u16delims;
		utf8::utf8to16(std::begin(delims), std::end(delims), std::back_inserter(u16delims));
		boost::char_separator<char16_t> sep{ u16delims.c_str(), nullptr, empty };
		std::u16string u16input;
		utf8::utf8to16(std::begin(input), std::end(input), std::back_inserter(u16input));
		for (auto u16token : boost::tokenizer<decltype(sep), std::u16string::const_iterator, std::u16string>{ std::begin(u16input), std::end(u16input), sep }) {
			if (!u16token.empty() && u16token.at(0) != '\0') {
				std::string token;
				utf8::utf16to8(std::begin(u16token), std::end(u16token), std::back_inserter(token));
				result.emplace_back(token);
			} else {
				result.emplace_back("");
			}
		}
	} else {
		boost::char_separator<char> sep{ std::string(delims).c_str(), nullptr, empty };
		for (auto token : boost::tokenizer{ std::begin(input), std::end(input), sep }) {
			if (!token.empty() && token.at(0) != '\0') {
				result.emplace_back(token);
			} else {
				result.emplace_back("");
			}
		}
	}
}

bool ircd::strmatch(std::string_view expr, std::string_view match, std::optional<char32_t> any_char, std::optional<char32_t> one_char)
{
	// adapted from https://www.geeksforgeeks.org/wildcard-pattern-matching/
	using std::vector;
	std::string str, pattern;
	utf8::replace_invalid(fold_case_nfkc(expr), std::back_inserter(str));
	utf8::replace_invalid(fold_case_nfkc(match), std::back_inserter(pattern));
	size_t const n = utf8::distance(std::begin(str), std::end(str));
	size_t const m = utf8::distance(std::begin(pattern), std::end(pattern));
	size_t i, j;

	if (m == 0) {
		return (n == 0);
	}

	vector<vector<bool>> lookup;

	lookup.resize(n + 1);
	for (size_t i = 0; i <= n; i++) {
		lookup[i].resize(m + 1);
	}

	lookup[0][0] = true;

	if (!any_char) any_char = '*';
	if (!one_char) one_char = '?';

	j = 1;
	for (auto p = utf8::begin(pattern); p != utf8::end(pattern); j++, p++) {
		if (*p == *any_char) {
			lookup[0][j] = lookup[0][j - 1];
		}
	}

	i = 1;
	for (auto s = utf8::begin(str); s != utf8::end(str); i++, s++) {
		j = 1;
		for (auto p = utf8::begin(pattern); p != utf8::end(pattern); j++, p++) {
			if (*p == *any_char) {
				lookup[i][j] = lookup[i][j - 1] || lookup[i - 1][j];
			} else if (*p == *one_char || *s == *p) {
				lookup[i][j] = lookup[i - 1][j - 1];
			} else {
				lookup[i][j] = false;
			}
		}
	}

	return lookup[n][m];
}

boost::uuids::uuid ircd::name_uuid(std::string_view name)
{
	boost::uuids::name_generator_sha1 gen(info.uuid_ns);
	return gen(info.fullname + "/" + std::string(name));
}

#if !BOOST_OS_WINDOWS
# if BOOST_ENDIAN_LITTLE_BYTE
inline uint64_t htonll(uint64_t const v)
{
	union {
		uint32_t dw[2];
		uint64_t ll;
	} u;
	u.dw[0] = ntohl(static_cast<uint32_t>(v >> 32));
	u.dw[1] = ntohl(static_cast<uint32_t>(v & 0xFFFFFFFFULL));
	return u.ll;
}
# elif BOOST_ENDIAN_BIG_BYTE
#  define htonll(v) (v)
# else
#  error unknown endian
# endif
#endif

boost::uuids::uuid ircd::time_uuid()
{
	using boost::posix_time::ptime;
	static ptime epoch(boost::gregorian::date(1970, 1, 1));
	ptime t(boost::posix_time::microsec_clock::universal_time());
	auto timepoint = htonll(static_cast<uint64_t>((t - epoch).total_microseconds()));
	auto uuid = random_uuid();
	memcpy(&uuid, &timepoint, sizeof(timepoint));
	return uuid;
}

boost::uuids::uuid ircd::random_uuid()
{
	static boost::uuids::random_generator gen;
	return gen();
}

std::string ircd::uuid_string(boost::uuids::uuid const& u)
{
	return "{" + boost::uuids::to_string(u) + "}";
}

boost::uuids::uuid ircd::uuid_from_str(std::string_view in)
{
	static boost::uuids::string_generator gen;
	try {
		return gen(std::string(in));
	} catch (std::exception const&) {
		return boost::uuids::nil_generator()();
	}
}
