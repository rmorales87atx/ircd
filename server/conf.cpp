/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"
#if BOOST_OS_WINDOWS
# include <wincrypt.h>
#endif

using namespace std::placeholders;
using sol::optional;
using sol::table;
using std::string;

namespace ircd {
	limit_info const limit_table[] = {
		{ MAX_REASON, "reason", "MAX_REASON", 1, 500, 25 },
		{ MAX_CHANLIST, "chanlist", "MAX_CHANLIST", 1, 1000, 25 },
		{ MAX_TARGETS, "targets", "MAX_TARGETS", 1, 100, 10 },
		{ MAX_SILENCE, "silence", "MAX_SILENCE", 1, 1000, 25 },
		{ MAX_CHANNELS, "channels", "MAX_CHANNELS", 1, 1000, 10 },
		{ MAX_NAMELEN, "namelen", "MAX_NAMELEN", 1, 500, 100 },
		{ MAX_NICKLEN, "nicklen", "MAX_NICKLEN", 1, 40, 15 },
		{ MAX_CHANLEN, "chanlen", "MAX_CHANLEN", 1, 100, 50 },
		{ MAX_MONITOR, "monitor", "MAX_MONITOR", 1, 1000, 50 },
		{ MAX_KEYLEN, "keylen", "MAX_KEYLEN", 1, 80, 30 },
		{ MAX_NICKDELAY, "nickdelay", "MAX_NICKDELAY", 1, 1440, 10 },
		{ MAX_CHANDELAY, "chandelay", "MAX_CHANDELAY", 1, 1440, 20 },
		{ MAX_TSDELTA, "ts_delta", "MAX_TSDELTA", 1, 86400, 3 },
		{ -1, nullptr, nullptr, 0, 0, 0 }
	};
};

enum LOAD_FLAGS {
	F_REHASH = (1 << 0),
	F_ONLY_ONCE = (1 << 1),
	F_REQUIRED = (1 << 2),
	F_GOT_LOG = (1 << 3),
	F_GOT_MOTD = (1 << 4),
	F_GOT_SERVER = (1 << 5),
	F_GOT_NETWORK = (1 << 6),
	F_GOT_ADMIN = (1 << 7),
	F_GOT_LISTEN = (1 << 9),
	F_GOT_AUTH = (1 << 9),
	F_GOT_LIMITS = (1 << 10),
	F_GOT_SSL = (1 << 11),
	F_GOT_MODULES = (1 << 12),
	F_GOT_BLACKLIST = (1 << 13),
};

ircd::conf_reader::conf_reader(std::string_view src)
	: source_{ src }
	, flags_{ 0 }
{
	blocks_ = {
		{ "log", F_GOT_LOG | F_ONLY_ONCE, std::bind(&conf_reader::read_log, this, _1) },
		{ "server", F_GOT_SERVER | F_REQUIRED | F_ONLY_ONCE, std::bind(&conf_reader::read_server, this, _1) },
		{ "motd", F_GOT_MOTD | F_ONLY_ONCE, std::bind(&conf_reader::read_motd, this, _1) },
		{ "network", F_GOT_NETWORK | F_REQUIRED | F_ONLY_ONCE, std::bind(&conf_reader::read_network, this, _1) },
		{ "admin", F_GOT_ADMIN | F_REQUIRED | F_ONLY_ONCE, std::bind(&conf_reader::read_admin, this, _1) },
		{ "ssl", F_GOT_SSL | F_ONLY_ONCE, std::bind(&conf_reader::read_ssl, this, _1) },
		{ "listen", F_GOT_LISTEN, std::bind(&conf_reader::read_listen, this, _1) },
		{ "auth", F_GOT_AUTH, std::bind(&conf_reader::read_auth, this, _1) },
		{ "link", 0, std::bind(&conf_reader::read_link, this, _1) },
		{ "operator", 0, std::bind(&conf_reader::read_oper, this, _1) },
		{ "service", 0, std::bind(&conf_reader::read_service, this, _1) },
		{ "kline", 0, std::bind(&conf_reader::read_kline, this, _1) },
		{ "jupe", 0, std::bind(&conf_reader::read_jupe, this, _1) },
		{ "limits", F_GOT_LIMITS | F_ONLY_ONCE, std::bind(&conf_reader::read_limits, this, _1) },
		{ "modules", F_GOT_MODULES | F_ONLY_ONCE, std::bind(&conf_reader::read_modules, this, _1) },
		{ "blacklist", F_GOT_BLACKLIST | F_ONLY_ONCE, std::bind(&conf_reader::read_blacklist, this, _1) },
		{ "webirc", 0, std::bind(&conf_reader::read_webirc, this, _1) },
	};
	// create the functions used to process blocks
	for (conf_block& info : blocks_) {
		lua_[info.name] = [&, this](sol::table data) { block_function(info, data); };
	}
	// simple utility functions/aliases
	lua_.script("function range(start,stop,n) local r = {} for i = start,stop,n do r[#r+1] = i end return r end");
}

void ircd::conf_reader::block_function(conf_block& info, sol::table& data)
{
	try {
		int f = 0;
		if (info.flag != 0) {
			f = info.flag & ~(F_ONLY_ONCE | F_REQUIRED);
			if (f != 0 && (info.flag & F_ONLY_ONCE) && (flags_ & f)) {
				throw std::runtime_error{ "block already present" };
			}
		}
		if (data.valid() && !data.empty()) {
			info.handler(data);
			if (f != 0) flags_ |= f;
		}
	} catch (std::exception const& err) {
		std::cerr << "config: error in block '" << info.name << "': " << err.what() << std::endl;
		abort();
	}
}

void ircd::conf_reader::load(uint32_t flags)
{
	try {
		sqlite3xx::savepoint sp{ db };
		sp.start("conf_load");
		flags_ = flags;
		// if coming from REHASH, do the rehash prep
		if (flags & F_REHASH) {
			prep_rehash();
		}
		// execute the file
		lua_.script_file(source_);
		// check for required blocks
		for (auto const& info : blocks_) {
			if (!(info.flag & F_REQUIRED)) continue;
			int f = info.flag & ~(F_ONLY_ONCE | F_REQUIRED);
			if (f != 0 && !(flags_ & info.flag)) {
				throw std::runtime_error{ "config: missing block '" + string{info.name} + "'" };
			}
		}
		sp.release();
		// rehash post-processing
		if (flags & F_REHASH) {
			post_rehash();
		}
	} catch (sqlite3xx::error const& err) {
		if (flags & F_REHASH) {
			notify_snomask(SNO_OPER_STATUS, "*** Notice -- Database error during config load: %1%", err.what());
			notify_snomask(SNO_OPER_STATUS, "*** Notice -- Configuration reverted");
		} else {
			std::cerr << err.what() << std::endl;
			abort();
		}
	} catch (std::exception const& err) {
		if (flags & F_REHASH) {
			notify_snomask(SNO_OPER_STATUS, "*** Notice -- Failed to load config: %1%", err.what());
			notify_snomask(SNO_OPER_STATUS, "*** Notice -- Configuration reverted");
		} else {
			std::cerr << err.what() << std::endl;
			abort();
		}
	}
}

void ircd::conf_reader::prep_rehash()
{
	// use session monitor to detect changes that can affect clients (bans, auth, etc.)
	monitor_ = std::make_unique<sqlite3xx::session>(db, "main");
	for (char const* tbname : { "auth", "operators", "services", "oper-hosts", "server-links", "server-connects", "webirc-gateways" }) {
		monitor_->attach(tbname);
		db.exec_dml(format_str("DELETE FROM [%1%]", tbname));
	}
	// special handling for jupe/kline so we don't delete entries set by opers
	monitor_->attach("jupe");
	monitor_->attach("server-bans");
	db.exec_dml("DELETE FROM [jupe] WHERE [status]=?", JUPE_STATUS_PERM);
	db.exec_dml("DELETE FROM [server-bans] WHERE [status]=0");
}

void ircd::conf_reader::post_rehash()
{
	reload_modules();
	uint32_t flags = 0;
	auto chgset = monitor_->changes();
	for (auto iter = chgset.start(); !iter.eof(); iter.next()) {
		if (same_text(iter.table(), "auth")) {
			flags |= RELOAD_AUTH;
		} else if (same_text(iter.table(), "jupe")) {
			flags |= RELOAD_JUPES;
			// broadcast JUPE ADD/DEL as needed
			if (iter.type() == SQLITE_DELETE || iter.type() == SQLITE_UPDATE) {
				auto name = iter.old_value(0).get_text();
				if (iter.type() != SQLITE_UPDATE) {
					auto type = *iter.old_value(1).get_int();
					if (type == JUPE_NICK) {
						notify_snomask(SNO_NICK_JUPE, "*** Notice -- Nick jupe removed for %1%", name);
					} else if (type == JUPE_CHANNEL) {
						notify_snomask(SNO_CHAN_JUPE, "*** Notice -- Channel jupe removed for %1%", name);
					}
				}
			}
			if (iter.type() == SQLITE_INSERT || iter.type() == SQLITE_UPDATE) {
				if (iter.type() != SQLITE_UPDATE) {
					auto name = iter.new_value(0).get_text();
					auto type = *iter.new_value(1).get_int();
					if (type == JUPE_NICK) {
						notify_snomask(SNO_NICK_JUPE, "*** Notice -- Nick jupe added for %1%", name);
					} else if (type == JUPE_CHANNEL) {
						notify_snomask(SNO_CHAN_JUPE, "*** Notice -- Channel jupe added for %1%", name);
					}
				}
			}
		} else if (same_text(iter.table(), "operators")) {
			flags |= RELOAD_OPERS;
		} else if (same_text(iter.table(), "services")) {
			flags |= RELOAD_SERVICES;
		} else if (same_text(iter.table(), "oper-hosts")) {
			flags |= RELOAD_OPERS;
		} else if (same_text(iter.table(), "server-bans")) {
			flags |= RELOAD_KLINES;
			if (iter.type() == SQLITE_DELETE) {
				auto mask = iter.old_value(1).get_text();
				notify_snomask(SNO_KILLS, "*** Notice -- KLINE removed for %1%", mask);
			}
			if (iter.type() == SQLITE_INSERT) {
				auto mask = iter.new_value(1).get_text();
				notify_snomask(SNO_KILLS, "*** Notice -- KLINE added for %1%", mask);
			}
		} else if (same_text(iter.table(), "server-links")) {
			flags |= RELOAD_SERVERS;
		} else if (same_text(iter.table(), "server-connects")) {
			flags |= RELOAD_SERVERS;
		} else if (same_text(iter.table(), "services")) {
			flags |= RELOAD_SERVICES;
		}
		after_conf_reload(flags);
	}
}

void ircd::conf_reader::load_motd_file(std::string& dest, std::string_view source)
{
	static size_t const MAX_MOTD = 64 * 1024; // size of motd in kilobytes
	std::fstream fs{ std::string(source) };
	if (fs.is_open()) {
		fs.seekg(0, std::ios::end);
		auto size = static_cast<size_t>(fs.tellg());
		dest.resize(std::min(MAX_MOTD, size) + 1);
		fs.seekg(0);
		fs.read(&dest[0], dest.size());
		fs.close();
	}
}

void ircd::conf_reader::read_server(sol::table& tab)
{
	if (flags_ & F_REHASH) {
		unsigned int new_maxclients = tab["max_clients"];
		if (new_maxclients > info.max_clients) {
			notify_snomask(SNO_OPER_STATUS, "*** Notice -- Maximum clients increased to %1%", new_maxclients);
			info.max_clients = new_maxclients;
		} else if (new_maxclients < info.max_clients) {
			notify_snomask(SNO_OPER_STATUS, "*** Notice -- Maximum clients lower than current setting (%1% < %2%) - change will not take effect",
				new_maxclients, info.max_clients);
		}
	} else {
		if (tab.get<int>("max_clients") < 1) throw std::runtime_error{ "max_clients must be at least 1" };
		info.fullname = normalize_nfkc(tab.get<std::string>("full_name"));
		info.uuid_ns = uuid_from_str(tab["uuid_ns"].get_or<std::string>("{6ba7b811-9dad-11d1-80b4-00c04fd430c8}"));
		info.id = uuid_string(name_uuid(""));
		info.description = tab["description"];
		info.max_clients = tab["max_clients"];
		sol::optional<string> map = tab["utf8mapping"];
		if (!map || same_text(*map, "ascii")) {
			info.utf8mapping = UTF8MAP_ASCII;
		} else if (same_text(*map, "rfc7700")) {
			info.utf8mapping = UTF8MAP_RFC7700;
		}
		if (sol::optional kdb = tab["kline_db_file"]) {
			info.kline_db_file = *kdb;
		}
		db.exec_dml("INSERT INTO [link-status] ([name], [SID], [status]) VALUES(?, ?, null)", info.fullname, info.id);
		std::cout << info.fullname << " (" << info.id << ") - " << info.description << std::endl;
	}
}

void ircd::conf_reader::read_motd(sol::table& tab)
{
	if (optional<string> fname = tab["user_file"]) {
		load_motd_file(info.motd_user_text, *fname);
	} else if (optional<string> text = tab["user_text"]) {
		info.motd_user_text = *text;
	}
	if (optional<string> fname = tab["oper_file"]) {
		load_motd_file(info.motd_oper_text, *fname);
	} else if (optional<string> text = tab["oper_text"]) {
		info.motd_oper_text = *text;
	}
}

void ircd::conf_reader::read_log(sol::table& tab)
{
	if (flags_ & F_REHASH) return;
	std::optional<uint32_t> enable;
	if (optional<table> codes = tab["enable"]; codes.has_value()) {
		uint32_t value = 0;
		for (auto [k, v] : *codes) {
			auto val = v.as<string>();
			if (same_text(val, "debug")) value |= LOG_DEBUG;
			if (same_text(val, "user")) value |= LOG_USER;
			if (same_text(val, "oper")) value |= LOG_OPER;
			if (same_text(val, "server")) value |= LOG_SERVER;
			if (same_text(val, "kill")) value |= LOG_KILL;
		}
		enable = value;
	}
	if (optional<string> file = tab["file"]) {
		open_log(*file, enable);
	}
}

#if BOOST_OS_WINDOWS

static void add_windows_root_certs(boost::asio::ssl::context& ctx)
{
	HCERTSTORE hStore = CertOpenSystemStore(0, L"ROOT");
	if (hStore == NULL) {
		return;
	}

	X509_STORE* store = X509_STORE_new();
	PCCERT_CONTEXT pContext = NULL;
	while ((pContext = CertEnumCertificatesInStore(hStore, pContext)) != NULL) {
		X509* x509 = d2i_X509(NULL,
			(const unsigned char**)&pContext->pbCertEncoded,
			pContext->cbCertEncoded);
		if (x509 != NULL) {
			X509_STORE_add_cert(store, x509);
			X509_free(x509);
		}
	}

	CertFreeCertificateContext(pContext);
	CertCloseStore(hStore, 0);

	SSL_CTX_set_cert_store(ctx.native_handle(), store);
}

#endif

void ircd::conf_reader::read_ssl(sol::table& tab)
{
	if (flags_ & F_REHASH) return;
	optional<string> cert_file = tab["certificate_chain_file"];
	if (!cert_file) throw std::runtime_error{ "missing 'certificate_chain_file'" };
	optional<string> key_file = tab["private_key_file"];
	if (!cert_file) throw std::runtime_error{ "missing 'private_key_file'" };
	ssl_context.set_options(asio::ssl::context::default_workarounds
		| asio::ssl::context::no_sslv2
		| asio::ssl::context::no_sslv3
		| asio::ssl::context::no_tlsv1
		| asio::ssl::context::single_dh_use);
	ssl_context.use_certificate_chain_file(*cert_file);
	ssl_context.use_private_key_file(*key_file, asio::ssl::context::pem);
#if BOOST_OS_WINDOWS
	add_windows_root_certs(ssl_context);
#else
	ssl_context.set_default_verify_paths();
#endif
	info.require_client_certificate = tab["require_client_certificate"].get_or(true);
	if (optional<table> load_vf = tab["verify_files"]; load_vf.has_value()) {
		for (auto [k, v] : *load_vf) {
			ssl_context.load_verify_file(v.as<string>());
		}
	}
}

void ircd::conf_reader::read_listen(sol::table& tab)
{
	if (flags_ & F_REHASH) return;
	optional<int> port = tab["port"];
	optional<table> ports = tab["ports"];
	if (!port && !ports) throw std::runtime_error{ "missing port data" };
	table hosts = tab["hosts"];
	if (!hosts.valid()) throw std::runtime_error{ "missing host data" };
	bool use_ssl = tab["ssl"].get_or(false);
	for (auto [hk, hv] : hosts) {
		auto host = hv.as<std::string>();
		int type = 0;
		if (use_ssl != 0) {
			if (!(flags_ & F_GOT_SSL)) throw std::runtime_error{ "cannot use SSL in listen{} unless the SSL{} block is present" };
			type = LISTEN_SSL;
		} else {
			type = LISTEN_PLAINTEXT;
		}
		if (type != 0) {
			if (port) add_listen(type, host, *port);
			if (ports) {
				for (auto [pk, pv] : *ports) {
					add_listen(type, host, pv.as<int>());
				}
			}
		}
	}
}

void ircd::conf_reader::read_network(sol::table& tab)
{
	if (flags_ & F_REHASH) return;
	info.net_name = normalize_nfkc(tab.get<std::string>("name"));
	info.net_description = tab["description"];
}

void ircd::conf_reader::read_admin(sol::table& tab)
{
	info.admin[0] = tab["name"];
	info.admin[1] = tab["description"];
	info.admin[2] = tab["email"];
}

void ircd::conf_reader::read_link(sol::table& tab)
{
	auto name = normalize_nfkc(tab.get<std::string>("name"));
	table hosts = tab["hosts"];
	if (!hosts.valid() || hosts.empty()) throw std::runtime_error{ "missing 'hosts': " + name };
	sol::optional<string> passwd = tab["password"];
	if (!passwd) throw std::runtime_error{ "missing link password: " + name };
	bool ssl_req = tab["ssl_required"].get_or(false);
	for (auto [hk, hv] : hosts) {
		db.exec_dml("INSERT INTO [server-links] ([address], [name], [password], [ssl_required]) VALUES(?, ?, ?, ?)",
			hv.as<string>(), name, *passwd, ssl_req);
	}
	if (optional<table> conn = tab["connect"]) {
		auto n = db.exec_dml("INSERT INTO [server-connects] ([name], [host], [port], [frequency], [ssl_required]) VALUES(?, ?, ?, ?, ?)",
			name, conn->get<string>("host"), conn->get<int>("port"), conn->get<int>("frequency"), ssl_req);
		if (n != 0) db.exec_dml("INSERT OR REPLACE INTO [link-status] ([name], [status], [last_connect], [last_error]) VALUES(?, 0, NULL, NULL)", name);
	}
}

void ircd::conf_reader::read_auth(sol::table& tab)
{
	db.exec_dml("INSERT INTO [auth] ([user], [password], [spoof], [incomplete_limit], [check_ident], [require_ident]) VALUES(?, sha256(?), ?, ?, ?, ?)",
		tab["mask"].get_or(string{}), tab["password"].get_or(string{}), tab["spoof"].get_or(string{}), tab["incomplete_limit"].get_or(3),
		tab["check_ident"].get_or(true), tab["require_ident"].get_or(false));
}

void ircd::conf_reader::read_oper(sol::table& tab)
{
	string name = normalize_nfkc(tab.get<string>("name"));
	string passwd = tab["password"].get_or(string{});
	if (passwd.empty()) throw std::runtime_error{ "missing password: " + name };
	bool ssl_req = tab["ssl_required"].get_or(false);
	db.exec_dml("INSERT INTO [operators] ([name], [password], [ssl_required]) VALUES(?, sha256(?), ?)", name, passwd, ssl_req);
	if (optional<table> hosts = tab["hosts"]) {
		for (auto [k, v] : *hosts) {
			db.exec_dml("INSERT INTO [oper-hosts] ([name], [hostmask]) VALUES(?,?)", name, v.as<string>());
		}
	}
	if (optional<table> certs = tab["ssl_fingerprints"]) {
		for (auto [k, v] : *certs) {
			db.exec_dml("INSERT INTO [oper-certs] ([name], [fingerprint]) VALUES(?,?)", name, v.as<string>());
		}
	}
}

void ircd::conf_reader::read_service(sol::table& tab)
{
	string name = normalize_nfkc(tab.get<std::string>("name"));
	db.exec_dml("INSERT INTO [services] ([name], [password], [spoof], [type], [ssl_required]) VALUES(?,sha256(?),?,?,?)",
		name, tab.get<string>("password"), tab.get<string>("spoof"), tab.get<string>("type"), tab.get<bool>("ssl_required"));
	db.exec_dml("INSERT INTO [jupe] ([name], [type], [status], [reason]) VALUES(?, ?, ?, 'Service')",
		name, JUPE_NICK, JUPE_STATUS_PERM);
	if (optional<table> certs = tab["ssl_fingerprints"]) {
		for (auto [k, v] : *certs) {
			db.exec_dml("INSERT INTO [service-certs] ([name], [fingerprint]) VALUES(?,?)", name, v.as<string>());
		}
	}
}

void ircd::conf_reader::read_kline(sol::table& tab)
{
	string mask = tab["mask"];
	int type = valid_mask(mask);
	if (type != MASK_INVALID) {
		db.exec_dml("INSERT OR REPLACE INTO [server-bans] ([mask], [type], [status], [created]) VALUES(?, ?, 0, timestamp())", mask, type);
		auto rowid = db.last_rowid();
		if (optional<string> comment = tab["comment"]) {
			db.exec_dml("UPDATE [server-bans] SET [comment]=?2 WHERE [_rowid_]=?1", rowid, *comment);
		}
	}
}

void ircd::conf_reader::read_jupe(sol::table& tab)
{
	if (optional<string> name = tab["nick"]) {
		db.exec_dml("INSERT INTO [jupe] ([name], [type], [status], [reason]) VALUES(?, ?, ?, ?)",
			normalize_nfkc(*name), JUPE_NICK, JUPE_STATUS_PERM, tab["reason"].get_or(string{}));
	} else if (optional<string> name = tab["channel"]) {
		db.exec_dml("INSERT INTO [jupe] ([name], [type], [status], [reason]) VALUES(?, ?, ?, ?)",
			normalize_nfkc(*name), JUPE_CHANNEL, JUPE_STATUS_PERM, tab["reason"].get_or(string{}));
	}
}

void ircd::conf_reader::read_limits(sol::table& tab)
{
	if (flags_ & F_REHASH) return;
	for (limit_info const* iptr = limit_table; iptr->lua_name; iptr++) {
		optional<unsigned int> lvalue = tab[iptr->lua_name];
		if (lvalue && (*lvalue >= iptr->min_value) && (*lvalue <= iptr->max_value)) {
			info.limits[iptr->id] = *lvalue;
		} else {
			info.limits[iptr->id] = iptr->default_value;
		}
	}
}

void ircd::conf_reader::read_modules(sol::table& tab)
{
	if (flags_ & F_REHASH) return;
	std::vector<std::string> loaded;
	for (auto [k, v] : tab) {
		auto source = v.as<string>();
		try {
			auto mptr = load_module(source);
			loaded.push_back(mptr->name());
		} catch (std::exception const& err) {
			std::cerr << "Error loading module " << source << ": " << err.what() << std::endl;
			abort();
		}
	}
	std::cout << "Modules loaded: " << ircd::join(loaded, " ") << std::endl;
}

void ircd::conf_reader::read_blacklist(sol::table& tab)
{
	if (optional<std::string> bname = tab["name"]) {
		auto prior{ info.dnsbl_name };
		info.dnsbl_name = *bname;
		if (!same_text(prior, *bname)) {
			if (flags_ & F_REHASH) notify_snomask(SNO_OPER_STATUS, "DNSBL updated: %1%", *bname);
		}
	} else {
		info.dnsbl_name.clear();
		if (flags_ & F_REHASH) notify_snomask(SNO_OPER_STATUS, "DNSBL disabled");
	}

	if (optional<std::string> messg = tab["ban_message"]) {
		info.dnsbl_ban_message = *messg;
	} else {
		info.dnsbl_ban_message.clear();
	}
}

void ircd::conf_reader::read_webirc(sol::table& tab)
{
	string name = tab["host"];
	string passwd = tab["password"];
	string desc = tab["description"];
	db.exec_dml("INSERT INTO [webirc-gateways] ([host], [password], [description]) VALUES(?,sha256(?),?)",
		name, passwd, desc);
}
