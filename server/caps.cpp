/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"

namespace ircd {
	std::map<std::string, cap_info> cap_names = {
		{ "account-notify", { CAP_ACCOUNT_NOTIFY, 1 } },
		{ "account-tag", { CAP_ACCOUNT_TAG, 1 } },
		{ "away-notify", { CAP_AWAY_NOTIFY, 1 } },
		{ "batch", { CAP_BATCH, 1 } },
		{ "cap-notify", { CAP_CAPNOTIFY, 0 } },
		{ "chghost", { CAP_CHGHOST, 1 } },
		{ "echo-message", { CAP_ECHO_MESSAGE, 1 } },
		{ "extended-join", { CAP_EXTENDED_JOIN, 1 } },
		{ "invite-notify", { CAP_INVITE_NOTIFY, 1 } },
		{ "labeled-response", { CAP_LABELED_RESPONSE, 1 } },
		{ "message-tags", { CAP_MESSAGE_TAGS, 1 } },
		{ "metadata", { CAP_METADATA, 0 } },
		{ "monitor", { CAP_MONITOR, 1 } },
		{ "multi-prefix", { CAP_MULTI_PREFIX, 1 } },
		{ "sasl", { CAP_SASL, 0 } },
		{ "server-time", { CAP_SERVER_TIME, 1 } },
		{ "tls", { CAP_TLS, 0 } },
		{ "userhost-in-names", { CAP_USERHOST_IN_NAMES, 1 } },
	};
};

std::string ircd::cap_to_str(uint64_t caps)
{
	std::string res;
	for (auto [k,v] : cap_names) {
		if (caps & v.mask) {
			if (!res.empty()) {
				res.append(" ");
			}
			res.append(k);
		}
	}
	return res;
}

uint64_t ircd::cap_from_str(std::string_view src, std::string* err)
{
	uint64_t caps{ 0 };
	if (!src.empty()) {
		std::vector<std::string> tokens;
		split_text(tokens, src, " \t\r\n", std::nullopt);
		for (auto [k, v] : cap_names) {
			// check to see if the current cap matches the given source list
			if (!v.impl) continue;
			auto id = k; // clang seems to refuse to let us use `k` in the lambda below
			auto iter = std::find_if(tokens.begin(), tokens.end(),
				[id] (std::string const& token) { return same_text(token, id); });
			if (iter != tokens.end()) {
				// found cap in the source list
				// flip the bit and remove from the list
				caps |= v.mask;
				tokens.erase(iter);
			} else {
				// not found
				caps &= ~v.mask;
			}
		}
		// if the source list isn't empty
		// reset the bits to flag the error
		if (!tokens.empty()) {
			caps = 0;
			if (err) {
				*err = tokens.front();
			}
		}
	}
	return caps;
}

uint64_t ircd::create_cap(std::string_view name_)
{
	std::string name{ name_ };
	auto iter = cap_names.find(name);
	if (iter != cap_names.end()) {
		auto& info = iter->second;
		if (!info.impl) {
			info.impl = true;
		}
		return info.mask;
	} else {
		uint64_t sz = cap_names.size();
		if (sz < (sizeof(uint64_t) * 8)) {
			auto n = static_cast<uint64_t>(1) << (sz + 1);
			cap_names[name] = { n, 1 };
			return n;
		}
	}
	return 0;
}

uint64_t ircd::caps_implemented()
{
	uint64_t caps{ 0 };
	for (auto [k, v] : cap_names) {
		if (v.impl) caps |= v.mask;
	}
	return caps;
}
