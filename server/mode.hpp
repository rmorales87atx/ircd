/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#pragma once

namespace ircd {
	/// Mode categories.
	enum {
		MODE_CAT_A, // Mode that modifies list
		MODE_CAT_B, // Mode that changes setting, always with parameter
		MODE_CAT_C, // Mode that changes setting, parameter when + and no parameter when -
		MODE_CAT_D, // Mode that changes setting, never a parameter
	};

	/// Channel modes.
	enum : uint32_t {
		/* b */ CMODE_BAN = (1 << 0),
		/* e */ CMODE_EXEMPT = (1 << 1),
		/* i */ CMODE_INVITE_ONLY = (1 << 2),
		/* k */ CMODE_KEY = (1 << 3),
		/* l */ CMODE_LIMIT = (1 << 4),
		/* m */ CMODE_MODERATED = (1 << 5),
		/* n */ CMODE_NO_EXTERNAL = (1 << 6),
		/* o */ CMODE_OPERATOR = (1 << 7),
		/* r */ CMODE_REGISTERED = (1 << 8),
		/* s */ CMODE_SECRET = (1 << 9),
		/* t */ CMODE_TOPIC_LOCKED = (1 << 10),
		/* v */ CMODE_VOICE = (1 << 11),
		/* x */ CMODE_OPERS = (1 << 12),
	};

	/// User modes.
	enum : uint32_t {
		/* a */ UMODE_AWAY = (1 << 0),
		/* b */ UMODE_BOT = (1 << 1),
		/* d */ UMODE_DEAF = (1 << 2),
		/* h */ UMODE_HELPER = (1 << 3),
		/* i */ UMODE_INVISIBLE = (1 << 4),
		/* o */ UMODE_OPERATOR = (1 << 5),
		/* s */ UMODE_SNOTICES = (1 << 6),
		/* w */ UMODE_WALLOPS = (1 << 7),
		/* z */ UMODE_SECURE = (1 << 8),
		/* S */ UMODE_SERVICE = (1 << 9),
	};

	/// Structure used for the IRC mode tables.
	struct irc_mode {
		int category;
		std::string shortname;
		uint32_t mask;
	};

	/// Structure used to describe mode changes parsed from an input string.
	struct mode_change_info {
		std::string alpha;
		uint32_t mask;
		int category;
		bool value;
		std::string param;
	};

	/// Table of channel modes.
	extern tsl::ordered_map<char, irc_mode> chmodes;

	/// Table of user modes.
	extern tsl::ordered_map<char, irc_mode> umodes;

	/// Parse a channel mode string (e.g., "+ntlk 1 23").
	std::vector<mode_change_info> parse_chmode_change(std::string_view modes, std::vector<std::string> const& params, std::string* err = nullptr);

	/// Register a new user mode (from Lua module).
	uint32_t new_umode(char chr, std::string_view desc);

	/// Register a new channel mode (from Lua module).
	uint32_t new_chmode(char chr, int category, std::string_view desc);

	/// Retrieve information for the given channel mode.
	std::optional<irc_mode> find_chmode(char);

	/// Retrieve information for the given user mode.
	std::optional<irc_mode> find_umode(char);

	/// Apply a string of IRC user modes to the given bitfield.
	/// Invalid modes are returned in "err".
	void apply_umodes(std::string_view str, uint32_t& dest, std::string* err = nullptr);

	/// Convert a mode bitfield to a user mode string.
	std::string umode_str(uint32_t val);

	/// Compare two usermode bitfields.
	std::string umode_diff(uint32_t cur, uint32_t prev);

	/// Convert a mode bitfield to a channel mode string.
	std::string chmode_str(int category, uint32_t val);
};
