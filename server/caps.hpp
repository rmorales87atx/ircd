/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#pragma once

namespace ircd {
	/// IRCv3 Capabilities
	enum : uint64_t {
		CAP_ACCOUNT_NOTIFY = (1 << 0),
		CAP_ACCOUNT_TAG = (1 << 1),
		CAP_AWAY_NOTIFY = (1 << 2),
		CAP_BATCH = (1 << 3),
		CAP_CAPNOTIFY = (1 << 4),
		CAP_CHGHOST = (1 << 5),
		CAP_LABELED_RESPONSE = (1 << 6),
		CAP_ECHO_MESSAGE = (1 << 7),
		CAP_EXTENDED_JOIN = (1 << 8),
		CAP_INVITE_NOTIFY = (1 << 9),
		CAP_MESSAGE_TAGS = (1 << 10),
		CAP_METADATA = (1 << 11),
		CAP_MONITOR = (1 << 12),
		CAP_MULTI_PREFIX = (1 << 13),
		CAP_SASL = (1 << 14),
		CAP_SERVER_TIME = (1 << 15),
		CAP_TLS = (1 << 16),
		CAP_USERHOST_IN_NAMES = (1 << 17),
		CAP_SETNAME = (1 << 18),
	};

	struct cap_info {
		uint64_t mask;
		bool impl;
	};

	extern std::map<std::string, cap_info> cap_names;

	/// Determine which capabilities are currently implemented
	uint64_t caps_implemented();

	/// Convert a list of capabilities to a space-delimited string of identifiers
	std::string cap_to_str(uint64_t caps);

	/// @brief Convert a space-delimited string of CAP identifiers to a list.
	/// If there is at least one invalid identifier an empty list is returned;
	/// if the 'err' pointer is present the invalid code can be determined.
	uint64_t cap_from_str(std::string_view src, std::string* err = nullptr);

	/// @brief Add new IRCv3 capability (presumably implemented by a Lua module)
	/// The value returned is an identifier which can be used for client::has_cap()
	/// 0 is returned if the capability name already exists
	uint64_t create_cap(std::string_view name);
};
