/*
 * ircd - An IRCv3 Server
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#pragma once

namespace ircd {
	/// Global ASIO io_context object.
	extern boost::asio::io_context io_ctx;

	/// SSL context.
	extern boost::asio::ssl::context ssl_context;

	/// Global database handle.
	extern sqlite3xx::database db;

	/// Server information block.
	extern server_info info;

	/// App initialization.
	void initialize();

	/// Load and parse the specified config file.
	void load_config(std::string_view source);

	/// Reload ("rehash") config file.
	void reload_config(client_ptr sptr);

	/// Called in main() to start the ASIO processing loop.
	void run();

	/// Shut down the server.
	void shutdown();

	/// Post a function to the ASIO io_service object for execution.
	void post(std::function<void()> fn);

	/// Load the specified Lua module.
	module_ptr load_module(std::string_view source);

	/// Retrieve an iterator referencing the first loaded module.
	std::vector<module_ptr>::const_iterator first_module();

	/// Retrieve an iterator referencing the last loaded module.
	std::vector<module_ptr>::const_iterator last_module();

	/// Reload all active modules.
	void reload_modules();

	/// Retrieve the next sequence number for clients subscribed to CAP 'draft/msgid'.
	uint64_t next_msg_id();

	/// Determine the number of registered client connections.
	size_t num_sessions();

	/// Determine the number of total connections, including currently unregistered clients.
	size_t num_connections();

	/// Register built-in command handler.
	void add_command(std::string_view name, std::function<void(client_ptr cptr, client_ptr sptr, std::optional<message> msg)> handler);

	/// @brief Execute command received from the client.
	/// @param sptr Source client
	/// @param msg Parsed IRC message buffer
	/// @param nbytes Number of bytes received with this message; used for STATS M
	///
	/// PONG commands are quietly ignored from all connections.
	///
	/// Client connections SHOULD NOT include a prefix; but, if they do it must be the correct nick!user@host.
	///
	/// Server connections that include a prefix MUST use entity identifiers (EID) as the prefix.
	///
	/// Servers that transmit an unknown EID will receive SQUIT/KILL as appropriate, and the message will be discarded.
	///
	/// Servers MAY transmit numerics as commands; these MUST be three digits leading to a local client on this server.
	///
	/// Unknown commands from server clients WILL NOT generate error code 421 (ERR_UNKNOWNCOMMAND).
	/// This is to avoid infinite loop situations.
	///
	/// Successfully completed commands, from users and servers, will update command statistics.
	/// User commands will update the 'last_command' timestamp, which is used as an idle timer for WHOIS.
	void do_command(client_ptr sptr, message const& msg, size_t nbytes);

	/// @brief Execute command on behalf of the specified user.
	/// @param sptr Affected source client.
	/// @param cmd Name of command being executed.
	///
	/// This is typically used for situations when additional related command replies "should" be generated.
	/// e.g., the MOTD generates as part of user registration acknowledgement.
	void do_command(client_ptr sptr, char const* cmd);

	/// @brief Determine IRC message prefix
	/// @param sptr Source client
	/// @param uptr Client receiving the message
	std::string msg_prefix(client_ptr sptr, client_ptr uptr, ircd::message& msg);

	/// Establish a new client connection bound to the specified socket..
	client_ptr new_client(session_ptr peer);

	/// Find an appropriate AUTH block for the given client.
	std::optional<sqlite3xx::statement> find_auth(client_ptr sptr);

	/// @brief Retrieve client connection data..
	client_ptr get_client(boost::uuids::uuid uuid);

	/// @brief Retrieve client connection data.
	/// @param endpt_id Socket endpoint identifier ("address:port")
	client_ptr find_client(std::string_view endpt_id);

	/// Destroy client connection object.
	void del_client(boost::uuids::uuid uuid);

	/// Terminate client connection, generating the appropriate notices.
	void kill_client(client_ptr target, std::string_view message);

	/// Locate user data for the given nickname..
	client_ptr find_user(std::string_view);

	/// Determine if a user exists for the given nickname..
	bool find_nick(std::string_view);

	/// Locate server data for the given name..
	client_ptr find_server_name(std::string_view);

	/// Locate client data for the given EID..
	client_ptr find_entity(std::string_view);

	/// Generate numeric 005 for the given client.
	void send_isupport(client_ptr cptr);

	/// @brief Generate PRIVMSG/NOTICE for a given client.
	/// @param cptr Original client connection
	/// @param sptr Source client
	/// @param dest Nick or channel name of target receiving PRIVMSG/NOTICE
	/// @param messg Originating command message
	/// @param text Message text
	///
	/// Servers MUST use entity identifiers (EID) for the 'dest' parameter.
	/// This method performs full delivery validation for users & channels.
	void do_message(client_ptr cptr, client_ptr sptr, std::string_view dest, message const& messg, std::string_view text);

	/// @brief Transmit message to all server clients.
	/// @param cptr Original client connection
	/// @param sptr Source client
	/// @param flags Message flags
	/// @param cmd Protocol command
	/// @param params Additional arguments
	///
	/// The message will not be sent if:
	///   - cptr is present, and
	///   - the target client's identifier is equal to cptr's identifier
	template<typename... Args>
	void notify_servers(client_ptr cptr, client_ptr sptr, uint32_t flags, std::string_view cmd, Args&&... params);

	/// @brief Transmit message to all server clients.
	/// @param cptr Original client connection
	/// @param sptr Source client
	/// @param msg IRC message data
	void notify_servers(client_ptr cptr, client_ptr sptr, ircd::message msg);

	/// Retrieve all server clients.
	sqlite3xx::statement get_server_clients();

	/// Retrieve all local user clients.
	sqlite3xx::statement get_local_users();

	/// @brief Generate a formatted server notification to users with the specified SNOMASK.
	/// @param snomask Server notification mask
	/// @param spec Format specification (boost.format)
	/// @param params Additional parameters
	///
	/// This method uses boost.format to generate the message text being sent.
	template<typename... Args>
	void notify_snomask(uint16_t snomask, std::string_view spec, Args&&... params);

	/// @brief Generate a server notification to users with the specified SNOMASK.
	/// @param snomask Server notification mask
	/// @param spec Format specification (boost.format)
	/// @param params Additional parameters
	///
	/// This method uses boost.format to generate the message text being sent.
	/// All user clients matching the given SNOMASK will receive the message as a NOTICE.
	void notify_snomask(uint16_t snomask, std::string_view message);

	/// @brief Create a new nickname/channel jupe.
	/// @param type The type of jupe being created, either JUPE_NICK, or JUPE_CHANNEL.
	/// @param what Either a nickname or channel name, dependent on the jupe type.
	/// @param sptr Source client. If present, this client's userhost is recorded as the jupe creator.
	/// @param why Optional message, which may state the reason the jupe was created.
	/// @param status Status code.
	///
	/// The status code is either:
	///   - JUPE_STATUS_PERM (0), for "permanent" jupes (typically those set in CONFIG.LUA)
	///   - JUPE_STATUS_TEMP (1), for "temporary" jupes (typically those received via /JUPE ADD)
	///   - JUPE_STATUS_NICKDELAY (2), for jupes used in the nick-delay system
	///   - JUPE_STATUS_CHANDELAY (3), for jupes used in the chan-delay system
	/// Nick/chan delay jupes will expire based on the current NICKDELAY/CHANDELAY.
	void add_jupe(int type, std::string_view what, client_ptr sptr, std::string_view why, int status = JUPE_STATUS_TEMP);

	/// Delete the specified jupe..
	bool del_jupe(int type, std::string_view what);

	/// @brief Determine if the nick/channel specified is juped.
	/// @param type Either JUPE_NICK or JUPE_CHANNEL.
	/// @param what Nick or channel name to query.
	/// @return Status code, or -1 if no jupe is found.
	/// The status code returned is the same status code used by add_jupe().
	int get_jupe_status(int type, std::string_view what);

	/// @brief Retrieve database record for the specified jupe.
	/// @param type Either JUPE_NICK or JUPE_CHANNEL.
	/// @param what Nick or channel name to query.
	sqlite3xx::statement get_jupe(int type, std::string_view what);

	/// @brief Establish connection to the specified server.
	/// @param sptr Source client that initiated the request.
	/// @param name Name of the server to be connected.
	///
	/// This method is used in response to the CONNECT command from an IRCop.
	/// If a server link is not present for the given server name,
	/// a connection is then established.
	///
	/// This method is also used by the periodic auto-connect mechanism
	/// found in timer_cycle(). In this event, 'sptr' is not present.
	bool connect_to_link(client_ptr sptr, std::string_view name);

	/// Initialize server connection.
	void init_link(client_ptr sptr);

	/// Generate IRC message for server-to-server CAP.
	void send_server_caps(client_ptr sptr);

	/// Verify that we received appropriate CAP messages from a server.
	bool validate_server_caps(client_ptr sptr);

	/// Create server ban (K-LINE).
	bool add_kline(client_ptr sptr, std::string_view mask, std::string_view reason);

	/// Create temporary server ban (K-LINE).
	bool add_kline_temp(client_ptr sptr, std::string_view mask, std::string_view reason, uint64_t expire_time);

	/// Delete server ban (K-LINE).
	bool del_kline(std::string_view mask);

	/// Determine if user is banned (K-LINE).
	bool is_user_kline(client_ptr sptr);

	/// Determine if address is banned (K-LINE).
	bool is_addr_kline(boost::asio::ip::address const& addr);

	/// Determine if a connecting socket should be allowed.
	bool allow_connection(session_ptr peer);

	/// Retrieve all server bans.
	sqlite3xx::statement get_klines();

	/// Run a specific callback in all Lua modules.
	template<typename... Args>
	std::optional<sol::protected_function_result> notify_modules(std::string_view hook_name, Args&&... args);

	/// Determine if two users share any common channels.
	bool has_common_channels(client_ptr usr1, client_ptr usr2);

	/// Retrieve common channels between two users.
	sqlite3xx::statement get_common_channels(client_ptr usr1, client_ptr usr2);

	/// Initialize the log file.
	void open_log(std::string_view source, std::optional<uint32_t> enable);

	/// Open KLINE database.
	void open_kline_db();

	/// Record an entry into the log file.
	uint64_t write_log(int category, module_ptr module, std::string_view what);

	/// Record an entry into the log file.
	uint64_t write_log(int category, std::string_view what);

	/// Record a formatted text entry into the log file, and generate a server notice (SNOMASK).
	uint64_t snomask_log(int category, uint16_t snomask, module_ptr module, std::string_view what);

	/// Record a formatted text entry into the log file, and generate a server notice (SNOMASK).
	uint64_t snomask_log(int category, uint16_t snomask, std::string_view what);

	/// Record a formatted text entry into the log file.
	template<typename... Args>
	uint64_t write_log(int category, module_ptr module, std::string_view spec, Args&&... args);

	/// Record a formatted text entry into the log file, and generate a server notice (SNOMASK).
	template<typename... Args>
	uint64_t snomask_log(int category, uint16_t snomask, module_ptr module, std::string_view spec, Args&&... args);

	/// Record a formatted text entry into the log file, and generate a server notice (SNOMASK).
	template<typename... Args>
	uint64_t snomask_log(int category, uint16_t snomask, std::string_view spec, Args&&... args);

	/// Retrieve records from the log file.
	sqlite3xx::statement query_log(std::string_view sql);

	/// Error codes returned by valid_nick, valid_username, and valid_chan
	enum NAME_VALIDITY_ERROR {
		NAME_ERROR_NONE = 0,    // No validity errors in the given name
		NAME_ERROR_LENGTH = 1,  // Name is too long
		NAME_ERROR_CHARS = 2,   // Name contains unacceptable characters
	};

	/// Determine if the given nickname is valid
	int valid_nick(std::string_view what);

	/// Determine if the given username is valid (for ident purposes)
	int valid_username(std::string_view what);

	/// Determine if the given channel name is valid
	int valid_chan(std::string_view what);

	/// Enable lua debugger
	void enable_lua_debug();

	/// Determine if the lua debugger is enabled
	bool lua_debug();

	/// Log connection attempt
	void log_connect_attempt(boost::asio::ip::tcp::endpoint const& endpt);

	/// Determine if connection attempts have been exceeded
	bool connect_attempts_exceeded(boost::asio::ip::tcp::endpoint const& endpt);

	/// Initialize SQL data tables
	void init_db();

	/// Initialize built-in commands
	void init_commands();

	/// Config reloaded by REHASH command
	void after_conf_reload(uint32_t flags);

	/// Establish local host/port binding
	void add_listen(int type, std::string_view host, int port);

	/// Process WHO query
	bool who_query(int type, client_ptr cptr, client_ptr sptr, std::vector<std::string> masks, std::string_view request);
};
