/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#pragma once

namespace ircd {
	template <typename iterator>
	std::wstring str_widen(iterator start, iterator end)
	{
		std::wstring out;
#if BOOST_OS_WINDOWS
		assert(sizeof(wchar_t) == 2);
		utf8::utf8to16(start, end, std::back_inserter(out));
#else
		assert(sizeof(wchar_t) == 4);
		utf8::utf8to32(start, end, std::back_inserter(out));
#endif
		return out;
	}

	template <typename iterator>
	std::string str_narrow(iterator start, iterator end)
	{
		std::string out;
#if BOOST_OS_WINDOWS
		assert(sizeof(wchar_t) == 2);
		utf8::utf16to8(start, end, std::back_inserter(out));
#else
		assert(sizeof(wchar_t) == 4);
		utf8::utf32to8(start, end, std::back_inserter(out));
#endif
		return out;
	}

	inline std::wstring str_widen(std::string_view str)
	{
		return str_widen(std::begin(str), std::end(str));
	}

	inline std::string str_narrow(std::wstring_view str)
	{
		return str_narrow(std::begin(str), std::end(str));
	}

	template<typename... Args>
	void channel::notify_members(std::string_view chname, client_ptr cptr, client_ptr sptr, uint32_t flags, std::string_view cmd, Args&&... params)
	{
		for (auto uptr : get_members(chname)) {
			if (!uptr) continue;
			if (cptr && cptr->has_flag(CLI_SERVER) && uptr->uuid() == cptr->uuid()) continue;
			if (!uptr->has_flag(CLI_REMOTE)) uptr->write_msg(sptr, flags, cmd, params...);
		}
	}

	template<typename... Args>
	void client::write_msg(client_ptr sptr, uint32_t flags, std::string_view cmd, Args&&... args)
	{
		message msg;
		msg.prefix = msg_prefix(sptr, shared_from_this(), msg);
		msg.command = cmd;
		msg.flags = flags;
		(msg.args.emplace_back(args), ...);
		write_msg(msg);
	}

	template<typename... Args>
	void client::reply_msg(std::string_view label_id, client_ptr sptr, uint32_t flags, std::string_view cmd, Args&&... args)
	{
		message msg;
		msg.prefix = msg_prefix(sptr, shared_from_this(), msg);
		msg.command = cmd;
		msg.flags = flags;
		if (!label_id.empty()) msg.tags["label"] = label_id;
		(msg.args.emplace_back(args), ...);
		write_msg(msg);
	}

	template<typename... Args>
	void client::batch_msg(std::string_view batch_id, client_ptr sptr, uint32_t flags, std::string_view cmd, Args&&... args)
	{
		message msg;
		msg.prefix = msg_prefix(sptr, shared_from_this(), msg);
		msg.command = cmd;
		msg.flags = flags;
		if (!batch_id.empty()) msg.tags["batch"] = batch_id;
		(msg.args.emplace_back(args), ...);
		write_msg(msg);
	}

	template<typename... Args>
	void client::send_numeric(int n, Args&&... args)
	{
		if (this->has_flag(CLI_REMOTE) && !rpl_allow_remote(n)) return;
		using namespace boost::io;
		boost::format fmt{ rpl_str(n) };
		fmt.exceptions(all_error_bits ^ (too_many_args_bit | too_few_args_bit));
		(fmt % ... % std::forward<Args>(args));
		write_msg(nullptr, 0, format_str("%03u", n), fmt.str());
	}

	template<typename... Args>
	void client::batch_numeric(std::string_view batch_id, int n, Args&&... args)
	{
		if (this->has_flag(CLI_REMOTE) && !rpl_allow_remote(n)) return;
		using namespace boost::io;
		boost::format fmt{ rpl_str(n) };
		fmt.exceptions(all_error_bits ^ (too_many_args_bit | too_few_args_bit));
		(fmt % ... % std::forward<Args>(args));
		batch_msg(batch_id, nullptr, 0, format_str("%03u", n), fmt.str());
	}

	template<typename... Args>
	void client::reply_numeric(std::string_view label_id, int n, Args&&... args)
	{
		if (this->has_flag(CLI_REMOTE) && !rpl_allow_remote(n)) return;
		using namespace boost::io;
		boost::format fmt{ rpl_str(n) };
		fmt.exceptions(all_error_bits ^ (too_many_args_bit | too_few_args_bit));
		(fmt % ... % std::forward<Args>(args));
		reply_msg(label_id, nullptr, 0, format_str("%03u", n), fmt.str());
	}

	template<typename... Args>
	void client::send_notice(std::string_view spec, Args&&... args)
	{
		using namespace boost::io;
		boost::format fmt{ std::string(spec) };
		fmt.exceptions(all_error_bits ^ (too_many_args_bit | too_few_args_bit));
		(fmt % ... % std::forward<Args>(args));
		send_notice(fmt.str());
	}

	template<typename... Args>
	message create_message(std::string_view src, uint32_t flags, std::string_view cmd, Args&&... args)
	{
		message msg;
		msg.prefix = src;
		msg.command = cmd;
		msg.flags = flags;
		(msg.args.emplace_back(args), ...);
		return msg;
	}

	template<typename... Args>
	message create_message(client_ptr src, uint32_t flags, std::string_view cmd, Args&&... args)
	{
		message msg;
		msg.prefix = src->userhost();
		msg.command = cmd;
		msg.flags = flags;
		(msg.args.emplace_back(args), ...);
		if (src->has_flag(CLI_ACCOUNT)) {
			msg.tags["account"] = src->account();
		}
		return msg;
	}

	template<typename... Args>
	void notify_servers(client_ptr cptr, client_ptr sptr, uint32_t flags, std::string_view cmd, Args&&... params)
	{
		auto sql = get_server_clients();
		while (sql.step()) {
			auto uptr = get_client(sql.field_value(0).get_uuid());
			if (!uptr || uptr->has_flag(CLI_REMOTE)) continue;
			if (cptr && cptr->has_flag(CLI_SERVER) && uptr->uuid() == cptr->uuid()) continue;
			uptr->write_msg(sptr, flags, cmd, params...);
		}
	}

	template<typename... Args>
	void notify_snomask(uint16_t snomask, std::string_view spec, Args&&... args)
	{
		using namespace boost::io;
		boost::format fmt{ std::string(spec) };
		fmt.exceptions(all_error_bits ^ (too_many_args_bit | too_few_args_bit));
		(fmt % ... % std::forward<Args>(args));
		notify_snomask(snomask, fmt.str());
	}

	template<typename ...Args>
	inline std::optional<sol::protected_function_result> notify_modules(std::string_view hook_name, Args&& ...args)
	{
		std::optional<sol::protected_function_result> last;
		for (auto it = first_module(); it != last_module(); it++) {
			auto& mptr{ *it };
			last = mptr->call_hook(hook_name, args...);
			if (last && !last->valid()) {
				sol::error err = *last;
				write_log(LOG_DEBUG, mptr, "Lua hook '%1%' error: %2%", hook_name, err.what());
				break;
			}
		}
		return last;
	}

	template<typename ...Args>
	inline uint64_t write_log(int category, module_ptr module, std::string_view spec, Args&&... args)
	{
		using namespace boost::io;
		boost::format fmt{ std::string(spec) };
		fmt.exceptions(all_error_bits ^ (too_many_args_bit | too_few_args_bit));
		(fmt % ... % std::forward<Args>(args));
		return write_log(category, module, fmt.str());
	}

	template<typename ...Args>
	inline uint64_t snomask_log(int category, uint16_t snomask, module_ptr module, std::string_view spec, Args&&... args)
	{
		using namespace boost::io;
		boost::format fmt{ std::string(spec) };
		fmt.exceptions(all_error_bits ^ (too_many_args_bit | too_few_args_bit));
		(fmt % ... % std::forward<Args>(args));
		return snomask_log(category, snomask, module, fmt.str());
	}

	template<typename ...Args>
	inline uint64_t snomask_log(int category, uint16_t snomask, std::string_view spec, Args&&... args)
	{
		using namespace boost::io;
		boost::format fmt{ std::string(spec) };
		fmt.exceptions(all_error_bits ^ (too_many_args_bit | too_few_args_bit));
		(fmt % ... % std::forward<Args>(args));
		return snomask_log(category, snomask, fmt.str());
	}

	template<typename ...Args>
	std::optional<sol::protected_function_result> module::call_hook(std::string_view name, Args&& ...args)
	{
		sol::protected_function fn = lua_["hooks"][name];
		if (fn.valid()) {
			return fn(args...);
		} else {
			return std::nullopt;
		}
	}
}
