/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"

using std::optional;

namespace ircd { namespace channel {
	bool member_status(std::string_view chname, client_ptr who, int stat);
	bool set_member_status(std::string_view chname, client_ptr who, int stat, bool value);
	bool process_mode(std::optional<channel_info> chrec, ircd::mode_change_info const& change, client_ptr sptr);
}}

ircd::channel::member_iterator::member_iterator()
	: index_{ -1 }
{}

ircd::channel::member_iterator::member_iterator(sqlite3xx::statement&& data)
	: data_{ std::move(data) }
	, index_{ clock() }
{}

ircd::channel::member_iterator::member_iterator(member_iterator&& it)
	: data_{ std::move(it.data_) }
	, index_{ it.index_ }
{}

ircd::channel::member_iterator::member_iterator(member_iterator const& it)
	: data_{ it.data_ }
	, index_{ it.index_ }
{}

ircd::client_ptr ircd::channel::member_iterator::operator*() const
{
	if (!data_.has_value() || data_->eof()) return nullptr;
	return get_client(data_->field_value("EID").get_uuid());
}

ircd::channel::member_iterator& ircd::channel::member_iterator::operator++()
{
	assert(data_.has_value());
	if (data_->step()) index_++;
	else index_ = -1;
	return *this;
}

bool ircd::channel::member_iterator::operator==(member_iterator const& it)
{
	return index_ == it.index_;
}

ircd::channel::list_iterator::list_iterator()
	: index_{ -1 }
{}

ircd::channel::list_iterator::list_iterator(sqlite3xx::statement&& data)
	: data_{ std::move(data) }
	, index_{ clock() }
{}

ircd::channel::list_iterator::list_iterator(list_iterator&& it)
	: data_{ std::move(it.data_) }
	, index_{ it.index_ }
{}

ircd::channel::list_iterator::list_iterator(list_iterator const& it)
	: data_{ it.data_ }
	, index_{ it.index_ }
{}

std::optional<ircd::chan_mask_info> ircd::channel::list_iterator::operator*() const
{
	if (!data_.has_value() || data_->eof()) return std::nullopt;
	chan_mask_info mask;
	mask.mask_type = data_->field_value("MaskType").get_int(0);
	mask.mask_value = data_->field_value("Mask").get_text();
	mask.creator = data_->field_value("Creator").get_text();
	mask.timestamp = data_->field_value("TS").get_uint64(0);
	return mask;
}

ircd::channel::list_iterator& ircd::channel::list_iterator::operator++()
{
	assert(data_.has_value());
	if (data_->step()) index_++;
	else index_ = -1;
	return *this;
}

bool ircd::channel::list_iterator::operator==(list_iterator const& it)
{
	return index_ == it.index_;
}

std::optional<ircd::channel_info> ircd::channel::find(std::string_view name)
{
	auto data = db.exec_query("SELECT [Name], [TS], [Topic], [TopicTime], [TopicSetBy], [Modes], [Limit] FROM [channels] WHERE [Name]=?", name);
	if (!data.eof()) {
		auto chname = data("Name").get_text();
		return channel_info{ type(chname), chname, data("TS").get_uint64(0),
			data("Topic").get_text(), data("TopicTime").get_uint64(0), data("TopicSetBy").get_text(),
			data("Modes").get_uint(0), data("Limit").get_int(0) };
	}
	return std::nullopt;
}

std::optional<ircd::channel_info> ircd::channel::create(std::string_view name)
{
	if (!find(name)) {
		uint32_t modes = 0;
		if (name.at(0) != CHTYPE_MODELESS) {
			modes |= CMODE_NO_EXTERNAL | CMODE_TOPIC_LOCKED;
		}
		db.exec_dml("INSERT INTO [channels] ([Name], [TS], [Modes]) VALUES(?,?,?)", name, ircd::timestamp(), modes);
	}
	return find(name);
}

int ircd::channel::type(std::string_view chname)
{
	return static_cast<int>(chname.at(0));
}

uint64_t ircd::channel::timestamp(std::string_view chname)
{
	return db.exec_scalar("SELECT [TS] FROM [channels] WHERE [name]=?", chname).get_uint64(0);
}

bool ircd::channel::set_topic(std::string_view chname, std::string_view text, std::optional<client_ptr> src)
{
	int rc = 0;
	if (src) {
		auto cptr = *src;
		rc = db.exec_dml("UPDATE [channels] SET [Topic]=?, [TopicTime]=timestamp(), [TopicSetBy]=? WHERE [Name]=?",
			text, cptr->nick(), chname);
	} else {
		rc = db.exec_dml("UPDATE [channels] SET [Topic]=?, [TopicTime]=NULL, [TopicSetBy]=NULL WHERE [Name]=?",
			text, chname);
	}
	return 1 == rc;
}

void ircd::channel::send_topic(std::string_view chname, client_ptr usr)
{
	auto res = db.exec_query("SELECT [Topic], [TopicTime], [TopicSetBy] FROM [channels] WHERE [Name]=?", chname);
	if (!res.eof() && !res("Topic").is_null()) {
		std::string text = res("Topic").get_text();
		std::string ts = res("TopicTime").get_text();
		std::string setter = res("TopicSetBy").get_text();
		usr->send_numeric(RPL_TOPIC, usr->nick(), chname, text);
		if (!ts.empty() || setter.empty()) {
			if (setter.empty()) setter = "*";
			usr->send_numeric(RPL_TOPICWHOTIME, usr->nick(), chname, ts, setter);
		}
	} else {
		usr->send_numeric(RPL_NOTOPIC, usr->nick(), chname);
	}
}

uint32_t ircd::channel::get_modes(std::string_view chname)
{
	return db.exec_scalar("SELECT [Modes] FROM [channels] WHERE [Name]=?", chname).get_uint(0);
}

bool ircd::channel::has_mode(std::string_view chname, unsigned int bit)
{
	return 0 != db.exec_scalar("SELECT COUNT(*) FROM [channels] WHERE [Name]=?2 AND ([Modes] & ?1)=?1", bit, chname).get_int(0);
}

std::string ircd::channel::get_key(std::string_view chname)
{
	return db.exec_scalar("SELECT [Key] FROM [channels] WHERE [Name]=?", chname).get_text();
}

bool ircd::channel::set_key(std::string_view chname, std::string_view val)
{
	int rc = 0;
	if (val.empty() || val == "*") {
		rc = db.exec_dml("UPDATE [channels] SET [Key]=NULL WHERE [Name]=?", chname);
	} else {
		rc = db.exec_dml("UPDATE [channels] SET [Key]=? WHERE [Name]=?", val, chname);
	}
	return 1 == rc;
}

int ircd::channel::get_limit(std::string_view chname)
{
	return db.exec_scalar("SELECT [Limit] FROM [channels] WHERE [Name]=?", chname).get_int(0);
}

bool ircd::channel::set_limit(std::string_view chname, int val)
{
	return 1 == db.exec_dml("UPDATE [channels] SET [Limit]=? WHERE [Name]=?", val, chname);
}

bool ircd::channel::add_user(std::string_view chname, client_ptr cptr, client_ptr sptr, optional<std::string> key_param)
{
	if (find(chname)) {
		auto modes = get_modes(chname);
		if ((modes & CMODE_OPERS) && !sptr->has_umode(UMODE_SERVICE) && !sptr->has_umode(UMODE_OPERATOR)) {
			sptr->send_numeric(ERR_BANNEDFROMCHAN, sptr->nick(), chname);
		} else if ((modes & CMODE_INVITE_ONLY) && !sptr->has_umode(UMODE_SERVICE) && !list_find(chname, CHLIST_INVITE, sptr)) {
			sptr->send_numeric(ERR_INVITEONLYCHAN, sptr->nick(), chname);
		} else if ((modes & CMODE_KEY) && !sptr->has_umode(UMODE_SERVICE) && (!key_param || (0 == compare_nfkc(*key_param, get_key(chname))))) {
			sptr->send_numeric(ERR_BADCHANNELKEY, sptr->nick(), chname);
		} else if ((modes & CMODE_LIMIT) && !sptr->has_umode(UMODE_SERVICE) && ((count(chname) + 1) >= get_limit(chname))) {
			sptr->send_numeric(ERR_CHANNELISFULL, sptr->nick(), chname);
		} else if (is_banned(chname, sptr) && !sptr->has_umode(UMODE_SERVICE)) {
			sptr->send_numeric(ERR_BANNEDFROMCHAN, sptr->nick(), chname);
		} else {
			return ins_user(chname, cptr, sptr);
		}
	}
	return false;
}

bool ircd::channel::ins_user(std::string_view chname, client_ptr cptr, client_ptr sptr)
{
	int rc = db.exec_dml("INSERT OR REPLACE INTO [channel-users] ([ChannelName], [EID], [Status]) VALUES(?,?,0)", chname, sptr->eid());
	if (1 == rc) {
		// automatically give user +o for creating the channel
		if (count(chname) == 1) {
			set_op(chname, sptr, true);
		}
		// notify channel that user has joined
		for (auto uptr : get_members(chname)) {
			if (uptr && !uptr->has_flag(CLI_REMOTE)) {
				if (uptr->has_cap(CAP_EXTENDED_JOIN)) {
					uptr->write_msg(sptr, MSG_TRAILING_ARG, "JOIN", chname, sptr->account(), sptr->gecos());
				} else {
					uptr->write_msg(sptr, 0, "JOIN", chname);
				}
			}
		}
		notify_servers(cptr, sptr, 0, "JOIN", chname);
		return true;
	}
	return false;
}

bool ircd::channel::del_user(std::string_view chname, client_ptr sptr)
{
	if (list_find(chname, CHLIST_INVITE, sptr) && !sptr->has_flag(CLI_REMOTE)) {
		list_remove(chname, CHLIST_INVITE, sptr->userhost());
	}
	int rc = db.exec_dml("DELETE FROM [channel-users] WHERE [ChannelName]=? AND [EID]=?", chname, sptr->eid());
	if (1 == rc && count(chname) == 0) {
		db.exec_dml("DELETE FROM [channels] WHERE [Name]=?", chname);
	}
	return 1 == rc;
}

bool ircd::channel::has_user(std::string_view chname, client_ptr usr)
{
	auto val = db.exec_scalar("SELECT COUNT(*) FROM [channel-users] WHERE [ChannelName]=? AND [EID]=?", chname, usr->eid());
	if (!val.is_null()) {
		return val.get_int(0) > 0;
	} else {
		return false;
	}
}

void ircd::channel::kill(std::string_view chname)
{
	for (auto uptr : get_members(chname)) {
		if (uptr) {
			notify_servers(nullptr, uptr, 0, "PART", chname);
			notify_members(chname, nullptr, uptr, 0, "PART", chname);
			del_user(chname, uptr);
		}
	}
}

int ircd::channel::count(std::string_view chname)
{
	return db.exec_scalar("SELECT COUNT(*) FROM [channel-users] WHERE [ChannelName]=?", chname).get_int(0);
}

inline bool ircd::channel::member_status(std::string_view chname, client_ptr who, int stat)
{
	return 0 != db.exec_scalar("SELECT COUNT(*) FROM [channel-users] WHERE [ChannelName]=?1 AND [EID]=?2 AND ([Status] & ?3)=?3",
		chname, who->eid(), stat).get_uint(0);
}

inline bool ircd::channel::set_member_status(std::string_view chname, client_ptr who, int stat, bool value)
{
	int rc = 0;
	if (type(chname) != CHTYPE_MODELESS) {
		if (value) {
			rc = db.exec_dml("UPDATE [channel-users] SET [Status]=([Status]|?3) WHERE [ChannelName]=?1 AND [EID]=?2",
				chname, who->eid(), stat);
		} else {
			rc = db.exec_dml("UPDATE [channel-users] SET [Status]=([Status]&?3) WHERE [ChannelName]=?1 AND [EID]=?2",
				chname, who->eid(), ~stat);
		}
	}
	return 1 == rc;
}

bool ircd::channel::is_op(std::string_view chname, client_ptr who)
{
	return member_status(chname, who, CSTATUS_OP);
}

bool ircd::channel::set_op(std::string_view chname, client_ptr who, bool value)
{
	return set_member_status(chname, who, CSTATUS_OP, value);
}

bool ircd::channel::is_voice(std::string_view chname, client_ptr who)
{
	return member_status(chname, who, CSTATUS_VOICE);
}

bool ircd::channel::set_voice(std::string_view chname, client_ptr who, bool value)
{
	return set_member_status(chname, who, CSTATUS_VOICE, value);
}

ircd::iterator_range<ircd::channel::member_iterator> ircd::channel::get_members(std::string_view chname)
{
	return { member_iterator(db.exec_query("SELECT [EID] FROM [channel-users] WHERE [ChannelName]=?", chname)), member_iterator() };
}

void ircd::channel::do_message(std::string_view chname, client_ptr cptr, client_ptr sptr, int type, std::string_view text, tsl::ordered_map<std::string, std::string> const& tags)
{
	static char const* cmd[] = { "NOTICE", "PRIVMSG", "TAGMSG" };
	assert(type >= 0 && type <= (sizeof(cmd) / sizeof(cmd[0])));
	if (!find(chname)) return;
	std::string label;
	if (tags.count("label")) label = tags.at("label");
	// if the channel is +n, check for membership
	if (!sptr->has_flag(CLI_REMOTE) && has_mode(chname, CMODE_NO_EXTERNAL) && !has_user(chname, sptr)) {
		sptr->reply_numeric(label, ERR_CANNOTSEND, sptr->nick(), chname);
	// if channel is +m, check for op/voice status
	} else if (!sptr->has_flag(CLI_REMOTE) && has_mode(chname, CMODE_MODERATED) && !is_op(chname, sptr) && !is_voice(chname, sptr)) {
		sptr->reply_numeric(label, ERR_CANNOTSEND, sptr->nick(), chname);
	} else if (!sptr->has_flag(CLI_REMOTE) && is_banned(chname, sptr) && !is_op(chname, sptr) && !is_voice(chname, sptr)) {
		sptr->reply_numeric(label, ERR_CANNOTSEND, sptr->nick(), chname);
	} else {
		message m;
		m.command = cmd[type];
		m.flags = (type != CMSG_TAGMSG)? MSG_TRAILING_ARG : 0;
		std::copy_if(tags.begin(), tags.end(), std::inserter(m.tags, m.tags.end()), [](auto const& key) { return key.first[0] == '+'; });
		m.args.emplace_back(chname);
		if (type != CMSG_TAGMSG) m.args.emplace_back(text);
		// broadcast message
		for (auto target : get_members(chname)) {
			if (!target || target->has_flag(CLI_REMOTE)) continue;
			if (type == CMSG_TAGMSG && !target->has_cap(CAP_MESSAGE_TAGS)) continue;
			if (type != CMSG_TAGMSG && target->has_umode(UMODE_DEAF)) continue;
			if (target->uuid() != sptr->uuid()) {
				m.prefix = msg_prefix(sptr, target, m);
				target->write_msg(m);
			}
		}
		notify_servers(cptr, sptr, m);
		if (sptr->has_cap(CAP_ECHO_MESSAGE)) {
			if (!label.empty()) m.tags["label"] = label;
			sptr->write_msg(m);
		}
	}
}

void ircd::channel::send_names(std::string_view chname, client_ptr sptr)
{
	if (!find(chname)) return;

	std::string symbol{ "=" };

	if (has_mode(chname, CMODE_SECRET)) {
		symbol = "@";
	}

	int n = 0;
	std::string str;
	for (auto member : get_members(chname)) {
		if (!member) continue;
		if (!str.empty()) str.append(" ");
		std::string prefix;
		if ((sptr->has_cap(CAP_MULTI_PREFIX)) || sptr->has_flag(CLI_SERVER)) {
			if (is_op(chname, member)) prefix.append("@");
			if (is_voice(chname, member)) prefix.append("+");
		} else {
			if (is_voice(chname, member)) prefix = "+";
			if (is_op(chname, member)) prefix = "@";
		}
		str.append(prefix);
		if (sptr->has_flag(CLI_SERVER)) {
			str.append(member->eid());
		} else if (sptr->has_cap(CAP_USERHOST_IN_NAMES)) {
			str.append(member->userhost());
		} else {
			str.append(member->nick());
		}
		// TODO: eventually fix send_numeric to automagically limit output
		if (0 == (++n % 10)) {
			if (sptr->has_flag(CLI_SERVER)) {
				sptr->write_msg(nullptr, 0, "SJOIN", chname, str);
			} else {
				sptr->send_numeric(RPL_NAMES, sptr->nick(), symbol, chname, str);
			}
			str.clear();
		}
	}

	if (!str.empty()) {
		if (sptr->has_flag(CLI_SERVER)) {
			sptr->write_msg(nullptr, 0, "SJOIN", chname, str);
		} else {
			sptr->send_numeric(RPL_NAMES, sptr->nick(), symbol, chname, str);
		}
	}

	if (!sptr->has_flag(CLI_SERVER)) sptr->send_numeric(RPL_ENDOFNAMES, sptr->nick(), chname);
}

void ircd::channel::set_modes(std::string_view chname, client_ptr cptr, client_ptr sptr, std::string_view str, std::vector<std::string> const& params)
{
	if (type(chname) == CHTYPE_MODELESS) {
		if (sptr && sptr->has_flag(CLI_REGISTERED)) sptr->send_numeric(ERR_NOCHANMODES, sptr->nick(), chname);
		return;
	}
	auto chrec = find(chname);
	if (!chrec) return;
	uint32_t base = chrec->modes;
	uint32_t prev = base;
	std::string err;
	// parse & process
	auto changes = parse_chmode_change(str, params, &err);
	for (size_t i = 0; i < changes.size();) {
		auto const& change = changes[i];
		// category A modes should never be set as a flag
		if (change.category == MODE_CAT_A || !change.value) {
			base &= ~change.mask;
		} else {
			base |= change.mask;
		}
		if (!process_mode(chrec, change, sptr)) {
			base &= ~change.mask;
			changes.erase(changes.begin() + i);
		} else {
			i++;
		}
	}
	// emit unknown chars
	for (char const& ch : err) {
		std::string chr{ ch };
		sptr->send_numeric(ERR_UNKNOWNMODE, sptr->nick(), chr);
	}
	// update database
	db.exec_dml("UPDATE [channels] SET [Modes]=?, [TS]=timestamp() WHERE [Name]=?", base, chrec->name);
	// if the channel is changed to +x, remove non-opers
	if (!(prev & CMODE_OPERS) && (base & CMODE_OPERS)) {
		for (auto target : get_members(chrec->name)) {
			if (target && !target->has_umode(UMODE_OPERATOR) && !target->has_umode(UMODE_SERVICE)) {
				notify_servers(cptr, target, MSG_TRAILING_ARG, "PART", chrec->name, "Not an oper");
				notify_members(chrec->name, cptr, target, MSG_TRAILING_ARG, "PART", chrec->name, "Not an oper");
				del_user(chrec->name, target);
			}
		}
	}
	// broadcast changes
	// TODO: limit to MAX_TARGETS
	if (!changes.empty()) {
		ircd::message messg = create_message(sptr, 0, "MODE", chrec->name);
		char state = '\0';
		std::string res;
		for (auto const& change : changes) {
			if (change.value && state != '+') {
				state = '+';
				res.append("+");
			} else if (!change.value && state != '-') {
				state = '-';
				res.append("-");
			}
			res.append(change.alpha);
			if (!change.param.empty()) messg.args.push_back(change.param);
		}
		messg.args.insert(messg.args.begin() + 1, res);
		for (auto uptr : get_members(chname)) {
			if (uptr && !uptr->has_flag(CLI_REMOTE)) uptr->write_msg(messg);
		}
		// notify servers
		messg.prefix = sptr->eid();
		notify_servers(cptr, sptr, messg);
	}
}

bool ircd::channel::process_mode(std::optional<channel_info> chrec, ircd::mode_change_info const& change, client_ptr sptr)
{
	if (change.mask == CMODE_INVITE_ONLY) {
		db.exec_dml("DELETE FROM [channel-masks] WHERE [ChannelName]=? AND [ListID]=?", chrec->name, CHLIST_INVITE);
		return true;
	}
	if (change.mask == CMODE_BAN || change.mask == CMODE_EXEMPT) {
		int const type = (change.mask == CMODE_BAN) ? CHLIST_BAN : CHLIST_EXEMPT;
		if (!change.param.empty()) {
			int res = -1;
			if (change.value) {
				res = list_add(chrec->name, type, change.param, sptr->userhost(), ircd::timestamp());
			} else {
				list_remove(chrec->name, type, change.param);
			}
			if (change.value && res != LIST_ADD_OK) {
				if (res == LIST_ADD_FULL) {
					sptr->send_numeric(ERR_BANLISTFULL, sptr->nick(), chrec->name, change.alpha);
					return false;
				}
				if (res == LIST_ADD_INVALID) {
					sptr->send_numeric(ERR_INVALIDBAN, sptr->nick(), chrec->name, change.alpha, change.param);
					return false;
				}
			}
		} else {
			send_list(chrec->name, type, sptr);
			return false;
		}
		return true;
	}
	if (change.mask == CMODE_KEY) {
		if (change.value && !change.param.empty()) {
			std::string keyval = change.param;
			if (keyval.length() > info.limits[MAX_KEYLEN]) {
				keyval = keyval.substr(0, info.limits[MAX_KEYLEN]);
			}
			set_key(chrec->name, keyval);
			return true;
		} else if (!change.value) {
			set_key(chrec->name, "");
			return true;
		}
		return false;
	}
	if (change.mask == CMODE_LIMIT) {
		if (change.value && !change.param.empty()) {
			try {
				auto lval = boost::lexical_cast<int>(change.param);
				if (lval >= 0) {
					set_limit(chrec->name, lval);
					return true;
				}
			} catch (std::exception const&) {
				return false;
			}
		} else if (!change.value) {
			set_limit(chrec->name, 0);
			return true;
		}
		return false;
	}
	if (change.mask == CMODE_OPERATOR || change.mask == CMODE_VOICE) {
		client_ptr target = find_user(change.param);
		if (!target) {
			return false;
		} else if (!has_user(chrec->name, target)) {
			sptr->send_numeric(ERR_USERNOTINCHANNEL, sptr->nick(), target->nick(), chrec->name);
		} else if (!change.value && target->has_umode(UMODE_SERVICE) && change.mask == CMODE_OPERATOR) {
			sptr->send_numeric(ERR_ISSERVICE, sptr->nick(), target->nick());
		} else {
			if (change.mask == CMODE_OPERATOR) set_op(chrec->name, target, change.value);
			if (change.mask == CMODE_VOICE) set_voice(chrec->name, target, change.value);
			return true;
		}
		return false;
	}
	if (change.mask == CMODE_OPERS || change.mask == CMODE_REGISTERED) {
		return sptr->has_umode(UMODE_OPERATOR) || sptr->has_umode(UMODE_SERVICE) || sptr->has_flag(CLI_SERVER);
	}
	return true;
}

std::string ircd::channel::get_mode_str(std::string_view chname)
{
	auto modes = get_modes(chname);
	std::string res{ _S("+") }, add;
	for (auto [id, info] : ircd::chmodes) {
		if (modes & info.mask) {
			utf8::append(id, std::back_inserter(res));
			if (info.category < MODE_CAT_D) {
				add.append(" ");
				switch (info.mask) {
				case CMODE_KEY:
					add.append(get_key(chname));
					break;
				case CMODE_LIMIT:
					add.append(std::to_string(get_limit(chname)));
					break;
				}
			}
		}
	}
	if (add.length() > 0) res.append(add);
	return res;
}

ircd::iterator_range<ircd::channel::list_iterator> ircd::channel::get_list(std::string_view chname, int id)
{
	return { list_iterator(db.exec_query("SELECT [Mask], [Creator], [TS], [MaskType] FROM [channel-masks] WHERE [ChannelName]=? AND [ListID]=?", chname, id)), list_iterator() };
}

bool ircd::channel::list_find(std::string_view chname, int id, client_ptr client)
{
	static char const* SQL = R"(SELECT COUNT()
		FROM [channel-masks]
		WHERE [ChannelName]=?1
		AND [ListID]=?2
		AND ([MaskType]=2 AND ?3 GLOB [Mask]) OR ([MaskType] IN (3, 4) AND cidr_match([Mask], ?4)))";
	auto res = db.exec_scalar(SQL, chname, id, client->userhost(), client->address());
	return res.get_int(0) > 0;
}

int ircd::channel::list_add(std::string_view chname, int id, std::string_view mask, std::string_view creator, uint64_t timestamp)
{
	// validate mask
	int mtype = valid_mask(mask);
	if (mtype == MASK_INVALID || mtype == MASK_HOST) {
		return LIST_ADD_INVALID;
	}
	// determine if ban/except list is at maximum capacity
	if (id != CHLIST_INVITE) {
		auto next = db.exec_scalar("SELECT 1+COUNT(*) FROM [channel-masks] WHERE [ChannelName]=? AND [MaskType]=?", chname, id).get_uint(0);
		if (next > info.limits[MAX_CHANLIST]) {
			return LIST_ADD_FULL;
		}
	}
	// allow entry to be inserted
	db.exec_dml("INSERT OR REPLACE INTO [channel-masks] ([ChannelName], [ListID], [Mask], [MaskType], [Creator], [TS]) VALUES(?,?,?,?,?,?)",
		chname, id, normalize_nfkc(mask), mtype, normalize_nfkc(creator), timestamp);
	return LIST_ADD_OK;
}

void ircd::channel::list_remove(std::string_view chname, int id, std::string_view mask)
{
	db.exec_dml("DELETE FROM [channel-masks] WHERE [ChannelName]=? AND [ListID]=? AND [Mask]=?",
		chname, id, normalize_nfkc(mask));
}

size_t ircd::channel::list_count(std::string_view chname, int id)
{
	return db.exec_scalar("SELECT COUNT(*) FROM [channel-masks] WHERE [ChannelName]=? AND [ListID]=?", chname, id).get_uint(0);
}

void ircd::channel::send_list(std::string_view chname, int id, client_ptr sptr)
{
	if (find(chname)) {
		int item_numeric = 0, end_numeric = 0;
		switch (id) {
		case CHLIST_BAN:
			item_numeric = RPL_BANLIST;
			end_numeric = RPL_ENDOFBANLIST;
			break;
		case CHLIST_EXEMPT:
			item_numeric = RPL_EXCEPTLIST;
			end_numeric = RPL_ENDOFEXCEPTLIST;
			break;
		case CHLIST_INVITE:
			item_numeric = RPL_INVITELIST;
			end_numeric = RPL_ENDOFINVITELIST;
			break;
		}
		for (auto data : get_list(chname, id)) {
			if (data) sptr->send_numeric(item_numeric, sptr->nick(), chname, data->mask_value);
		}
		sptr->send_numeric(end_numeric, sptr->nick(), chname);
	}
}

bool ircd::channel::is_banned(std::string_view chname, client_ptr sptr)
{
	return list_find(chname, CHLIST_BAN, sptr) && !list_find(chname, CHLIST_EXEMPT, sptr);
}

void ircd::channel::kick(std::string_view chname, client_ptr cptr, client_ptr sptr, client_ptr target, std::string_view reason)
{
	if (!has_user(chname, target)) {
		sptr->send_numeric(ERR_NOTONCHANNEL, sptr->nick(), chname);
	} else if (!is_op(chname, sptr) && !sptr->has_flag(CLI_REMOTE)) {
		sptr->send_numeric(ERR_CHANOPRIVSNEEDED, sptr->nick(), chname);
	} else if (target->has_umode(UMODE_SERVICE)) {
		sptr->send_numeric(ERR_ISSERVICE, sptr->nick(), target->nick());
	} else {
		notify_servers(cptr, sptr, MSG_TRAILING_ARG, "KICK", chname, target->eid(), reason);
		notify_members(chname, cptr, sptr, MSG_TRAILING_ARG, "KICK", chname, target->nick(), reason);
		del_user(chname, target);
	}
}
