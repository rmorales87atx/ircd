/*
 * ircd - An IRCv3 Server
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"

using boost::system::error_code;
using namespace std::placeholders;

ircd::listener::listener(session_manager& mgr, int type, tcp::endpoint endpoint)
	: owner_{ mgr }
	, type_{ type }
	, endpt_{ endpoint }
	, acceptor_{ io_ctx }
{
	acceptor_.open(endpt_.protocol());
	if (acceptor_.is_open()) {
		acceptor_.set_option(tcp::acceptor::reuse_address(true));
		acceptor_.bind(endpt_);
		acceptor_.listen();
		start();
	}
}

ircd::listener::~listener()
{
	error_code ec;
	acceptor_.cancel(ec);
	acceptor_.close(ec);
}

int ircd::listener::type() const
{
	return type_;
}

tcp::endpoint ircd::listener::endpoint() const
{
	return endpt_;
}

void ircd::listener::start()
{
	acceptor_.async_accept([this](error_code const& err, tcp::socket peer) {
		if (!err) {
			owner_.accept(*this, std::move(peer));
			start();
		}
	});
}
