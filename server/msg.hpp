/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#pragma once

namespace ircd {
	/// Flags used for IRC protocol messages.
	enum MSG_FLAGS : uint32_t {
		/// Last arg is a trailing parameter.
		MSG_TRAILING_ARG = (1 << 0),

		/// When the message is being wrapped due to excess params, repeat the first arg;
		/// e.g., for ISUPPORT this will consistently place the client's name (1st arg)
		/// in front of each ISUPPORT message.
		MSG_REPEAT_1ST = (1 << 1),

		/// Indicates message should never include a prefix; e.g, ERROR for local clients.
		MSG_NO_PREFIX = (1 << 2),
	};

	/// Structure describing an IRC protocol message.
	/// When receiving messages, all strings will be normalized to NFC.
	struct message {
		/// Original text that was received.
		std::string text;

		/// IRCv3 tags associated with this message.
		tsl::ordered_map<std::string, std::string> tags;

		/// Source prefix - usually blank from clients.
		std::string prefix;

		/// Command that was received.
		std::string command;

		/// Command arguments.
		std::vector<std::string> args;

		/// Flags for internal processing (not received via IRC).
		uint32_t flags;

		/// Empty constructor.
		message() : flags{ 0 } {}

		/// Copy constructor.
		message(message const& m) : text{ m.text }, tags{ m.tags }, prefix{ m.prefix }, command{ m.command }, args{ m.args }, flags{ m.flags } {}

		/// Move constructor.
		message(message&& m) : text{ std::move(m.text) }, tags{ std::move(m.tags) }, prefix{ std::move(m.prefix) }, command{ std::move(m.command) }, args{ std::move(m.args) }, flags{ m.flags } {}

		/// Copy operator.
		message& operator=(message&& m) noexcept
		{
			text = std::move(m.text);
			tags = std::move(m.tags);
			prefix = std::move(m.prefix);
			command = std::move(m.command);
			args = std::move(m.args);
			flags = m.flags;
			return *this;
		}

		/// Retrieve message tag.
		std::string tag(std::string const& key) const
		{
			try {
				return tags.at(key);
			} catch (std::out_of_range const&) {
				return std::string();
			}
		}
	};

	/// @brief Read a line of text and parse an IRC protocol message.
	/// @return std::nullopt if the parse fails; otherwise, a completed message object.
	///
	/// The data will be normalized to form NFC before parsing.
	/// This function parses IRCv3 message tags, the optional prefix, command,
	/// and parameters (including the "trail" parameter). The command text will
	/// be case-folded and renormalized to NFKC.
	///
	/// If a trail parameter is present the flag MSG_TRAILING_ARG is set and
	/// the trail parameter is included with all other parameters in 'args'.
	std::optional<message> tokenize(std::string_view data);

	/// Generate an IRC protocol message.
	template<typename... Args>
	message create_message(std::string_view src, uint32_t flags, std::string_view cmd, Args&&... params);

	/// Generate an IRC protocol message.
	template<typename... Args>
	message create_message(client_ptr src, uint32_t flags, std::string_view cmd, Args&&... params);
};
