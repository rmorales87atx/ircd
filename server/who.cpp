/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"

/* who_query --
 * @brief This function implements the actual brains behind the WHO command.
 * @param n RPL_WHO for the "standard" syntax, and RPL_WHOX for the "extended" syntax
 * @param sptr User who is requesting information
 * @param masks A list of masks to query, the meaning of which is defined by the `request` parameter
 * @param request WHOX-style request format, even if a regular WHO is being used.
 *
 * The WHOX request format is as follows:
 *   filter%fields,tag
 *
 * The following WHOX filters are implemented:
 *   c -- match channel names
 *   n -- match nicknames
 *   u -- match usernames
 *   h -- match hostnames
 *   i -- match address
 *   s -- match server names
 *   r -- match realnames (gecos)
 *   a -- match account names
 * Masks must be provided in the exact order above for every requested field.
 * If you use a request string "hnu" (as opposed to "nuh") you must still provide
 * a nick mask, user mask, and host mask, in that order.
 *
 * The following WHOX fields are implemented:
 *   t -- Returns the request tag that was given
 *   c -- Returns either one common channel or "*"
 *   u -- Returns the username
 *   i -- For IRCops only, returns the IP address; otherwise returns "*"
 *   h -- Returns the hostname
 *   s -- Returns the user's unique server identifier (SID)
 *   f -- Returns flag information
 *        [GH][*][@+]
 *        G -- away
 *        H -- not away
 *        * -- IRCop
 *        @ -- channel operator
 *        + -- channel voice
 *   d -- Returns "1" for a remote user (not connected to this server), or "0" for a local connection
 *   l -- Returns the user's idle time (in seconds)
 *   a -- Returns the account name, or "*" for no account
 *   m -- Returns the user's current modes
 *   r -- Returns the realname (gecos)
 *
 * NOTE: When using RPL_WHO as the numeric parameter (`n`), the 'd' field will not be output
 *       separately. It will be combined with the 'r' field.
 *
 * Field "c" will return multiple channels for service clients.
 *
 * Fields will be output in the order specified above regardless of the way they were input,
 * such that "cfhnrsu" and "cuhsnfr" will both result in the following output:
 *   channel, user, host, server, nick, flags, realname
 *
 * The request tag may be composed of up to 9 alphanumeric ASCII characters.
 */
bool ircd::who_query(int type, client_ptr cptr, client_ptr sptr, std::vector<std::string> masks, std::string_view request)
{
	static constexpr int MAX_RESULTS = 100;
	assert(type == ircd::RPL_WHO || type == ircd::RPL_WHOX);
	std::vector<std::string> match;
	// validate the request
	if (!strscan(request, R"(^([a-zA-Z]*)\%([a-zA-Z]+)(?:,([A-Za-z0-9]{1,9})|(?:))$)", match)) {
		return false;
	}
	// use bitsets to determine the requested filter and fields
	std::bitset<256> filter, fields;
	if (match[1].empty()) {
		if (strscan(masks[0], R"([#&+]{1}.*)")) {
			filter.set('c');
		} else {
			auto i = std::begin(masks);
			for (char elem : {'n', 'u', 'h', 's', 'r'}) {
				if (i == std::end(masks)) break;
				filter.set(elem);
				i++;
			}
		}
	}
	for (char elem : match[1]) filter.set(elem);
	for (char elem : match[2]) fields.set(elem);
	auto const& tag = match[3];
	// check parameters
	if (masks.size() != filter.count()) {
		return false;
	}
	if (filter.test('i') && !sptr->has_flag(UMODE_OPERATOR) && !sptr->has_flag(UMODE_SERVICE)) {
		sptr->send_numeric(ERR_NOTOPER, sptr->nick());
		return false;
	}
	// set up SQL query
	size_t mx = 0;
	std::string sql{ u8"SELECT * FROM [who_view]" };
	std::vector<std::string> sql_match;
	if (filter.test('c')) sql_match.push_back(format_str("[ChannelName] GLOB %1%", sqlite3xx::escape_text('Q', masks[mx++])));
	if (filter.test('n')) sql_match.push_back(format_str("[Nick] GLOB %1%", sqlite3xx::escape_text('Q', masks[mx++])));
	if (filter.test('u')) sql_match.push_back(format_str("[UserName] GLOB %1%", sqlite3xx::escape_text('Q', masks[mx++])));
	if (filter.test('h')) sql_match.push_back(format_str("[Hostname] GLOB %1%", sqlite3xx::escape_text('Q', masks[mx++])));
	if (filter.test('i')) sql_match.push_back(format_str("([Address]=%1% OR cidr_match(%1%, [Address]))", sqlite3xx::escape_text('Q', masks[mx++])));
	if (filter.test('s')) sql_match.push_back(format_str("[Server] GLOB %1%", sqlite3xx::escape_text('Q', masks[mx++])));
	if (filter.test('r')) sql_match.push_back(format_str("[Gecos] GLOB %1%", sqlite3xx::escape_text('Q', masks[mx++])));
	if (filter.test('a')) sql_match.push_back(format_str("[Account] GLOB %1%", sqlite3xx::escape_text('Q', masks[mx++])));
	// build and execute query
	if (!sql_match.empty()) {
		sql.append(" WHERE ");
		sql.append(ircd::join(sql_match, " AND "));
	}
	sql.append(" GROUP BY [EID]");
	auto results = db.exec_query(sql);
	// now build and send responses
	ircd::message reply;
	reply.prefix = info.fullname;
	reply.command = format_str("%03i", type);
	reply.args.clear();
	reply.args.push_back(sptr->nick());
	reply.flags = MSG_REPEAT_1ST;
	for (int cnt = 0; !results.eof(); results.step(), ++cnt) {
		if ((cnt > MAX_RESULTS) && !sptr->has_umode(UMODE_OPERATOR) && !sptr->has_umode(UMODE_SERVICE)) {
			sptr->send_numeric(ERR_TOOMANYMATCHES, sptr->nick(), "*");
			break;
		}
		auto target = get_client(results.field_value("EID").get_uuid());
		if (target == nullptr) continue;
		// process requested fields
		for (int field : {'t', 'c', 'u', 'i', 'h', 's', 'n', 'f', 'd', 'l', 'a', 'm', 'r'}) {
			if (fields.test(field)) {
				switch (field) {
				case 't':
					reply.args.push_back(tag);
					break;
				case 'c':
					reply.args.push_back(results.field_value("ChannelName").get_text("*"));
					break;
				case 'u':
					reply.args.push_back(results.field_value("UserName").get_text("*"));
					break;
				case 'i':
					if (sptr->has_umode(UMODE_OPERATOR) || target->uuid() == sptr->uuid()) {
						reply.args.push_back(target->address());
					} else {
						reply.args.push_back("*");
					}
					break;
				case 'h':
					reply.args.push_back(results.field_value("Hostname").get_text("*"));
					break;
				case 's':
					reply.args.push_back(results.field_value("Server").get_text("*"));
					break;
				case 'n':
					reply.args.push_back(results.field_value("Nick").get_text("*"));
					break;
				case 'f':
				{
					std::string flags;
					flags.reserve(5);
					if (target->has_umode(UMODE_AWAY)) {
						flags.append("G");
					} else {
						flags.append("H");
					}
					if (target->has_umode(UMODE_OPERATOR)) {
						flags.append("*");
					}
					auto chflags = results.field_value("Status").get_uint(0);
					if (chflags & CSTATUS_OP) {
						flags.append("@");
					}
					if (chflags & CSTATUS_VOICE) {
						flags.append("+");
					}
					reply.args.push_back(flags);
					break;
				}
				case 'd':
					if (type == RPL_WHOX) {
						reply.args.push_back(target->has_flag(CLI_REMOTE) ? "1" : "0");
					}
					break;
				case 'l':
					if (auto ts = target->get_metadata("last_command"); !ts.is_null()) {
						reply.args.push_back(std::to_string(timestamp() - ts.get_uint64(0)));
					} else {
						reply.args.push_back("*");
					}
					break;
				case 'a':
					reply.args.push_back(results.field_value("Account").get_text("*"));
					break;
				case 'm':
					reply.args.push_back(umode_str(target->get_umodes()));
					break;
				case 'r':
					reply.flags |= MSG_TRAILING_ARG;
					if (type == RPL_WHO) {
						std::string str;
						str.append(target->has_flag(CLI_REMOTE) ? "1" : "0");
						str.append(" ");
						str.append(results.field_value("Gecos").get_text("*"));
						reply.args.push_back(str);
					} else {
						reply.args.push_back(results.field_value("Gecos").get_text("*"));
					}
					break;
				}
			}
		}
		// send reply
		if (!reply.args.empty()) {
			sptr->write_msg(reply);
			reply.args.clear();
			reply.args.push_back(sptr->nick());
			reply.flags &= ~MSG_TRAILING_ARG;
		}
	}
	return true;
}
