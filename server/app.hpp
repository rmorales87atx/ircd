/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#pragma once

#if defined(_WIN32) || defined(_MSC_VER)
# pragma warning(disable: 4996)
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
# undef min
# undef max
#endif

#include <tuple>
#include <ctime>
#include <cstdio>
#include <iostream>
#include <string>
#include <string_view>
#include <memory>
#include <deque>
#include <iomanip>
#include <bitset>
#include <functional>
#include <list>
#include <forward_list>
#include <iostream>
#include <fstream>
#include <optional>
#include <unordered_map>
#include <boost/predef.h>
#include <boost/format.hpp>
#include <boost/tokenizer.hpp>
#include <boost/regex.hpp>
#if !BOOST_OS_WINDOWS
# include <boost/regex/icu.hpp>
#endif
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/asio/ip/network_v4.hpp>
#include <boost/asio/ip/network_v6.hpp>
#include <boost/date_time.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/spirit/home/x3.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/fusion/include/std_pair.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_serialize.hpp>
#include <boost/functional/hash.hpp>
#include <boost/locale.hpp>
#include "utf8.h"
#include "sha2.hpp"
#include "SQLite3xx.hpp"
#include "sol/sol.hpp"
#include "tsl/ordered_map.h"

/// Character literal macro
#define _C(C) U##C

/// UTF-8 string literal macro
#define _S(S) u8##S

namespace asio = boost::asio;
using asio::ip::tcp;

namespace ircd {
	constexpr int VERSION_MAJOR = 0;
	constexpr int VERSION_MINOR = 23;
	constexpr int VERSION_PATCH = 0;

	/// Maximum number of bytes in any IRC message
	constexpr int IRC_MAXBUF = 1024;

	/// Limit identifiers
	enum SERVER_LIMITS {
		MAX_REASON,
		MAX_CHANLIST,
		MAX_TARGETS,
		MAX_SILENCE,
		MAX_CHANNELS,
		MAX_NAMELEN,
		MAX_NICKLEN,
		MAX_CHANLEN,
		MAX_MONITOR,
		MAX_KEYLEN,
		MAX_NICKDELAY,
		MAX_CHANDELAY,
		MAX_TSDELTA,
		NUM_LIMITS
	};

	/// Jupe types
	enum JUPE_TYPES {
		JUPE_NICK,
		JUPE_CHANNEL,
		MAX_JUPE_TYPES,
	};

	/// Jupe status codes
	enum JUPE_STATUS_CODES {
		JUPE_STATUS_PERM = 0,   // from config.lua, cannot be removed
		JUPE_STATUS_TEMP,       // temporary, no expiration
		JUPE_STATUS_NICKDELAY,  // nick-delay jupe
		JUPE_STATUS_CHANDELAY,  // chan-delay jupe
	};

	/// Server notification masks (SNOMASK)
	enum SNO_MASKS {
		/* d */ SNO_DEBUG = (1 << 0),
		/* u */ SNO_LOCAL_USER_CONN = (1 << 1),
		/* U */ SNO_REMOTE_USER_CONN = (1 << 2),
		/* o */ SNO_OPER_STATUS = (1 << 3),
		/* s */ SNO_SERVICE_CONN = (1 << 4),
		/* l */ SNO_SERVER_LINK = (1 << 5),
		/* r */ SNO_REJECT_CONN = (1 << 6),
		/* S */ SNO_STATS = (1 << 7),
		/* k */ SNO_KILLS = (1 << 8),
		/* n */ SNO_NICK_JUPE = (1 << 9),
		/* c */ SNO_CHAN_JUPE = (1 << 10),
	};

	/// Log categories
	enum LOG_CATEGORY {
		LOG_DEBUG = (1 << 0),
		LOG_GENERAL = (1 << 1),
		LOG_USER = (1 << 2),
		LOG_OPER = (1 << 3),
		LOG_SERVER = (1 << 4),
		LOG_KILL = (1 << 5),
	};

	// Forward declarations
	class client;
	class session_manager;
	struct message;
	struct session;

	typedef std::shared_ptr<client> client_ptr;
	typedef std::shared_ptr<session> session_ptr;

	/// Listener socket types
	enum LISTEN_TYPE {
		LISTEN_PLAINTEXT = 1,
		LISTEN_SSL
	};

	/// Simple object which opens a TCP listening socket for the given endpoint
	class listener {
	public:
		listener(session_manager& mgr, int type, tcp::endpoint endpoint);
		~listener();
		int type() const;
		tcp::endpoint endpoint() const;
	private:
		session_manager& owner_;
		int type_;
		tcp::endpoint endpt_;
		tcp::acceptor acceptor_;
		void start();
	};

	/// Data from an IDENT response
	struct ident_response {
		int server_port;
		int client_port;
		std::string os_name;
		std::string user_id;
	};

	/// Manage socket connections for clients and server links
	class session_manager {
	public:
		session_manager();
		~session_manager();
		void close();
		void listen(int type, std::string_view host, int port);
		void accept(listener& src, tcp::socket peer);
		void connect(bool use_ssl, std::string_view name, std::string_view host, std::string_view port);
	private:
		std::list<std::unique_ptr<listener>> listen_;
		tsl::ordered_map<std::string, std::weak_ptr<session>> sockets_;
		void deleter(session* sptr);
		bool process(listener& src, session_ptr peer);
	};

	/// Class used to resolve IDENT responses
	class ident_handler {
	public:
		/// Type used for callback in run() method
		typedef std::function<void(std::optional<std::string>)> handler_t;

		/// Default constructor
		ident_handler();

		/// @brief Execute an IDENT request for the given client.
		/// @param sptr Source client
		/// @param onComplete Callback handler
		///
		/// An IDENT request is established by connecting to the source
		/// client's address on port 113, then using the IDENT protocol
		/// to request an IDENT response.
		///
		/// If no response is received, due either to a failed connection or
		/// an incorrect IDENT response, the onComplete handler is called
		/// with the value std::nullopt.
		///
		/// If a valid IDENT response is received, the onComplete handler is
		/// called with the value of the IDENT response.
		void run(client_ptr sptr, handler_t onComplete);

		/// Terminate the IDENT request, if one is being currently processed.
		void kill();

	private:
		/// Socket used to establish IDENT connection
		tcp::socket socket_;

		/// Endpoints used for socket connections
		tcp::endpoint remote_, local_;

		/// IDENT protocol message buffer
		asio::streambuf buf_;

		/// Callback received in run() method
		handler_t handler_;

		/// ASIO callback for IDENT socket connection
		void on_connect(boost::system::error_code const&);

		/// ASIO callback for IDENT socket data
		void on_read(boost::system::error_code const& err, size_t len);

		/// Parse response
		bool parse(std::string_view src, ident_response& res);
	};

	/// Retrieve build timestamp
	char const* id_buildtime();

	/// Determine name of compiled architecture
	char const* id_arch();

	/// Determine name of compiler
	char const* id_comp();

	/// Determine name of compiled operating system
	char const* id_os();
};

#include "misc.hpp"
#include "caps.hpp"
#include "msg.hpp"
#include "mode.hpp"
#include "numeric.hpp"
#include "client.hpp"
#include "channel.hpp"
#include "module.hpp"
#include "conf.hpp"
#include "ircd.hpp"
#include "ircd.ipp" // template function implementations
