/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"

using boost::system::error_code;
using boost::format;
using std::nullopt;
using namespace std::placeholders;

#define DEBUG_PRINT_READWRITE 0

namespace ircd {
	bool ssl_verify_certificate(bool preverified, asio::ssl::verify_context& ctx, session_ptr sptr, client_ptr cptr);
	void on_ssl_handshake(error_code const& err, session_ptr sptr, client_ptr cptr);
	void socket_read(session_ptr sptr, error_code const& err, std::size_t nbytes);
}

ircd::session::session()
	: socket{ io_ctx }
{}

ircd::session::session(tcp::socket sock)
	: socket{ std::move(sock) }
	, remote{ socket.remote_endpoint() }
	, local{ socket.local_endpoint() }
{
	// setup TCP keep-alive
	constexpr unsigned int base_timeout_value = 75000; // milliseconds
#if BOOST_OS_WINDOWS
	constexpr std::int32_t w32_timeout = base_timeout_value;
	setsockopt(socket.native_handle(), SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<const char*>(&w32_timeout), sizeof(w32_timeout));
	setsockopt(socket.native_handle(), SOL_SOCKET, SO_SNDTIMEO, reinterpret_cast<const char*>(&w32_timeout), sizeof(w32_timeout));
#else
	// assume POSIX
	struct timeval tv;
	tv.tv_sec = base_timeout_value / 1000;
	tv.tv_usec = base_timeout_value % 1000;
	setsockopt(socket.native_handle(), SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
	setsockopt(socket.native_handle(), SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv));
#endif
}

ircd::session::~session()
{
	close();
}

std::string ircd::session::endpoint_id()
{
	error_code ec;
	auto addr = remote.address().to_string(ec);
	if (remote.address().is_v6() && addr.at(0) == ':') {
		addr.insert(addr.begin(), '0');
	}
	return addr + ":" + std::to_string(remote.port());
}

asio::ip::address ircd::session::address()
{
	return remote.address();
}

void ircd::session::close()
{
	error_code ec;
	queue.clear();
	socket.cancel(ec);
	if (ssl_socket) {
		ssl_socket->shutdown(ec);
	}
	socket.close(ec);
}

void ircd::session::enable_ssl(client_ptr cptr)
{
	if (!ssl_socket && socket.is_open()) {
		ssl_socket = std::make_unique<asio::ssl::stream<tcp::socket&>>(socket, ssl_context);
		if (!cptr->has_flag(CLI_REMOTE)) {
			auto self = shared_from_this();
			int vflags = asio::ssl::verify_peer;
			auto hflags = asio::ssl::stream_base::server;
			if (info.require_client_certificate) {
				vflags |= asio::ssl::verify_fail_if_no_peer_cert;
			}
			if (cptr->has_flag(CLI_SERVER)) {
				hflags = asio::ssl::stream_base::client;
				vflags |= asio::ssl::verify_fail_if_no_peer_cert; // always required for servers
			}
			ssl_socket->set_verify_mode(vflags);
			ssl_socket->set_verify_callback(std::bind(&ircd::ssl_verify_certificate, _1, _2, self, cptr));
			ssl_socket->async_handshake(hflags, std::bind(&ircd::on_ssl_handshake, _1, self, cptr));
		}
	}
}

bool ircd::ssl_verify_certificate(bool preverified, asio::ssl::verify_context& ctx, session_ptr sptr, client_ptr cptr)
{
	if (!cptr->has_metadata("ssl_fingerprint")) {
		X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
		unsigned cb;
		unsigned char md[EVP_MAX_MD_SIZE];
		std::stringstream ss;
		auto rc = X509_digest(cert, EVP_sha256(), md, &cb);
		for (unsigned i = 0; i < cb; i++) {
			ss << std::setw(2) << std::setfill('0') << std::hex << (int)(md[i]);
		}
		cptr->set_metadata("ssl_fingerprint", ss.str());
		write_log(LOG_USER, nullptr, "SSL fingerprint for %1% is %2%", sptr->endpoint_id(), ss.str());
	}
	return cptr->has_flag(CLI_SERVER) ? preverified : true;
}

void ircd::on_ssl_handshake(error_code const& err, session_ptr sptr, client_ptr cptr)
{
	if (!sptr->socket.is_open()) return;
	if (err) {
		log_connect_attempt(sptr->remote);
		snomask_log(LOG_USER, SNO_REJECT_CONN, "Error processing secure connection from %1% on port %2% (%3%: %4%)",
			cptr->endpoint_id(), sptr->local.port(), err.value(), err.message());
		kill_client(cptr, "");
		sptr->error(err.message());
		return;
	}
	if (cptr->has_flag(CLI_SERVER)) {
		init_link(cptr);
		return;
	}
	post([sptr, cptr] {
		cptr->do_host_resolve();
		sptr->read();
	});
}

void ircd::session::read()
{
	try {
		if (ssl_socket) {
			asio::async_read_until(*ssl_socket, data, "\n", std::bind(&socket_read, shared_from_this(), _1, _2));
		} else {
			asio::async_read_until(socket, data, "\n", std::bind(&socket_read, shared_from_this(), _1, _2));
		}
	} catch (std::exception const& err) {
		write_log(LOG_DEBUG, nullptr, "Socket error: %1%", err.what());
		error("Socket error");
	}
}

void ircd::socket_read(session_ptr sptr, error_code const& err, std::size_t nbytes)
{
	if (!sptr->socket.is_open()) return;
	if (!err) {
		std::string line;
		std::istream is{ &(sptr->data) };
		std::getline(is, line);
#if DEBUG_PRINT_READWRITE == 1
		std::cerr << "R: " << line << std::endl;
#endif
		if (auto msg = ircd::tokenize(line)) {
			auto cptr = find_client(sptr->endpoint_id());
			assert(cptr != nullptr);
			do_command(cptr, *msg, nbytes);
			db.exec_dml("UPDATE [client-stats] SET [RecvCount]=[RecvCount]+1, [RecvBytes]=[RecvBytes]+?2 WHERE [EID]=?1", cptr->eid(), nbytes);
		}
		if (sptr->socket.is_open()) post([sptr]() { sptr->read(); });
	} else {
		if (err != asio::error::shut_down) {
			snomask_log(LOG_USER, SNO_LOCAL_USER_CONN, "Read error from %1%, closing connection (%2%)", sptr->endpoint_id(), err.message());
		}
		auto cptr = find_client(sptr->endpoint_id());
		if (cptr) {
			kill_client(cptr, "Read error");
		} else {
			sptr->error("Read error");
		}
	}
}

void ircd::session::write(std::string_view str)
{
	if (!socket.is_open()) return;
	// queue message for writing
	bool sending = !queue.empty();
	queue.push_back(std::string(str) + "\r\n");
	if (!sending) {
		send_message();
	}
}

void ircd::session::send_message()
{
	if (!socket.is_open()) return;
	auto cptr = find_client(endpoint_id());
	// ensure that the outgoing message is valid UTF8
	std::string const& temp = queue.front();
	std::string message;
	message.reserve(temp.size());
	utf8::replace_invalid(temp.begin(), temp.end(), std::back_inserter(message));
#if DEBUG_PRINT_READWRITE == 1
	std::cerr << "W: " << message;
#endif
	// transmit data
	error_code ec;
	size_t nbytes = 0;
	if (ssl_socket) {
		nbytes = asio::write(*ssl_socket, asio::buffer(message.c_str(), message.size()), ec);
	} else {
		nbytes = asio::write(socket, asio::buffer(message.c_str(), message.size()), ec);
	}
	if (!ec) {
		queue.pop_front();
		if (cptr) db.exec_dml("UPDATE [client-stats] SET [SendCount]=[SendCount]+1, [SendBytes]=[SendBytes]+?2 WHERE [EID]=?1", cptr->eid(), nbytes);
		if (!queue.empty() && socket.is_open()) {
			post([this, self = shared_from_this()](){ send_message(); });
		}
	} else {
		if (ec != asio::error::shut_down) {
			snomask_log(LOG_USER, SNO_LOCAL_USER_CONN, "Write error on %1%, closing connection (%2%)", endpoint_id(), ec.message());
		}
		if (cptr) {
			kill_client(cptr, "");
		} else {
			error("");
		}
	}
}

void ircd::client::write_msg(client_ptr sptr, uint32_t flags, std::string_view cmd, sqlite3xx::statement& query)
{
	message msg;
	msg.prefix = msg_prefix(sptr, shared_from_this(), msg);
	msg.command = cmd;
	msg.flags = flags;
	for (int ix = 0; ix < query.field_count(); ++ix) {
		msg.args.push_back(query.field_value(ix).get_text());
	}
	write_msg(msg);
}

void ircd::client::write_msg(client_ptr sptr, uint32_t flags, std::string_view cmd, sol::variadic_args va)
{
	message msg;
	msg.prefix = msg_prefix(sptr, shared_from_this(), msg);
	msg.command = cmd;
	msg.flags = flags;
	for (auto v : va) {
		if (v.is<sqlite3xx::column>()) {
			auto& col{ v.as<sqlite3xx::column>() };
			msg.args.push_back(col.get_text());
		} else if (v.is<sqlite3xx::statement>()) {
			auto& query{ v.as<sqlite3xx::statement>() };
			for (int ix = 0; ix < query.field_count(); ++ix) {
				msg.args.push_back(query.field_value(ix).get_text());
			}
		} else {
			msg.args.push_back(v.as<std::string>());
		}
	}
	write_msg(msg);
}

void ircd::client::write_msg(ircd::message msg)
{
	// transmit prepared IRC protocol message
	//
	// protocol limits each transmitted message to approx.
	// 15 parameters including the literal command with
	// the (optional) trailing parameter
	static constexpr size_t MAX_PARAM = 15;
	size_t num_groups = (size_t)std::ceil(msg.args.size() / (double)MAX_PARAM);
	size_t idx = 0;
	std::string trail;
	if (msg.args.size() == 0) num_groups = 1;
	// pull the last arg if we're expecting a trailing param
	if (msg.flags & MSG_TRAILING_ARG) {
		trail = msg.args.back();
		msg.args.pop_back();
	}
	// begin processing messages
	for (size_t group = 0; group < num_groups; ++group) {
		std::string res;
		if (has_cap(CAP_MESSAGE_TAGS)) {
			msg.tags["msgid"] = uuid_string(time_uuid());
		}
		if (has_cap(CAP_SERVER_TIME)) {
			boost::posix_time::ptime pt(boost::posix_time::microsec_clock::universal_time());
			auto timestr = boost::posix_time::to_iso_extended_string(pt).substr(0, 23);
			msg.tags["time"] = timestr + "Z";
		}
		// output other tags
		if (!msg.tags.empty() && (has_cap(CAP_MESSAGE_TAGS) || has_cap(CAP_ACCOUNT_TAG) || has_cap(CAP_SERVER_TIME) || has_cap(CAP_BATCH))) {
			res.append("@");
			res.append(join_pairs(msg.tags, ";", "="));
			res.append(" ");
		}
		// output the message source if specified and not overridden in flags
		if (!msg.prefix.empty() && !(msg.flags & MSG_NO_PREFIX)) {
			res.append(":");
			res.append(msg.prefix);
			res.append(" ");
		}
		res.append(msg.command);
		// if requested, output the first arg for additional transmissions
		if ((msg.flags & MSG_REPEAT_1ST) && group > 0) {
			res.append(" ");
			res.append(msg.args[0]);
		}
		// output params up to the limit for this transmission
		for (size_t nparam = 0; idx < msg.args.size() && nparam++ < MAX_PARAM; ++idx) {
			res.append(" ");
			res.append(msg.args[idx]);
		}
		// always output the trailing param
		if (msg.flags & MSG_TRAILING_ARG) {
			res.append(" :");
			res.append(trail);
		}
		// transmit
		session_->write(res);
	}
}

std::string ircd::client::start_batch(std::string_view type, std::string_view label, std::vector<std::string> params)
{
	if (has_cap(CAP_BATCH)) {
		auto uuid = boost::uuids::random_generator()();
		auto batch_id = uuid_string(uuid);
		message msg;
		msg.prefix = has_flag(CLI_REMOTE) ? info.id : info.fullname;
		msg.command = "BATCH";
		msg.flags = 0;
		if (!label.empty() && has_cap(CAP_LABELED_RESPONSE)) msg.tags["label"] = label;
		msg.args.push_back("+" + batch_id);
		msg.args.emplace_back(type);
		for (std::string_view par : params) {
			msg.args.emplace_back(par);
		}
		write_msg(msg);
		return batch_id;
	} else {
		return "";
	}
}

void ircd::client::stop_batch(std::string_view batch_id)
{
	if (!batch_id.empty() && has_cap(CAP_BATCH)) {
		write_msg(nullptr, 0, "BATCH", "-" + std::string(batch_id));
	}
}

void ircd::session::error(std::string_view text)
{
	if (socket.is_open()) {
		error_code ec;
		queue.clear();
		if (!text.empty()) {
			auto addr{ remote.address().to_string(ec) };
			auto message{ format_str("ERROR :Closing Link: %1% (%2%)\r\n", addr, text) };
#if DEBUG_PRINT_READWRITE == 1
			std::cerr << "W: " << message;
#endif
			if (ssl_socket) {
				asio::write(*ssl_socket, asio::buffer(message.c_str(), message.size()), asio::transfer_all(), ec);
			} else {
				asio::write(socket, asio::buffer(message.c_str(), message.size()), asio::transfer_all(), ec);
			}
		}
		close();
	}
}
