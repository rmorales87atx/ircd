/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"

struct mask_info {
	uint16_t bit;
	uint32_t cp;
};

namespace ircd {
	static mask_info const sno_masks[] = {
		{ SNO_DEBUG, _C('d') },
		{ SNO_LOCAL_USER_CONN, _C('u') },
		{ SNO_REMOTE_USER_CONN, _C('U') },
		{ SNO_OPER_STATUS, _C('o') },
		{ SNO_SERVICE_CONN, _C('s') },
		{ SNO_SERVER_LINK, _C('l') },
		{ SNO_REJECT_CONN, _C('r') },
		{ SNO_STATS, _C('S') },
		{ SNO_KILLS, _C('k') },
		{ SNO_NICK_JUPE, _C('n') },
		{ SNO_CHAN_JUPE, _C('c') },
		{ 0, 0 }
	};
};

void ircd::client::set_snomask(uint16_t what, bool state)
{
	if (state) {
		snomask_ |= what;
	} else {
		snomask_ &= ~what;
	}
}

uint16_t ircd::client::set_snowmask(uint16_t what)
{
	uint16_t tmp = snomask_;
	if (!has_umode(UMODE_OPERATOR)) {
		what &= ~(SNO_DEBUG | SNO_LOCAL_USER_CONN | SNO_REMOTE_USER_CONN | SNO_REJECT_CONN | SNO_STATS);
	}
	snomask_ = what;
	return tmp;
}

uint16_t ircd::client::change_snomask(std::string_view what)
{
	if (boost::all(what, boost::is_any_of("01")) && what.length() == SNOMASK_STRLEN) {
		size_t n = SNOMASK_STRLEN;
		uint16_t tmp = 0;
		for (auto const& chr : what) {
			--n; // bits are right-to-left (big endian)
			if (chr == '0') {
				tmp &= (1 << n);
			} else if (chr == '1') {
				tmp |= (1 << n);
			} else {
				return snomask_;
			}
		}
		return set_snowmask(tmp);
	} else if (strscan(what, R"(^[a-zA-Z\+\-]+$)")) {
		uint16_t tmp = snomask_;
		bool state = false;
		for (auto iter = what.begin(); iter != what.end();) {
			auto cp = utf8::next(iter, what.end());
			if (cp == _C('+') || cp == _C('-')) {
				state = cp == _C('+');
			} else {
				for (auto snm = sno_masks; snm->bit != 0; snm++) {
					if (cp != snm->cp) continue;
					if (state) tmp |= snm->bit;
					else tmp &= ~(snm->bit);
				}
			}
		}
		return set_snowmask(tmp);
	} else {
		return snomask_;
	}
}

std::string ircd::client::get_snomask(int format) const
{
	std::vector<char> buf;
	if (format == 0) {
		buf.reserve(SNOMASK_STRLEN + 1);
		for (size_t n = 0; n < SNOMASK_STRLEN; n++) {
			if (snomask_ & (1 << n)) {
				buf.insert(buf.begin(), '1');
			} else {
				buf.insert(buf.begin(), '0');
			}
		}
	} else if (format == 1 && snomask_ != 0) {
		for (auto snm = sno_masks; snm->bit != 0; snm++) {
			if (snomask_ & snm->bit) {
				utf8::append(snm->cp, std::back_inserter(buf));
			}
		}
	}
	buf.push_back(0);
	return buf.data();
}

uint16_t ircd::client::raw_snomask() const
{
	return snomask_;
}
