/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"

namespace ircd {
	/* Base commands for users */

	void cmd_account(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_admin(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_away(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_cap(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_error(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_invite(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_join(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_kick(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_kill(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_list(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_lusers(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_mode(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_motd(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_names(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_nick(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_notice(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_oper(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_part(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_pass(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_ping(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_pong(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_privmsg(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_quit(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_rehash(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_service(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_snomask(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_squery(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_stats(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_summon(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_tagmsg(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_time(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_topic(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_user(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_users(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_version(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_wallops(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_who(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_whois(client_ptr cptr, client_ptr sptr, std::optional<message> msg);
	void cmd_webirc(client_ptr cptr, client_ptr sptr, std::optional<message> msg);

	/* Utility functions & constants */

	void update_channel_invites(client_ptr sptr, std::string_view chname, client_ptr target);
	void db_stats_reply(std::string_view query, int rpl_line, int rpl_end, client_ptr sptr, std::optional<message> msg);
	bool accept_oper(client_ptr sptr, std::string_view name, std::string_view passwd);
	bool accept_service(client_ptr sptr, std::vector<std::string> const& args);
	void show_whois(client_ptr sptr, std::string_view target_name, std::optional<message> msg);
	bool accept_webirc(client_ptr sptr, std::vector<std::string> const& args);

	struct mask_info {
		uint16_t bit;
		char const* tag;
		char const* description;
	};

	struct labeled_response_batch {
		std::string id;
		client_ptr sptr;

		labeled_response_batch(std::optional<message> msg, client_ptr sptr_)
			: sptr{ sptr_ }
		{
			if (msg && msg->tags.count("label")) {
				id = sptr->start_batch("labeled-response", msg->tag("label"), {});
			}
		}

		~labeled_response_batch()
		{
			sptr->stop_batch(id);
		}
	};
}

void ircd::init_commands()
{
	add_command("ACCOUNT", cmd_account);
	add_command("ADMIN", cmd_admin);
	add_command("AWAY", cmd_away);
	add_command("CAP", cmd_cap);
	add_command("ERROR", cmd_error);
	add_command("INVITE", cmd_invite);
	add_command("JOIN", cmd_join);
	add_command("KICK", cmd_kick);
	add_command("KILL", cmd_kill);
	add_command("LIST", cmd_list);
	add_command("LUSERS", cmd_lusers);
	add_command("MODE", cmd_mode);
	add_command("MOTD", cmd_motd);
	add_command("NAMES", cmd_names);
	add_command("NICK", cmd_nick);
	add_command("NOTICE", cmd_notice);
	add_command("OPER", cmd_oper);
	add_command("PART", cmd_part);
	add_command("PASS", cmd_pass);
	add_command("PING", cmd_ping);
	add_command("PONG", cmd_pong);
	add_command("PRIVMSG", cmd_privmsg);
	add_command("QUIT", cmd_quit);
	add_command("REHASH", cmd_rehash);
	add_command("SERVICE", cmd_service);
	add_command("SNOMASK", cmd_snomask);
	add_command("SQUERY", cmd_squery);
	add_command("STATS", cmd_stats);
	add_command("SUMMON", cmd_summon);
	add_command("TAGMSG", cmd_tagmsg);
	add_command("TIME", cmd_time);
	add_command("TOPIC", cmd_topic);
	add_command("USER", cmd_user);
	add_command("USERS", cmd_users);
	add_command("VERSION", cmd_version);
	add_command("WALLOPS", cmd_wallops);
	add_command("WHO", cmd_who);
	add_command("WHOIS", cmd_whois);
	add_command("WEBIRC", cmd_webirc);
}

void ircd::cmd_cap(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!msg || !sptr) return;
	if (cptr && cptr->has_flag(CLI_SERVER)) return;
	if (msg->prefix.empty()) msg->prefix = "*";
	if (msg->args.empty()) {
		sptr->send_numeric(ERR_NEEDPARAMS, sptr->nick(), msg->command);
	} else if (same_text(msg->args[0], "LS") && !sptr->has_flag(CLI_REMOTE)) {
		// send list of available caps
		sptr->write_msg(nullptr, MSG_TRAILING_ARG, "CAP", msg->prefix, "LS", cap_to_str(caps_implemented()));
		// if the client isn't registered, set the cap negotiation flag
		// CAP negotiation must be completed before registration is allowed
		if (!sptr->has_flag(CLI_REGISTERED)) {
			sptr->set_flag(CLI_NEGOTIATING_CAPS, true);
		}
	} else if (same_text(msg->args[0], "REQ") && !sptr->has_flag(CLI_REMOTE)) {
		if (msg->args.size() < 2) {
			sptr->send_numeric(ERR_NEEDPARAMS, sptr->nick(), msg->command);
		} else {
			std::string err;
			auto add = cap_from_str(msg->args[1], &err);
			if (err.empty()) {
				// client sent a good list of CAPs, go ahead and ACK
				sptr->write_msg(nullptr, MSG_TRAILING_ARG, "CAP", msg->prefix, "ACK", msg->args[1]);
				// if the client isn't registered, set the cap negotiation flag
				// CAP negotiation must be completed before registration is allowed
				if (!sptr->has_flag(CLI_REGISTERED)) {
					auto caps = add | sptr->get_metadata("cap_request").get_uint64(0);
					sptr->set_metadata("cap_request", std::to_string(caps));
					sptr->set_flag(CLI_NEGOTIATING_CAPS, true);
				} else {
					sptr->set_cap(add, true);
				}
			} else {
				// client included at least one bad CAP identifier; drop the entire request
				sptr->write_msg(nullptr, MSG_TRAILING_ARG, "CAP", msg->prefix, "NAK", err);
			}
		}
	} else if (same_text(msg->args[0], "END") && !sptr->has_flag(CLI_REMOTE)) {
		// accept "CAP END" only when client is unregistered
		if (!sptr->has_flag(CLI_REGISTERED)) {
			sptr->set_caps(sptr->get_metadata("cap_request").get_uint64(0));
			sptr->set_flag(CLI_NEGOTIATING_CAPS, false);
			// attempt to complete registration, most clients seem to have sent PASS/NICK/USER by this point
			sptr->do_register();
		} else {
			sptr->send_numeric(ERR_REGISTERED, sptr->nick(), msg->command);
		}
	} else if (same_text(msg->args[0], "LIST") && !sptr->has_flag(CLI_REMOTE)) {
		// send client a list of their active CAPs
		// for unregistered clients this is a list of caps previously accepted by CAP REQ
		uint64_t caps = 0;
		if (!sptr->has_flag(CLI_REGISTERED)) {
			caps = sptr->get_metadata("cap_request").get_uint64(0);
		} else {
			caps = sptr->get_caps();
		}
		sptr->write_msg(nullptr, MSG_TRAILING_ARG, "CAP", msg->prefix, "LIST", cap_to_str(caps));
	} else if (!sptr->has_flag(CLI_REMOTE)) {
		sptr->send_numeric(ERR_INVALIDCAP, sptr->nick(), msg->args[0]);
	}
}

void ircd::cmd_account(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	// command used by services to update a user's ACCOUNT identifier
	// format:
	//     ACCOUNT <target> <new account identifier>
	// asterisk "*" must be used to indicate user has logged out
	if (!sptr->has_umode(UMODE_SERVICE)) {
		sptr->reply_numeric(msg->tag("label"), ERR_UNKNOWNCOMMAND, sptr->nick(), msg->command);
		snomask_log(LOG_DEBUG, SNO_DEBUG, "Received invalid command from %1%: %2%", sptr->userhost(), msg->command);
	} else {
		client_ptr target;
		if (msg->args.size() < 2) {
			sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
		} else if (!(target = find_user(msg->args[0]))) {
			sptr->reply_numeric(msg->tag("label"), ERR_NOSUCHNICK, sptr->nick(), msg->args[0]);
		} else if (target->has_umode(UMODE_SERVICE)) {
			sptr->reply_numeric(msg->tag("label"), ERR_ISSERVICE, sptr->nick(), target->nick());
		} else {
			target->set_account(msg->args[1].substr(0, info.limits[MAX_NAMELEN]));
			notify_servers(cptr, sptr, 0, "ACCOUNT", target->eid(), target->account());
		}
	}
}

void ircd::cmd_admin(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->send_numeric(ERR_NOTREGISTERED, sptr->nick());
	} else if (msg->args.size() == 1 && !sptr->has_flag(CLI_REMOTE) && !same_text(msg->args[0], info.fullname)) {
		auto serv_ptr = find_server_name(msg->args[0]);
		if (!serv_ptr) {
			sptr->reply_numeric(msg->tag("label"), ERR_NOSUCHSERVER, sptr->nick(), msg->args[0]);
		} else {
			serv_ptr->write_msg(sptr, MSG_TRAILING_ARG, "ADMIN", msg->args[0]);
		}
	} else {
		std::string batch_id;
		if (msg && msg->tags.count("label")) {
			batch_id = sptr->start_batch("labeled-response", msg->tag("label"), {});
		}
		sptr->batch_numeric(batch_id, RPL_ADMINME, sptr->nick(), info.fullname);
		sptr->batch_numeric(batch_id, RPL_ADMINLOC1, sptr->nick(), info.admin[0]);
		sptr->batch_numeric(batch_id, RPL_ADMINLOC2, sptr->nick(), info.admin[1]);
		sptr->batch_numeric(batch_id, RPL_ADMINEMAIL, sptr->nick(), info.admin[2]);
		if (!batch_id.empty()) sptr->stop_batch(batch_id);
	}
}

void ircd::cmd_away(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->send_numeric(ERR_NOTREGISTERED, sptr->nick());
	} else if (sptr->has_umode(UMODE_SERVICE)) {
		sptr->send_notice("Services cannot use AWAY");
	} else if (msg->args.size() < 1 || msg->args[0].empty()) {
		sptr->change_umodes("-a", false);
		sptr->set_metadata("away_message", {});
		sptr->cap_notify(CAP_AWAY_NOTIFY, create_message(sptr, 0, "AWAY", ""));
		if (!sptr->has_flag(CLI_REMOTE)) {
			sptr->reply_numeric(msg->tag("label"), RPL_UNAWAY, sptr->nick());
		}
	} else {
		bool is_away = sptr->has_umode(UMODE_AWAY);
		auto messg = msg->args[0].substr(0, info.limits[MAX_REASON]);
		sptr->change_umodes("+a", false);
		sptr->set_metadata("away_message", messg);
		sptr->cap_notify(CAP_AWAY_NOTIFY, create_message(sptr, MSG_TRAILING_ARG, "AWAY", messg));
		if (!sptr->has_flag(CLI_REMOTE)) {
			sptr->reply_numeric(msg->tag("label"), RPL_AWAY, sptr->nick(), is_away ? "Your away message has been updated" : "You have been marked as being away");
		}
	}
}

void ircd::cmd_error(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED)) {
		kill_client(sptr, "Connection closed");
	} else if (!sptr->has_flag(CLI_REMOTE)) {
		int snomask = SNO_LOCAL_USER_CONN;
		if (sptr->has_flag(CLI_SERVER)) {
			snomask = SNO_SERVER_LINK;
		}
		if (msg->args.size() == 1) {
			notify_snomask(snomask, "*** Notice -- Received ERROR from %1%: %2%", sptr->userhost(), msg->args[1]);
		} else {
			notify_snomask(snomask, "*** Notice -- Received ERROR from %1%", sptr->userhost());
		}
		kill_client(sptr, "Connection closed");
	}
}

void ircd::update_channel_invites(client_ptr sptr, std::string_view chname, client_ptr target)
{
	if (channel::has_mode(chname, CMODE_INVITE_ONLY)) {
		// add to channels' INVITE list
		// invites are designed to stay persistent until:
		//   � the user QUITs;
		//   � the user is KICKed from the channel;
		//   � the +i mode is removed
		channel::list_add(chname, CHLIST_INVITE, target->userhost(), sptr->userhost(), ircd::timestamp());
		// IRCv3 notification
		for (auto member : channel::get_members(chname)) {
			if (member && !member->has_flag(CLI_REMOTE) && member->has_cap(CAP_INVITE_NOTIFY)) {
				member->write_msg(sptr, 0, "INVITE", target->nick(), chname);
			}
		}
	}
}

void ircd::cmd_invite(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->send_numeric(ERR_NOTREGISTERED, sptr->nick());
	} else if (msg->args.size() < 2) {
		sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
	} else {
		auto chrec = channel::find(msg->args[1]);
		auto target = sptr->has_flag(CLI_SERVER) ? find_entity(msg->args[0]) : find_user(msg->args[0]);
		// note: RFC 2812 permits a user to INVITE to a non-existent channel
		// and all users may invite when a channel is not +i
		if (!target) {
			sptr->reply_numeric(msg->tag("label"), ERR_NOSUCHNICK, sptr->nick(), msg->args[0]);
		} else if (chrec && !sptr->has_flag(CLI_REMOTE) && !channel::is_op(chrec->name, sptr) && channel::has_mode(chrec->name, CMODE_INVITE_ONLY)) {
			sptr->reply_numeric(msg->tag("label"), ERR_CHANOPRIVSNEEDED, sptr->nick(), chrec->name);
		} else {
			update_channel_invites(sptr, chrec->name, target);
			auto const& chname = chrec.has_value()? chrec->name : msg->args[1];
			// RFC 2812 says the order should be: <channel> <name>
			// an unfortunate "mistake" means everyone else implemented it in reverse
			if (!sptr->has_flag(CLI_REMOTE)) {
				sptr->reply_numeric(msg->tag("label"), RPL_INVITING, sptr->nick(), target->nick(), chname);
				// away notification
				if (target->has_umode(UMODE_AWAY)) {
					sptr->reply_numeric(msg->tag("label"), RPL_ISAWAY, sptr->nick(), target->nick(), target->get_metadata("away_message"));
				}
			}
			// send invite notification
			if (!target->has_flag(CLI_REMOTE)) target->write_msg(sptr, 0, "INVITE", target->nick(), chname);
			notify_servers(cptr, sptr, 0, "INVITE", target->eid(), chname);
		}
	}
}

void ircd::cmd_join(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->send_numeric(ERR_NOTREGISTERED, sptr->nick());
	} else if (msg->args.size() < 1) {
		sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
	} else {
		size_t nkey = 0;
		std::vector<std::string> key_params, names;
		if (msg->args.size() >= 2) {
			split_text(key_params, msg->args[1], ",", std::nullopt);
		}
		split_text(names, msg->args[0], ",", std::nullopt);
		auto num_chans = db.exec_scalar("SELECT COUNT(*) FROM [channel-users] WHERE [EID]=?", sptr->eid()).get_uint(0);
		for (std::string_view what : names) {
			int jupe_code = -1;
			if (auto rc = valid_chan(what); rc != NAME_ERROR_NONE) {
				switch (rc) {
				case NAME_ERROR_LENGTH:
					sptr->reply_numeric(msg->tag("label"), ERR_BADCHANNAME, sptr->nick(), what, "name is too long");
					break;
				case NAME_ERROR_CHARS:
					sptr->reply_numeric(msg->tag("label"), ERR_BADCHANNAME, sptr->nick(), what, "invalid characters");
					break;
				}
			} else if (sptr->has_flag(CLI_REMOTE)) {
				if (what.at(0) != CHTYPE_LOCAL) {
					auto chrec = channel::create(what);
					assert(chrec.has_value());
					channel::ins_user(chrec->name, cptr, sptr);
				}
			} else if ((jupe_code = get_jupe_status(JUPE_CHANNEL, what)) != -1) {
				if (jupe_code == JUPE_STATUS_CHANDELAY) {
					sptr->reply_numeric(msg->tag("label"), ERR_UNAVAILRESOURCE, sptr->nick(), what);
				} else {
					sptr->reply_numeric(msg->tag("label"), ERR_BADCHANNAME, sptr->nick(), what);
				}
			} else if (num_chans >= info.limits[MAX_CHANNELS] && !sptr->has_umode(UMODE_SERVICE)) {
				sptr->reply_numeric(msg->tag("label"), ERR_TOOMANYCHANNELS, sptr->nick(), what);
			} else {
				auto chrec = channel::create(what);
				assert(chrec.has_value());
				std::string key_param = (nkey < key_params.size() ? key_params[nkey++] : "");
				if (channel::add_user(chrec->name, cptr, sptr, key_param)) {
					num_chans++;
					// if a service joins a channel, always give it op status
					if (channel::count(chrec->name) > 1 && sptr->has_umode(UMODE_SERVICE) && chrec->type != CHTYPE_MODELESS) {
						channel::set_modes(chrec->name, cptr, sptr, "+o", { sptr->nick() });
					}
					// send topic & names
					channel::send_topic(chrec->name, sptr);
					channel::send_names(chrec->name, sptr);
				}
			}
		}
	}
}

void ircd::cmd_kick(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->send_numeric(ERR_NOTREGISTERED, sptr->nick());
	} else if (msg->args.size() < 2) {
		sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
	} else {
		std::string reason;
		if (msg->args.size() >= 3) {
			reason = msg->args[2];
		}
		if (reason.length() > info.limits[MAX_REASON]) {
			reason = reason.substr(0, info.limits[MAX_REASON] - 1);
		}
		auto chrec = channel::find(msg->args[0]);
		auto target = find_user(msg->args[1]);
		if (!chrec) {
			sptr->reply_numeric(msg->tag("label"), ERR_NOSUCHCHANNEL, sptr->nick(), msg->args[0]);
		} else if (!target) {
			sptr->reply_numeric(msg->tag("label"), ERR_NOSUCHNICK, sptr->nick(), msg->args[1]);
		} else {
			channel::kick(chrec->name, cptr, sptr, target, reason);
		}
	}
}

void ircd::cmd_kill(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	client_ptr target;
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->send_numeric(ERR_NOTREGISTERED, sptr->nick());
	} else if ((sptr->has_flag(CLI_REMOTE) || sptr->has_flag(CLI_SERVER)) && msg->args.size() >= 1) {
		if ((target = find_user(msg->args[0])) && !target->has_flag(CLI_REMOTE)) {
			if (msg->args.size() == 2) {
				notify_snomask(SNO_OPER_STATUS, "*** Notice -- Received KILL message for %1%. From %2% (%3%)",
					target->userhost(), sptr->userhost(), msg->args[1]);
				kill_client(target, msg->args[1]);
			} else {
				notify_snomask(SNO_OPER_STATUS, "*** Notice -- Received KILL message for %1%. From %2% ()",
					target->userhost(), sptr->userhost());
				kill_client(target, "Connection closed");
			}
		}
	} else if (msg->args.size() < 2) {
		sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
	} else if (!sptr->has_umode(UMODE_OPERATOR)) {
		sptr->reply_numeric(msg->tag("label"), ERR_NOTOPER, sptr->nick());
	} else if (!(target = find_user(msg->args[0]))) {
		sptr->reply_numeric(msg->tag("label"), ERR_NOSUCHNICK, sptr->nick(), msg->args[0]);
	} else if (target->has_umode(UMODE_SERVICE)) {
		sptr->reply_numeric(msg->tag("label"), ERR_ISSERVICE, sptr->nick(), target->nick());
	} else {
		notify_snomask(SNO_OPER_STATUS, "*** Notice -- Received KILL message for %1%. From %2% (%3%)",
			target->userhost(), sptr->userhost(), msg->args[1]);
		kill_client(target, format_str("Killed (%1% (%2%))", sptr->nick(), msg->args[1]));
	}
}

void ircd::cmd_list(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->send_numeric(ERR_NOTREGISTERED, sptr->nick());
	} else if (!sptr->has_flag(CLI_REMOTE) && msg->args.size() >= 2) {
		auto serv_ptr = find_server_name(msg->args[1]);
		if (!serv_ptr) {
			sptr->reply_numeric(msg->tag("label"), ERR_NOSUCHSERVER, sptr->nick(), msg->args[1]);
		} else {
			serv_ptr->write_msg(sptr, MSG_TRAILING_ARG, "LIST", msg->args[0]);
		}
	} else {
		/* prepare the SQL text based on user input */
		std::stringstream sql_text;
		sql_text << "SELECT channels.Name, UserCount, channels.Topic, channels.Modes"
			" FROM channels JOIN (SELECT ChannelName, COUNT(*) AS UserCount FROM [channel-users] GROUP BY ChannelName) AS U ON U.ChannelName=channels.Name";
		if (msg->args.size() == 1) {
			std::vector<std::string> targets;
			split_text(targets, msg->args[0], ",", std::nullopt);
			if (targets.size() > info.limits[MAX_TARGETS]) {
				sptr->reply_numeric(msg->tag("label"), ERR_TOOMANYTARGETS, sptr->nick(), "Too many targets for LIST");
				return;
			}
			size_t i = 0;
			sql_text << " WHERE ";
			for (std::string_view target : targets) {
				if (++i > 1) sql_text << " OR ";
				sql_text << "(channels.Name GLOB " << sqlite3xx::escape_text('Q', target) << ")";
			}
		}
		sql_text << " ORDER BY channels.Name";
		/* run query and display results */
		labeled_response_batch batch(msg, sptr);
		for (auto data = db.exec_query(sql_text.str()); !data.eof(); data.step()) {
			auto chname = data("Name").get_text();
			// don't show secret (+s) channels to regular users
			if (!channel::has_mode(chname, CMODE_SECRET) || sptr->has_umode(UMODE_OPERATOR)) {
				sptr->batch_numeric(batch.id, RPL_LIST, sptr->nick(), data.field_value("Name"), data.field_value("UserCount"), data.field_value("Topic"));
			}
		}
		sptr->batch_numeric(batch.id, RPL_LISTEND, sptr->nick());
	}
}

void ircd::cmd_lusers(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (sptr->has_flag(CLI_REGISTERED) && !sptr->has_flag(CLI_REMOTE)) {
		labeled_response_batch batch(msg, sptr);
		int ucount = db.exec_scalar("SELECT COUNT(*) FROM [users]").get_int(0);
		int icount = db.exec_scalar("SELECT COUNT(*) FROM [users] WHERE ([Mode] & ?1) = ?1", UMODE_INVISIBLE).get_int(0);
		int scount = db.exec_scalar("SELECT COUNT(*) FROM [servers]").get_int(0);
		int ocount = db.exec_scalar("SELECT COUNT(*) FROM [users] WHERE ([Mode] & ?1) = ?1", UMODE_OPERATOR).get_int(0);
		int zcount = db.exec_scalar("SELECT COUNT(*) FROM [unknown]").get_int(0);
		int ccount = db.exec_scalar("SELECT COUNT(*) FROM [channels]").get_int(0);

		sptr->batch_numeric(batch.id, RPL_LUSERCLIENT, sptr->nick(), ucount, icount, 1 + scount);
		sptr->batch_numeric(batch.id, RPL_LUSEROP, sptr->nick(), ocount);
		sptr->batch_numeric(batch.id, RPL_LUSERUNKNOWN, sptr->nick(), zcount);
		sptr->batch_numeric(batch.id, RPL_LUSERCHANNELS, sptr->nick(), ccount);

		ucount = db.exec_scalar("SELECT COUNT(*) FROM [users] WHERE [SID]=?", info.id).get_int(0);
		sptr->batch_numeric(batch.id, RPL_LUSERME, sptr->nick(), ucount, scount);
	} else {
		sptr->reply_numeric(msg->tag("label"), ERR_NOTREGISTERED, sptr->nick());
	}
}

void ircd::cmd_mode(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->send_numeric(ERR_NOTREGISTERED, sptr->nick());
	} else if (msg->args.size() < 1) {
		sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
	} else if (valid_nick(msg->args[0]) == NAME_ERROR_NONE) {
		if (!same_text(sptr->nick(), msg->args[0])) {
			sptr->reply_numeric(msg->tag("label"), ERR_USERSDONTMATCH, sptr->nick());
		} else if (msg->args.size() == 1) {
			sptr->reply_numeric(msg->tag("label"), RPL_UMODE, sptr->nick(), umode_str(sptr->get_umodes()));
		} else if (msg->args.size() >= 2 && strscan(msg->args[1], R"(^(\+|\-)[a-zA-Z\+\-]{1,255}$)")) {
			sptr->change_umodes(msg->args[1], !cptr->has_flag(CLI_SERVER));
		}
	} else if (valid_chan(msg->args[0]) == NAME_ERROR_NONE) {
		auto param = msg->args[0];
		auto chrec = channel::find(param);
		if (sptr->has_flag(CLI_REMOTE)) {
			if (chrec && msg->args.size() >= 2) {
				std::vector<std::string> params;
				if (msg->args.size() >= 3) {
					std::copy(msg->args.begin() + 2, msg->args.end(), std::back_inserter(params));
				}
				channel::set_modes(chrec->name, cptr, sptr, msg->args[1], params);
			}
		} else {
			if (!chrec) {
				sptr->reply_numeric(msg->tag("label"), ERR_NOSUCHCHANNEL, sptr->nick(), param);
			} else if (!channel::has_user(chrec->name, sptr) && channel::has_mode(chrec->name, CMODE_NO_EXTERNAL)) {
				sptr->reply_numeric(msg->tag("label"), ERR_NOTONCHANNEL, sptr->nick(), param);
			} else if (chrec->type == CHTYPE_MODELESS) {
				sptr->reply_numeric(msg->tag("label"), ERR_NOCHANMODES, sptr->nick(), chrec->name);
			} else if (msg->args.size() == 1) {
				sptr->reply_numeric(msg->tag("label"), RPL_CHANNELMODEIS, sptr->nick(), chrec->name, channel::get_mode_str(chrec->name));
			} else if (!channel::is_op(chrec->name, sptr)) {
				sptr->reply_numeric(msg->tag("label"), ERR_CHANOPRIVSNEEDED, sptr->nick(), param);
			} else if (msg->args.size() >= 2) {
				std::vector<std::string> params;
				if (msg->args.size() >= 3) {
					std::copy(msg->args.begin() + 2, msg->args.end(), std::back_inserter(params));
				}
				channel::set_modes(chrec->name, cptr, sptr, msg->args[1], params);
			}
		}
	}
}

void ircd::cmd_motd(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->send_numeric(ERR_NOTREGISTERED, sptr->nick());
	} else if (msg && msg->args.size() == 1 && !sptr->has_flag(CLI_REMOTE)) {
		auto serv_ptr = find_server_name(msg->args[0]);
		if (!serv_ptr) {
			sptr->reply_numeric(msg->tag("label"), ERR_NOSUCHSERVER, sptr->nick(), msg->args[0]);
		} else {
			serv_ptr->write_msg(sptr, MSG_TRAILING_ARG, "MOTD", msg->args[0]);
		}
	} else if (!msg || msg->args.size() < 1 || same_text(msg->args[0], info.fullname)) {
		if (info.motd_user_text.empty()) {
			sptr->reply_numeric(msg->tag("label"), ERR_NOMOTD, sptr->nick());
		} else {
			labeled_response_batch batch(msg, sptr);
			sptr->batch_numeric(batch.id, RPL_MOTDSTART, sptr->nick());
			std::vector<std::string> lines;
			split_text(lines, info.motd_user_text, "\r\n", boost::empty_token_policy::keep_empty_tokens);
			for (auto const& line : lines) {
				sptr->batch_numeric(batch.id, RPL_MOTD, sptr->nick(), line);
			}
			sptr->batch_numeric(batch.id, RPL_ENDOFMOTD, sptr->nick());
		}
	}
}

void ircd::cmd_names(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->send_numeric(ERR_NOTREGISTERED, sptr->nick());
	} else if (msg->args.size() < 1) {
		sptr->reply_numeric(msg->tag("label"), RPL_ENDOFNAMES, sptr->nick(), "*");
	} else if (!sptr->has_flag(CLI_REMOTE)) {
		std::vector<std::string> targets;
		split_text(targets, msg->args[0], ",", std::nullopt);
		if (targets.size() > info.limits[MAX_TARGETS]) {
			sptr->reply_numeric(msg->tag("label"), ERR_TOOMANYTARGETS, sptr->nick(), "Too many targets for NAMES");
		} else {
			for (auto param : targets) {
				auto chrec = channel::find(param);
				if (!chrec || !channel::has_user(chrec->name, sptr)) {
					sptr->reply_numeric(msg->tag("label"), RPL_ENDOFNAMES, sptr->nick(), param);
				} else {
					channel::send_names(chrec->name, sptr);
				}
			}
		}
	}
}

void ircd::cmd_nick(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	int jupe_code = -1;
	if (msg->args.size() == 0) {
		sptr->reply_numeric(msg->tag("label"), ERR_NONICKNAMEGIVEN, sptr->nick(), "No nickname given");
	} else if (cptr->has_flag(CLI_SERVER)) {
		sptr->set_nick(msg->args[0]);
		if (msg->args.size() >= 2) {
			sptr->set_timestamp(boost::lexical_cast<uint64_t>(msg->args[1]));
		} else {
			sptr->set_timestamp(ircd::timestamp());
		}
		if (!sptr->has_flag(CLI_REMOTE) && msg->args.size() == 3) {
			sptr->send_notice("Your nick was changed due to: %1%", msg->args[2]);
		}
		if (valid_nick(sptr->nick()) != NAME_ERROR_NONE && !sptr->has_flag(CLI_REMOTE)) {
			sptr->set_flag(CLI_REGISTERED, false);
			sptr->send_notice("Your nickname is invalid. Change to a valid nickname within 60 seconds to avoid a forced quit.");
		}
	} else if (auto rc = valid_nick(msg->args[0]); rc != NAME_ERROR_NONE) {
		switch (rc) {
		case NAME_ERROR_LENGTH:
			sptr->reply_numeric(msg->tag("label"), ERR_BADNICK, sptr->nick(), msg->args[0], "name is too long");
			break;
		case NAME_ERROR_CHARS:
			sptr->reply_numeric(msg->tag("label"), ERR_BADNICK, sptr->nick(), msg->args[0], "invalid characters");
			break;
		}
	} else if ((jupe_code = get_jupe_status(JUPE_NICK, msg->args[0])) != -1) {
		if (jupe_code == JUPE_STATUS_NICKDELAY) {
			sptr->reply_numeric(msg->tag("label"), ERR_UNAVAILRESOURCE, sptr->nick(), msg->args[0]);
		} else {
			sptr->reply_numeric(msg->tag("label"), ERR_BADNICK, sptr->nick(), msg->args[0], "not allowed");
		}
	} else if (find_nick(msg->args[0])) {
		sptr->reply_numeric(msg->tag("label"), ERR_NICKNAMEINUSE, sptr->nick(), msg->args[0]);
	} else {
		sptr->set_nick(msg->args[0]);
		if (!sptr->has_flag(CLI_REGISTERED)) {
			sptr->do_register();
		} else if (!sptr->has_flag(CLI_REMOTE)) {
			notify_servers(cptr, sptr, 0, "NICK", sptr->nick(), std::to_string(sptr->timestamp()));
		}
	}
}

void ircd::cmd_notice(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (sptr->has_flag(CLI_REGISTERED)) {
		if (msg->args.size() < 2) {
			sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
		} else {
			std::vector<std::string> targets;
			split_text(targets, msg->args[0], ",", std::nullopt);
			if (targets.size() > info.limits[MAX_TARGETS]) {
				sptr->reply_numeric(msg->tag("label"), ERR_TOOMANYTARGETS, sptr->nick(), "Too many targets for NOTICE");
			} else {
				for (auto dest : targets) {
					do_message(cptr, sptr, dest, *msg, msg->args[1]);
				}
			}
		}
	}
}

void ircd::cmd_oper(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->reply_numeric(msg->tag("label"), ERR_NOTREGISTERED, sptr->nick());
	} else if (sptr->has_flag(CLI_REMOTE)) {
		sptr->reply_numeric(msg->tag("label"), ERR_UNKNOWNCOMMAND, sptr->nick());
	} else if (msg->args.size() < 2) {
		sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
	} else if (accept_oper(sptr, normalize_nfkc(msg->args[0]), msg->args[1])) {
		if (!info.motd_oper_text.empty()) {
			labeled_response_batch batch(msg, sptr);
			sptr->batch_numeric(batch.id, RPL_OMOTDSTART, sptr->nick());
			std::vector<std::string> lines;
			split_text(lines, info.motd_oper_text, "\r\n", boost::empty_token_policy::keep_empty_tokens);
			for (auto const& line : lines) {
				sptr->batch_numeric(batch.id, RPL_OMOTD, sptr->nick(), line);
			}
			sptr->batch_numeric(batch.id, RPL_ENDOFOMOTD, sptr->nick());
		}
	}
}

bool ircd::accept_oper(client_ptr sptr, std::string_view name, std::string_view passwd)
{
	if (!sptr->has_umode(UMODE_OPERATOR)) {
		// check for SSL requirement
		auto ssl_required = db.exec_scalar("SELECT [ssl_required] FROM [operators] WHERE [name]=?1", name).get_int(0);
		if (ssl_required != 0) {
			if (!sptr->has_flag(CLI_SSL)) {
				sptr->send_numeric(ERR_NOPRIVS, sptr->nick());
				snomask_log(LOG_OPER, SNO_OPER_STATUS, "OPER failed from %1% (secure connection required)", sptr->userhost());
				return false;
			}
			// if requested, verify cert fingerprints
			auto cert_fp_count = db.exec_scalar("SELECT COUNT(*) FROM [oper-certs] WHERE [name]=?1", name).get_int(0);
			int cert_fp_match = 0;
			if (cert_fp_count > 0 && sptr->has_flag(CLI_SSL) && sptr->has_metadata("ssl_fingerprint")) {
				cert_fp_match = db.exec_scalar("SELECT COUNT(*) FROM [oper-certs] WHERE [name]=?1 AND [fingerprint]=?2", name, sptr->get_metadata("ssl_fingerprint")).get_int(0);
				if (cert_fp_match == 0) {
					sptr->send_numeric(ERR_NOOPERHOST, sptr->nick());
					snomask_log(LOG_OPER, SNO_OPER_STATUS, "OPER failed from %1% (certificate mismatch)", sptr->userhost());
					return false;
				}
			}
		}
		// search for oper record by the full userhost and IP address
		auto host_count = db.exec_scalar("SELECT COUNT(*) FROM [oper-hosts] WHERE [name]=?1", name).get_int(0);
		if (host_count > 0) {
			auto host_match = db.exec_scalar("SELECT COUNT(*) FROM [oper-hosts] WHERE [name]=?1 AND ((?2 GLOB [hostmask]) OR (('*@' || ?3) GLOB [hostmask]))", name, sptr->userhost(), sptr->address()).get_int(0);
			if (host_match < 1) {
				sptr->send_numeric(ERR_NOOPERHOST, sptr->nick());
				snomask_log(LOG_OPER, SNO_OPER_STATUS, "OPER failed from %1% (host mismatch)", sptr->userhost());
				return false;
			}
		}
		// now match the password
		auto pw_match = db.exec_scalar("SELECT COUNT(*) FROM [operators] WHERE [name]=? AND [password]=sha256(?)", name, passwd).get_int(0);
		if (pw_match == 0) {
			sptr->send_numeric(ERR_NOPRIVS, sptr->nick());
			snomask_log(LOG_OPER, SNO_OPER_STATUS, "OPER failed from %1% (invalid name/password)", sptr->userhost());
			return false;
		}
		// all the checks have passed
		sptr->set_metadata("oper_name", name);
		sptr->change_umodes("+ow", false);
		snomask_log(LOG_OPER, SNO_OPER_STATUS, "%1% is now operator (O)", sptr->userhost());
	}
	sptr->send_numeric(RPL_OPER, sptr->nick());
	return true;
}

void ircd::cmd_part(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->reply_numeric(msg->tag("label"), ERR_NOTREGISTERED, sptr->nick());
	} else if (msg->args.size() < 1) {
		sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
	} else {
		std::vector<std::string> names;
		split_text(names, msg->args[0], ",", std::nullopt);
		for (std::string_view param : names) {
			if (valid_chan(param) == NAME_ERROR_NONE) {
				auto chrec = channel::find(param);
				if (!chrec || !channel::has_user(chrec->name, sptr)) {
					sptr->reply_numeric(msg->tag("label"), ERR_NOSUCHCHANNEL, sptr->nick(), param);
				} else {
					channel::notify_members(chrec->name, cptr, sptr, 0, "PART", chrec->name);
					channel::del_user(chrec->name, sptr);
					notify_servers(cptr, sptr, 0, "PART", chrec->name);
				}
			} else {
				sptr->reply_numeric(msg->tag("label"), ERR_NOSUCHCHANNEL, sptr->nick(), param);
			}
		}
	}
}

void ircd::cmd_pass(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (sptr->has_flag(CLI_REGISTERED)) {
		sptr->reply_numeric(msg->tag("label"), ERR_REGISTERED, sptr->nick());
	} else if (msg->args.size() == 0) {
		sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
	} else {
		sptr->set_metadata("recv_pass", msg->args[0]);
	}
}

void ircd::cmd_ping(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (msg->args.size() < 1) {
		sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
	} else if (msg->args.size() >= 2 && !same_text(msg->args[1], info.fullname)) {
		client_ptr target;
		if ((target = find_server_name(msg->args[1]))) {
			target->write_msg(nullptr, MSG_TRAILING_ARG, "PING", sptr->nick(), msg->args[0]);
		} else {
			sptr->reply_numeric(msg->tag("label"), ERR_NOSUCHSERVER, sptr->nick(), msg->args[1]);
		}
	} else if (!sptr->has_flag(CLI_SERVER)) {
		sptr->reply_msg(msg->tag("label"), nullptr, MSG_TRAILING_ARG, "PONG", sptr->nick(), msg->args[0]);
	}
}

void ircd::cmd_pong(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (msg->args.size() < 1) {
		sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
	} else if (sptr->has_flag(CLI_SERVER)) {
		client_ptr target;
		if (msg->args.size() < 2) {
			sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
		} else if (!same_text(msg->args[1], info.fullname) && !(target = find_server_name(msg->args[1]))) {
			sptr->reply_numeric(msg->tag("label"), ERR_NOSUCHSERVER, sptr->nick(), msg->args[1]);
		} else if (target) {
			target->reply_msg(msg->tag("label"), sptr, 0, "PONG", msg->args[0], msg->args[1]);
		}
	}
}

void ircd::cmd_privmsg(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->reply_numeric(msg->tag("label"), ERR_NOTREGISTERED, sptr->nick());
	} else if (msg->args.size() < 2) {
		sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
	} else {
		std::vector<std::string> targets;
		split_text(targets, msg->args[0], ",", std::nullopt);
		if (targets.size() > info.limits[MAX_TARGETS]) {
			sptr->reply_numeric(msg->tag("label"), ERR_TOOMANYTARGETS, sptr->nick(), "Too many targets for PRIVMSG");
		} else {
			for (auto dest : targets) {
				do_message(cptr, sptr, dest, *msg, msg->args[1]);
			}
		}
	}
}

void ircd::cmd_quit(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->reply_numeric(msg->tag("label"), ERR_NOTREGISTERED, sptr->nick());
	} else if (msg->args.empty() || msg->args[0].empty()) {
		kill_client(sptr, "Client Quit");
	} else {
		kill_client(sptr, "Quit: " + msg->args[0]);
	}
}

void ircd::cmd_rehash(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	client_ptr target;
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->reply_numeric(msg->tag("label"), ERR_NOTREGISTERED, sptr->nick());
	} else if (!sptr->has_umode(UMODE_OPERATOR)) {
		sptr->reply_numeric(msg->tag("label"), ERR_NOTOPER, sptr->nick());
	} else {
		reload_config(sptr);
	}
}

void ircd::cmd_service(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	// from RFC 2812:
	//
	// <name> <reserved> <distribution> <type> <reserved> <info>
	// 0      1          2              3      4          5
	//
	// currently ignoring <distribution>
	// and treating <type> as text
	if (sptr->has_flag(CLI_REGISTERED) || sptr->has_flag(CLI_REMOTE)) {
		sptr->reply_numeric(msg->tag("label"), ERR_REGISTERED, sptr->nick());
	} else if (msg->args.size() < 5) {
		sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
	} else if (auto rc = valid_nick(msg->args[0]); rc != NAME_ERROR_NONE) {
		switch (rc) {
		case NAME_ERROR_LENGTH:
			sptr->reply_numeric(msg->tag("label"), ERR_BADNICK, sptr->nick(), msg->args[0], "name is too long");
			break;
		case NAME_ERROR_CHARS:
			sptr->reply_numeric(msg->tag("label"), ERR_BADNICK, sptr->nick(), msg->args[0], "invalid characters");
			break;
		}
	} else if (!accept_service(sptr, msg->args)) {
		kill_client(sptr, "Unauthorized");
	}
}

bool ircd::accept_service(client_ptr sptr, std::vector<std::string> const& args)
{
	std::string const& name = args[0];
	std::string const& type = args[3];
	if (sptr->has_metadata("webirc_address")) {
		snomask_log(LOG_USER, SNO_REJECT_CONN, "Rejecting SERVICE connection from %1% (connected via WEBIRC)", sptr->endpoint_id());
		return false;
	} else if (!sptr->has_metadata("recv_pass")) {
		snomask_log(LOG_USER, SNO_REJECT_CONN, "Rejecting SERVICE connection from %1% (did not receive password)", sptr->endpoint_id());
		return false;
	}
	auto ssl_required = db.exec_scalar("SELECT [ssl_required] FROM [services] WHERE [name]=?", name).get_int(0);
	if (ssl_required != 0) {
		if (!sptr->has_flag(CLI_SSL)) {
			snomask_log(LOG_USER, SNO_REJECT_CONN, "Rejecting SERVICE connection from %1% (SSL required)", sptr->endpoint_id());
			return false;
		}
		auto cert_fp_count = db.exec_scalar("SELECT COUNT(*) FROM [service-certs] WHERE [name]=?", name).get_int(0);
		if (cert_fp_count > 0) {
			auto cert_fp_match = db.exec_scalar("SELECT COUNT(*) FROM [service-certs] WHERE [name]=? AND [fingerprint]=?", name, sptr->get_metadata("ssl_fingerprint")).get_int(0);
			if (cert_fp_match < 1) {
				snomask_log(LOG_USER, SNO_REJECT_CONN, "Rejecting SERVICE connection from %1% (certificate mismatch)", sptr->endpoint_id());
				return false;
			}
		}
	}
	auto stmt = db.exec_query("SELECT [name], [spoof] FROM [services] WHERE [password]=sha256(?) AND [name]=? AND [type]=?",
		sptr->get_metadata("recv_pass"), name, type);
	if (stmt.eof()) {
		snomask_log(LOG_USER, SNO_REJECT_CONN, "Rejecting SERVICE connection from %1% (name/password mismatch)", sptr->endpoint_id());
		return false;
	}
	sptr->set_nick(args[0]);
	sptr->set_username(args[0]);
	sptr->set_gecos(args[5]);
	sptr->set_hostname(stmt("spoof").get_text());
	sptr->set_flag(CLI_SERVICE, true);
	sptr->set_metadata("service", type);
	sptr->do_register();
	return true;
}

void ircd::cmd_snomask(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	static mask_info const info_table[] = {
		{ SNO_DEBUG, "d", "Debug messages (oper only)" },
		{ SNO_LOCAL_USER_CONN, "u", "Local connection notices (oper only)" },
		{ SNO_REMOTE_USER_CONN, "U", "Remote connection notices (oper only)" },
		{ SNO_OPER_STATUS, "o", "Notices from /OPER command attempts" },
		{ SNO_SERVICE_CONN, "S", "Service connection notices" },
		{ SNO_SERVER_LINK, "l", "Server linking notices" },
		{ SNO_REJECT_CONN, "r", "Rejected connection notices (oper only)" },
		{ SNO_STATS, "s", "Notices from /STATS command (oper only)" },
		{ SNO_KILLS, "k", "KILL and K-line notifications" },
		{ SNO_NICK_JUPE, "n", "Nickname JUPE notifications" },
		{ SNO_CHAN_JUPE, "c", "Channel JUPE notifications" },
		{ 0, nullptr }
	};
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->reply_numeric(msg->tag("label"), ERR_NOTREGISTERED, sptr->nick());
	} else if (sptr->has_flag(CLI_REMOTE)) {
		return; // just quietly ignore
	} else if (msg->args.size() == 0) {
		sptr->reply_numeric(msg->tag("label"), RPL_SNOMASK, sptr->nick(), sptr->get_snomask(1), sptr->raw_snomask());
	} else if (same_text(msg->args[0], "HELP")) {
		sptr->send_numeric(RPL_INFOSTART, sptr->nick(), "SNOMASK information & usage");
		sptr->send_numeric(RPL_INFO, sptr->nick(), "The SNOMASK value controls the type of Server Notifications you can receive.");
		if (sptr->raw_snomask() != 0) {
			sptr->send_numeric(RPL_INFO, sptr->nick(), format_str("Your current SNOMASK is +%1%", sptr->get_snomask(1)));
		}
		sptr->send_numeric(RPL_INFO, sptr->nick(), "The /SNOMASK LIST command will display all currently available SNOMASKs.");
		sptr->send_numeric(RPL_INFO, sptr->nick(), "To update your SNOMASK use: /SNOMASK SET [+|-][...]");
		sptr->send_numeric(RPL_ENDOFINFO, sptr->nick(), "End of /SNOMASK HELP");
	} else if (same_text(msg->args[0], "LIST")) {
		size_t pos = 0;
		sptr->send_numeric(RPL_INFOSTART, sptr->nick(), "SNOMASK currently available notifications");
		for (mask_info const* iptr = info_table; iptr->tag; iptr++) {
			sptr->send_numeric(RPL_INFO, sptr->nick(), format_str("+%1% %2%", iptr->tag, iptr->description));
		}
		sptr->send_numeric(RPL_ENDOFINFO, sptr->nick(), "End of /SNOMASK LIST");
	} else if (same_text(msg->args[0], "SET") && msg->args.size() == 2) {
		sptr->change_snomask(msg->args[1]);
		sptr->change_umodes("+s", 0);
		sptr->reply_numeric(msg->tag("label"), RPL_SNOMASK, sptr->nick(), sptr->get_snomask(1), sptr->raw_snomask());
	}
}

void ircd::cmd_squery(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	client_ptr target;
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->reply_numeric(msg->tag("label"), ERR_NOTREGISTERED, sptr->nick());
	} else if (msg->args.size() < 2) {
		sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
	} else if (!(target = find_user(msg->args[0])) || !target->has_umode(UMODE_SERVICE)) {
		sptr->reply_numeric(msg->tag("label"), ERR_NOSUCHSERVICE, sptr->nick(), msg->args[0]);
	} else {
		target->write_msg(sptr, MSG_TRAILING_ARG, "SQUERY", target->nick(), msg->args[1]);
	}
}

void ircd::cmd_stats(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->reply_numeric(msg->tag("label"), ERR_NOTREGISTERED, sptr->nick());
	} else if (msg->args.size() == 0) {
		sptr->reply_msg(msg->tag("label"), nullptr, MSG_TRAILING_ARG, "NOTICE", sptr->userhost(), "Missing parameter for STATS command");
	} else if (compare_nfkc(msg->args[0], "l") == 0) {
		// link statistics
		db_stats_reply("SELECT [Nick], [SendCount], [SendBytes], [RecvCount], [RecvBytes], [TimeOpen] FROM [link-stats]",
			RPL_STATSLINKINFO, RPL_ENDOFSTATS, sptr, msg);
	} else if (compare_nfkc(msg->args[0], "m") == 0) {
		// command usage statistics
		db_stats_reply("SELECT [Command], [UseCount], [ByteCount], [RemoteCount] FROM [command-stats] ORDER BY [Command]",
			RPL_STATSCOMMANDS, RPL_ENDOFSTATS, sptr, msg);
	} else if (compare_nfkc(msg->args[0], "o") == 0) {
		// allowed oper hosts
		db_stats_reply("SELECT [hostmask], [name] FROM [oper-hosts]",
			RPL_STATSOLINE, RPL_ENDOFSTATS, sptr, msg);
	} else if (compare_nfkc(msg->args[0], "u") == 0) {
		uint64_t tval = ircd::timestamp() - info.create_time;
		uint64_t days = 0, hours = 0, mins = 0, secs = 0;

		days = tval / 86400;
		tval %= 86400;
		hours = tval / 3600;
		tval %= 3600;
		mins = tval / 60;
		tval %= 60;
		secs = tval;

		sptr->reply_numeric(msg->tag("label"), RPL_STATSUPTIME, sptr->nick(), days, hours, mins, secs);
	} else if (compare_nfkc(msg->args[0], "j") == 0) {
		// JUPE listing
		db_stats_reply("SELECT [name], ifnull([source], '*'), [reason] FROM [jupe] GROUP BY [name]",
			RPL_JUPELIST, RPL_ENDOFJUPELIST, sptr, msg);
	} else if (compare_nfkc(msg->args[0], "k") == 0) {
		// server bans
		db_stats_reply("SELECT [mask], ifnull([created], 0), ifnull([source], '*'), [comment] FROM [server-bans] ORDER BY [mask]",
			RPL_STATSKLINE, RPL_ENDOFSTATS, sptr, msg);
	} else if (compare_nfkc(msg->args[0], "w") == 0) {
		// WEBIRC gateways
		db_stats_reply("SELECT [host], [description] FROM [webirc-gateways]",
			RPL_STATSWLINE, RPL_ENDOFSTATS, sptr, msg);
	} else {
		sptr->reply_numeric(msg->tag("label"), RPL_ENDOFSTATS, sptr->nick(), msg->args[0]);
		sptr->write_msg(nullptr, MSG_TRAILING_ARG, "NOTICE", sptr->userhost(), "Invalid parameter for STATS command");
	}
}

void ircd::cmd_summon(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	sptr->reply_msg(msg->tag("label"), nullptr, MSG_TRAILING_ARG, "445", sptr->nick(), "SUMMON has been disabled");
}

void ircd::cmd_tagmsg(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->reply_numeric(msg->tag("label"), ERR_NOTREGISTERED, sptr->nick());
	} else if (!sptr->has_cap(CAP_MESSAGE_TAGS)) {
		sptr->send_numeric(ERR_UNKNOWNCOMMAND, sptr->nick(), msg->command);
	} else if (msg->args.size() < 1) {
		sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
	} else {
		std::vector<std::string> targets;
		split_text(targets, msg->args[0], ",", std::nullopt);
		if (targets.size() > info.limits[MAX_TARGETS]) {
			sptr->reply_numeric(msg->tag("label"), ERR_TOOMANYTARGETS, sptr->nick(), "Too many targets for TAGMSG");
		} else {
			for (auto dest : targets) {
				do_message(cptr, sptr, dest, *msg, "");
			}
		}
	}
}

void ircd::db_stats_reply(std::string_view query, int rpl_line, int rpl_end, client_ptr sptr, std::optional<message> msg)
{
	using namespace boost::io;
	labeled_response_batch batch(msg, sptr);
	for (auto stmt = db.exec_query(query); !stmt.eof(); stmt.step()) {
		auto fmt = rpl_str(rpl_line);
		fmt.exceptions(all_error_bits ^ (too_many_args_bit | too_few_args_bit));
		fmt% sptr->nick();
		for (int i = 0; i < stmt.field_count(); i++) fmt% stmt.field_value(i);
		sptr->batch_msg(batch.id, nullptr, 0, format_str("%03u", rpl_line), fmt.str());
	}
	sptr->batch_numeric(batch.id, rpl_end, sptr->nick(), msg->args[0]);
}

void ircd::cmd_time(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->reply_numeric(msg->tag("label"), ERR_NOTREGISTERED, sptr->nick());
	} else {
		auto ts = ircd::timestamp();
		boost::posix_time::ptime pt(boost::posix_time::microsec_clock::universal_time());
		auto timestr = boost::posix_time::to_iso_extended_string(pt);
		sptr->reply_numeric(msg->tag("label"), RPL_TIME, sptr->nick(), info.fullname, ts, 0, timestr);
	}
}

void ircd::cmd_topic(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->reply_numeric(msg->tag("label"), ERR_NOTREGISTERED, sptr->nick());
	} else if (msg->args.size() < 1) {
		sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
	} else {
		auto param = msg->args[0];
		auto chrec = channel::find(param);
		if (!chrec || !channel::has_user(chrec->name, sptr)) {
			sptr->reply_numeric(msg->tag("label"), ERR_NOSUCHCHANNEL, sptr->nick(), param);
		} else if (msg->args.size() >= 2 && !channel::is_op(chrec->name, sptr) && channel::has_mode(chrec->name, CMODE_TOPIC_LOCKED) && !sptr->has_flag(CLI_REMOTE)) {
			sptr->reply_numeric(msg->tag("label"), ERR_CHANOPRIVSNEEDED, sptr->nick(), param);
		} else if (msg->args.size() < 2 && !sptr->has_flag(CLI_REMOTE)) {
			channel::send_topic(chrec->name, sptr);
		} else {
			auto text = msg->args[1].substr(0, info.limits[MAX_REASON]);
			channel::set_topic(chrec->name, text, sptr);
			for (auto member : channel::get_members(chrec->name)) {
				if (member && !member->has_flag(CLI_REMOTE)) member->write_msg(sptr, MSG_TRAILING_ARG, "TOPIC", chrec->name, text);
			}
			notify_servers(cptr, sptr, MSG_TRAILING_ARG, "TOPIC", chrec->name, text);
		}
	}
}

void ircd::cmd_user(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (sptr->has_flag(CLI_REGISTERED)) {
		sptr->reply_numeric(msg->tag("label"), ERR_REGISTERED, sptr->nick());
	} else if (msg->args.size() < 4) {
		sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
	} else if (msg->args[0].size() > info.limits[MAX_NICKLEN]) {
		kill_client(sptr, "Username is too long");
	} else if (msg->args[3].size() > info.limits[MAX_NAMELEN]) {
		kill_client(sptr, "Realname (gecos) is too long");
	} else if (!sptr->has_flag(CLI_SERVER)) {
		auto uname = normalize_nfkc(msg->args[0]);
		sptr->set_username("~" + uname);
		sptr->set_gecos(msg->args[3]);
		auto auth = find_auth(sptr);
		if (!auth) {
			kill_client(sptr, "Unauthorized");
			return;
		}
		if (0 == auth->field_value("check_ident").get_int(1)) {
			sptr->set_flag(CLI_NO_IDENT, true);
			sptr->set_flag(CLI_DONE_IDENT, true);
		} else {
			sptr->do_ident_resolve();
		}
		sptr->do_register(); // NICK/USER may come in any order
	}
}

void ircd::cmd_users(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	sptr->reply_msg(msg->tag("label"), nullptr, MSG_TRAILING_ARG, "446", sptr->nick(), "USERS has been disabled");
}

void ircd::cmd_version(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->reply_numeric(msg->tag("label"), ERR_NOTREGISTERED, sptr->nick());
	} else if (msg->args.size() == 1 && !sptr->has_flag(CLI_REMOTE)) {
		auto serv_ptr = find_server_name(msg->args[0]);
		if (!serv_ptr) {
			sptr->reply_numeric(msg->tag("label"), ERR_NOSUCHSERVER, sptr->nick(), msg->args[0]);
		} else {
			serv_ptr->reply_msg(msg->tag("label"), sptr, MSG_TRAILING_ARG, "VERSION", msg->args[0]);
		}
	} else if (msg->args.size() < 1 || same_text(msg->args[0], info.fullname)) {
		send_isupport(sptr);
		std::stringstream ss;
		ss << id_arch() << " " << id_comp() << " " << id_os();
		sptr->reply_numeric(msg->tag("label"), RPL_VERSION, sptr->nick(), ircd::version_str(), info.fullname, ss.str());
	}
}

void ircd::cmd_wallops(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED) && !sptr->has_flag(CLI_SERVER)) {
		sptr->reply_numeric(msg->tag("label"), ERR_NOTREGISTERED, sptr->nick());
	} else if (msg->args.size() < 1) {
		sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
	} else if (!sptr->has_umode(UMODE_OPERATOR) && !sptr->has_flag(CLI_SERVER) && !sptr->has_flag(CLI_REMOTE)) {
		sptr->reply_numeric(msg->tag("label"), ERR_NOTOPER, sptr->nick());
	} else if (msg->args[0].size() != 0) {
		for (auto query = get_local_users(); !query.eof(); query.step()) {
			auto uptr = get_client(query(0).get_uuid());
			if (uptr && uptr->has_umode(UMODE_WALLOPS)) {
				uptr->write_msg(sptr, MSG_TRAILING_ARG, "WALLOPS", msg->args[0]);
			}
		}
		notify_servers(cptr, sptr, MSG_TRAILING_ARG, "WALLOPS", msg->args[0]);
	}
}

void ircd::cmd_who(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->reply_numeric(msg->tag("label"), ERR_NOTREGISTERED, sptr->nick());
	} else if (msg->args.size() < 1) {
		sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
	} else if (msg->args.size() == 1 && !sptr->has_flag(CLI_REMOTE)) {
		// classic "WHO" syntax
		// emulate a WHOX query for the client
		std::vector<std::string> targets;
		split_text(targets, msg->args[0], ",", std::nullopt);
		for (std::string const& mask : targets) {
			std::vector<std::string> masks;
			if (strscan(mask, R"(^([^ !@]+)\!([^ !@]+)\@(\S+)$)", masks)) {
				// match nick!user@host
				masks.erase(masks.begin()); // remove first 'match'
				who_query(RPL_WHO, cptr, sptr, masks, "nuh%cuhsnfr");
			} else if (strscan(mask, R"(^([^ !@]+)\@(\S+)$)", masks)) {
				// match user@host
				masks.erase(masks.begin()); // remove first 'match'
				who_query(RPL_WHO, cptr, sptr, masks, "uh%cuhsnfr");
			} else {
				// match name only
				masks.push_back(mask);
				if (strscan(mask, R"([#&+]{1}.*)")) {
					who_query(RPL_WHO, cptr, sptr, masks, "c%cuhsnfr");
				} else {
					who_query(RPL_WHO, cptr, sptr, masks, "n%cuhsnfr");
				}
			}
			sptr->send_numeric(RPL_ENDWHO, sptr->nick(), mask);
		}
	} else if (msg->args.size() == 2 && !sptr->has_flag(CLI_REMOTE)) {
		// WHOX query from the client
		std::vector<std::string> masks;
		split_text(masks, msg->args[0], ",", std::nullopt);
		who_query(RPL_WHOX, cptr, sptr, masks, msg->args[1]);
		sptr->send_numeric(RPL_ENDWHO, sptr->nick(), msg->args[0]);
	}
}

void ircd::cmd_whois(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (!sptr->has_flag(CLI_REGISTERED)) {
		sptr->reply_numeric(msg->tag("label"), ERR_NOTREGISTERED, sptr->nick());
	} else if (msg->args.size() < 1) {
		sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
	} else if (msg->args.size() == 2 && !sptr->has_flag(CLI_REMOTE)) {
		client_ptr serv_ptr;
		auto serv = msg->args[0];
		auto nick = msg->args[1];
		if (same_text(nick, serv)) {
			auto res = db.exec_scalar("SELECT [SID] FROM [users] WHERE [Nick]=?", nick).get_text();
			serv = res;
			serv_ptr = find_entity(res);
		} else {
			serv_ptr = find_server_name(serv);
		}
		if (!serv_ptr) {
			if (same_text(serv, info.id)) {
				show_whois(sptr, nick, msg);
			} else {
				sptr->reply_numeric(msg->tag("label"), ERR_NOSUCHSERVER, sptr->nick(), msg->args[0]);
			}
		} else {
			serv_ptr->reply_msg(msg->tag("label"), sptr, 0, "WHOIS", nick);
		}
	} else {
		std::vector<std::string> targets;
		split_text(targets, msg->args[0], ",", std::nullopt);
		if (targets.size() > info.limits[MAX_TARGETS]) {
			sptr->reply_numeric(msg->tag("label"), ERR_TOOMANYTARGETS, sptr->nick(), "Too many targets for WHOIS");
		} else {
			for (std::string_view dest : targets) {
				show_whois(sptr, dest, msg);
			}
		}
	}
}

void ircd::show_whois(client_ptr sptr, std::string_view target_name, std::optional<message> msg)
{
	client_ptr target;
	if (!(target = find_user(target_name)) || !target->has_flag(CLI_REGISTERED)) {
		sptr->reply_numeric(msg->tag("label"), ERR_NOSUCHNICK, sptr->nick(), target_name);
		return;
	}
	labeled_response_batch batch(msg, sptr);
	// basic info
	sptr->batch_numeric(batch.id, RPL_WHOISUSER, sptr->nick(), target->nick(), target->username(), target->hostname(), target->gecos());
	sptr->batch_numeric(batch.id, RPL_WHOISSERVER, sptr->nick(), target->nick(), target->server(), "");
	// show channels
	if (!target->has_umode(UMODE_SERVICE)) {
		sqlite3xx::statement channels{ db };
		if (sptr->has_umode(UMODE_OPERATOR)) {
			// for opers, show all channels
			channels = db.exec_query("SELECT [ChannelName] FROM [channel-users] WHERE [EID]=?", target->eid());
		} else {
			// for non-opers, show only common channels
			// (chanmode +p is now the default behavior)
			channels = db.exec_query("SELECT [ChannelName] FROM [channel-users] WHERE [EID]=? INTERSECT SELECT [ChannelName] FROM [channel-users] WHERE [EID]=?",
				target->eid(), sptr->eid());
		}
		assert(channels.is_valid());
		// send the channel list
		std::stringstream ss;
		for (; !channels.eof(); channels.step()) {
			if (ss.tellp() > 255) {
				sptr->batch_numeric(batch.id, RPL_WHOISCHANNELS, sptr->nick(), target->nick(), ss.str());
				ss.str("");
			}
			if (ss.tellp() > 0) ss << " ";
			ss << channels.field_value(0);
		}
		if (ss.tellp() > 0) sptr->batch_numeric(batch.id, RPL_WHOISCHANNELS, sptr->nick(), target->nick(), ss.str());
	}
	// only send IP address for opers and self-lookups
	if ((sptr->uuid() == target->uuid() || sptr->has_umode(UMODE_OPERATOR)) && !target->has_umode(UMODE_SERVICE)) {
		sptr->batch_numeric(batch.id, RPL_WHOISACTUALLY, sptr->nick(), target->nick(), target->username(), target->hostname(), target->address());
		if (target->has_metadata("webirc_address")) {
			sptr->batch_numeric(batch.id, RPL_WHOISWEBIRC, sptr->nick(), target->nick(), target->get_metadata("webirc_address"), target->get_metadata("webirc_name"));
		}
	}
	// oper notification
	if (target->has_umode(UMODE_OPERATOR) && !target->has_umode(UMODE_SERVICE)) {
		sptr->batch_numeric(batch.id, RPL_WHOISOPERATOR, sptr->nick(), target->nick());
	}
	// service status notification
	if (target->has_umode(UMODE_SERVICE)) {
		sptr->batch_numeric(batch.id, RPL_WHOISSVCMSG, sptr->nick(), target->nick());
	}
	// bot status notification
	if (target->has_umode(UMODE_BOT)) {
		sptr->batch_numeric(batch.id, RPL_WHOISBOT, sptr->nick(), target->nick());
	}
	// helper notification
	if (target->has_umode(UMODE_HELPER)) {
		sptr->batch_numeric(batch.id, RPL_WHOISHELPER, sptr->nick(), target->nick());
	}
	// away notification
	if (target->has_umode(UMODE_AWAY)) {
		sptr->batch_numeric(batch.id, RPL_ISAWAY, sptr->nick(), target->nick(), sptr->get_metadata("away_message"));
	}
	// idle & signon time notification
	if (!target->has_umode(UMODE_SERVICE) && target->has_metadata("last_command")) {
		auto last_cmd = target->get_metadata("last_command").get_uint64(0);
		sptr->batch_numeric(batch.id, RPL_WHOISIDLE, sptr->nick(), target->nick(), ircd::timestamp() - last_cmd,
			format_str("%1%", target->timestamp()));
	}
	// secure connection notification
	if (target->has_flag(CLI_SSL)) {
		sptr->batch_numeric(batch.id, RPL_USINGSSL, sptr->nick(), target->nick());
		// only show cert fingerprints to the same user, IRCops, and services
		if (target->has_metadata("ssl_fingerprint") && ((sptr->uuid() == target->uuid()) || sptr->has_umode(UMODE_OPERATOR) || sptr->has_umode(UMODE_SERVICE))) {
			sptr->batch_numeric(batch.id, RPL_WHOISCERTFP, sptr->nick(), target->nick(), target->get_metadata("ssl_fingerprint"));
		}
	}
	// end of listing
	sptr->batch_numeric(batch.id, RPL_ENDOFWHOIS, sptr->nick(), target->nick());
}

void ircd::cmd_webirc(client_ptr cptr, client_ptr sptr, std::optional<message> msg)
{
	if (sptr->has_flag(CLI_REGISTERED)) {
		sptr->reply_numeric(msg->tag("label"), ERR_REGISTERED, sptr->nick());
	} else if (sptr->has_flag(CLI_REMOTE)) {
		sptr->reply_numeric(msg->tag("label"), ERR_UNKNOWNCOMMAND, sptr->nick());
	} else if (msg->args.size() < 4) {
		sptr->reply_numeric(msg->tag("label"), ERR_NEEDPARAMS, sptr->nick(), msg->command);
	} else if (!accept_webirc(sptr, msg->args)) {
		kill_client(sptr, "Unauthorized");
	}
}

bool ircd::accept_webirc(client_ptr sptr, std::vector<std::string> const& args)
{
	// WEBIRC password gateway hostname ip :options
	//        0        1       2        3  4
	auto num_gateways = db.exec_scalar("SELECT COUNT(*) FROM [webirc-gateways]").get_int(0);
	if (num_gateways == 0) {
		snomask_log(LOG_USER, SNO_REJECT_CONN, "Rejecting WEBIRC connection from %1% (no gateways are configured)", sptr->endpoint_id());
		return false;
	}
	auto match = db.exec_query("SELECT * FROM [webirc-gateways] WHERE [host]=?2 OR cidr_match([host], ?2)", args[1], sptr->address());
	if (match.eof()) {
		snomask_log(LOG_USER, SNO_REJECT_CONN, "Rejecting WEBIRC connection from %1% (host not authorized)", sptr->endpoint_id());
		return false;
	}
	if (compare_nfc(match("password").get_text(), picosha2::hash256_hex_string(args[0])) != 0) {
		snomask_log(LOG_USER, SNO_REJECT_CONN, "Rejecting WEBIRC connection from %1% (password mismatch)", sptr->endpoint_id());
		return false;
	}
	boost::system::error_code ec;
	auto addr = asio::ip::make_address(args[3], ec);
	if (ec) {
		snomask_log(LOG_USER, SNO_REJECT_CONN, "Rejecting WEBIRC connection from %1% (invalid address parameter: %2%)", sptr->endpoint_id(), args[3]);
		return false;
	}
	if (is_addr_kline(addr)) {
		snomask_log(LOG_USER, SNO_REJECT_CONN, "Rejecting WEBIRC connection from %1% (address is banned: %2%)", sptr->endpoint_id(), args[3]);
		return false;
	}
	auto gateway = match("description").get_text();
	sptr->set_metadata("webirc_address", sptr->address());
	sptr->set_metadata("webirc_name", gateway);
	sptr->set_hostname(args[2]);
	sptr->set_address(args[3]);
	if (is_user_kline(sptr)) {
		snomask_log(LOG_USER, SNO_REJECT_CONN, "Rejecting WEBIRC connection from %1% (address is banned: %2%)", sptr->endpoint_id(), sptr->userhost());
		return false;
	}
	snomask_log(LOG_USER, SNO_LOCAL_USER_CONN, "Received WEBIRC connection data from %1% (%2%): %3% (%4%)", sptr->endpoint_id(), gateway, args[3], args[2]);
	return true;
}
