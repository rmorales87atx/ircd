﻿/*
 * ircd - An IRCv3 Server
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"

using boost::system::error_code;
using std::nullopt;

static auto const TIMER_CYCLE = 10; // seconds

namespace ircd {
	/* Global application data - the order of the fields IS important */
	struct app_data_instance {
		/// Command handlers
		std::map<std::string, std::function<void(client_ptr cptr, client_ptr sptr, std::optional<message> msg)>> commands;

		/// Config file source
		std::unique_ptr<conf_reader> config;

		/// Periodic timer (used for auto-connect, nick/chan delay timeouts, etc.)
		std::unique_ptr<asio::deadline_timer> timer1;

		/// Lua modules
		std::vector<module_ptr> modules;

		/// Socket table
		tsl::ordered_map<boost::uuids::uuid, client_ptr, boost::hash<boost::uuids::uuid>> clients;

		/// Socket sessions
		session_manager sessions;

		/// msgid sequence (see next_msg_id())
		uint32_t seqmsg;

		/// Lua debug indicator
		bool lua_debug;
	};

	static std::unique_ptr<app_data_instance> app_data;
	static sqlite3xx::database log;
	boost::asio::io_context io_ctx;
	boost::asio::ssl::context ssl_context{ asio::ssl::context::tls };
	sqlite3xx::database db;
	server_info info;

	/// Initialize SQL data tables
	void init_db();

	/// Update command statistics for /STATS M
	void update_cmd_stats(std::string_view name, size_t msg_nbytes, client_ptr sptr);

	/// Perform auto-connect to servers
	void timer_cycle();

	/// Reset timer to continue cycle
	void timer_reset();

	/// Stop timer
	void timer_stop();
}

void ircd::initialize()
{
	app_data.reset(new app_data_instance);
	app_data->seqmsg = 0;
	app_data->lua_debug = false;
	ssl_context.set_default_verify_paths();
	info.log_enabled = 0;
	init_db();
}

void ircd::load_config(std::string_view source)
{
	if (!app_data) return;
	app_data->config = std::make_unique<conf_reader>(source);
	// default values for limits
	for (limit_info const* iptr = limit_table; iptr->lua_name; iptr++) {
		info.limits[iptr->id] = iptr->default_value;
	}
	// process config
	app_data->config->load(0);
}

void ircd::reload_config(client_ptr sptr)
{
	assert(app_data != nullptr);
	if (sptr) {
		notify_snomask(SNO_OPER_STATUS, "*** Notice -- Reloading server configuration, requested by %1%", sptr->userhost());
	} else {
		notify_snomask(SNO_OPER_STATUS, "*** Notice -- Reloading server configuration");
	}
	app_data->config->load(1);
}

void ircd::add_listen(int type, std::string_view host, int port)
{
	assert(app_data != nullptr);
	app_data->sessions.listen(type, host, port);
}

void ircd::run()
{
	if (!app_data) throw std::runtime_error{ "internal error: bork bork bork" };
	if (!app_data->config) throw std::runtime_error{ "internal error: failed to load a config file???" };
	write_log(LOG_GENERAL, "Starting up");
	// at this point the config should have been loaded
	// we need to test that the environment supports UTF-8 when 'utf8mapping' is not 'ascii'
	if (info.utf8mapping != UTF8MAP_ASCII) {
		static std::string u8_test = u8"RichardⅣ";
		auto result = normalize_nfkc(u8_test);
		// it's been observed that a non-UTF8 locale will cause boost::localize::normal to return empty strings with "icu" backend
		// "posix" and "std" will simply pass the string back as-is since those don't support normalizing
		if (result.empty() || result.compare("RichardIV") != 0) {
			info.utf8mapping = UTF8MAP_ASCII;
			write_log(LOG_GENERAL, "Locale doesn't appear to support UTF-8, reverting to utf8mapping='ascii'");
			std::cerr << "WARNING: the current environment doesn't seem to properly support UTF-8." << std::endl;
			std::cerr << "utf8mapping will now be reset to 'ascii'. Check your terminal configuration to verify the correct UTF-8 locale." << std::endl;
		}
	}
	// continue with server startup
	info.create_time = ircd::timestamp();
	if (!info.kline_db_file.empty()) open_kline_db();
	timer_reset();
	init_commands();
	io_ctx.run();
}

void ircd::shutdown()
{
	if (!app_data) return;
	write_log(LOG_GENERAL, "Shutting down");
	timer_stop();
	// shut down all local sockets first
	// the listener destructor will generate "ERROR" messages for connected clients
	app_data->sessions.close();
	while (io_ctx.run_for(std::chrono::milliseconds(100)) != 0)
		;
	io_ctx.stop();
	// now shut down the rest of the app
	app_data.reset();
	log.exec_dml("PRAGMA wal_checkpoint(TRUNCATE)");
}

void ircd::post(std::function<void()> fn)
{
	asio::post(io_ctx, fn);
}

ircd::module_ptr ircd::load_module(std::string_view source)
{
	assert(app_data != nullptr);
	auto mptr = std::make_shared<module>(source);
	mptr->load();
	app_data->modules.push_back(mptr);
	return mptr;
}

std::vector<ircd::module_ptr>::const_iterator ircd::first_module()
{
	assert(app_data != nullptr);
	return std::begin(app_data->modules);
}

std::vector<ircd::module_ptr>::const_iterator ircd::last_module()
{
	assert(app_data != nullptr);
	return std::end(app_data->modules);
}

void ircd::reload_modules()
{
	assert(app_data != nullptr);
	for (auto& mptr : app_data->modules) {
		mptr->load();
	}
}

uint64_t ircd::next_msg_id()
{
	assert(app_data != nullptr);
	static uint64_t base_time = 0;
	if (base_time == 0) base_time = info.create_time;
	if ((app_data->seqmsg + 1) >= std::numeric_limits<uint32_t>::max()) {
		base_time = app_data->seqmsg;
		app_data->seqmsg = 0;
	}
	app_data->seqmsg++;
	return info.create_time + app_data->seqmsg;
}

size_t ircd::num_sessions()
{
	assert(app_data != nullptr);
	return db.exec_scalar("SELECT COUNT(*) FROM [clients] WHERE [Nick] IS NOT NULL").get_uint(0);
}

size_t ircd::num_connections()
{
	assert(app_data != nullptr);
	return app_data->clients.size();
}

void ircd::add_command(std::string_view name, std::function<void(client_ptr cptr, client_ptr sptr, std::optional<message>msg)> handler)
{
	assert(app_data != nullptr);
	app_data->commands[upper_case_nfkc(name)] = handler;
}

void ircd::update_cmd_stats(std::string_view name, size_t msg_nbytes, client_ptr sptr)
{
	assert(app_data != nullptr);
	if (!sptr->has_flag(CLI_REMOTE)) {
		sptr->set_metadata("last_command", format_str("%1%", ircd::timestamp()));
	}
	int res = db.exec_dml("UPDATE [command-stats] SET [UseCount]=[UseCount]+1, [ByteCount]=[ByteCount]+?2, [RemoteCount]=[RemoteCount]+?3 WHERE [Command]=?1",
		name, msg_nbytes, sptr->has_flag(CLI_REMOTE) ? 1 : 0);
	if (res == 0) {
		db.exec_dml("INSERT INTO [command-stats] ([Command], [UseCount], [ByteCount], [RemoteCount]) VALUES(?, 1, ?, ?)",
			name, msg_nbytes, sptr->has_flag(CLI_REMOTE) ? 1 : 0);
	}
}

void ircd::do_command(client_ptr sptr, message const& msg, size_t nbytes)
{
	assert(app_data != nullptr);
	client_ptr cptr = sptr;
	// validate the prefix we received
	if (!msg.prefix.empty()) {
		if (sptr->has_flag(CLI_SERVER)) {
			sptr = find_entity(msg.prefix);
			if (!sptr) {
				snomask_log(LOG_DEBUG, SNO_DEBUG, "Received message with invalid entity '%2%' from %1%",
					cptr->nick(), msg.prefix);
				/*if (msg.prefix.length() == 2) {
					cptr->write_msg(nullptr, MSG_TRAILING_ARG, "SQUIT", msg.prefix, "Unknown entity");
				} else {
					cptr->write_msg(nullptr, MSG_TRAILING_ARG, "KILL", msg.prefix, "Unknown entity");
				}*/
				return;
			}
		} else if ((compare_nfkc(msg.prefix, sptr->nick()) != 0) && (compare_nfkc(msg.prefix, sptr->userhost()) != 0)) {
			sptr->send_numeric(ERR_UNKNOWN, sptr->userhost(), "Invalid command format");
			return;
		}
	}
	// numeric commands (must be three digits)
	if (boost::all(msg.command, boost::is_digit()) && msg.command.length() == 3) {
		if (sptr->has_flag(CLI_SERVER)) {
			cptr = find_user(msg.args[0]);
			if (cptr && !cptr->has_flag(CLI_REMOTE)) {
				cptr->write_msg(msg);
			}
		} else {
			snomask_log(LOG_DEBUG, SNO_DEBUG, "Received numeric %2% from %1% (not a server)", sptr->userhost(), msg.command);
		}
		return;
	}
	// search for built-in handler first
	if (auto iter = app_data->commands.find(msg.command); iter != app_data->commands.end()) {
		try {
			iter->second(cptr, sptr, msg);
			update_cmd_stats(msg.command, nbytes, sptr);
		} catch (std::exception const& err) {
			snomask_log(LOG_DEBUG, SNO_DEBUG, "Error in command '%2%' from %1%: %3%", sptr->userhost(), msg.command, err.what());
			sptr->send_numeric(ERR_UNKNOWN, sptr->nick(), msg.command, err.what());
		}
		return;
	}
	// run command through all modules
	for (auto mptr : app_data->modules) {
		if (mptr->do_command(cptr, sptr, msg)) {
			update_cmd_stats(msg.command, nbytes, sptr);
			return;
		}
	}
	// reaching this point, the command isn't valid
	if (!sptr->has_flag(CLI_REMOTE) && !sptr->has_flag(CLI_SERVER)) {
		sptr->send_numeric(ERR_UNKNOWNCOMMAND, sptr->nick(), msg.command);
		snomask_log(LOG_DEBUG, SNO_DEBUG, "Unknown command from %1%: %2%", sptr->userhost(), msg.text);
	}
}

void ircd::do_command(client_ptr sptr, char const* what)
{
	message dummy;
	dummy.command = upper_case_nfkc(what);
	dummy.prefix = msg_prefix(sptr, sptr, dummy);
	do_command(sptr, dummy, strlen(what));
}

std::string ircd::msg_prefix(client_ptr sptr, client_ptr uptr, ircd::message& msg)
{
	assert(app_data != nullptr);
	if (sptr) {
		if (uptr->has_flag(CLI_SERVER) || uptr->has_flag(CLI_REMOTE)) {
			return sptr->eid();
		} else {
			return sptr->userhost();
		}
		if (uptr->has_cap(CAP_ACCOUNT_TAG) && sptr->has_flag(CLI_ACCOUNT)) {
			msg.tags["account"] = sptr->account();
		}
	} else if (uptr->has_flag(CLI_SERVER) || uptr->has_flag(CLI_REMOTE)) {
		return info.id;
	} else {
		return info.fullname;
	}
}

ircd::client_ptr ircd::new_client(session_ptr peer)
{
	if (!app_data) return nullptr;
	auto sptr = std::shared_ptr<ircd::client>(new ircd::client{ peer });
	app_data->clients[sptr->uuid()] = sptr;
	return sptr;
}

std::optional<sqlite3xx::statement> ircd::find_auth(client_ptr sptr)
{
	return db.exec_query("SELECT * FROM [auth] WHERE ((?1 || '@' || ?2) GLOB [user]) OR ((?1 || '@' || ?3) GLOB [user])",
		sptr->username(), sptr->address(), sptr->hostname());
}

ircd::client_ptr ircd::get_client(boost::uuids::uuid uuid)
{
	if (!app_data) return nullptr;
	auto iter = app_data->clients.find(uuid);
	if (iter != std::end(app_data->clients)) {
		return iter->second;
	}
	return nullptr;
}

ircd::client_ptr ircd::find_client(std::string_view endpt_id)
{
	if (!app_data) return nullptr;
	auto result = db.exec_scalar("SELECT [EID] FROM [clients] WHERE [SocketID]=?1", endpt_id);
	if (!result.is_null()) {
		return get_client(result.get_uuid());
	}
	return nullptr;
}

void ircd::del_client(boost::uuids::uuid uuid)
{
	if (!app_data) return;
	auto iter = app_data->clients.find(uuid);
	if (iter != std::end(app_data->clients)) {
		app_data->clients.erase(iter);
	}
}

void ircd::kill_client(client_ptr target, std::string_view message)
{
	if (target->has_flag(CLI_SERVER)) {
		db.exec_dml("UPDATE [link-status] SET [status]=0 WHERE [name]=?1", target->nick());
		snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Lost connection to %1%", target->nick());
		for (auto stmt = db.exec_query("SELECT [EID] FROM [users] WHERE [SID]=?", target->eid()); !stmt.eof(); stmt.step()) {
			auto uptr = get_client(stmt(0).get_uuid());
			if (!uptr) continue;
			if (uptr->has_flag(CLI_REGISTERED)) {
				add_jupe(JUPE_NICK, uptr->nick(), target, "Nick Delay: Server link closed", JUPE_STATUS_NICKDELAY);
				snomask_log(LOG_DEBUG, SNO_DEBUG, "Created nick delay jupe on %1%", uptr->nick());
			}
			kill_client(uptr, "Server link closed");
		}
		target->set_flag(CLI_SERVER, false);
	}
	if (target->has_flag(CLI_REGISTERED)) {
		if (!target->has_flag(CLI_REMOTE)) {
			snomask_log(LOG_USER, SNO_LOCAL_USER_CONN, "Client exiting: %1% (%2%)", target->userhost(), message);
		} else {
			snomask_log(LOG_USER, SNO_REMOTE_USER_CONN, "Client exiting on %1%: %2% (%3%)", target->server(), target->userhost(), message);
		}
		// prepare notification message
		auto source = target->userhost();
		// notify channel participants
		for (auto stmt = db.exec_query("SELECT [ChannelName] FROM [channel-users] WHERE [EID] = ?", target->eid()); !stmt.eof(); stmt.step()) {
			auto chname = stmt("ChannelName").get_text();
			channel::del_user(chname, target);
			channel::notify_members(chname, nullptr, target, MSG_TRAILING_ARG, "QUIT", message);
			if (channel::count(chname) == 0 && target->has_flag(CLI_REMOTE) && !channel::has_mode(chname, CMODE_REGISTERED)) {
				add_jupe(JUPE_CHANNEL, chname, target, "Channel Delay: Server link closed", JUPE_STATUS_CHANDELAY);
				snomask_log(LOG_DEBUG, SNO_DEBUG, "Created channel delay jupe on %1%", chname);
			}
		}
		notify_modules("on_quit", target);
		target->set_flag(CLI_REGISTERED, false);
		// server notifications
		if (!target->has_flag(CLI_REMOTE)) notify_servers(nullptr, target, MSG_TRAILING_ARG, "QUIT", message);
	}
	if (!target->has_flag(CLI_REMOTE)) {
		target->set_metadata("kill_message", message);
		target->session()->error(message);
	}
	target->close();
	post([target]() { del_client(target->uuid()); });
}

ircd::client_ptr ircd::find_user(std::string_view who)
{
	if (!app_data) return nullptr;
	auto q = db.exec_query("SELECT [EID] FROM [users] WHERE [Nick]=?1 OR ([Nick]||'!'||[Username]||'@'||[Hostname])=?1", who);
	if (!q.eof()) {
		return get_client(q(0).get_uuid());
	}
	return nullptr;
}

bool ircd::find_nick(std::string_view who)
{
	assert(app_data != nullptr);
	return 0 != db.exec_scalar("SELECT COUNT(*) FROM [clients] WHERE [Nick]=?1", who).get_int(0);
}

ircd::client_ptr ircd::find_server_name(std::string_view who)
{
	if (!app_data) return nullptr;
	auto q = db.exec_scalar("SELECT [EID] FROM [servers] WHERE [Nick]=?", who);
	if (!q.is_null()) {
		return get_client(q.get_uuid());
	}
	return nullptr;
}

ircd::client_ptr ircd::find_entity(std::string_view eid)
{
	if (!app_data) return nullptr;
	auto q = db.exec_scalar("SELECT COUNT(*) FROM [clients] WHERE [EID]=?", eid);
	if (!q.is_null()) {
		return get_client(uuid_from_str(eid));
	}
	return nullptr;
}

void ircd::notify_servers(client_ptr cptr, client_ptr sptr, ircd::message msg)
{
	assert(app_data != nullptr);
	for (auto stmt = get_server_clients(); !stmt.eof(); stmt.step()) {
		auto uptr = get_client(stmt(0).get_uuid());
		if (uptr) {
			if (cptr && cptr->uuid() == uptr->uuid()) continue;
			msg.prefix = msg_prefix(sptr, uptr, msg);
			uptr->write_msg(msg);
		}
	}
}

sqlite3xx::statement ircd::get_server_clients()
{
	assert(app_data != nullptr);
	return db.exec_query("SELECT * FROM [servers]");
}

sqlite3xx::statement ircd::get_local_users()
{
	assert(app_data != nullptr);
	return db.exec_query("SELECT * FROM [users] WHERE [SID]=?", info.id);
}

void ircd::notify_snomask(uint16_t snomask, std::string_view message)
{
	assert(app_data != nullptr);
	for (auto stmt = get_local_users(); !stmt.eof(); stmt.step()) {
		auto uptr = get_client(stmt(0).get_uuid());
		if (uptr && uptr->has_snomask(snomask)) {
			uptr->send_notice(message);
		}
	}
}

void ircd::add_jupe(int type, std::string_view what, client_ptr sptr, std::string_view why, int status)
{
	assert(app_data != nullptr);
	assert(type >= 0 && type < MAX_JUPE_TYPES);
	db.exec_dml("INSERT INTO [jupe] ([type], [name], [status], [source], [reason], [time]) VALUES(?,?,?,?,?,timestamp())",
		type, normalize_nfkc(what), status, (sptr) ? sptr->userhost() : std::string{}, why);
}

bool ircd::del_jupe(int type, std::string_view what)
{
	assert(app_data != nullptr);
	assert(type >= 0 && type < MAX_JUPE_TYPES);
	return 0 != (int)db.exec_dml("DELETE FROM [jupe] WHERE [type]=? AND [name]=? AND [status]<>0", type, normalize_nfkc(what));
}

int ircd::get_jupe_status(int type, std::string_view what)
{
	assert(app_data != nullptr);
	assert(type >= 0 && type < MAX_JUPE_TYPES);
	return db.exec_scalar("SELECT [status] FROM [jupe] WHERE [type]=? AND [name]=?", type, normalize_nfkc(what)).get_int(-1);
}

sqlite3xx::statement ircd::get_jupe(int type, std::string_view what)
{
	assert(app_data != nullptr);
	assert(type >= 0 && type < MAX_JUPE_TYPES);
	return db.exec_query("SELECT * FROM [jupe] WHERE [type]=? AND [name]=?", type, normalize_nfkc(what));
}

void ircd::send_isupport(client_ptr sptr)
{
	assert(app_data != nullptr);
	int n = 0;
	ircd::message msg;
	msg.prefix = info.fullname;
	msg.command = format_str("%03i", RPL_ISUPPORT);
	msg.flags = MSG_REPEAT_1ST;
	msg.args.push_back(sptr->nick());
	for (auto isup = ircd::isupport_table; isup->label; ++isup) {
		std::string str;
		str.append(isup->label);
		if (isup->get_value) {
			str.append("=");
			str.append(isup->get_value());
		}
		msg.args.push_back(str);
	}
	if (!sptr->has_flag(CLI_SERVER)) {
		msg.args.push_back("supported by this server");
		msg.flags |= MSG_TRAILING_ARG;
	}
	sptr->write_msg(msg);
}

void ircd::do_message(client_ptr cptr, client_ptr sptr, std::string_view dest, message const& messg, std::string_view text)
{
	assert(app_data != nullptr);
	if (valid_nick(dest) == NAME_ERROR_NONE || cptr->has_flag(CLI_SERVER)) {
		client_ptr target;
		if (cptr->has_flag(CLI_SERVER)) {
			target = find_entity(dest);
		} else {
			target = find_user(dest);
		}
		if (target && target->has_flag(CLI_REGISTERED)) {
			if (target->has_umode(UMODE_SERVICE) && !target->is_service("user")) {
				sptr->reply_numeric(messg.tag("label"), ERR_UNKNOWN, sptr->userhost(), messg.command, "Use the SQUERY command for this service");
				return;
			}
			auto mres = notify_modules("allow_message_user", cptr, sptr, target, messg.command, text);
			if (mres && !static_cast<bool>(*mres)) {
				return; // module should handle error output
			} else if (target->has_flag(CLI_REMOTE)) {
				target->write_msg(sptr, MSG_TRAILING_ARG, messg.command, target->eid(), text);
			} else {
				target->write_msg(sptr, MSG_TRAILING_ARG, messg.command, target->nick(), text);
				if (sptr->has_cap(CAP_ECHO_MESSAGE)) {
					sptr->reply_msg(messg.tag("label"), sptr, MSG_TRAILING_ARG, messg.command, target->nick(), text);
				} else if (messg.tags.count("label") && sptr->has_cap(CAP_LABELED_RESPONSE)) {
					sptr->reply_msg(messg.tag("label"), sptr, 0, "ACK");
				}
				if (target->has_umode(UMODE_AWAY)) {
					sptr->send_numeric(RPL_ISAWAY, sptr->nick(), target->nick(), target->get_metadata("away_message"));
				}
			}
		} else {
			sptr->reply_numeric(messg.tag("label"), ERR_NOSUCHNICK, sptr->nick(), dest);
		}
	} else if (valid_chan(dest) == NAME_ERROR_NONE) {
		int cmd;
		if (same_text(messg.command, "NOTICE")) {
			cmd = CMSG_NOTICE;
		} else if (same_text(messg.command, "TAGMSG")) {
			cmd = CMSG_TAGMSG;
		} else {
			cmd = CMSG_PRIVMSG;
		}
		if (auto chrec = channel::find(dest)) {
			channel::do_message(chrec->name, cptr, sptr, cmd, text, messg.tags);
			if (messg.tags.count("label") && sptr->has_cap(CAP_LABELED_RESPONSE) && !sptr->has_cap(CAP_ECHO_MESSAGE)) {
				sptr->reply_msg(messg.tag("label"), sptr, 0, "ACK");
			}
		} else {
			sptr->reply_numeric(messg.tag("label"), ERR_NOSUCHCHANNEL, sptr->nick(), dest);
		}
	}
}

bool ircd::connect_to_link(client_ptr sptr, std::string_view name)
{
	assert(app_data != nullptr);
	auto status = db.exec_scalar("SELECT [status] FROM [link-status] WHERE [name]=?", name);
	if (0 != status.get_int(-1)) return false;
	auto result = db.exec_query("SELECT [host], [port], [ssl_required] FROM [server-connects] WHERE [name]=?", name);
	if (result.eof()) return false;
	std::string host = result("host").get_text(), port = result("port").get_text();
	if (sptr) {
		snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Connecting to %1% (%2%@%3%), requested by %4%",
			name, host, port, sptr->userhost());
	} else {
		snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Connecting to %1% (%2%@%3%)", name, host, port);
	}
	db.exec_dml("INSERT OR REPLACE INTO [link-status] ([name], [status], [last_connect], [last_error]) VALUES(?, -1, timestamp(), NULL)", name);
	bool use_ssl = 0 != result("ssl_required").get_int(0);
	app_data->sessions.connect(use_ssl, name, host, port);
	return true;
}

void ircd::init_link(client_ptr cptr)
{
	error_code ec;
	auto sptr = cptr->session();
	auto host = cptr->address();
	auto port = sptr->remote.port();
	auto result = db.exec_scalar("SELECT [name] FROM [server-connects] WHERE [host]=? AND [port]=?", host, port);
	if (result.is_null()) {
		snomask_log(LOG_DEBUG, SNO_REJECT_CONN, "Invalid connection: %1%:%2%", host, port);
		kill_client(cptr, "");
	} else {
		auto name = result.get_text();
		db.exec_dml("INSERT OR REPLACE INTO [link-status] ([name], [status], [last_connect], [last_error]) VALUES(?, 1, timestamp(), NULL)", name);
		snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Connection established: %1% (%2%@%3%)", name, host, port);
		notify_modules("on_link_connect", cptr);
		post([sptr] { sptr->read(); });
	}
}

void ircd::send_server_caps(client_ptr sptr)
{
	assert(app_data != nullptr);
	ircd::message capmsg;
	capmsg.prefix = info.id;
	capmsg.command = "SCAP";
	capmsg.flags = 0;
	for (limit_info const* iptr = limit_table; iptr->lua_name; iptr++) {
		capmsg.args.push_back(format_str("%1%=%2%", iptr->lua_name, info.limits[iptr->id]));
	}
	sptr->write_msg(capmsg);
}

bool ircd::validate_server_caps(client_ptr sptr)
{
	assert(app_data != nullptr);
	for (limit_info const* iptr = limit_table; iptr->lua_name; iptr++) {
		if (sptr->has_metadata(iptr->lua_name)) {
			auto rval = sptr->get_metadata(iptr->lua_name).get_uint(0);
			if (rval != info.limits[iptr->id]) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

bool ircd::add_kline(client_ptr sptr, std::string_view mask, std::string_view reason)
{
	assert(app_data != nullptr);
	int n = 0;
	int type = valid_mask(mask);
	if (type != MASK_INVALID) {
		n = db.exec_dml("INSERT OR REPLACE INTO [server-bans] ([mask], [type], [status], [created], [source], [comment]) VALUES(?, ?, 1, timestamp(), ?, ?)",
			normalize_nfkc(mask), type, (sptr) ? sptr->userhost() : "", reason);
	}
	return n != 0;
}

bool ircd::add_kline_temp(client_ptr sptr, std::string_view mask, std::string_view reason, uint64_t expire_time)
{
	assert(app_data != nullptr);
	int n = 0;
	int type = valid_mask(mask);
	if (type != MASK_INVALID) {
		n = db.exec_dml("INSERT OR REPLACE INTO [server-bans] ([mask], [type], [status], [created], [source], [comment], [expire]) VALUES(?, ?, 1, timestamp(), ?, ?, ?)",
			normalize_nfkc(mask), type, (sptr) ? sptr->userhost() : "", reason, expire_time);
	}
	return n != 0;
}

bool ircd::del_kline(std::string_view mask)
{
	assert(app_data != nullptr);
	return 0 != db.exec_dml("DELETE FROM [server-bans] WHERE [mask]=?1 AND [status]=1", normalize_nfkc(mask));
}

bool ircd::is_user_kline(client_ptr sptr)
{
	assert(app_data != nullptr);
	auto res = db.exec_scalar("SELECT COUNT(*) FROM [server-bans] WHERE ([type] IN (1, 2) AND ?1 GLOB [mask]) OR ([type] IN (3, 4) AND cidr_match([mask], ?2))",
		format_str("%1%!%2%@%3%", sptr->nick(), sptr->username(), sptr->hostname()), sptr->address());
	return 0 != res.get_int(0);
}

bool ircd::is_addr_kline(boost::asio::ip::address const& addr)
{
	assert(app_data != nullptr);
	error_code ec;
	std::string str = addr.to_string(ec);
	if (ec) return false;
	if (addr.is_v6()) str.insert(str.begin(), '0');
	auto res = db.exec_scalar("SELECT COUNT(*) FROM [server-bans] WHERE ([type] IN (1, 2) AND ?1 GLOB [mask]) OR ([type] IN (3, 4) AND cidr_match([mask], ?1))",
		str);
	return 0 != res.get_int(0);
}

bool ircd::allow_connection(session_ptr peer)
{
	assert(app_data != nullptr);
	error_code ec;
	auto const& endpt{ peer->remote };
	auto const addr{ endpt.address().to_string(ec) };
	auto const port{ endpt.port() };
	if (ec) return false;
	if ((num_connections() + 1) > info.max_clients) {
		snomask_log(LOG_USER, SNO_REJECT_CONN, "Rejecting connection from %1%:%2% (server is full)", addr, port);
		peer->error("Server is full");
		return false;
	}
	if (is_addr_kline(endpt.address())) {
		write_log(LOG_USER, nullptr, "Rejecting connection from %1%:%2% (address K-lined)", addr, port);
		peer->error("Banned from server");
		return false;
	}
	if (connect_attempts_exceeded(endpt)) {
		int constexpr ban_duration = 900;
		time_t ban_time = timestamp() + ban_duration;
		add_kline_temp(nullptr, addr, "Excessive incomplete connections", ban_time);
		snomask_log(LOG_USER, SNO_REJECT_CONN, "Rejecting connection from %1%:%2% (too many incomplete attempts)", addr, port);
		static char stamp[100] = "";
		strftime(stamp, sizeof(stamp), "%c", std::localtime(&ban_time));
		snomask_log(LOG_USER, SNO_REJECT_CONN, "All future connections from %1% will be blocked until %2%", addr, stamp);
		peer->close();
		db.exec_scalar("DELETE FROM [connect-attempts] WHERE [addr]=?", addr);
		return false;
	}
	auto const& locpt{ peer->local };
	if (auto res = notify_modules("on_connect", addr, port, locpt.address().to_string(ec), locpt.port())) {
		bool val{ false };
		std::string messg;
		sol::tie(val, messg) = *res;
		if (!val) {
			peer->error(messg);
		}
		return val;
	}
	// default is to allow
	return true;
}

sqlite3xx::statement ircd::get_klines()
{
	assert(app_data != nullptr);
	return db.exec_query("SELECT [_rowid_], * FROM [server-bans]");
}

bool ircd::has_common_channels(client_ptr usr1, client_ptr usr2)
{
	assert(app_data != nullptr);
	auto stmt = get_common_channels(usr1, usr2);
	return !stmt.eof();
}

sqlite3xx::statement ircd::get_common_channels(client_ptr usr1, client_ptr usr2)
{
	assert(app_data != nullptr);
	return db.exec_query("SELECT [ChannelName] FROM [channel-users] WHERE [EID]=?1 INTERSECT SELECT [ChannelName] FROM [channel-users] WHERE [EID]=?2 GROUP BY [ChannelName]",
		usr1->eid(), usr2->eid());
}

void ircd::open_log(std::string_view source, std::optional<uint32_t> enable)
{
	using namespace std::placeholders;
	assert(app_data != nullptr);
	log.open(source);
	log.exec_dml("PRAGMA journal_mode=WAL");
	log.exec_dml("CREATE TABLE IF NOT EXISTS [log_data] ([ID] INTEGER PRIMARY KEY, [TS] DATETIME, [Category] TEXT COLLATE NOCASE, [Module] TEXT COLLATE NOCASE, [Message] TEXT COLLATE NOCASE);");
	log.exec_dml("PRAGMA wal_checkpoint(TRUNCATE)");
	log.create_collation("NOCASE", std::bind(&compare_text, _1, _2));
	log.create_function("glob", 2, [](sqlite3xx::context& ctx, std::vector<sqlite3xx::value> const& args) {
		ctx.set_result(strmatch(args[1].get_text(), args[0].get_text()));
	});
	log.create_function("like", 2, [](sqlite3xx::context& ctx, std::vector<sqlite3xx::value> const& args) {
		ctx.set_result(strmatch(args[1].get_text(), args[0].get_text(), '%', '_'));
	});
	info.log_enabled |= LOG_GENERAL;
	if (enable) {
		info.log_enabled |= *enable;
	}
}

static std::string replace_one(std::string str, std::string_view sub1, std::string_view sub2)
{
	if (sub1.empty())
		return str;

	std::size_t pos;
	if ((pos = str.find(sub1)) != std::string::npos)
		return str.replace(pos, sub1.size(), sub2);
	else
		return str;
}

void ircd::open_kline_db()
{
	static int const kline_db_version = 1;
	try {
		sqlite3xx::savepoint sp{ db };
		sp.start("open_kline_db");
		auto create_sql = db.exec_scalar("SELECT sql FROM main.sqlite_master WHERE [name]='server-bans'").get_text();
		db.exec_dml("DROP TABLE main.[server-bans]");
		// attach the database file
		db.exec_dml("ATTACH DATABASE " + sqlite3xx::escape_text('Q', info.kline_db_file) + " AS kline_db");
		db.exec_dml("PRAGMA kline_db.journal_mode=WAL");
		db.exec_dml("PRAGMA kline_db.wal_checkpoint(TRUNCATE)");
		// retrieve the database "version" information
		auto vers = db.exec_scalar("PRAGMA kline_db.user_version").get_int(0);
		// determine if the attached database has the [server-bans] table
		auto kline_table = db.exec_query("PRAGMA kline_db.table_info('server-bans')");
		// if the version # doesn't match, and the [server-bans] exists, don't use the file
		if (vers != kline_db_version && !kline_table.eof()) {
			throw std::runtime_error("invalid database version");
		}
		// if the [server-bans] table doesn't exist, create it
		if (kline_table.eof()) {
			db.exec_dml("PRAGMA kline_db.user_version = " + std::to_string(kline_db_version));
			// use the existing 'in-memory' schema to create the table
			// ensure that the correct schema prefix is inserted
			create_sql = replace_one(create_sql, "[server-bans]", "kline_db.[server-bans]");
			db.exec_dml(create_sql);
		}
		sp.release();
	} catch (std::exception const& err) {
		write_log(LOG_GENERAL, nullptr, "Error opening k-line database: %1%", err.what());
	}
}

uint64_t ircd::write_log(int category, module_ptr module, std::string_view what)
{
	if (log.is_open() && (info.log_enabled & category)) {
		std::string cat_name;
		switch (category) {
		case LOG_DEBUG: cat_name = "debug"; break;
		case LOG_GENERAL: cat_name = "general"; break;
		case LOG_USER: cat_name = "user"; break;
		case LOG_OPER: cat_name = "oper"; break;
		case LOG_SERVER: cat_name = "server"; break;
		case LOG_KILL: cat_name = "kill"; break;
		}
		log.exec_dml("INSERT INTO [log_data] ([ID], [TS], [Category], [Module], [Message]) VALUES(NULL, datetime('now'), ?, ?, ?)",
			cat_name, module ? module->name() : "", what);
		return log.last_rowid();
	} else {
		return 0;
	}
}

uint64_t ircd::write_log(int category, std::string_view what)
{
	return write_log(category, nullptr, what);
}

uint64_t ircd::snomask_log(int category, uint16_t snomask, std::string_view what)
{
	if (app_data) notify_snomask(snomask, "*** Notice -- %1%", what);
	return write_log(category, nullptr, what);
}

uint64_t ircd::snomask_log(int category, uint16_t snomask, module_ptr module, std::string_view what)
{
	if (app_data) notify_snomask(snomask, "*** Notice -- %1%", what);
	return write_log(category, module, what);
}

void ircd::timer_cycle()
{
	if (!app_data) return;
	// auto-connect to server links
	for (auto result = db.exec_query("SELECT * FROM [server-connects] WHERE [frequency]>0"); !result.eof(); result.step()) {
		auto sname = result("name").get_text();
		auto status = db.exec_scalar("SELECT [last_connect] FROM [link-status] WHERE [name]=? AND [status]=0", sname);
		unsigned int freq = result("frequency").get_uint(0) * 60;
		auto last_time = status.get_uint64(0);
		int64_t diff = ircd::timestamp() - last_time;
		if (diff >= freq) {
			connect_to_link(nullptr, sname);
		}
	}

	// purge expired nick delay jupes
	db.exec_dml("DELETE FROM [jupe] WHERE [status]=?1 AND (timestamp() - [time]) >= ?2", JUPE_STATUS_NICKDELAY, info.limits[MAX_NICKDELAY] * 60);

	// purge expired channel delay jupes
	db.exec_dml("DELETE FROM [jupe] WHERE [status]=?1 AND (timestamp() - [time]) >= ?2", JUPE_STATUS_CHANDELAY, info.limits[MAX_CHANDELAY] * 60);

	// purge expired server bans
	auto n = db.exec_scalar("SELECT COUNT(*) FROM [server-bans] WHERE timestamp() > [expire]").get_int64(0);
	if (n != 0) db.exec_dml("DELETE FROM [server-bans] WHERE timestamp() > [expire]");

	// purge connection attempts
	db.exec_dml("DELETE FROM [connect-attempts] WHERE (timestamp() - [ts]) > 10");

	// trim the WAL file on the log database
	if (log.is_open()) log.exec_dml("PRAGMA wal_checkpoint(PASSIVE)");
}

void ircd::timer_reset()
{
	if (!app_data) return;
	auto& timer1_{ app_data->timer1 };
	if (!timer1_) {
		timer1_.reset(new asio::deadline_timer{ io_ctx, boost::posix_time::seconds(TIMER_CYCLE) });
	} else {
		timer1_->expires_from_now(boost::posix_time::seconds(TIMER_CYCLE));
	}
	timer1_->async_wait([](error_code const& e) {
		if (!e) {
			timer_cycle();
			timer_reset();
		}
	});
}

void ircd::timer_stop()
{
	if (!app_data) return;
	auto& timer1_{ app_data->timer1 };
	if (timer1_) {
		error_code ec;
		timer1_->cancel(ec);
	}
}

void ircd::after_conf_reload(uint32_t flags)
{
	assert(app_data != nullptr);
	if (flags & RELOAD_AUTH) {
		// disconnect users that no longer match any auth records
		auto stmt = db.exec_query("SELECT * FROM [users] WHERE [SID]=? AND [Auth_RowID] IS NULL", info.id);
		for (; !stmt.eof(); stmt.step()) {
			auto uptr = get_client(stmt(0).get_uuid());
			if (uptr && !uptr->has_flag(CLI_SERVICE)) kill_client(uptr, "Unauthorized");
		}
	}
	if (flags & RELOAD_OPERS) {
		// revoke oper status for removed oper records
		for (auto stmt = get_local_users(); !stmt.eof(); stmt.step()) {
			auto uptr = get_client(stmt(0).get_uuid());
			if (!uptr || !uptr->has_umode(UMODE_OPERATOR)) continue;
			auto res = db.exec_scalar("SELECT COUNT(*) FROM [oper-hosts] WHERE [name]=? AND ((? GLOB [hostmask]) OR (('*@' || ?) GLOB [hostmask]))",
				uptr->get_metadata("oper_name"), uptr->userhost(), uptr->address());
			if (0 == res.get_int(0)) {
				uptr->change_umodes("-o", 0);
			}
		}
	}
	if (flags & RELOAD_JUPES) {
		// disconnect nicknames that are now juped
		auto stmt = db.exec_query("SELECT * FROM [users] JOIN [jupe] ON [jupe].[name]=[users].[Nick] AND [users].[SID]=?", info.id);
		for (; !stmt.eof(); stmt.step()) {
			auto uptr = get_client(stmt(0).get_uuid());
			if (!uptr || !uptr->has_flag(CLI_SERVICE)) kill_client(uptr, "Invalid nickname");
		}
		// remove juped channels
		stmt = db.exec_query("SELECT [channels].[Name] FROM [channels] JOIN [jupe] ON [jupe].[name]=[channels].[Name]");
		for (; !stmt.eof(); stmt.step()) {
			channel::kill(stmt("Name").get_text());
		}
	}
	if (flags & RELOAD_KLINES) {
		auto stmt = db.exec_query("SELECT * FROM [users]");
		for (; !stmt.eof(); stmt.step()) {
			auto uptr = get_client(stmt(0).get_uuid());
			if (uptr && !uptr->has_flag(CLI_REMOTE) && is_user_kline(uptr)) {
				notify_snomask(SNO_KILLS, "*** Notice -- K-line active for %1%", uptr->userhost());
				kill_client(uptr, "User is banned");
			}
		}
	}
}

sqlite3xx::statement ircd::query_log(std::string_view sql)
{
	assert(app_data != nullptr);
	return log.exec_query(sql);
}

int ircd::valid_nick(std::string_view what)
{
	namespace x3 = boost::spirit::x3;
	namespace ucs = boost::spirit::x3::unicode;
	namespace acs = boost::spirit::x3::ascii;
	static auto const nickname_acs = x3::rule<struct name_, std::u32string>{ "nickname_acs" } =
		+acs::alpha >> *(acs::alnum | '{' | '}' | '[' | ']' | '-') >> x3::eoi;
	static auto const nickname_ucs = x3::rule<struct name_, std::u32string>{ "nickname_ucs" } =
		+ucs::alpha >> *(ucs::alnum | '{' | '}' | '[' | ']' | '-') >> x3::eoi;
	bool parse_ok = false;
	auto iter = utf8::iterator{ what.begin(), what.begin(), what.end() };
	auto end = utf8::iterator{ what.end(), what.begin(), what.end() };
	switch (info.utf8mapping) {
	case UTF8MAP_RFC7700:
		parse_ok = x3::parse(iter, end, nickname_ucs);
		break;
	case UTF8MAP_ASCII:
		parse_ok = x3::parse(iter, end, nickname_acs);
		break;
	}
	if (!parse_ok || iter != end) {
		return NAME_ERROR_CHARS;
	} else if (utf8::distance(what.begin(), what.end()) > info.limits[MAX_NICKLEN]) {
		return NAME_ERROR_LENGTH;
	} else {
		return NAME_ERROR_NONE;
	}
}

int ircd::valid_username(std::string_view what)
{
	namespace x3 = boost::spirit::x3;
	namespace ucs = boost::spirit::x3::unicode;
	namespace acs = boost::spirit::x3::ascii;
	static auto const username_acs = x3::rule<struct name_, std::u32string>{ "username_acs" } =
		+acs::alpha >> *(acs::alnum | '-' | '.') >> x3::eoi;
	static auto const username_ucs = x3::rule<struct name_, std::u32string>{ "username_ucs" } =
		+ucs::alpha >> *(ucs::alnum | '-' | '.') >> x3::eoi;
	bool parse_ok = false;
	auto iter = utf8::iterator{ what.begin(), what.begin(), what.end() };
	auto end = utf8::iterator{ what.end(), what.begin(), what.end() };
	switch (info.utf8mapping) {
	case UTF8MAP_RFC7700:
		parse_ok = x3::parse(iter, end, username_ucs);
		break;
	case UTF8MAP_ASCII:
		parse_ok = x3::parse(iter, end, username_acs);
		break;
	}
	if (!parse_ok || iter != end) {
		return NAME_ERROR_CHARS;
	} else if (utf8::distance(what.begin(), what.end()) > info.limits[MAX_NICKLEN]) {
		return NAME_ERROR_LENGTH;
	} else {
		return NAME_ERROR_NONE;
	}
}

int ircd::valid_chan(std::string_view what)
{
	namespace x3 = boost::spirit::x3;
	namespace ucs = boost::spirit::x3::unicode;
	namespace acs = boost::spirit::x3::ascii;
	static auto const channame_ucs = x3::rule<struct name_, std::u32string>{ "channame_ucs" } =
		x3::char_("#&+") >> +(ucs::print - ucs::space - ":" - ",") >> x3::eoi;
	static auto const channame_acs = x3::rule<struct name_, std::u32string>{ "channame_acs" } =
		x3::char_("#&+") >> +(acs::print - acs::space - ":" - ",") >> x3::eoi;
	bool parse_ok = false;
	auto iter = utf8::iterator{ what.begin(), what.begin(), what.end() };
	auto end = utf8::iterator{ what.end(), what.begin(), what.end() };
	switch (info.utf8mapping) {
	case UTF8MAP_RFC7700:
		parse_ok = x3::parse(iter, end, channame_ucs);
		break;
	case UTF8MAP_ASCII:
		parse_ok = x3::parse(iter, end, channame_acs);
		break;
	}
	if (!parse_ok || iter != end) {
		return NAME_ERROR_CHARS;
	} else if (utf8::distance(what.begin(), what.end()) > info.limits[MAX_CHANLEN]) {
		return NAME_ERROR_LENGTH;
	} else {
		return NAME_ERROR_NONE;
	}
}

void ircd::enable_lua_debug()
{
	assert(app_data != nullptr);
	app_data->lua_debug = true;
}

bool ircd::lua_debug()
{
	assert(app_data != nullptr);
	return app_data->lua_debug;
}

void ircd::log_connect_attempt(boost::asio::ip::tcp::endpoint const& endpt)
{
	static std::optional<sqlite3xx::statement> stmt;
	try {
		if (!stmt) stmt.emplace(db.compile("INSERT OR REPLACE INTO [connect-attempts] ([addr], [ts], [count]) VALUES(?, ?, ?)"));
		auto addr_str = endpt.address().to_string();
		auto ts = timestamp();
		auto n = db.exec_scalar("SELECT SUM([count]) FROM [connect-attempts] WHERE [addr]=? AND [ts]=?", addr_str, ts).get_int(0);
		stmt->bind_text(1, addr_str);
		stmt->bind_int64(2, ts);
		stmt->bind_int(3, ++n);
		stmt->step();
		stmt->reset();
	} catch (error_code const&) {
		// ignore boost error
	} catch (std::exception const& err) {
		write_log(LOG_DEBUG, nullptr, "Error while logging connect attempt: %1%", err.what());
	}
}

bool ircd::connect_attempts_exceeded(boost::asio::ip::tcp::endpoint const& endpt)
{
	try {
		auto addr = endpt.address().to_string();
		auto limit = db.exec_scalar("SELECT [incomplete_limit] FROM [auth] WHERE ('*@' || ?1) GLOB [user]", addr).get_int(3);
		auto res = db.exec_scalar("SELECT SUM([count]) FROM [connect-attempts] WHERE [addr]=? AND (timestamp() - [ts]) < 10", addr);
		return res.get_int(0) > limit;
	} catch (std::exception const&) {
		return false;
	}
}
