/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"

static char const* BUILD_TIMESTAMP = __TIMESTAMP__;

char const* ircd::id_buildtime()
{
	return BUILD_TIMESTAMP;
}

char const* ircd::id_arch()
{
#if BOOST_ARCH_ALPHA
	return "alpha";
#elif BOOST_ARCH_ARM
	return "arm";
#elif BOOST_ARCH_BLACKFIN
	return "blackfin";
#elif BOOST_ARCH_CONVEX
	return "convex";
#elif BOOST_ARCH_IA64
	return "ia64";
#elif BOOST_ARCH_M68K
	return "m68k";
#elif BOOST_ARCH_MIPS
	return "mips";
#elif BOOST_ARCH_PARISC
	return "hp/pa-risc";
#elif BOOST_ARCH_PPC
	return "ppc";
#elif BOOST_ARCH_PTX
	return "ptx";
#elif BOOST_ARCH_PYRAMID
	return "pyramid";
#elif BOOST_ARCH_RISCV
	return "risc-v";
#elif BOOST_ARCH_RS6000
	return "rs/6000";
#elif BOOST_ARCH_SPARC
	return "sparc";
#elif BOOST_ARCH_SH
	return "super-h";
#elif BOOST_ARCH_SYS370
	return "s/370";
#elif BOOST_ARCH_SYS390
	return "s/390";
#elif BOOST_ARCH_X86
# if BOOST_ARCH_X86_32
	return "x86-32";
# elif BOOST_ARCH_X86_64
	return "x86-64";
# elif BOOST_ARCH_X86_64
	return "x86";
# endif
#elif BOOST_ARCH_Z
	return "ibm-z";
#else
	return "<unknown arch>";
#endif
}

char const* ircd::id_comp()
{
#if BOOST_COMP_BORLAND
	return "borland";
#elif BOOST_COMP_CLANG
	return "clang";
#elif BOOST_COMP_COMO
	return "comeau";
#elif BOOST_COMP_DEC
	return "dec";
#elif BOOST_COMP_DIAB
	return "diab";
#elif BOOST_COMP_DMC
	return "dmc";
#elif BOOST_COMP_SYSC
	return "sysc";
#elif BOOST_COMP_EDG
	return "edg";
#elif BOOST_COMP_PATH
	return "path";
#elif BOOST_COMP_GNUC
	return "gnuc";
#elif BOOST_COMP_GCCXML
	return "gccxml";
#elif BOOST_COMP_GHS
	return "ghs";
#elif BOOST_COMP_HPACC
	return "hpacc";
#elif BOOST_COMP_IAR
	return "iar";
#elif BOOST_COMP_IBM
	return "ibm";
#elif BOOST_COMP_INTEL
	return "intel";
#elif BOOST_COMP_KCC
	return "kcc";
#elif BOOST_COMP_LLVM
	return "llvm";
#elif BOOST_COMP_HIGHC
	return "highc";
#elif BOOST_COMP_MWERKS
	return "mwerks";
#elif BOOST_COMP_MRI
	return "mri";
#elif BOOST_COMP_MPW
	return "mpw";
#elif BOOST_COMP_NVCC
	return "nvcc";
#elif BOOST_COMP_PALM
	return "palm";
#elif BOOST_COMP_PGI
	return "pgi";
#elif BOOST_COMP_SGI
	return "sgi";
#elif BOOST_COMP_SUNPRO
	return "sunpro";
#elif BOOST_COMP_TENDRA
	return "tendra";
#elif BOOST_COMP_MSVC
	return "msvc";
#elif BOOST_COMP_WATCOM
	return "watcom";
#else
	return "<unknown compiler>";
#endif
}

char const* ircd::id_os()
{
#if BOOST_OS_AIX
	return "aix";
#elif BOOST_OS_AMIGAOS
	return "amigaos";
#elif BOOST_OS_BEOS
	return "beos";
#elif BOOST_OS_BSD
# if BOOST_OS_BSD_DRAGONFLY
	return "dragonfly";
# elif BOOST_OS_BSD_FREE
	return "freebsd";
# elif BOOST_OS_BSD_BSDI
	return "bsdi";
# elif BOOST_OS_BSD_NET
	return "netbsd";
# elif BOOST_OS_BSD_OPEN
	return "openbsd";
# else
	return "<unknown bsd>";
# endif
#elif BOOST_OS_CYGWIN
	return "cygwin";
#elif BOOST_OS_HAIKU
	return "haiku";
#elif BOOST_OS_HPUX
	return "hpux";
#elif BOOST_OS_IOS
	return "ios";
#elif BOOST_OS_IRIX
	return "irix";
#elif BOOST_OS_LINUX
	return "linux";
#elif BOOST_OS_MACOS
	return "macos";
#elif BOOST_OS_OS400
	return "os400";
#elif BOOST_OS_UNIX
	return "unix";
#elif BOOST_OS_QNX
	return "qnx";
#elif BOOST_OS_SOLARIS
	return "solaris";
#elif BOOST_OS_SVR4
	return "svr4";
#elif BOOST_OS_VMS
	return "vms";
#elif BOOST_OS_WINDOWS
	return "windows";
#else
	return "<unknown OS>";
#endif
}
