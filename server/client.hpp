/*
 * ircd - An IRCv3 Server
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#pragma once

namespace ircd {
	/// IRC client flags
	enum : uint32_t {
		CLI_SSL = (1 << 0),
		CLI_REGISTERED = (1 << 1),
		CLI_REMOTE = (1 << 2),
		CLI_SERVICE = (1 << 3),
		CLI_SERVER = (1 << 4),
		CLI_NEGOTIATING_CAPS = (1 << 5),
		CLI_RESOLVING_DNS = (1 << 6),
		CLI_RESOLVING_IDENT = (1 << 7),
		CLI_DONE_DNS = (1 << 8),
		CLI_DONE_IDENT = (1 << 9),
		CLI_NO_IDENT = (1 << 10),
		CLI_ACCOUNT = (1 << 11),
		CLI_SYNC = (1 << 12), // server client currently in SYNC status
		CLI_EOB = (1 << 13),  // server client completed SYNC (EOB)
	};

	/// Socket object for all IRC clients
	struct session : public std::enable_shared_from_this<session> {
		tcp::socket socket;
		std::unique_ptr<asio::ssl::stream<tcp::socket&>> ssl_socket;
		std::deque<std::string> queue;
		asio::streambuf data;
		tcp::endpoint remote, local;

		session();
		session(tcp::socket);
		virtual ~session();
		std::string endpoint_id();
		asio::ip::address address();
		void close();
		void enable_ssl(client_ptr cptr);
		void read();
		void write(std::string_view str);
		void error(std::string_view message);
		void send_message();
	};

	/// Class representing an active IRC client/server session.
	class client : public std::enable_shared_from_this<client> {
	public:
		/// @brief Initialize client connection.
		/// This establishes records in the SQL [clients] table,
		/// and initializes the periodic keep-alive timer.
		client(session_ptr);

		/// Default destructor
		virtual ~client();

		/// Determine if flags are present
		inline bool has_flag(uint32_t mask) const { return flags_ & mask; }

		/// Set flags
		void set_flag(uint32_t mask, bool state);

		/// Shutdown socket
		void close();

		/// Determine if an IRCv3 cap has been negotiated
		inline bool has_cap(uint64_t mask) const { return caps_ & mask; }

		/// Update IRCv3 cap status
		void set_cap(uint64_t mask, bool state);

		/// Return IRCv3 capabilities
		uint64_t get_caps() const { return caps_; }

		/// Update IRCv3 capabilities from CAP negotiation
		void set_caps(uint64_t what) { caps_ = what; }

		/// Returns the underlying socket
		tcp::socket& socket() { return session_->socket; }

		/// Returns the underlying socket
		session_ptr session() { return session_; }

		/// Return the client's current timestamp
		uint64_t timestamp() const;

		/// Update the client's timestamp
		void set_timestamp(uint64_t const);

		/// Returns the client's known IP address
		std::string address() const;

		/// Update a remote client's known IP address (remote connections only)
		void set_address(std::string_view);

		/// Returns the client's current hostname
		std::string hostname() const;

		/// Returns the client's nickname, or "*" if not registered
		std::string nick() const;

		/// Update the client's nickname
		void set_nick(std::string_view);

		/// Return an "endpoint identifier" (remote IP and port)
		std::string endpoint_id() const;

		/// Returns the client's username
		/// NOTE: Server clients do not have a username.
		std::string username() const;

		/// Update the client's username
		/// NOTE: Server clients do not have a username.
		void set_username(std::string_view);

		/// Returns the client's nick, user, and host, in the format "nick!user@host", or "*" if not registered.
		/// For server clients this is equivalent to nick().
		std::string userhost() const;

		/// Returns the client's gecos
		std::string gecos() const;

		/// Update the client's gecos
		void set_gecos(std::string_view);

		/// Return the client's account identifier
		std::string account() const;

		/// Update the client's account identifier
		void set_account(std::string_view);

		/// Determine the client's server name
		std::string server() const;

		/// Determine the client's entity identifier (EID)
		std::string eid() const;

		/// Retrieve UUID (which is the EID in its 'native' binary format)
		boost::uuids::uuid uuid() const;

		/// Update the client's entity identifier (EID)
		void set_eid(std::string_view);

		/// Transmit message data to the client
		void write_msg(message);

		/// Initiate IRCv3 batch output
		std::string start_batch(std::string_view type, std::string_view label, std::vector<std::string> params);

		/// End an IRCv3 batch
		void stop_batch(std::string_view batch_id);

		/// @brief Transmit message data to the client.
		/// @param batch_id Batch identifier, returned by a previous call to start_batch()
		/// @param sptr If present, specifies the message source; otherwise, the server name will be used
		/// @param flags Message flags
		/// @param cmd The protocol command being sent
		/// @param params Message parameters
		template<typename... Args>
		void batch_msg(std::string_view batch_id, client_ptr sptr, uint32_t flags, std::string_view cmd, Args&&... params);

		/// @brief Generate a formatted numeric message to the client.
		/// @param batch_id Batch identifier, returned by a previous call to start_batch()
		/// @param n Numeric code to generate.
		/// @param args Additional formatting parameters
		///
		/// Numerics are generated via the table found in numeric.cpp
		/// Numerics that are disallowed for remote connections will not generate.
		template<typename... Args>
		void batch_numeric(std::string_view batch_id, int n, Args&&... args);

		/// @brief Transmit message data to the client.
		/// @param label_id Client's label identifier.
		/// @param sptr If present, specifies the message source; otherwise, the server name will be used
		/// @param flags Message flags
		/// @param cmd The protocol command being sent
		/// @param params Message parameters
		template<typename... Args>
		void reply_msg(std::string_view label_id, client_ptr sptr, uint32_t flags, std::string_view cmd, Args&&... params);

		/// @brief Generate a formatted numeric message to the client.
		/// @param label_id Client's label identifier.
		/// @param n Numeric code to generate.
		/// @param args Additional formatting parameters
		///
		/// Numerics are generated via the table found in numeric.cpp
		/// Numerics that are disallowed for remote connections will not generate.
		template<typename... Args>
		void reply_numeric(std::string_view label_id, int n, Args&&... args);

		/// @brief Transmit message data to the client.
		/// @param sptr If present, specifies the message source; otherwise, the server name will be used
		/// @param flags Message flags
		/// @param cmd The protocol command being sent
		/// @param params Message parameters
		template<typename... Args>
		void write_msg(client_ptr sptr, uint32_t flags, std::string_view cmd, Args&&... params);

		/// @brief Transmit message data to the client.
		/// @param sptr If present, specifies the message source; otherwise, the server name will be used
		/// @param flags Message flags
		/// @param cmd The protocol command being sent
		/// @param query Results from a SQL query. All fields will be output.
		void write_msg(client_ptr sptr, uint32_t flags, std::string_view cmd, sqlite3xx::statement& query);

		/// @brief Transmit message data to the client.
		/// @param sptr If present, specifies the message source; otherwise, the server name will be used
		/// @param flags Message flags
		/// @param cmd The protocol command being sent
		/// @param va Variadic arguments from a Lua module
		void write_msg(client_ptr sptr, uint32_t flags, std::string_view cmd, sol::variadic_args va);

		/// @brief Generate a formatted numeric message to the client.
		/// @param n Numeric code to generate.
		/// @param args Additional formatting parameters
		///
		/// Numerics are generated via the table found in numeric.cpp
		/// Numerics that are disallowed for remote connections will not generate.
		template<typename... Args>
		void send_numeric(int n, Args&&... args);

		/// Generate a server notice to the client.
		void send_notice(std::string_view message);

		/// @brief Generate a formatted NOTICE message to the client.
		/// @param spec Format specification of the message
		/// @param args Additional formatting parameters
		///
		/// This method is typically used to generate server notices to a client.
		template<typename... Args>
		void send_notice(std::string_view spec, Args&&... args);

		/// @brief Update client's client modes.
		/// @param changes Mode string containing the requested changes
		/// @param from_user Indicates if the change is from a /MODE command
		void change_umodes(std::string_view changes, bool from_user);

		/// Determine if a usermode is set
		bool has_umode(uint32_t) const;

		/// Set one or more usermode masks
		void set_umode(uint32_t, bool);

		/// Update modes from USER command received by server link
		void set_umodes(uint32_t);

		/// Retrieve current mode mask
		uint32_t get_umodes() const;

		/// Change the visible hostname; this will also broadcast CHGHOST messages
		void set_hostname(std::string_view);

		/// Retrieve metadata item for this client
		sqlite3xx::column get_metadata(std::string_view key) const;

		/// Assign metadata item for this client
		void set_metadata(std::string_view key, std::string_view val);

		/// Determine if metadata is present
		bool has_metadata(std::string_view key) const;

		/// Determine if this client is the specified service type
		bool is_service(std::string_view what) const;

		/// Send an IRCv3 notification to other IRC users sharing a common channel.
		/// Notifications will only generate if the specified CAP is present.
		void cap_notify(int, message);

		/// Perform DNS lookup
		void do_host_resolve();

		/// Perform IDENT resolution
		void do_ident_resolve();

		/// Complete registration
		void do_register();

		/// Determine if SNOMASK is present
		inline bool has_snomask(uint16_t what) const { return (snomask_ & what) == what; }

		/// Assign value to a SNOMASK bit
		void set_snomask(uint16_t what, bool state);

		/// @brief Assign a SNOMASK value
		/// @param what Either in long form ('0000000000111010') or short form ("+du")
		/// @return Previous SNOMASK value
		uint16_t change_snomask(std::string_view what);

		/// @brief Retrieve the current SNOMASK in string format
		/// @param format '0' for long form, '1' for short form
		std::string get_snomask(int format) const;

		/// Retrieve the raw SNOMASK value
		uint16_t raw_snomask() const;

	private:
		session_ptr session_; /// Session handler
		boost::uuids::uuid uuid_;
		tcp::resolver resolver_; /// DNS resolver
		ident_handler ident_; /// IDENT resolver
		std::unique_ptr<asio::deadline_timer> timer_; /// Periodic keep-alive timer object
		uint64_t ts_; /// Assigned timestamp value
		std::string nick_, user_, host_, server_, acct_; /// Cache of common data fields
		uint64_t caps_; /// Negotiated IRCv3 capabilities (for users only)
		uint32_t umodes_; /// Currently assigned user modes
		uint32_t flags_; /// Currently assigned flags
		uint16_t snomask_; /// Current SNOMASK

		/// Periodic keep-alive
		void keepalive();

		/// Assign raw SNOMASK value
		uint16_t set_snowmask(uint16_t what);

		/// DNS hostname response
		void on_host_resolve(boost::system::error_code const& err, tcp::resolver::iterator result);

		/// DNSBL response
		void on_dnsbl_reply(boost::system::error_code const& err, tcp::resolver::iterator result);
	};

	constexpr size_t SNOMASK_STRLEN = sizeof(uint16_t) * 8;
};
