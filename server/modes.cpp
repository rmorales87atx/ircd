/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"

using ircd::irc_mode;

// The mode tables (chmodes/umodes) must be kept sorted
// in alphabetical order

namespace ircd {
	tsl::ordered_map<char, irc_mode> chmodes = {
	//	{ 'K', { MODE_CAT_D, _S("nfkc-text"), CMODE_NORM_NFKC } },
		{ 'b', { MODE_CAT_A, _S("ban"), CMODE_BAN } },
		{ 'e', { MODE_CAT_A, _S("exempt"), CMODE_EXEMPT } },
		{ 'i', { MODE_CAT_D, _S("invite-only"), CMODE_INVITE_ONLY } },
		{ 'k', { MODE_CAT_B, _S("key"), CMODE_KEY } },
		{ 'l', { MODE_CAT_C, _S("limit"), CMODE_LIMIT } },
		{ 'm', { MODE_CAT_D, _S("moderated"), CMODE_MODERATED } },
		{ 'n', { MODE_CAT_D, _S("no-external"), CMODE_NO_EXTERNAL } },
		{ 'o', { MODE_CAT_A, _S("op"), CMODE_OPERATOR } },
		{ 'r', { MODE_CAT_D, _S("registered"), CMODE_REGISTERED } },
		{ 's', { MODE_CAT_D, _S("secret"), CMODE_SECRET } },
		{ 't', { MODE_CAT_D, _S("topic-locked"), CMODE_TOPIC_LOCKED } },
		{ 'v', { MODE_CAT_A, _S("voice"), CMODE_VOICE } },
		{ 'x', { MODE_CAT_D, _S("opers-only"), CMODE_OPERS } },
	};

	tsl::ordered_map<char, irc_mode> umodes = {
		{ 'S', { MODE_CAT_D, _S("service"), UMODE_SERVICE } },
		{ 'a', { MODE_CAT_D, _S("away"), UMODE_AWAY } },
		{ 'b', { MODE_CAT_D, _S("bot"), UMODE_BOT } },
		{ 'd', { MODE_CAT_D, _S("deaf"), UMODE_DEAF } },
		{ 'h', { MODE_CAT_D, _S("helper"), UMODE_HELPER } },
		{ 'i', { MODE_CAT_D, _S("invisible"), UMODE_INVISIBLE } },
		{ 'o', { MODE_CAT_D, _S("oper"), UMODE_OPERATOR } },
		{ 's', { MODE_CAT_D, _S("server-notices"), UMODE_SNOTICES } },
		{ 'w', { MODE_CAT_D, _S("wallops"), UMODE_WALLOPS } },
		{ 'z', { MODE_CAT_D, _S("ssl"), UMODE_SECURE } },
	};
};

std::vector<ircd::mode_change_info> ircd::parse_chmode_change(std::string_view modes, std::vector<std::string> const& params, std::string* err)
{
	std::vector<mode_change_info> out;
	bool value = false;
	auto ip = std::begin(params);
	auto const end_param = std::end(params);
	for (auto cp = std::begin(modes); cp != std::end(modes); cp++) {
		if (*cp == '+' || *cp == '-') {
			value = *cp == '+';
			continue;
		}
		auto alpha = std::string{ *cp };
		auto chmode = find_chmode(*cp);
		if (!chmode) {
			if (err) err->append(alpha);
			continue;
		}
		switch (chmode->category) {
		case MODE_CAT_A:
		case MODE_CAT_B:
			out.emplace_back(mode_change_info{ alpha, chmode->mask, chmode->category, value, (ip != end_param) ? *ip++ : "" });
			break;
		case MODE_CAT_C:
			if (value) {
				out.emplace_back(mode_change_info{ alpha, chmode->mask, chmode->category, value, (ip != end_param) ? *ip++ : "" });
			} else {
				out.emplace_back(mode_change_info{ alpha, chmode->mask, chmode->category, value, "" });
			}
			break;
		case MODE_CAT_D:
			out.emplace_back(mode_change_info{ alpha, chmode->mask, chmode->category, value, "" });
			break;
		}
	}
	return out;
}

uint32_t ircd::new_umode(char chr, std::string_view desc)
{
	auto iter = umodes.find(chr);
	if (iter == umodes.end()) {
		uint32_t id = static_cast<uint32_t>(umodes.size()) + 1;
		if (id < (sizeof(uint32_t) * 8)) {
			umodes[chr] = { MODE_CAT_D, std::string(desc), static_cast<uint32_t>(1 << id) };
			return (1 << id);
		}
	} else {
		return iter->second.mask;
	}
	return 0;
}

uint32_t ircd::new_chmode(char chr, int category, std::string_view desc)
{
	auto iter = chmodes.find(chr);
	if (iter == chmodes.end()) {
		uint32_t id = static_cast<uint32_t>(chmodes.size()) + 1;
		if (id < (sizeof(uint32_t) * 8)) {
			chmodes[chr] = { category, std::string(desc), static_cast<uint32_t>(1 << id) };
			return (1 << id);
		}
	} else {
		return iter->second.mask;
	}
	return 0;
}

std::optional<irc_mode> ircd::find_chmode(char what)
{
	auto iter = chmodes.find(what);
	if (iter != chmodes.end()) {
		return iter->second;
	} else {
		return std::nullopt;
	}
}

std::optional<irc_mode> ircd::find_umode(char what)
{
	auto iter = umodes.find(what);
	if (iter != umodes.end()) {
		return iter->second;
	} else {
		return std::nullopt;
	}
}

void ircd::apply_umodes(std::string_view str, uint32_t& dest, std::string* err)
{
	bool state = false;
	auto data = normalize_nfkc(str);
	auto const end = utf8::iterator{ data.end(), data.begin(), data.end() };
	for (auto iter = utf8::iterator{ data.begin(), data.begin(), data.end() }; iter != end; ++iter) {
		auto const cp{ *iter };
		if (cp > 255) {
			if (err) utf8::append(cp, std::back_inserter(*err));
		} else if (cp == U'+' || cp == U'-') {
			state = (cp == U'+');
		} else if (auto mode = find_umode(static_cast<char>(cp))) {
			dest = state ? (dest | mode->mask) : (dest & ~mode->mask);
		} else if (!mode && err) {
			utf8::append(cp, std::back_inserter(*err));
		}
	}
}

std::string ircd::umode_str(uint32_t modes)
{
	std::string res{ _S("+") };
	for (auto [k, v] : umodes) {
		if (modes & v.mask) {
			utf8::append(k, std::back_inserter(res));
		}
	}
	std::sort(res.begin(), res.end());
	return normalize_nfkc(res);
}

std::string ircd::umode_diff(uint32_t cur, uint32_t prev)
{
	std::string res, add{ _S("+") }, rem{ _S("-") };
	for (auto [k, v] : umodes) {
		if ((cur & v.mask) && !(prev & v.mask)) {
			utf8::append(k, std::back_inserter(add));
		}
		if (!(cur & v.mask) && (prev & v.mask)) {
			utf8::append(k, std::back_inserter(rem));
		}
	}
	if (add.length() > 1) res.append(add);
	if (rem.length() > 1) res.append(rem);
	return res;
}

std::string ircd::chmode_str(int category, uint32_t modes)
{
	std::string res{ _S("+") };
	for (auto [k, v] : chmodes) {
		if ((modes & v.mask) && category == v.category) {
			utf8::append(k, std::back_inserter(res));
		}
	}
	std::sort(res.begin(), res.end());
	return normalize_nfkc(res);
}
