/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#pragma once

namespace ircd {
	/// Iterator range type.
	/// This is meant to support returning custom iterator types
	/// intended for use in for-each iteration loops.
	template<typename T>
	struct iterator_range {
		T begin_;
		T end_;
		T& begin() { return begin_; }
		T& end() { return end_; }
		T const& begin() const { return begin_; }
		T const& end() const { return end_; }
	};

	int compare_nfc(std::string_view left, std::string_view right);

	int compare_nfkc(std::string_view left, std::string_view right);

	int compare_text(std::string_view left, std::string_view right);

	bool same_text(std::string_view left, std::string_view right);

	std::string fold_case_nfc(std::string_view str);

	std::string fold_case_nfkc(std::string_view str);

	std::string normalize_nfc(std::string_view str);

	std::string normalize_nfkc(std::string_view str);

	std::string upper_case_nfc(std::string_view str);

	std::string upper_case_nfkc(std::string_view str);

	//! Create a formatted string.
	template<typename... Args>
	std::string format_str(std::string_view spec, Args&&... args)
	{
		using namespace boost::io;
		boost::format fmt{ std::string(spec) };
		fmt.exceptions(all_error_bits ^ (too_many_args_bit | too_few_args_bit));
		(fmt % ... % std::forward<Args>(args));
		return fmt.str();
	}

	//! Convert a number to base36.
	template<typename IntegerT = unsigned int>
	std::string to_base36(IntegerT val)
	{
		static char const base36[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		std::string result;
		do {
			result = base36[val % 36] + result;
		} while (val /= 36);
		return result;
	}

	/// Join elements of a given sequence.
	template<typename SequenceT>
	std::string join(SequenceT const& input, std::string_view strSep, size_t iStart = 0, size_t iStop = 0)
	{
		std::stringstream result;
		auto iter = input.begin();
		if (iStart > 0 && iStart < input.size()) {
			std::advance(iter, iStart);
		}
		auto stop = input.end();
		if (iStop > 0 && iStop < input.size()) {
			stop = input.begin();
			std::advance(stop, iStop);
		}
		if (iter != stop) {
			result << *iter;
			for (iter++; iter != stop; iter++) {
				result << strSep << *iter;
			}
		}
		return result.str();
	}

	/// Join elements of a given sequence.
	template<typename SequenceT>
	std::string join_pairs(SequenceT const& input, std::string_view elemSep, std::string_view valueSep, size_t iStart = 0, size_t iStop = 0)
	{
		std::stringstream result;
		auto iter = input.begin();
		if (iStart > 0 && iStart < input.size()) {
			std::advance(iter, iStart);
		}
		auto stop = input.end();
		if (iStop > 0 && iStop < input.size()) {
			stop = input.begin();
			std::advance(stop, iStop);
		}
		auto const append = [&]() {
			result << iter->first;
			if (!iter->second.empty()) {
				result << valueSep << iter->second;
			}
		};
		if (iter != stop) {
			append();
			for (iter++; iter != stop; iter++) {
				result << elemSep;
				append();
			}
		}
		return result.str();
	}

	/// Structure used for the ISUPPORT table.
	struct support_info {
		char const* label;
		std::function<std::string()> get_value;
	};

	/// Table for ISUPPORT.
	extern support_info const isupport_table[];

	/// Retrieve the ircd version as a string.
	std::string version_str();

	/// Returns the current UTC time.
	uint64_t timestamp();

	/// Convert UTF-8 input to a wide string.
	/// Results in UTF-16 on Windows, UTF-32 on all other platforms
	std::wstring str_widen(std::string_view);

	/// Convert wide string to UTF-8
	std::string str_narrow(std::wstring_view);

	/// Convert UTF-8 input to a wide string.
	/// Results in UTF-16 on Windows, UTF-32 on all other platforms
	template <typename iterator>
	std::wstring str_widen(iterator start, iterator end);

	/// Convert wide string to UTF-8
	/// On Windows the wide string must be UTF-16, and UTF-32 on other platforms
	template <typename iterator>
	std::string str_narrow(iterator start, iterator end);

	/// Determine if a string matches the given regular expression.
	bool strscan(std::string_view input, std::string_view pattern, std::vector<std::string>& matches);

	/// Determine if a string matches the given regular expression.
	bool strscan(std::string_view input, std::string_view pattern);

	/// Code returned by ircd::valid_mask()
	enum MASK_TYPES {
		MASK_INVALID = 0,
		MASK_HOST,     // hostname only (with wildcards); e.g. "*.some.network"
		MASK_USER,     // nick!user@host (with wildcards), nick part is optional; e.g. "*!bob@*.some.network"
		MASK_CIDR_V4,  // IPv4 address range; e.g. "127.0.0.1/8"
		MASK_CIDR_V6,  // IPv6 address range; e.g. "fe80:0000:0000:0000:0204:61ff:fe9d:f156/12"
	};

	/// Determine what type of mask is present
	int valid_mask(std::string_view what);

	/// Produce a "reversed" IPv4 address for use in a DNSBL query
	std::string reverse_ipv4(std::string_view);

	/// Determine if a UTF-8 string contains multi-byte codes.
	/// Returns 'true' or 'false', but DOES NOT validate the UTF-8 sequences.
	bool is_utf8_multibyte(std::string_view);

	/// Split a source string, generating tokens.
	///
	/// If either the input or delims contain multi-byte characters,
	/// all strings will be converted to UTF-16.
	void split_text(std::vector<std::string>& result, std::string_view input, std::string_view delims, std::optional<boost::empty_token_policy> empty_tokens);

	/// Match an expression against the given wildcard pattern.
	/// Both the expression and match strings are case-folded and normalized.
	///
	/// @param expr String containing the expression being matched.
	/// @param match String containing the match pattern.
	/// @param any_char An optional value specifying the character for matching any sequence of characters.
	///                 If not present (nullopt), the default is '*'.
	/// @param one_char An optional value specifying the character for matching a single character.
	///                 If not present (nullopt), the default is '?'.
	bool strmatch(std::string_view expr, std::string_view match, std::optional<char32_t> any_char = std::nullopt, std::optional<char32_t> one_char = std::nullopt);

	/// Generate name-based UUID using this IRC server's namespace.
	boost::uuids::uuid name_uuid(std::string_view name);

	/// Generate timestamp UUID.
	boost::uuids::uuid time_uuid();

	/// Generate completely random UUID.
	boost::uuids::uuid random_uuid();

	/// Generate UUID text string.
	std::string uuid_string(boost::uuids::uuid const&);

	/// Convert text to UUID value.
	boost::uuids::uuid uuid_from_str(std::string_view);
};
