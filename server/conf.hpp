/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#pragma once

namespace ircd {
	/// Configurable server limit
	struct limit_info {
		/// Identifier (see enum SERVER_LIMITS)
		int const id;

		/// config.lua identifier
		char const* lua_name;

		/// Module identifier
		char const* label;

		/// Minimum allowed value
		unsigned int min_value;

		/// Maximum allowed value
		unsigned int max_value;

		/// Default value (if the limit is not present in CONFIG.LUA)
		unsigned int default_value;
	};

	/// Table for config.lua limits
	extern limit_info const limit_table[];

	/// "utf8mapping" values
	enum {
		UTF8MAP_ASCII,
		UTF8MAP_RFC7700,
	};

	/// Configuration data for this IRC server.
	struct server_info {
		time_t create_time;
		std::string fullname; // server's FQDN hostname
		boost::uuids::uuid uuid_ns;
		std::string id;
		std::string description;
		std::string admin[3]; // name, description, email
		unsigned int max_clients;
		std::string net_name, net_description;
		unsigned int limits[NUM_LIMITS];
		std::string motd_user_text, motd_oper_text;
		uint32_t log_enabled;
		std::string dnsbl_name, dnsbl_ban_message;
		int utf8mapping;
		bool require_client_certificate;
		std::string kline_db_file;
	};

	/// Describes a config block, from config.lua
	struct conf_block {
		/// Name of block
		char const* name;

		/// Processing indicators
		int flag;

		/// Function that will process the block data
		std::function<void(sol::table&)> handler;
	};

	/// Status codes generated during REHASH
	enum RELOAD_FLAGS {
		RELOAD_AUTH = (1 << 0),
		RELOAD_OPERS = (1 << 1),
		RELOAD_JUPES = (1 << 2),
		RELOAD_SERVICES = (1 << 3),
		RELOAD_KLINES = (1 << 4),
		RELOAD_SERVERS = (1 << 5),
	};

	/// Class for processing configuration data
	class conf_reader {
	public:
		conf_reader(std::string_view src);
		void load(uint32_t flags);
	private:
		std::string source_;
		std::vector<conf_block> blocks_;
		sol::state lua_;
		std::unique_ptr<sqlite3xx::session> monitor_;
		uint32_t flags_;
		void block_function(conf_block& info, sol::table& data);
		void prep_rehash();
		void post_rehash();
		void load_motd_file(std::string& dest, std::string_view source);
		void read_server(sol::table&);
		void read_motd(sol::table&);
		void read_log(sol::table&);
		void read_ssl(sol::table&);
		void read_listen(sol::table&);
		void read_network(sol::table&);
		void read_admin(sol::table&);
		void read_link(sol::table&);
		void read_auth(sol::table&);
		void read_oper(sol::table&);
		void read_service(sol::table&);
		void read_kline(sol::table&);
		void read_jupe(sol::table&);
		void read_limits(sol::table&);
		void read_modules(sol::table&);
		void read_blacklist(sol::table&);
		void read_webirc(sol::table&);
	};
};
