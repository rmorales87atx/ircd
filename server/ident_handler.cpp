/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"

using boost::system::error_code;

ircd::ident_handler::ident_handler()
	: socket_{ io_ctx }
{}

void ircd::ident_handler::run(ircd::client_ptr cptr, handler_t handler)
{
	error_code ec;
	auto sptr = cptr->session();
	if (!sptr) return;
	handler_ = handler;
	local_ = sptr->local;
	remote_ = sptr->remote;
	if (!ec) {
		auto endpoints = { tcp::endpoint{ remote_.address(), 113 } };
		asio::async_connect(socket_, std::begin(endpoints), std::end(endpoints), [=](error_code const& err, auto iter) {
			on_connect(err);
		});
	}
}

void ircd::ident_handler::kill()
{
	asio::post(io_ctx, [this]() {
		error_code err;
		socket_.shutdown(tcp::socket::shutdown_both, err);
		socket_.close(err);
	});
}

void ircd::ident_handler::on_connect(error_code const& err)
{
	if (!err) {
		// connected to ident, send request
		std::string message = format_str("%1%,%2%\r\n", local_.port(), remote_.port());
		asio::async_write(socket_, asio::buffer(message.c_str(), message.length()), [this](error_code const& err, size_t len) {
			if (!err) {
				// wait for data
				asio::async_read_until(socket_, buf_, "\r\n", [=](error_code const& err, std::size_t nbytes) {
					on_read(err, len);
				});
			} else {
				// cancel operation
				kill();
				handler_(std::nullopt);
			}
		});
	} else {
		// cancel operation
		kill();
		handler_(std::nullopt);
	}
}

BOOST_FUSION_ADAPT_STRUCT(ircd::ident_response,
	server_port, client_port, os_name, user_id)

namespace parser {
	namespace x3 = boost::spirit::x3;
	namespace acs = boost::spirit::x3::ascii;
	using x3::rule;
	using x3::lexeme;
	using x3::eol;
	using x3::eoi;
	using acs::space;

	auto const servport = rule<struct servport_, int>{ "server_port" } = lexeme[ +x3::int_ ];
	auto const userport = rule<struct userport_, int>{ "client_port" } = lexeme[ +x3::int_ ];
	auto const os_name = rule<struct osname_, std::string>{ "os_name" } = lexeme[ +(acs::alnum | acs::punct) ];
	auto const user_id = rule<struct userid_, std::string>{ "user_id" } = lexeme[ +(acs::alnum | '.' | '-') ];

	auto const line = rule<struct line_, ircd::ident_response>{ "line" } =
		servport >> "," >> userport
		>> ":" >> "USERID" >> ":"
		>> os_name >> ":"
		>> user_id >> (eol | eoi)
		;
};

void ircd::ident_handler::on_read(error_code const& err, size_t len)
{
	// process ident response
	if (!err) {
		std::string line;
		std::istream is(&buf_);
		std::getline(is, line);
		ident_response res;
		if (parse(line, res)) {
			handler_(res.user_id);
		} else {
			handler_(std::nullopt);
		}
	} else {
		handler_(std::nullopt);
	}
	// close socket
	kill();
}

bool ircd::ident_handler::parse(std::string_view src, ident_response& res)
{
	auto iter = std::begin(src);
	auto end = std::end(src);
	return boost::spirit::x3::phrase_parse(iter, end, parser::line, parser::space, res)
		&& iter == end;
}
