/*
 * ircd - An IRCv3 Server
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"

using boost::system::error_code;
using boost::format;
using std::nullopt;

static int const KEEPALIVE_INTERVAL = 60;
static char const* CHAN_BROADCAST_SQL = u8"SELECT [channels].[Name] FROM [channel-users] JOIN [channels] ON [channels].[Name]=[channel-users].[ChannelName] WHERE [channel-users].[EID]=?";

ircd::client::client(session_ptr sptr)
	: uuid_{ time_uuid() }
	, session_{ sptr }
	, resolver_{ io_ctx }
	, ts_{ 0 }
	, snomask_{ 0 }
	, caps_{ 0 }
	, umodes_{ 0 }
	, flags_{ 0 }
{
	db.exec_dml("INSERT INTO [clients] ([EID], [SocketID], [Nick], [Username], [Hostname], [Gecos]) VALUES(?,?,null,null,null,null)", eid(), endpoint_id());
	db.exec_dml("INSERT INTO [client-stats] ([EID]) VALUES(?)", eid());
	set_flag(CLI_NO_IDENT, true);
	if (!has_flag(CLI_REMOTE)) {
		db.exec_dml("UPDATE [clients] SET [Address]=?2, [SID]=?3 WHERE [EID]=?1", eid(), address(), info.id);
		// keepalive timer
		timer_.reset(new asio::deadline_timer{ io_ctx, boost::posix_time::seconds(KEEPALIVE_INTERVAL) });
		timer_->async_wait([=](error_code const& e) { if (!e) keepalive(); });
	}
}

ircd::client::~client()
{
	try {
		close();
	} catch (...) {}
}

void ircd::client::set_flag(uint32_t mask, bool state)
{
	if (state) {
		flags_ |= mask;
	} else {
		flags_ &= ~mask;
	}
	// REMOTE and SERVICE exclude SERVER
	// SERVER excludes REGISTERED and SERVICE
	if ((flags_ & (CLI_REMOTE|CLI_SERVICE))) {
		flags_ &= ~CLI_SERVER;
	}
	if ((flags_ & CLI_SERVER)) {
		flags_ &= ~(CLI_REGISTERED | CLI_SERVICE);
	}
	// SYNC excludes EOB
	if ((flags_ & CLI_SYNC)) {
		flags_ &= ~CLI_EOB;
	}
	if ((flags_ & CLI_EOB)) {
		flags_ &= ~CLI_SYNC;
	}
	// once enabled, SSL can never be removed
	if (session_->ssl_socket && !(flags_ & CLI_SSL)) {
		flags_ |= CLI_SSL;
	}
	if (!session_->ssl_socket && (flags_ & CLI_SSL)) {
		flags_ &= ~CLI_SSL;
	}
	// update database
	db.exec_dml("UPDATE [clients] SET [Flags]=? WHERE [EID]=?", flags_, eid());
}

void ircd::client::set_cap(uint64_t mask, bool state)
{
	if (state) {
		caps_ |= mask;
	} else {
		caps_ &= ~mask;
	}
}

uint64_t ircd::client::timestamp() const
{
	return ts_;
}

void ircd::client::set_timestamp(uint64_t const what)
{
	ts_ = what;
	db.exec_dml("UPDATE [clients] SET [TS]=? WHERE [EID]=?", what, eid());
}

std::string ircd::client::address() const
{
	try {
		auto res = db.exec_scalar("SELECT [Address] FROM [clients] WHERE [EID]=?", eid());
		if (res.is_null()) {
			auto address = session_->address();
			std::string str = address.to_string();
			if (address.is_v6() && str[0] == ':') {
				str.insert(0, "0");
			}
			return str;
		} else {
			return res.get_text();
		}
	} catch (boost::system::system_error const&) {
		return u8"0.0.0.0";
	}
}

void ircd::client::set_address(std::string_view what)
{
	db.exec_dml("UPDATE [clients] SET [Address]=? WHERE [EID]=?", what, eid());
}

std::string ircd::client::hostname() const
{
	if (host_.empty()) {
		return address();
	} else {
		return host_;
	}
}

std::string ircd::client::nick() const
{
	if (!nick_.empty()) {
		return nick_;
	} else {
		return u8"*";
	}
}

void ircd::client::set_nick(std::string_view what)
{
	if (compare_nfkc(nick_, what) != 0) {
		auto self{ shared_from_this() };
		auto old_nick = nick();
		auto old_source = userhost();
		nick_ = normalize_nfkc(what);
		if (!has_flag(CLI_REMOTE)) ts_ = ircd::timestamp();
		// update database
		db.exec_dml("UPDATE [clients] SET [Nick]=?, [TS]=? WHERE [EID]=?", what, ts_, eid());
		if (has_flag(CLI_REGISTERED)) {
			auto msg = create_message(old_source, MSG_TRAILING_ARG, "NICK", what);
			if (has_flag(CLI_ACCOUNT)) {
				msg.tags["account"] = account();
			}
			if (!has_flag(CLI_REMOTE)) write_msg(msg);
			// send notification to channels
			for (auto stmt = db.exec_query(CHAN_BROADCAST_SQL, eid()); !stmt.eof(); stmt.step()) {
				auto chname = stmt("Name").get_text();
				for (auto member : channel::get_members(chname)) {
					if (member && !member->has_flag(CLI_REMOTE) && member->uuid() != this->uuid()) member->write_msg(msg);
				}
			}
			notify_modules("on_nick", self, old_nick);
		}
	}
}

std::string ircd::client::endpoint_id() const
{
	return (session_)? session_->endpoint_id() : "";
}

std::string ircd::client::username() const
{
	return user_;
}

void ircd::client::cap_notify(int cap, ircd::message msg)
{
	if (has_flag(CLI_REGISTERED)) {
		if (has_cap(CAP_ACCOUNT_TAG) && has_flag(CLI_ACCOUNT)) {
			msg.tags["account"] = account();
		}
		// push notification to everyone with a common channel
		for (auto stmt = db.exec_query(CHAN_BROADCAST_SQL, eid()); !stmt.eof(); stmt.step()) {
			auto chname = stmt("Name").get_text();
			for (auto member : channel::get_members(chname)) {
				if (!member || member->has_flag(CLI_REMOTE) || member->uuid() != this->uuid()) continue;
				if (member->has_cap(cap)) member->write_msg(msg);
			}
		}
	}
}

void ircd::client::set_username(std::string_view what)
{
	if (!has_flag(CLI_SERVER)) {
		auto self{ shared_from_this() };
		auto old_source = userhost();
		auto old_username = username();
		user_ = normalize_nfkc(what);
		if (has_flag(CLI_REGISTERED)) notify_modules("on_username", self, old_username);
		db.exec_dml("UPDATE [clients] SET [Username]=? WHERE [EID]=?", what, eid());
		cap_notify(CAP_CHGHOST, create_message(old_source, 0, "CHGHOST", username(), hostname()));
	}
}

std::string ircd::client::userhost() const
{
	if (!has_flag(CLI_SERVER)) {
		if (has_flag(CLI_REGISTERED)) {
			return format_str("%1%!%2%@%3%", nick(), username(), hostname());
		} else {
			return endpoint_id();
		}
	} else {
		return nick();
	}
}

std::string ircd::client::gecos() const
{
	return db.exec_scalar("SELECT [Gecos] FROM [clients] WHERE [EID]=?", eid()).get_text();
}

void ircd::client::set_gecos(std::string_view what)
{
	if (what.empty()) {
		db.exec_dml("UPDATE [clients] SET [Gecos]=NULL WHERE [EID]=?", eid());
	} else {
		db.exec_dml("UPDATE [clients] SET [Gecos]=? WHERE [EID]=?", normalize_nfc(what), eid());
	}
}

std::string ircd::client::account() const
{
	return acct_.empty() ? "*" : acct_;
}

void ircd::client::set_account(std::string_view what)
{
	acct_ = normalize_nfkc(what);
	if (what.empty() || (0 == what.compare("*"))) {
		set_flag(CLI_ACCOUNT, false);
		db.exec_dml("UPDATE [clients] SET [Account]=NULL WHERE [EID]=?", eid());
		cap_notify(CAP_ACCOUNT_NOTIFY, create_message(userhost(), 0, "ACCOUNT", "*"));
	} else {
		set_flag(CLI_ACCOUNT, true);
		db.exec_dml("UPDATE [clients] SET [Account]=? WHERE [EID]=?", what, eid());
		cap_notify(CAP_ACCOUNT_NOTIFY, create_message(userhost(), 0, "ACCOUNT", what));
	}
}

std::string ircd::client::server() const
{
	if (has_flag(CLI_SERVER)) {
		return nick();
	} else {
		return db.exec_scalar("SELECT [Server] FROM [users] WHERE [EID]=?", eid()).get_text();
	}
}

std::string ircd::client::eid() const
{
	return uuid_string(uuid_);
}

boost::uuids::uuid ircd::client::uuid() const
{
	return uuid_;
}

void ircd::client::set_eid(std::string_view what)
{
	std::string prev_id = eid();
	uuid_ = uuid_from_str(what);
	db.exec_dml("UPDATE [clients] SET [EID]=? WHERE [EID]=?", eid(), prev_id);
}

void ircd::client::send_notice(std::string_view message)
{
	if (has_flag(CLI_REMOTE)) {
		write_msg(nullptr, MSG_TRAILING_ARG, "NOTICE", eid(), message);
	} else {
		write_msg(nullptr, MSG_TRAILING_ARG, "NOTICE", nick(), message);
	}
}

void ircd::client::close()
{
	session_.reset();
	error_code ec;
	if (timer_) {
		timer_->cancel(ec);
		timer_.reset();
	}
	db.exec_dml("DELETE FROM [clients] WHERE [EID]=?", eid());
}

void ircd::client::keepalive()
{
	if (!session_ || has_flag(CLI_REMOTE)) return;

	// if we've been waiting all this time for an IDENT,
	// shut down the IDENT attempt and see if the client
	// is ready to be registered
	if (has_flag(CLI_RESOLVING_IDENT) && !has_flag(CLI_REGISTERED) && !has_flag(CLI_SERVER)) {
		set_flag(CLI_RESOLVING_IDENT, false);
		do_register();
		return;
	}

	// if the client isn't even registered by now, kick them out
	// otherwise we'll send a PING command to trigger the TCP keep-alive
	if (!has_flag(CLI_REGISTERED) && !has_flag(CLI_SERVER)) {
		snomask_log(LOG_USER, SNO_REJECT_CONN, "Rejecting connection from %1% (registration timeout)", userhost());
		kill_client(shared_from_this(), "Registration timeout");
		return;
	} else {
		bool send_ping = true;
		// if we recently received a command from the user, avoid sending PING
		if (has_metadata("last_command")) {
			auto last_cmd = get_metadata("last_command").get_uint64(0);
			auto idle = ircd::timestamp() - last_cmd;
			if (idle < KEEPALIVE_INTERVAL) send_ping = false;
		}
		if (send_ping) {
			write_msg(nullptr, MSG_TRAILING_ARG | MSG_NO_PREFIX, "PING", format_str("%1%", ircd::timestamp()));
		}
	}

	// reset timer
	if (timer_ && session_ && session_->socket.is_open()) {
		timer_->expires_from_now(boost::posix_time::seconds(KEEPALIVE_INTERVAL));
		timer_->async_wait([=](error_code const& e) { if (!e) keepalive(); });
	}
}

void ircd::client::change_umodes(std::string_view changes, bool from_user)
{
	auto prev = umodes_;
	auto cur = umodes_;
	std::string err;
	apply_umodes(changes, cur, &err);
	// allow modules to validate modes
	if (auto res = notify_modules("before_umode", shared_from_this(), cur, prev)) {
		cur = static_cast<uint32_t>(*res);
	}
	if (from_user) {
		// ahzS should not be changed
		for (int const mode : { UMODE_AWAY, UMODE_HELPER, UMODE_SERVICE, UMODE_SECURE }) {
			if ((cur & mode) != (prev & mode)) {
				if (mode == (prev & mode)) {
					cur |= mode;
				} else {
					cur &= ~mode;
				}
			}
		}
		// don't allow user to +o
		if (!(prev & UMODE_OPERATOR) && (cur & UMODE_OPERATOR)) {
			cur &= ~UMODE_OPERATOR;
		}
	}
	// +S implies +dio and excludes bh
	if ((cur & UMODE_SERVICE)) {
		cur |= UMODE_DEAF | UMODE_INVISIBLE | UMODE_OPERATOR;
		cur &= ~(UMODE_HELPER | UMODE_BOT);
	}
	if (!has_flag(CLI_REMOTE)) {
		// if gaining oper status, assign default SNOMASK
		if (!(prev & UMODE_OPERATOR) && (cur & UMODE_OPERATOR)) {
			snomask_ |= SNO_LOCAL_USER_CONN | SNO_OPER_STATUS | SNO_SERVICE_CONN | SNO_SERVER_LINK | SNO_REJECT_CONN | SNO_KILLS | SNO_NICK_JUPE | SNO_CHAN_JUPE;
			send_numeric(RPL_SNOMASK, nick(), get_snomask(1), snomask_);
		}
		// if losing oper status, revoke SNOMASKs for opers
		if ((prev & UMODE_OPERATOR) && !(cur & UMODE_OPERATOR)) {
			snomask_ &= ~(SNO_DEBUG | SNO_LOCAL_USER_CONN | SNO_REMOTE_USER_CONN | SNO_REJECT_CONN | SNO_STATS);
			send_numeric(RPL_SNOMASK, nick(), get_snomask(1), snomask_);
		}
	}
	// +s only allowed if a SNOMASK is present
	if ((cur & UMODE_SNOTICES) && snomask_ == 0) {
		cur &= ~UMODE_SNOTICES;
	}
	// if snomask is present, +s must be set
	if (!(cur & UMODE_SNOTICES) && snomask_ != 0) {
		cur |= UMODE_SNOTICES;
	}
	// update database
	umodes_ = cur;
	db.exec_dml("UPDATE [clients] SET [Mode]=? WHERE [EID]=?", umodes_, eid());
	if (session_ && !has_flag(CLI_REMOTE)) {
		// notify if there was unknown chars
		if (from_user) {
			for (auto const& ch : err) {
				send_numeric(ERR_UNKNOWNMODE, nick(), (char)ch);
			}
		}
		// send update to client
		if (prev != cur) {
			if (has_flag(CLI_REGISTERED)) {
				auto uptr{ shared_from_this() };
				auto str = umode_diff(umodes_, prev);
				write_msg(uptr, 0, "MODE", nick(), str);
				notify_servers(nullptr, uptr, 0, "MODE", nick(), str);
				if ((prev & UMODE_OPERATOR) && !(cur & UMODE_OPERATOR)) {
					snomask_log(LOG_OPER, SNO_OPER_STATUS, "%1% is no longer operator", userhost());
				}
			}
		} else if (from_user) {
			send_numeric(RPL_UMODE, nick(), umode_str(get_umodes()));
		}
	}
}

bool ircd::client::has_umode(uint32_t id) const
{
	return (umodes_ & id);
}

void ircd::client::set_umode(uint32_t m, bool state)
{
	if (state) {
		umodes_ |= m;
	} else {
		umodes_ &= ~m;
	}
	db.exec_dml("UPDATE [clients] SET [Mode]=? WHERE [EID]=?", umodes_, eid());
}

void ircd::client::set_umodes(uint32_t value)
{
	umodes_ = value;
	db.exec_dml("UPDATE [clients] SET [Mode]=? WHERE [EID]=?", value, eid());
}

uint32_t ircd::client::get_umodes() const
{
	return umodes_;
}

void ircd::client::set_hostname(std::string_view newhost)
{
	std::string old_source = userhost();
	host_ = normalize_nfkc(newhost);
	db.exec_dml("UPDATE [clients] SET [Hostname]=?1 WHERE [EID]=?2", newhost, eid());
	if (has_flag(CLI_REGISTERED)) cap_notify(CAP_CHGHOST, create_message(old_source, 0, "CHGHOST", username(), hostname()));
}

sqlite3xx::column ircd::client::get_metadata(std::string_view key) const
{
	return db.exec_scalar("SELECT [KeyValue] FROM [client-data] WHERE [EID]=? AND [KeyName]=?", eid(), fold_case_nfkc(key));
}

bool ircd::client::has_metadata(std::string_view key) const
{
	return !get_metadata(key).is_null();
}

bool ircd::client::is_service(std::string_view what) const
{
	if (has_flag(CLI_SERVICE)) {
		auto type = get_metadata("service").get_text();
		return same_text(what, type);
	} else {
		return false;
	}
}

void ircd::client::set_metadata(std::string_view key, std::string_view val)
{
	if (!session_) return;
	if (val.empty()) {
		db.exec_dml("DELETE FROM [client-data] WHERE [EID]=?1 AND [KeyName]=?2", eid(), fold_case_nfkc(key));
	} else {
		db.exec_dml("INSERT OR REPLACE INTO [client-data] ([EID], [KeyName], [KeyValue]) VALUES(?,?,?)", eid(), fold_case_nfkc(key), normalize_nfc(val));
	}
}

void ircd::client::do_host_resolve()
{
	using namespace std::placeholders;
	if (!session_ || !session_->socket.is_open()) return;
	if (has_flag(CLI_RESOLVING_DNS) || has_flag(CLI_DONE_DNS)) return;
	// start DNS resolution for the client's address
	// also checking the DNSBL if present
	try {
		set_flag(CLI_RESOLVING_DNS, true);
		if (!session_ || !session_->socket.is_open()) return;
		auto const& endpt{ session_->remote };
		if (!info.dnsbl_name.empty() && endpt.address().is_v4()) {
			error_code ec;
			auto query = reverse_ipv4(endpt.address().to_string(ec)) + "." + info.dnsbl_name;
			resolver_.async_resolve(query, "", std::bind(&ircd::client::on_dnsbl_reply, this, _1, _2));
		} else {
			resolver_.async_resolve(endpt, std::bind(&ircd::client::on_host_resolve, this, _1, _2));
		}
	} catch (std::exception const&) {
		set_flag(CLI_RESOLVING_DNS, false);
		set_hostname(address());
	}
}

void ircd::client::on_host_resolve(boost::system::error_code const& err, tcp::resolver::iterator result)
{
	if (!session_ || !session_->socket.is_open()) return;
	set_flag(CLI_RESOLVING_DNS, false);
	set_flag(CLI_DONE_DNS, true);
	if (has_flag(CLI_SERVER)) return;
	auto const rhost{ result->host_name() };
	if (err) {
		set_hostname(address());
		write_log(LOG_USER, nullptr, "Unable to lookup hostname for %1% (%2%)", endpoint_id(), err.message());
	} else if (same_text(address(), rhost) || rhost.empty()) {
		set_hostname(address());
		write_log(LOG_USER, nullptr, "No hostname for %1%", endpoint_id());
	} else {
		write_log(LOG_USER, nullptr, "Found hostname for %1%: %2%", endpoint_id(), rhost);
		set_hostname(result->host_name());
	}
	do_register(); // if registration is just waiting for DNS, complete it
}

void ircd::client::on_dnsbl_reply(boost::system::error_code const& err, tcp::resolver::iterator result)
{
	using namespace std::placeholders;
	if (!session_ || !session_->socket.is_open()) return;
	if (err) {
		// not in the DNSBL
		error_code ec;
		resolver_.async_resolve(session_->remote, std::bind(&ircd::client::on_host_resolve, this, _1, _2));
	} else {
		auto self{ shared_from_this() };
		auto reply{ result->endpoint().address().to_string() };
		std::string messg{ format_str(info.dnsbl_ban_message, address()) };
		add_kline(nullptr, address(), format_str("DNSBL response <%1%>", reply));
		snomask_log(LOG_USER, SNO_REJECT_CONN, "Rejecting connection from %1% on port %2%: DNSBL response <%3%>",
			endpoint_id(), session_->local.port(), reply);
		post([self, messg]() { kill_client(self, messg); });
	}
}

void ircd::client::do_ident_resolve()
{
	if (has_flag(CLI_RESOLVING_IDENT) || has_flag(CLI_DONE_IDENT)) return;
	if (!session_ || !session_->socket.is_open()) return;
	// start ident resolution
	set_flag(CLI_RESOLVING_IDENT, true);
	auto self{ shared_from_this() };
	ident_.run(self, [this, self](std::optional<std::string> result) {
		if (!session_ || !session_->socket.is_open()) return;
		set_flag(CLI_DONE_IDENT, true);
		if (has_flag(CLI_SERVER)) {
			set_flag(CLI_RESOLVING_IDENT, false);
			return;
		}
		if (has_flag(CLI_RESOLVING_IDENT)) {
			set_flag(CLI_RESOLVING_IDENT, false);
			if (result) {
				set_flag(CLI_NO_IDENT, false);
				set_username(*result);
				write_log(LOG_USER, nullptr, "Received ident response from %1%: %2%", endpoint_id(), *result);
			} else {
				set_flag(CLI_NO_IDENT, true);
				write_log(LOG_USER, nullptr, "No ident response from %1%", endpoint_id());
			}
			do_register(); // continue registration if we were only waiting for IDENT
		}
	});
}

void ircd::client::do_register()
{
	if (!session_ || !session_->socket.is_open()) return;

	auto self{ shared_from_this() };

	// only finalize the registration if:
	// 1) the client is not already registered (duh),
	// 2) we're done negotiating CAPs, and
	// 3) we got the USER/NICK (or SERVICE) commands
	// 4) DNS/IDENT resolution is done

	// reject if the user@host matches a K-line
	if (!has_flag(CLI_REMOTE) && is_user_kline(self)) {
		snomask_log(LOG_USER, SNO_KILLS, "K-line active for %1%, rejecting connection", userhost());
		kill_client(self, "K-lined");
		return;
	}

	if (has_flag(CLI_RESOLVING_DNS) || has_flag(CLI_RESOLVING_IDENT) || has_flag(CLI_NEGOTIATING_CAPS)) {
		return; // keep waiting
	}

	if (!has_flag(CLI_REGISTERED) && ((!username().empty() && (nick() != "*")) || has_flag(CLI_SERVICE))) {
		// check for authorization
		auto pwcheck = picosha2::hash256_hex_string(get_metadata("recv_pass").get_text());
		auto record = find_auth(self);
		if (!record || record->eof()) {
			snomask_log(LOG_USER, SNO_REJECT_CONN, "Rejecting connection from %1% (host is not authorized)", endpoint_id());
			kill_client(self, "Unauthorized");
		} else if (has_flag(CLI_NO_IDENT) && 1 == record->field_value("require_ident").get_int(0)) {
			snomask_log(LOG_USER, SNO_REJECT_CONN, "Rejecting connection from %1% (no ident)", endpoint_id());
			kill_client(self, "Unauthorized");
		} else if (!record->is_field_null("password") && compare_nfc(pwcheck, record->field_value("password").get_text()) != 0) {
			snomask_log(LOG_USER, SNO_REJECT_CONN, "Rejecting connection from %1% (wrong password)", endpoint_id());
			kill_client(self, "Unauthorized");
		} else {
			// continue registration
			set_timestamp(ircd::timestamp());
			std::string const ts_str{ std::to_string(timestamp()) };
			if (has_flag(CLI_SSL)) change_umodes("+z", false);
			if (has_flag(CLI_SERVICE)) {
				auto type = get_metadata("service").get_text();
				change_umodes("+S", false);
			} else {
				// apply the spoof if it's set
				if (!record->is_field_null("spoof")) {
					set_hostname(record->field_value("spoof").get_text());
				}
				// adjust flags
				set_umode(UMODE_INVISIBLE, true);
			}
			db.exec_dml("DELETE FROM [connect-attempts] WHERE [addr]=?", address());
			set_flag(CLI_REGISTERED, true);
			snomask_log(LOG_USER, SNO_LOCAL_USER_CONN, "Client connecting on port %2%: %1%",
				userhost(), session_->local.port());
			// generate strings for RPL_MYINFO
			std::string umodes, chmodes, chmode_params;
			for (auto [id, info] : ircd::umodes) {
				umodes.push_back(id);
			}
			std::sort(umodes.begin(), umodes.end());
			for (auto [id, info] : ircd::chmodes) {
				chmodes.push_back(id);
				if (info.category < MODE_CAT_D) chmode_params.push_back(id);
			}
			std::sort(chmodes.begin(), chmodes.end());
			std::sort(chmode_params.begin(), chmode_params.end());
			// send the welcome messages
			send_numeric(RPL_WELCOME, nick(), userhost());
			send_numeric(RPL_YOURHOST, nick(), info.fullname, version_str());
			send_numeric(RPL_CREATED, nick(), id_buildtime());
			send_numeric(RPL_MYINFO, nick(), info.fullname, version_str(), umodes, chmodes, chmode_params);
			send_isupport(self);
			send_numeric(RPL_UMODE, nick(), umode_str(get_umodes()));
			if (has_flag(CLI_SERVICE)) {
				send_numeric(RPL_SERVICE, nick(), get_metadata("service"));
			}
			do_command(self, "LUSERS");
			do_command(self, "MOTD");
			notify_modules("on_user", self);
		}
	}
}
