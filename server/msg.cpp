/*
 * ircd - An IRCv3 Server
 * Copyright © 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"

/*
 *	 <message>       ::= ['@' <tags> <SPACE>] [':' <prefix> <SPACE> ] <command> <params> <crlf>
 *	 <tags>          ::= <tag> [';' <tag>]*
 *	 <tag>           ::= <key> ['=' <escaped_value>]
 *	 <key>           ::= [ <client_prefix> ] [ <vendor> '/' ] <key_name>
 *	 <client_prefix> ::= '+'
 *	 <key_name>      ::= <non-empty sequence of ascii letters, digits, hyphens ('-')>
 *	 <escaped_value> ::= <sequence of zero or more utf8 characters except NUL, CR, LF, semicolon (`;`) and SPACE>
 *	 <vendor>        ::= <host>
 */

/// Convert spirit Unicode token back to UTF8
template<typename T>
std::string conv_u32(T const& src)
{
	std::string res;
	res.reserve(src.size());
	utf8::utf32to8(std::begin(src), std::end(src), std::back_inserter(res));
	return std::move(res);
}

/// Convert a container of Unicode tokens to UTF8
template<typename T>
void conv_u32_args(T const& src, std::vector<std::string>& out)
{
	out.reserve(src.size());
	for (auto const& arg : src) {
		out.push_back(conv_u32(arg));
	}
}

/// Convert a map of Unicode tokens to UTF8
template<typename T>
void conv_u32_tags(T const& src, tsl::ordered_map<std::string, std::string>& out)
{
	for (auto const& arg : src) {
		out.insert_or_assign(conv_u32(arg.first), conv_u32(arg.second));
	}
}

namespace parser {
	namespace x3 = boost::spirit::x3;
	using x3::rule;
	using x3::lit;
	using x3::eol;
	using x3::eoi;
	using x3::unicode::char_;
	using x3::unicode::space;
	using std::u32string;

	auto set_prefix = [](auto& ctx) { _val(ctx).prefix = conv_u32(_attr(ctx)); };
	auto set_command = [](auto& ctx) { _val(ctx).command = conv_u32(_attr(ctx)); };
	auto set_args = [](auto& ctx) { conv_u32_args(_attr(ctx), _val(ctx).args); };
	auto set_trail = [](auto& ctx) { _val(ctx).flags |= ircd::MSG_TRAILING_ARG; _val(ctx).args.push_back(conv_u32(_attr(ctx))); };
	auto set_tags = [](auto& ctx) { conv_u32_tags(_attr(ctx), _val(ctx).tags); };

	auto const token = rule<struct token_, u32string>{ "token" } = ~char_(" :\r\n") >> *~char_(" \r\n");
	auto const command = token[set_command];
	auto const params = (token % ' ')[set_args];
	auto const prefix = ':' >> (token)[set_prefix];
	auto const trail = ':' >> (*~char_("\r\n"))[set_trail];

	auto const tag_name = rule<struct tag_key_, u32string>{ "tag_name" } = +(x3::ascii::alnum | char_('+') | char_('/') | char_('-') | char_('.'));
	auto const tag_value = rule<struct tag_val_, u32string>{ "tag_value" } = *~char_(" =;\r\n");
	auto const tag_pair = rule<struct tag_par_, std::pair<u32string, u32string>>{ "tag_pair" } = tag_name >> -('=' >> tag_value);
	auto const tag_pairs = rule<struct tag_prs_, tsl::ordered_map<u32string, u32string>>{ "tag_pairs" } = tag_pair % ';';
	auto const tags = x3::omit['@'] >> tag_pairs[set_tags];

	auto const line = rule<class line, ircd::message>{ "line" }
		=  -(tags >> space)
		>> -(prefix >> space)
		>> command >> -space
		>> *params
		>> -((space >> trail) | trail)
		>> (eol | eoi);
};

std::optional<ircd::message> ircd::tokenize(std::string_view data)
{
	if (!data.empty() && utf8::is_valid(data.begin(), data.end())) {
		std::string text;
		if (info.utf8mapping != UTF8MAP_ASCII) {
			text = normalize_nfc(data);
		} else {
			text = data;
		}
		ircd::message msg;
		msg.flags = 0;
		auto iter = utf8::iterator{ text.begin(), text.begin(), text.end() };
		auto const end = utf8::iterator{ text.end(), text.begin(), text.end() };
		bool r = boost::spirit::x3::parse(iter, end, parser::line, msg);
		if (r && iter == end) {
			msg.text = text;
			msg.command = upper_case_nfkc(msg.command);
			if (!msg.prefix.empty()) msg.prefix = normalize_nfkc(msg.prefix);
			return msg;
		}
	}
	return std::nullopt;
}
