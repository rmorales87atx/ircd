/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"

// SQL text used to define the internal tables
// used for server config & IRC session data
static std::string const sql_init = R"(
-- data tables

CREATE TABLE [clients] (
  [EID] TEXT UNIQUE COLLATE BINARY,
  [SID] TEXT COLLATE BINARY,
  [SocketID] TEXT COLLATE BINARY,
  [Nick] TEXT COLLATE NOCASE,
  [TS] INTEGER,
  [UserName] TEXT COLLATE NOCASE,
  [Hostname] TEXT COLLATE NOCASE,
  [Address] TEXT COLLATE NOCASE,
  [Account] TEXT COLLATE NOCASE,
  [Mode] INTEGER NOT NULL DEFAULT 0,
  [Gecos] TEXT COLLATE NOCASE,
  [Flags] INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY([EID]));

CREATE VIEW users AS
SELECT
  [EID],
  [clients].[SID],
  [Nick],
  [TS],
  [UserName],
  [HostName],
  ([Nick] || '!' || [UserName] || '@' || [HostName]) AS [UserHost],
  [Address],
  [Account],
  [Mode],
  [Flags],
  [link-status].[name] AS [Server],
  [Gecos],
  (SELECT MIN([auth].[id]) FROM [auth] WHERE (([clients].[UserName] || '@' || [clients].[Address]) GLOB [auth].[user]) OR (([clients].[UserName] || '@' || [clients].[Hostname]) GLOB [auth].[user])) AS [Auth_RowID]
FROM [clients]
JOIN [link-status] ON [link-status].[SID] = [clients].[SID]
WHERE [Nick] IS NOT NULL
  AND [UserName] IS NOT NULL
  AND [HostName] IS NOT NULL
  AND [clients].[SID] IS NOT NULL;

CREATE VIEW unknown AS
SELECT
  [EID],
  [clients].[SID],
  [HostName],
  [Address],
  [Mode],
  [Flags],
  [link-status].[name] AS [Server],
  (SELECT MIN([auth].[id]) FROM [auth] WHERE (([clients].[UserName] || '@' || [clients].[Address]) GLOB [auth].[user]) OR (([clients].[UserName] || '@' || [clients].[Hostname]) GLOB [auth].[user])) AS [Auth_RowID]
FROM [clients]
JOIN [link-status] ON [link-status].[SID] = [clients].[SID]
WHERE [Nick] IS NULL AND [clients].[SID] IS NOT NULL;

CREATE VIEW servers AS
SELECT
  [EID],
  [Nick],
  [TS],
  [Address],
  [Mode],
  [Flags],
  [Gecos]
FROM [clients]
JOIN [link-status] ON [link-status].[name] = [clients].[Nick];

CREATE TABLE [client-stats](
  [EID] TEXT COLLATE BINARY REFERENCES [clients]([EID]) ON UPDATE CASCADE ON DELETE CASCADE,
  [RecvBytes] INTEGER DEFAULT 0,
  [SendBytes] INTEGER DEFAULT 0,
  [RecvCount] INTEGER DEFAULT 0,
  [SendCount] INTEGER DEFAULT 0,
  PRIMARY KEY([EID]));

CREATE VIEW [link-stats] AS
SELECT
  [clients].[Nick],
  [SendCount],
  [SendBytes],
  [RecvCount],
  [RecvBytes],
  timestamp()-[clients].[TS] AS [TimeOpen]
FROM [client-stats]
JOIN [clients] ON [clients].[EID] = [client-stats].[EID]
WHERE [clients].[Nick] IS NOT NULL;

CREATE TABLE [client-data] (
  [EID] TEXT COLLATE BINARY REFERENCES [clients]([EID]) ON DELETE CASCADE ON UPDATE CASCADE,
  [KeyName] TEXT COLLATE NOCASE,
  [KeyValue],
  PRIMARY KEY([EID],[KeyName]));

CREATE TABLE [channels] (
  [Name] TEXT PRIMARY KEY COLLATE NOCASE,
  [TS] INTEGER,
  [Topic] TEXT COLLATE NOCASE,
  [TopicTime] INTEGER,
  [TopicSetBy] TEXT COLLATE NOCASE,
  [Modes] INTEGER NOT NULL DEFAULT 0,
  [Key] TEXT COLLATE UTF8,
  [Limit] INTEGER);

CREATE TABLE [channel-users] (
  [ChannelName] TEXT COLLATE NOCASE REFERENCES [channels]([Name]) ON DELETE CASCADE ON UPDATE CASCADE,
  [EID] TEXT COLLATE BINARY REFERENCES [clients]([EID]) ON DELETE CASCADE ON UPDATE CASCADE,
  [Status] INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY([ChannelName], [EID]));

CREATE TABLE [channel-masks] (
  [ChannelName] TEXT COLLATE NOCASE REFERENCES [channels]([Name]) ON DELETE CASCADE ON UPDATE CASCADE,
  [ListID] INTEGER,
  [Mask] TEXT COLLATE NOCASE,
  [Creator] TEXT COLLATE NOCASE,
  [TS] INTEGER,
  [MaskType] INTEGER,
  PRIMARY KEY([ChannelName],[ListID],[Mask]));

CREATE TABLE [command-stats] (
  [Command] TEXT COLLATE NOCASE PRIMARY KEY,
  [UseCount] INTEGER,
  [ByteCount] INTEGER,
  [RemoteCount] INTEGER);

CREATE TABLE [link-status](
  [name] TEXT COLLATE NOCASE PRIMARY KEY,
  [SID] TEXT COLLATE NOCASE,
  [status] INTEGER DEFAULT 0,
  [last_connect] INTEGER DEFAULT 0,
  [last_error] TEXT COLLATE NOCASE);

CREATE TABLE [connect-attempts](
  [addr] TEXT COLLATE NOCASE,
  [ts] INTEGER,
  [count] INTEGER,
  PRIMARY KEY([addr], [ts]));

CREATE VIEW IF NOT EXISTS who_view AS
	SELECT
	  [users].[EID],
	  [users].[Mode],
	  [users].[Nick],
	  [users].[UserName],
	  [users].[Hostname],
	  [users].[Address],
	  [users].[Server],
	  [users].[Gecos],
	  [users].[Account],
	  [channel-users].[ChannelName],
	  [channel-users].[Status]
	FROM [users] LEFT JOIN [channel-users] ON [channel-users].[EID]=users.[EID];

-- config tables

CREATE TABLE [auth](
  [id] INTEGER PRIMARY KEY,
  [user] TEXT UNIQUE COLLATE NOCASE,
  [password] TEXT COLLATE UTF8,
  [spoof] TEXT COLLATE NOCASE,
  [incomplete_limit] INTEGER,
  [check_ident] INTEGER,
  [require_ident] INTEGER,
  CONSTRAINT "check_ident" CHECK([check_ident]=0 OR [check_ident]=1),
  CONSTRAINT "require_ident" CHECK([require_ident]=0 OR ([require_ident]=1 AND [check_ident]=1)));

CREATE TABLE [jupe](
-- caution: do not rearrange columns on this table
  [name] TEXT PRIMARY KEY COLLATE NOCASE,
  [type] INTEGER,
  [status] INTEGER DEFAULT 0,
  [source] TEXT COLLATE NOCASE,
  [time] INTEGER,
  [reason] TEXT COLLATE UTF8);

CREATE TABLE [operators](
  [name] TEXT PRIMARY KEY UNIQUE COLLATE NOCASE,
  [password] TEXT COLLATE UTF8,
  [ssl_required] INTEGER DEFAULT 0);

CREATE TABLE [oper-hosts](
  [name] TEXT COLLATE NOCASE REFERENCES [operators]([name]) ON DELETE CASCADE ON UPDATE CASCADE,
  [hostmask] TEXT NOT NULL COLLATE NOCASE,
  PRIMARY KEY([name], [hostmask]));

CREATE TABLE [oper-certs](
  [name] TEXT COLLATE NOCASE REFERENCES [operators]([name]) ON DELETE CASCADE ON UPDATE CASCADE,
  [fingerprint] TEXT NOT NULL COLLATE NOCASE,
  PRIMARY KEY([name], [fingerprint]));

CREATE TABLE [services](
  [name] TEXT PRIMARY KEY UNIQUE COLLATE NOCASE,
  [password] TEXT COLLATE UTF8 NOT NULL,
  [spoof] TEXT COLLATE NOCASE,
  [type] TEXT NOT NULL COLLATE NOCASE,
  [ssl_required] INTEGER DEFAULT 0);

CREATE TABLE [service-certs](
  [name] TEXT COLLATE NOCASE REFERENCES [services]([name]) ON DELETE CASCADE ON UPDATE CASCADE,
  [fingerprint] TEXT NOT NULL COLLATE NOCASE,
  PRIMARY KEY([name], [fingerprint]));

CREATE TABLE [server-bans](
-- caution: do not rearrange columns on this table
  [status] INTEGER DEFAULT 0,
  [mask] TEXT COLLATE NOCASE,
  [type] INTEGER,
  [created] INTEGER,
  [source] TEXT COLLATE NOCASE,
  [comment] TEXT COLLATE NOCASE,
  [expire] INTEGER,
  PRIMARY KEY([mask]));

CREATE TABLE [server-links](
  [address] TEXT COLLATE NOCASE,
  [name] TEXT COLLATE NOCASE,
  [password] TEXT COLLATE UTF8,
  [ssl_required] INTEGER DEFAULT 0,
  PRIMARY KEY([address], [name]));

CREATE TABLE [server-connects](
  [name] TEXT COLLATE NOCASE,
  [host] TEXT COLLATE NOCASE,
  [port] INTEGER,
  [frequency] INTEGER DEFAULT 0,
  [ssl_required] INTEGER DEFAULT 0,
  PRIMARY KEY([name], [host], [port]));

CREATE TABLE [webirc-gateways](
  [host] TEXT COLLATE NOCASE,
  [password] TEXT COLLATE UTF8,
  [description] TEXT COLLATE NOCASE,
  PRIMARY KEY([host]));
)";

using namespace std::placeholders;

void ircd::init_db()
{
	db.set_error_mask(sqlite3xx::all_db_errors, true);
	db.open(":memory:");
	db.create_collation("UTF8", std::bind(&compare_nfkc, _1, _2));
	db.create_collation("NOCASE", std::bind(&compare_text, _1, _2));
	// sha256() function to generate hash values in SQL
	db.create_function("sha256", 1, [](sqlite3xx::context& ctx, std::vector<sqlite3xx::value> const& args) {
		if (args[0].is_null()) {
			ctx.no_result();
		} else {
			std::string src = args[0].get_text();
			std::string result = picosha2::hash256_hex_string(src);
			ctx.set_result(result);
		}
	});
	// timestamp() SQL function returns UTC timestamp
	db.create_function("timestamp", 0, [](sqlite3xx::context& ctx, std::vector<sqlite3xx::value> const& args) {
		ctx.set_result(ircd::timestamp());
	});
	// choose(n, ...) SQL function returns value based on the provided index
	db.create_function("choose", -1, [](sqlite3xx::context& ctx, std::vector<sqlite3xx::value> const& args) {
		if (args.size() < 2) {
			ctx.set_error("not enough parameters (at least 2)");
		} else if (args[0].is_null()) {
			ctx.set_error("index parameter is null");
		} else if (args[0].numeric_type() != SQLITE_INTEGER) {
			ctx.set_error("index parameter must be numeric");
		} else {
			size_t index = args[0].get_uint(0);
			if (++index > args.size()) {
				ctx.no_result();
			} else {
				ctx.set_result(args[index]);
			}
		}
	});
	// cidr_match(net, addr) SQL function to determine if addr is in the specified net
	// net and addr must match the same IP type (v4 or v6)
	db.create_function("cidr_match", 2, [](sqlite3xx::context& ctx, std::vector<sqlite3xx::value> const& args) {
		auto net_str = args[0].get_text();
		auto addr_str = args[1].get_text();
		try {
			if (valid_mask(net_str) == MASK_CIDR_V4) {
				auto net = asio::ip::make_network_v4(net_str);
				auto hosts = net.hosts();
				ctx.set_result(hosts.find(asio::ip::make_address_v4(addr_str)) != hosts.end());
			} else if (valid_mask(net_str) == MASK_CIDR_V6) {
				auto net = asio::ip::make_network_v6(net_str);
				auto hosts = net.hosts();
				ctx.set_result(hosts.find(asio::ip::make_address_v6(addr_str)) != hosts.end());
			}
		} catch (std::exception const&) {
			ctx.no_result();
		}
	});
	// channel member status text
	db.create_function("channel_status_text", 1, [](sqlite3xx::context& ctx, std::vector<sqlite3xx::value> const& args) {
		if (args[0].is_null()) {
			ctx.no_result();
		} else {
			unsigned int stat = args[0].get_uint(0);
			std::string ret;
			if (stat & CSTATUS_OP) ret.append("@");
			if (stat & CSTATUS_VOICE) ret.append("+");
			ctx.set_result(ret);
		}
	});
	// normalize text to NFC
	db.create_function("NFC", 1, [](sqlite3xx::context& ctx, std::vector<sqlite3xx::value> const& args) {
		if (args[0].is_null()) {
			ctx.no_result();
		} else {
			ctx.set_result(normalize_nfc(args[0].get_text()));
		}
	});
	// normalize text to NFKC
	db.create_function("NFKC", 1, [](sqlite3xx::context& ctx, std::vector<sqlite3xx::value> const& args) {
		if (args[0].is_null()) {
			ctx.no_result();
		} else {
			ctx.set_result(normalize_nfkc(args[0].get_text()));
		}
	});
	// fold case, and normalize text to NFC
	db.create_function("fold_nfc", 1, [](sqlite3xx::context& ctx, std::vector<sqlite3xx::value> const& args) {
		if (args[0].is_null()) {
			ctx.no_result();
		} else {
			ctx.set_result(fold_case_nfc(args[0].get_text()));
		}
	});
	// fold case, and normalize text to NFKC
	db.create_function("fold_nfkc", 1, [](sqlite3xx::context& ctx, std::vector<sqlite3xx::value> const& args) {
		if (args[0].is_null()) {
			ctx.no_result();
		} else {
			ctx.set_result(fold_case_nfkc(args[0].get_text()));
		}
	});
	// override glob() function for unicode input
	db.create_function("glob", 2, [](sqlite3xx::context& ctx, std::vector<sqlite3xx::value> const& args) {
		ctx.set_result(strmatch(args[1].get_text(), args[0].get_text()));
	});
	// override like() function for unicode input
	db.create_function("like", 2, [](sqlite3xx::context& ctx, std::vector<sqlite3xx::value> const& args) {
		ctx.set_result(strmatch(args[1].get_text(), args[0].get_text(), '%', '_'));
	});
	// function to generate UUIDs
	db.create_function("name_uuid", 1, [](sqlite3xx::context& ctx, std::vector<sqlite3xx::value> const& args) {
		ctx.set_result(uuid_string(name_uuid(args[0].get_text())));
	});
	// function to generate UUIDs
	db.create_function("time_uuid", 0, [](sqlite3xx::context& ctx, std::vector<sqlite3xx::value> const& args) {
		ctx.set_result(uuid_string(time_uuid()));
	});
	// initialize tables
	db.exec_dml(sql_init);
}
