//
// SQLite3++ (sqlite3xx.hpp)
//
// Based on code developed by NeoSmart Technologies: https://github.com/neosmart/CppSQLite
// and Rob Groves <rob.groves@btinternet.com>

#include <cstdint>
#include <cstring>
#include <memory>
#include <stdexcept>
#include <ostream>
#include "SQLite3xx.hpp"
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#if defined(SQLITE_ENABLE_SESSION) && defined(SQLITE_ENABLE_PREUPDATE_HOOK)

sqlite3xx::changeset::changeset(database const& db, int n, void* p)
	: size_{ n }
	, data_{ p }
	, db_{ db }
	, sql_{ db.handle() }
{}

sqlite3xx::changeset::changeset(changeset&& cs) noexcept
	: size_{ cs.size_ }
	, data_{ std::move(cs.data_) }
	, db_{ cs.db_ }
	, sql_{ std::move(cs.sql_) }
{}

sqlite3xx::changeset::~changeset()
{
	sqlite3_free(data_);
}

sqlite3xx::changeset::iterator sqlite3xx::changeset::start() const
{
	sqlite3_changeset_iter* iter = nullptr;
	int res = sqlite3changeset_start(&iter, size_, data_);
	if (res == SQLITE_OK) {
		return iterator{ db_, iter };
	} else {
		db_.raise_error(error_mask::changeset_start);
		return iterator{ db_, nullptr };
	}
}

sqlite3xx::changeset::iterator::iterator(database const& db, sqlite3_changeset_iter* obj)
	: db_{ db }
	, sql_{ db.handle() }
	, iter_{ obj, sqlite3changeset_finalize }
	, eof_{ true }
	, op_{ 0 }
	, ncol_{ 0 }
{
	if (iter_ && sql_) next();
}

sqlite3xx::changeset::iterator::iterator(iterator&& i) noexcept
	: db_{ i.db_ }
	, sql_{ std::move(i.sql_) }
	, iter_{ std::move(i.iter_) }
	, eof_{ true }
	, op_{ 0 }
	, ncol_{ 0 }
{
	if (iter_ && sql_) get_data();
}

void sqlite3xx::changeset::iterator::get_data()
{
	char const* pzTab = nullptr;
	int res = sqlite3changeset_op(iter_.get(), &pzTab, &ncol_, &op_, &indirect_);
	if (res == SQLITE_OK) {
		ztab_ = pzTab;
		eof_ = false;
	} else {
		eof_ = true;
		db_.raise_error(error_mask::changeset_op);
	}
}

int sqlite3xx::changeset::iterator::next()
{
	int res = sqlite3changeset_next(iter_.get());
	switch (res) {
	case SQLITE_ROW:
		get_data();
		return SQLITE_OK;
	case SQLITE_DONE:
		eof_ = true;
		return SQLITE_OK;
	default:
		db_.raise_error(error_mask::changeset_next);
		return res;
	}
}

bool sqlite3xx::changeset::iterator::eof() const
{
	return eof_;
}

std::string sqlite3xx::changeset::iterator::table() const
{
	return ztab_;
}

int sqlite3xx::changeset::iterator::ncols() const
{
	return ncol_;
}

int sqlite3xx::changeset::iterator::type() const
{
	return op_;
}

int sqlite3xx::changeset::iterator::get_indirect() const
{
	return indirect_;
}

sqlite3xx::value sqlite3xx::changeset::iterator::new_value(int col) const
{
	sqlite3_value* val = nullptr;
	int res = sqlite3changeset_new(iter_.get(), col, &val);
	if (res == SQLITE_OK) {
		return value{ val };
	} else {
		db_.raise_error(error_mask::changeset_new);
		return value{};
	}
}

sqlite3xx::value sqlite3xx::changeset::iterator::old_value(int col) const
{
	sqlite3_value* val = nullptr;
	int res = sqlite3changeset_old(iter_.get(), col, &val);
	if (res == SQLITE_OK) {
		return value{ val };
	} else {
		db_.raise_error(error_mask::changeset_old);
		return value{};
	}
}

void sqlite3xx::changeset::set_apply_filter(std::function<int(void* pCtx, std::string_view)> xFilter)
{
	m_xFilter = xFilter;
}

void sqlite3xx::changeset::set_apply_conflict(std::function<int(void* pCtx, int eConflict, iterator const& op)> xConflict)
{
	m_xConflict = xConflict;
}

int sqlite3xx::changeset::apply(void* pCtx)
{
	// lambda wrapper for xFilter callback
	auto xFilter = [](void* pCtx, char const* pzTab) -> int {
		std::unique_ptr<context_data> ctx{ reinterpret_cast<context_data*>(pCtx) };
		if (ctx->owner.m_xFilter) {
			std::string strTab;
			strTab = pzTab;
			return ctx->owner.m_xFilter(ctx->user_ptr, strTab);
		} else {
			return 1;
		}
	};
	// lambda wrapper for xConflict callback
	auto xConflict = [](void* pCtx, int eConflict, sqlite3_changeset_iter* obj) -> int {
		std::unique_ptr<context_data> ctx{ reinterpret_cast<context_data*>(pCtx) };
		iterator it{ ctx->owner.db_, obj };
		if (!ctx->owner.m_xConflict) {
			if (eConflict == SQLITE_CHANGESET_DATA || eConflict == SQLITE_CHANGESET_CONFLICT) {
				return SQLITE_CHANGESET_REPLACE;
			} else {
				return SQLITE_CHANGESET_OMIT;
			}
		} else {
			return ctx->owner.m_xConflict(ctx->user_ptr, eConflict, it);
		}
	};
	int res = sqlite3changeset_apply(sql_, size_, data_, xFilter, xConflict, new context_data{ pCtx, *this });
	if (res != SQLITE_OK) db_.raise_error(error_mask::changeset_apply);
	return res;
}

int sqlite3xx::changeset::concat(sqlite3xx::changeset& other)
{
	int nbytes = 0;
	void* buf = nullptr;
	int res = sqlite3changeset_concat(size_, data_, other.size_, other.data_, &nbytes, &buf);
	if (res == SQLITE_OK) {
		sqlite3_free(data_);
		data_ = buf;
		size_ = nbytes;
	} else {
		db_.raise_error(error_mask::changeset_concat);
	}
	return res;
}

sqlite3xx::changeset sqlite3xx::changeset::invert() const
{
	int nbytes = 0;
	void* buf = nullptr;
	int res = sqlite3changeset_invert(size_, data_, &nbytes, &buf);
	if (res == SQLITE_OK) {
		return changeset{ db_, nbytes, buf };
	} else {
		db_.raise_error(error_mask::changeset_invert);
		throw changeset{ db_, 0, nullptr };
	}
}

int sqlite3xx::changeset::size() const
{
	return size_;
}

void* sqlite3xx::changeset::data() const
{
	return data_;
}

void sqlite3xx::changeset::assign(int nbytes, void* buf)
{
	sqlite3_free(data_);
	size_ = nbytes;
	data_ = buf;
}

#endif
