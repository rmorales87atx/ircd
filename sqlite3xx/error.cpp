//
// SQLite3++ (sqlite3xx.hpp)
//
// Based on code developed by NeoSmart Technologies: https://github.com/neosmart/CppSQLite
// and Rob Groves <rob.groves@btinternet.com>

#include <cstdint>
#include <cstring>
#include <memory>
#include <stdexcept>
#include <ostream>
#include "SQLite3xx.hpp"
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

sqlite3xx::error::error(int err_code, std::optional<int> ext_code)
	: std::runtime_error{ "sqlite3 error" }
	, code_{ err_code }
	, ext_{ ext_code }
{
}

sqlite3xx::error::error(std::string_view const message)
	: std::runtime_error{ "sqlite3 error" }
	, code_{ std::nullopt }
	, ext_{ std::nullopt }
	, messg_{ message }
{}

sqlite3xx::error::error(database const& db)
	: std::runtime_error{ "sqlite3 error" }
{
	auto codes = db.last_error();
	code_ = codes.first;
	ext_ = codes.second;
	messg_ = db.last_error_message();
}

std::optional<int> sqlite3xx::error::code() const noexcept
{
	return code_;
}

std::optional<int> sqlite3xx::error::code_ext() const noexcept
{
	return ext_;
}

char const* sqlite3xx::error::what() const noexcept
{
	if (messg_.empty() && code_) {
		return sqlite3_errstr(*code_);
	} else {
		return messg_.empty() ? "" : messg_.c_str();
	}
}
