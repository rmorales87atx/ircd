//
// SQLite3++ (sqlite3xx.hpp)
//
// Based on code developed by NeoSmart Technologies: https://github.com/neosmart/CppSQLite
// and Rob Groves <rob.groves@btinternet.com>

#include <cstdint>
#include <cstring>
#include <memory>
#include <stdexcept>
#include <ostream>
#include "SQLite3xx.hpp"
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

struct collate_function_data {
	sqlite3xx::database* db;
	std::string name;
	sqlite3xx::collate_function_t callback;
};

sqlite3xx::database::database()
	: dbp_{ nullptr }
	, timeout_{ 60000 }
	, error_{ error_mask::sql_errors }
{
}

sqlite3xx::database::database(database&& ref)
	: dbp_{ std::move(ref.dbp_) }
	, timeout_{ ref.timeout_ }
	, error_{ ref.error_ }
{
	exec_dml("PRAGMA foreign_keys = ON");
}

sqlite3xx::database::database(sqlite3* ptr)
	: dbp_{ nullptr }
	, timeout_{ 60000 }
	, error_{ error_mask::sql_errors }
{
	// take pointer without assuming ownership
	// (no automatic call to sqlite3_close)
	dbp_.reset(ptr, [](sqlite3*) {});
	exec_dml("PRAGMA foreign_keys = ON");
}

sqlite3xx::database::~database()
{
	close();
}

int sqlite3xx::database::open(std::string_view fname_, std::optional<int> flags, std::string_view vfsname_)
{
	sqlite3* ptr = nullptr;
	std::string const fname{ fname_ }, vfs_name{ vfsname_ };
	if (!flags) flags = SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE;
	assert(flags.has_value());
	int res = sqlite3_open_v2(fname.c_str(), &ptr, *flags, vfs_name.empty() ? nullptr : vfs_name.c_str());
	if (res == SQLITE_OK) {
		dbp_.reset(ptr, sqlite3_close); // assume ownership
		set_busy_timeout(timeout_);
		exec_dml("PRAGMA foreign_keys = ON");
	} else {
		raise_error(error_mask::open);
	}
	return res;
}

bool sqlite3xx::database::is_open() const
{
	return dbp_ != nullptr;
}

std::string sqlite3xx::database::filename() const
{
	if (dbp_) {
		char const* zName = sqlite3_db_filename(dbp_.get(), nullptr);
		if (zName)
			return std::string{ zName };
	}
	return std::string{};
}

#if SQLITE_HAS_CODEC
int sqlite3xx::database::key(std::string_view key_)
{
	std::string key{ key_ };
	int res = sqlite3_key(dbp_.get(), key.c_str(), key.length());
	if (res != SQLITE_OK) raise_error(error_mask::key);
	return res;
}

int sqlite3xx::database::key(std::vector<unsigned char> const& key)
{
	int res = sqlite3_key(dbp_.get(), &key[0], key.size());
	if (res != SQLITE_OK) raise_error(error_mask::key);
	return res;
}
#endif

void sqlite3xx::database::close()
{
	dbp_.reset();
}

bool sqlite3xx::database::table_exists(std::string_view name) const
{
	auto stmt = compile("SELECT count() FROM sqlite_master WHERE type='table' AND name=?");
	stmt.bind_text(1, name);
	stmt.step();
	auto res = stmt.field_value(0);
	if (auto val = res.get_int()) {
		return *val > 0;
	} else {
		return false;
	}
}

int sqlite3xx::database::changes() const
{
	if (dbp_) {
		return sqlite3_changes(dbp_.get());
	} else {
		return 0;
	}
}

int sqlite3xx::database::exec_dml(std::string_view sql_text)
{
	if (!dbp_ || sql_text.empty()) return 0;
	int rc = SQLITE_OK;
	int nchanges = 0;
	std::string sql, tail;
	sql = sql_text;
	do {
		auto vmptr = prepare(sql, &tail);
		if (!vmptr) {
			std::exchange(sql, tail);
			continue;
		}
		do {
			rc = sqlite3_step(vmptr.get());
			nchanges += changes();
			// no need to call an explicit reset here, vmptr will destruct it
			if (rc != SQLITE_ROW) {
				auto it = std::begin(tail);
				for (; it != std::end(tail); it++) {
					if (!std::isspace(*it)) break;
				}
				std::exchange(sql, std::string{ it, std::end(tail) });
				rc = SQLITE_OK;
			}
		} while (rc == SQLITE_ROW);
	} while (rc == SQLITE_OK && !sql.empty());
	if (rc != SQLITE_OK) raise_error(error_mask::exec);
	return nchanges;
}

sqlite3xx::column sqlite3xx::database::exec_scalar(std::string_view sql) const
{
	if (auto stmt = compile(sql); stmt.step()) {
		return stmt.field_value(0);
	} else {
		return column{};
	}
}

sqlite3xx::statement sqlite3xx::database::exec_query(std::string_view sql) const
{
	if (auto stmt = compile(sql); stmt.is_valid()) {
		stmt.step();
		return stmt;
	} else {
		return statement{ *this };
	}
}

sqlite3xx::statement sqlite3xx::database::compile(std::string_view sql) const
{
	if (auto vmptr = prepare(sql, nullptr)) {
		return statement{ *this, vmptr };
	} else {
		return statement{ *this };
	}
}

int64_t sqlite3xx::database::last_rowid() const
{
	if (dbp_) {
		return sqlite3_last_insert_rowid(dbp_.get());
	}
	return 0;
}

bool sqlite3xx::database::get_autocommit() const
{
	if (dbp_) {
		return sqlite3_get_autocommit(dbp_.get()) != 0;
	}
	return false;
}

int sqlite3xx::database::get_busy_timeout() const
{
	return timeout_;
}

int sqlite3xx::database::set_busy_timeout(int value)
{
	if (dbp_) {
		timeout_ = value;
		return sqlite3_busy_timeout(dbp_.get(), value);
	}
	return SQLITE_ERROR;
}

int sqlite3xx::database::set_busy_handler(std::function<int(int)> handler)
{
	if (dbp_) {
		busy_ = handler;
		if (handler) {
			return sqlite3_busy_handler(dbp_.get(), &busy_handler, this);
		} else {
			return sqlite3_busy_handler(dbp_.get(), nullptr, nullptr);
		}
	}
	return SQLITE_ERROR;
}

std::string sqlite3xx::database::version() const
{
	return sqlite3_libversion();
}

std::shared_ptr<sqlite3_stmt> sqlite3xx::database::prepare(std::string_view sql, std::string* remainder) const
{
	if (dbp_) {
		sqlite3_stmt* vm = nullptr;
		char const* zTailSQL = nullptr;
		int res = sqlite3_prepare_v2(dbp_.get(), sql.data(), (int)sql.size(), &vm, &zTailSQL);
		if (res == SQLITE_OK) {
			if (remainder) {
				if (zTailSQL) {
					*remainder = zTailSQL;
				} else {
					remainder->clear();
				}
			}
			return std::shared_ptr<sqlite3_stmt>(vm, sqlite3_finalize);
		} else {
			raise_error(error_mask::prepare);
		}
	}
	return nullptr;
}

int sqlite3xx::database::last_error_code() const
{
	if (dbp_) {
		return sqlite3_errcode(dbp_.get());
	} else {
		return 0;
	}
}

std::pair<int, int> sqlite3xx::database::last_error() const
{
	if (dbp_) {
		return std::make_pair(sqlite3_errcode(dbp_.get()), sqlite3_extended_errcode(dbp_.get()));
	} else {
		return std::make_pair(0, 0);
	}
}

std::string sqlite3xx::database::last_error_message() const
{
	if (dbp_) {
		return sqlite3_errmsg(dbp_.get());
	} else {
		return {};
	}
}

int sqlite3xx::database::set_authorizer(std::function<int(int, std::string, std::string, std::string, std::string)> fn)
{
	int res = SQLITE_ERROR;
	if (dbp_) {
		authorizer_ = fn;
		if (authorizer_) {
			res = sqlite3_set_authorizer(dbp_.get(), &default_auth, this);
		} else {
			res = sqlite3_set_authorizer(dbp_.get(), nullptr, nullptr);
		}
	}
	if (res != SQLITE_OK) {
		raise_error(error_mask::authorizer);
	}
	return res;
}

int sqlite3xx::database::default_auth(void* dptr, int acd, char const* arg1, char const* arg2, char const* arg3, char const* arg4)
{
	auto dbp = reinterpret_cast<sqlite3xx::database*>(dptr);
	if (dbp->authorizer_) {
		return dbp->authorizer_(acd, arg1, arg2, arg3, arg4);
	} else {
		return SQLITE_OK;
	}
}

int sqlite3xx::database::set_update_hook(std::function<void(int, sqlite3xx::database&, std::string, int64_t)> fn)
{
	if (dbp_) {
		update_hook_ = fn;
		if (update_hook_) {
			sqlite3_update_hook(dbp_.get(), &default_hook, this);
		} else {
			sqlite3_update_hook(dbp_.get(), nullptr, nullptr);
		}
		return SQLITE_OK;
	} else {
		return SQLITE_ERROR;
	}
}

void sqlite3xx::database::default_hook(void* dptr, int op, char const* db, char const* tab, sqlite3_int64 id)
{
	auto dbp = reinterpret_cast<sqlite3xx::database*>(dptr);
	if (dbp->update_hook_) {
		dbp->update_hook_(op, *dbp, tab, id);
	}
}

int sqlite3xx::database::create_function(std::string_view name_, int num_args,
	decltype(function::xFunc) xFunc,
	decltype(function::xStep) xStep,
	decltype(function::xInverse) xInverse,
	decltype(function::xFinal) xFinal,
	decltype(function::xValue) xValue,
	int flags)
{
	int res = SQLITE_ERROR;
	if (dbp_) {
		static auto xCollectArgs = [](int argc, sqlite3_value** argv) -> std::vector<value> {
			std::vector<value> args;
			for (int ax = 0; ax < argc; ax++) args.push_back(value{ argv[ax] });
			return args;
		};
		static auto sqFunc = [](sqlite3_context* cptr, int argc, sqlite3_value** argv) {
			auto args = xCollectArgs(argc, argv);
			context ctx{ cptr };
			auto info = ctx.user_data<function>();
			if (info->xFunc) info->xFunc(ctx, args);
		};
		static auto sqStep = [](sqlite3_context* cptr, int argc, sqlite3_value** argv) {
			auto args = xCollectArgs(argc, argv);
			context ctx{ cptr };
			auto info = ctx.user_data<function>();
			if (info->xStep) info->xStep(ctx, args);
		};
		static auto sqFinal = [](sqlite3_context* cptr) {
			context ctx{ cptr };
			auto info = ctx.user_data<function>();
			if (info->xFinal) info->xFinal(ctx);
		};
		static auto sqValue = [](sqlite3_context* cptr) {
			context ctx{ cptr };
			auto info = ctx.user_data<function>();
			if (info->xValue) info->xValue(ctx);
		};
		static auto sqInverse = [](sqlite3_context* cptr, int argc, sqlite3_value** argv) {
			auto args = xCollectArgs(argc, argv);
			context ctx{ cptr };
			auto info = ctx.user_data<function>();
			if (info->xInverse) info->xInverse(ctx, args);
		};
		std::string const name{ name_ };
		auto fnptr = std::make_shared<function>();
		fnptr->name = name;
		fnptr->num_args = num_args;
		if (xFunc) {
			fnptr->xFunc = xFunc;
			res = sqlite3_create_function_v2(dbp_.get(), name.c_str(), num_args,
				SQLITE_UTF8 | flags, fnptr.get(), sqFunc, nullptr, nullptr, nullptr);
		} else if (xStep && xFinal) {
			fnptr->xStep = xStep;
			fnptr->xFinal = xFinal;
			res = sqlite3_create_function_v2(dbp_.get(), name.c_str(), num_args,
				SQLITE_UTF8 | flags, fnptr.get(), nullptr, sqStep, sqFinal, nullptr);
		} else {
			res = sqlite3_create_function_v2(dbp_.get(), name.c_str(), -1, 0, nullptr, nullptr, nullptr, nullptr, nullptr);
		}
		if (res == SQLITE_OK) {
			funcs_.push_back(fnptr);
		}
	}
	if (res != SQLITE_OK) {
		raise_error(error_mask::create_function);
	}
	return res;
}

int sqlite3xx::database::collate_function(void* arg, int len1, void const* pstr1, int len2, void const* pstr2)
{
	auto context = reinterpret_cast<collate_function_data*>(arg);
	auto sv1 = std::string_view(reinterpret_cast<char const*>(pstr1 ? pstr1 : ""), len1);
	auto sv2 = std::string_view(reinterpret_cast<char const*>(pstr2 ? pstr2 : ""), len2);
	return context->callback(sv1, sv2);
}

int sqlite3xx::database::create_collation(std::string_view name, collate_function_t callback)
{
	if (dbp_) {
		auto context = new collate_function_data{ this, std::string(name), callback };
		int rc = sqlite3_create_collation_v2(dbp_.get(), context->name.c_str(), SQLITE_UTF8, context,
			&database::collate_function,
			[](void* p) { delete reinterpret_cast<collate_function_data*>(p); });
		if (rc != SQLITE_OK) {
			delete context;
			raise_error(error_mask::create_collation);
		}
		return rc;
	}
	return SQLITE_MISUSE;
}

std::vector<std::string> sqlite3xx::database::get_field_names(std::string_view table_name)
{
	auto stmt = compile("SELECT name FROM pragma_table_info(?)");
	stmt.bind_text(1, table_name);
	std::vector<std::string> out;
	while (stmt.step()) {
		out.emplace_back(stmt.field_value(0).get_text());
	}
	return out;
}

uint64_t sqlite3xx::database::get_error_mask() const
{
	return error_;
}

void sqlite3xx::database::set_error_mask(uint64_t mask, bool state)
{
	error_ = state ? (error_ | mask) : (error_ & ~mask);
}

inline void sqlite3xx::database::raise_error(uint64_t mask) const
{
	if (last_error_code() != SQLITE_OK && (error_ & mask) == mask) {
		throw error{ *this };
	}
}

int sqlite3xx::database::busy_handler(void* arg, int n)
{
	auto db{ reinterpret_cast<sqlite3xx::database*>(arg) };
	if (db->busy_) {
		return db->busy_(n);
	} else {
		return 0;
	}
}
