//
// SQLite3++ (sqlite3xx.hpp)
//
// Based on code developed by NeoSmart Technologies: https://github.com/neosmart/CppSQLite
// and Rob Groves <rob.groves@btinternet.com>

#include <cstdint>
#include <cstring>
#include <memory>
#include <stdexcept>
#include <ostream>
#include "SQLite3xx.hpp"
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

sqlite3xx::context::context(sqlite3_context* cptr)
	: cptr_{ cptr }
{}

void* sqlite3xx::context::agg_data(int nbytes)
{
	if (cptr_) {
		return sqlite3_aggregate_context(cptr_, nbytes);
	} else {
		return nullptr;
	}
}

void* sqlite3xx::context::get_udata() const
{
	if (cptr_) {
		return sqlite3_user_data(cptr_);
	} else {
		return nullptr;
	}
}

sqlite3xx::database sqlite3xx::context::db_handle() const
{
	return database{ sqlite3_context_db_handle(cptr_) };
}

void sqlite3xx::context::no_result()
{
	if (cptr_) {
		sqlite3_result_null(cptr_);
	}
}

void sqlite3xx::context::set_result(std::vector<char> const& buf)
{
	if (cptr_) {
		sqlite3_result_blob64(cptr_, &buf[0], buf.size(), SQLITE_TRANSIENT);
	}
}

void sqlite3xx::context::set_result(double const val)
{
	if (cptr_) {
		sqlite3_result_double(cptr_, val);
	}
}

void sqlite3xx::context::set_result(int const val)
{
	if (cptr_) {
		sqlite3_result_int(cptr_, val);
	}
}

void sqlite3xx::context::set_result(int64_t const val)
{
	if (cptr_) {
		sqlite3_result_int64(cptr_, val);
	}
}

void sqlite3xx::context::set_result(unsigned int const val)
{
	if (cptr_) {
		sqlite3_result_int(cptr_, val);
	}
}

void sqlite3xx::context::set_result(uint64_t const val)
{
	if (cptr_) {
		sqlite3_result_int64(cptr_, val);
	}
}

void sqlite3xx::context::set_result(std::string_view val)
{
	if (cptr_) {
		sqlite3_result_text(cptr_, val.data(), (int)val.length(), SQLITE_TRANSIENT);
	}
}

void sqlite3xx::context::set_result(std::u16string_view val)
{
	if (cptr_) {
		sqlite3_result_text16(cptr_, val.data(), (int)val.length(), SQLITE_TRANSIENT);
	}
}

void sqlite3xx::context::set_result(sqlite3xx::value const& val)
{
	if (cptr_) {
		if (val.is_null()) {
			sqlite3_result_null(cptr_);
		} else {
			sqlite3_result_text16(cptr_, val.c_str16(), -1, SQLITE_TRANSIENT);
		}
	}
}

void sqlite3xx::context::set_error(std::string_view val, std::optional<int> code)
{
	if (cptr_) {
		sqlite3_result_error(cptr_, val.data(), (int)val.length());
		if (code) {
			sqlite3_result_error_code(cptr_, *code);
		}
	}
}
