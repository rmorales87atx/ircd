﻿//
// SQLite3++ (sqlite3xx.hpp)
//
// Based on code developed by NeoSmart Technologies: https://github.com/neosmart/CppSQLite
// and Rob Groves <rob.groves@btinternet.com>
//
// Requires C++17 compiler support

#pragma once

#include "sqlite3.h"
#include <vector>
#include <unordered_map>
#include <string>
#include <string_view>
#include <optional>
#include <variant>
#include <functional>
#include <sstream>
#include <cassert>

#include <boost/uuid/uuid.hpp>

#define MIN_SQLITE_VERSION(X,Y,Z) (SQLITE_VERSION_NUMBER >= (X*1000000 + Y*1000 + Z))

namespace sqlite3xx {
	/// Forward declarations

	class statement;
	class database;
	namespace detail { struct statement_temporary; }

	/// Type used for field/parameter references (int for index, string for name).
	typedef std::variant<int, std::string_view> index_reference;
}

#include "sqlite3xx/error.hpp"
#include "sqlite3xx/query.hpp"
#include "sqlite3xx/function.hpp"
#include "sqlite3xx/transaction.hpp"
#include "sqlite3xx/session.hpp"
#include "sqlite3xx/simple.hpp"
#include "sqlite3xx/database.hpp"

/// General SQLite functions not part of class implementations
namespace sqlite3xx {
	/// @brief Produce an 'escaped' string suitable for use in a SQL statement.
	/// The 'fmt_type' parameter must be one of the following values:
	///   'q'  -- Standard usage (escapes single quotes)
	///   'Q'  -- Adds single quotes around the string, and returns NULL for empty strings
	///   'w'  -- Uses double quotes instead of single quotes
	std::string escape_text(char fmt_type, std::string_view const input);

	/// @brief Configure global SQLite settings
	template<typename... Args>
	void config(int op, Args&&... args);

	/// @brief Initialize SQLite.
	/// This function registers a call to shutdown() using std::atexit
	void initialize();

	/// @brief Shutdown SQLite
	void shutdown();
};

#include "sqlite3xx/detail.ipp"
