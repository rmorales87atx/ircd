//
// SQLite3++ (sqlite3xx.hpp)
//
// Based on code developed by NeoSmart Technologies: https://github.com/neosmart/CppSQLite
// and Rob Groves <rob.groves@btinternet.com>

#include <cstdint>
#include <cstring>
#include <memory>
#include <stdexcept>
#include <ostream>
#include "SQLite3xx.hpp"
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

sqlite3xx::column::column(std::shared_ptr<sqlite3_stmt> vmptr, int col_idx)
	: vmptr_{ vmptr }
	, index_{ col_idx }
{}

sqlite3xx::column::column()
	: vmptr_{}
	, index_{ -1 }
{}

sqlite3xx::column::column(column&& fv)
	: vmptr_{ std::move(fv.vmptr_) }
	, index_{ fv.index_ }
{}

int sqlite3xx::column::type() const
{
	if (vmptr_) {
		return sqlite3_column_type(vmptr_.get(), index_);
	} else {
		return SQLITE_NULL;
	}
}

std::string sqlite3xx::column::decl_type() const
{
	if (vmptr_) {
		auto p{ sqlite3_column_decltype(vmptr_.get(), index_) };
		return (p == nullptr) ? std::string{} : std::string{ p };
	} else {
		return {};
	}
}

bool sqlite3xx::column::is_null() const
{
	return !vmptr_ || index_ < 0 || type() == SQLITE_NULL;
}

std::optional<int> sqlite3xx::column::get_int() const
{
	if (is_null()) {
		return std::nullopt;
	} else {
		return sqlite3_column_int(vmptr_.get(), index_);
	}
}

int sqlite3xx::column::get_int(int def) const
{
	if (is_null()) {
		return def;
	} else {
		return sqlite3_column_int(vmptr_.get(), index_);
	}
}

std::optional<int64_t> sqlite3xx::column::get_int64() const
{
	if (is_null()) {
		return std::nullopt;
	} else {
		return sqlite3_column_int64(vmptr_.get(), index_);
	}
}

int64_t sqlite3xx::column::get_int64(int64_t def) const
{
	if (is_null()) {
		return def;
	} else {
		return sqlite3_column_int64(vmptr_.get(), index_);
	}
}

std::optional<unsigned int> sqlite3xx::column::get_uint() const
{
	if (is_null()) {
		return std::nullopt;
	} else {
		return (unsigned int)sqlite3_column_int(vmptr_.get(), index_);
	}
}

unsigned int sqlite3xx::column::get_uint(unsigned int def) const
{
	if (is_null()) {
		return def;
	} else {
		return (unsigned int)sqlite3_column_int(vmptr_.get(), index_);
	}
}

std::optional<uint64_t> sqlite3xx::column::get_uint64() const
{
	if (is_null()) {
		return std::nullopt;
	} else {
		return (uint64_t)sqlite3_column_int64(vmptr_.get(), index_);
	}
}

uint64_t sqlite3xx::column::get_uint64(uint64_t def) const
{
	if (is_null()) {
		return def;
	} else {
		return (uint64_t)sqlite3_column_int64(vmptr_.get(), index_);
	}
}

std::optional<double> sqlite3xx::column::get_double() const
{
	if (is_null()) {
		return std::nullopt;
	} else {
		return sqlite3_column_double(vmptr_.get(), index_);
	}
}

double sqlite3xx::column::get_double(double def) const
{
	if (is_null()) {
		return def;
	} else {
		return sqlite3_column_double(vmptr_.get(), index_);
	}
}

std::string sqlite3xx::column::get_text() const
{
	std::vector<unsigned char> buf;
	if (!is_null()) {
		auto src = sqlite3_column_text(vmptr_.get(), index_);
		buf.resize(sqlite3_column_bytes(vmptr_.get(), index_));
		if (buf.size() == 0) {
			return std::string{};
		}
		memcpy(&buf[0], src, buf.size());
	}
	return std::string{ buf.begin(), buf.end() };
}

std::u16string sqlite3xx::column::get_text16() const
{
	std::vector<char16_t> buf;
	if (!is_null()) {
		auto src = sqlite3_column_text16(vmptr_.get(), index_);
		buf.resize(sqlite3_column_bytes16(vmptr_.get(), index_));
		if (buf.size() == 0) {
			return std::u16string{};
		}
		memcpy(&buf[0], src, buf.size());
	}
	return std::u16string{ buf.begin(), buf.end() };
}

#if defined(_MSC_VER) || defined(WIN32)

std::wstring sqlite3xx::column::get_textW() const
{
	std::vector<wchar_t> buf;
	if (!is_null()) {
		auto src = sqlite3_column_text16(vmptr_.get(), index_);
		buf.resize(sqlite3_column_bytes16(vmptr_.get(), index_));
		if (buf.size() == 0) {
			return std::wstring{};
		}
		memcpy(&buf[0], src, buf.size());
	}
	return std::wstring{ buf.begin(), buf.end() };
}

wchar_t const* sqlite3xx::column::wc_str() const
{
	if (is_null()) {
		return L"";
	} else {
		return reinterpret_cast<wchar_t const*>(sqlite3_column_text16(vmptr_.get(), index_));
	}
}

#endif

boost::uuids::uuid sqlite3xx::column::get_uuid() const
{
	static boost::uuids::string_generator gen;
	if (!is_null()) {
		try {
			auto str = reinterpret_cast<char const*>(sqlite3_column_text(vmptr_.get(), index_));
			return gen(str);
		} catch (std::exception const&) {
			return boost::uuids::nil_generator()();
		}
	}
	return boost::uuids::nil_generator()();
}

char const* sqlite3xx::column::c_str() const
{
	if (is_null()) {
		return "";
	} else {
		return reinterpret_cast<char const*>(sqlite3_column_text(vmptr_.get(), index_));
	}
}

char16_t const* sqlite3xx::column::c_str16() const
{
	if (is_null()) {
		return u"";
	} else {
		return reinterpret_cast<char16_t const*>(sqlite3_column_text16(vmptr_.get(), index_));
	}
}

std::string sqlite3xx::column::get_text(std::string_view null_value) const
{
	return is_null() ? std::string(null_value) : get_text();
}

std::u16string sqlite3xx::column::get_text16(std::u16string_view null_value) const
{
	return is_null() ? std::u16string(null_value) : get_text16();
}

char const* sqlite3xx::column::c_str(char const* null_value) const
{
	return is_null() ? null_value : c_str();
}

char16_t const* sqlite3xx::column::c_str16(char16_t const* null_value) const
{
	return is_null() ? null_value : c_str16();
}

std::vector<char> sqlite3xx::column::get_blob() const
{
	std::vector<char> buf;
	if (!is_null()) {
		auto src = reinterpret_cast<char const*>(sqlite3_column_blob(vmptr_.get(), index_));
		size_t nbytes = sqlite3_column_bytes(vmptr_.get(), index_);
		if (nbytes > 0) {
			buf.resize(nbytes);
			memcpy(&buf[0], src, nbytes);
		}
	}
	return buf;
}

std::ostream& sqlite3xx::column::operator<<(std::ostream& os) const
{
	if (!is_null()) {
		os << reinterpret_cast<char const*>(sqlite3_column_text(vmptr_.get(), index_));
	}
	return os;
}

#if defined(_MSC_VER) || defined(WIN32)

std::wostream& sqlite3xx::column::operator<<(std::wostream& os) const
{
	if (!is_null()) {
		os << reinterpret_cast<wchar_t const*>(sqlite3_column_text16(vmptr_.get(), index_));
	}
	return os;
}

#endif
