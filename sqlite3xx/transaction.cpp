//
// SQLite3++ (sqlite3xx.hpp)
//
// Based on code developed by NeoSmart Technologies: https://github.com/neosmart/CppSQLite
// and Rob Groves <rob.groves@btinternet.com>

#include <cstdint>
#include <cstring>
#include <memory>
#include <stdexcept>
#include <ostream>
#include "SQLite3xx.hpp"
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

sqlite3xx::transaction::transaction(database& db, std::optional<int> mode)
	: db_{ db }
	, active_{ false }
	, mode_{ mode ? *mode : TRANSACTION_DEFERRED }
{
	switch (mode_) {
	case TRANSACTION_DEFERRED:
		db_.exec_dml("BEGIN DEFERRED TRANSACTION");
		break;
	case TRANSACTION_IMMEDIATE:
		db_.exec_dml("BEGIN IMMEDIATE TRANSACTION");
		break;
	case TRANSACTION_EXCLUSIVE:
		db_.exec_dml("BEGIN EXCLUSIVE TRANSACTION");
		break;
	}
	active_ = true;
}

sqlite3xx::transaction::~transaction()
{
	try {
		rollback();
	} catch (...) {}
}

bool sqlite3xx::transaction::is_active() const
{
	return active_;
}

int sqlite3xx::transaction::get_mode() const
{
	return mode_;
}

void sqlite3xx::transaction::commit()
{
	if (active_) {
		db_.exec_dml("COMMIT TRANSACTION");
		active_ = false;
	}
}

void sqlite3xx::transaction::rollback()
{
	if (active_) {
		db_.exec_dml("ROLLBACK TRANSACTION");
		active_ = false;
	}
}
