//
// SQLite3++ (sqlite3xx.hpp)
//
// Based on code developed by NeoSmart Technologies: https://github.com/neosmart/CppSQLite
// and Rob Groves <rob.groves@btinternet.com>
//
// Requires C++17 compiler support

#pragma once

namespace sqlite3xx {
	/// Callback function for collation sequences
	typedef std::function<int(std::string_view const str1, std::string_view const str2)> collate_function_t;

	/// SQLite database object
	class database {
	public:
		database();
		database(sqlite3*);
		database(database const&) = delete;
		database(database&&);
		virtual ~database();

		/// @brief Retrieve SQLite database handle
		sqlite3* handle() const { return dbp_.get(); }

		/// @brief Opens the specified SQLite database file.
		/// Foreign keys will be automatically enabled by this method.
		/// @param file Database filename.
		/// @param flags SQLite flags. If none are specified (std::nullopt), the default flags are "SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE".
		///              Refer to https://sqlite.org/c3ref/open.html for details about the flags.
		/// @param vfs_name Name of a VFS module.
		int open(std::string_view file, std::optional<int> flags = std::nullopt, std::string_view vfs_name = "");

		/// @brief Determine if a database file has been opened.
		bool is_open() const;

		/// @brief Retrieve the database filename
		std::string filename() const;

#if SQLITE_HAS_CODEC
		/// @brief Set database key. (Only useful if SQLCipher, or SEE, are available.)
		int key(std::string_view);

		/// @brief Set database key. (Only useful if SQLCipher, or SEE, are available.)
		int key(std::vector<unsigned char> const&);
#endif

		/// @brief Close database.
		void close();

		/// @brief Determine if a table exists in the database.
		bool table_exists(std::string_view) const;

		/// @brief Determine the number of rows that were affected by a recently executed DML query
		int changes() const;

		/// @brief Configure this database connection (sqlite3_db_config)
		template<typename... Args>
		void config(int op, Args&&... args) const;

		/// @brief Execute multiple SQL statements.
		///
		/// This method is essentially equivalent to sqlite3_exec().
		/// Multiple statements within the SQL text are compiled and executed.
		///
		/// @param sql Full SQL text to be executed.
		int exec_dml(std::string_view sql);

		/// @brief Execute a single query, returning a single value.
		column exec_scalar(std::string_view) const;

		/// @brief Execute a single query, returning the results in a statement object.
		statement exec_query(std::string_view) const;

		/// @brief Compile SQL text into a statement object.
		statement compile(std::string_view) const;

		/// @brief Execute a single DML query with parameters.
		/// Automatically binds the given parameters to a
		/// compiled SQL statement and executes the query,
		/// returning the number of rows affected.
		template<typename... Args>
		int exec_dml(std::string_view const sql, Args&&... args) const;

		/// @brief Execute a single query, returning a single value.
		/// Automatically binds the given parameters to a
		/// compiled SQL statement and executes the query,
		/// returning the single value in the query.
		template<typename... Args>
		column exec_scalar(std::string_view const sql, Args&&... args) const;

		/// @brief Execute a single query, returning the results in a statement object.
		/// Automatically binds the given parameters to a
		/// compiled SQL statement and executes the query.
		template<typename... Args>
		statement exec_query(std::string_view const sql, Args&&... args) const;

		/// @brief Compile SQL text into a statement object, with parameters.
		template<typename... Args>
		statement compile(std::string_view const sql, Args&&... args) const;

		/// @brief Compile SQL text for use in "simplified syntax".
		/// @remarks
		/// The "simplified syntax" is inspired by SOCI: http://soci.sourceforge.net/doc/release/4.0/queries/
		/// 
		/// Example of a simple "scalar query", retrieving exactly one value:
		///		sqlite3xx::database db;
		///		db.open(...);
		///		int count = 0;
		///		db << "SELECT COUNT(*) FROM my_table", sqlite3xx::into(count);
		///
		/// Using query parameters:
		///		int count = 0;
		///		std::string name = "Smith";
		///		db << "SELECT COUNT(*) FROM employees WHERE LastName=?",
		///			sqlite3xx::into(count, 0), sqlite3xx::use(name, 1);
		/// 
		/// This interface only supports the following types for use():
		///		int, unsigned int, float, double, std::string, char const*
		/// For win32 only:
		///		std::wstring, wchar_t const*
		/// 
		/// The following types are supported for into():
		///		int, unsigned int, double, std::string, std::wstring (Win32 only)
		/// 
		/// If a null column is accessed, the exception null_column_value() is thrown.
		/// This only applies to numeric types (int, double). String types will simply receive an empty string object for nulls.
		/// This exception can be avoided by using a std::optional. Example:
		///		std::optional<int> zipcode;
		///		db << "SELECT Zipcode FROM employees WHERE ID=1",
		///			sqlite3xx::into(zipcode, 0);
		/// 
		detail::statement_temporary operator<<(std::string_view) const;

		/// @brief Returns the last ROWID in the database.
		int64_t last_rowid() const;

		/// @brief Determine if the database is in "auto-commit" mode.
		bool get_autocommit() const;

		/// @brief Retrieve the current SQLite3 busy timeout value.
		int get_busy_timeout() const;

		/// @brief Change the SQLite3 busy timeout value.
		int set_busy_timeout(int);

		/// @brief Assign a new SQLite3 busy handler.
		int set_busy_handler(std::function<int(int)> handler);

		/// @brief Return the SQLite3 library version.
		std::string version() const;

		/// @brief Retrieve the most recent error code
		int last_error_code() const;

		/// @brief Retrieve the most recent error code, including the extended result code
		std::pair<int, int> last_error() const;

		/// @brief Retrieve the most recent error message
		std::string last_error_message() const;

		/// @brief Change (or remove) the authorizer
		int set_authorizer(std::function<int(int, std::string, std::string, std::string, std::string)> fn);

		/// @brief Change (or remove) the update hook
		int set_update_hook(std::function<void(int, sqlite3xx::database&, std::string, int64_t)> fn);

		/// @brief Create a new SQL function
		int create_function(std::string_view name, int num_args,
			decltype(function::xFunc) xFunc,
			decltype(function::xStep) xStep = nullptr,
			decltype(function::xInverse) xInverse = nullptr,
			decltype(function::xFinal) xFinal = nullptr,
			decltype(function::xValue) xValue = nullptr,
			int flags = 0);

#if defined(SQLITE_ENABLE_SESSION) && defined(SQLITE_ENABLE_PREUPDATE_HOOK)
		/// @brief Create a new session object
		session create_session(std::string_view name);
#endif

		/// @brief Determine if an error mask is present
		uint64_t get_error_mask() const;

		/// @brief Update the error mask
		void set_error_mask(uint64_t mask, bool state);

		/// @brief Generate error exception (used internally by the other SQLite3xx objects)
		void raise_error(uint64_t mask) const;

		/// @brief Define a new collation sequence
		int create_collation(std::string_view name, collate_function_t callback);

		/// @brief Retrieve a list of field names in the given table
		std::vector<std::string> get_field_names(std::string_view table_name);

	private:
		std::shared_ptr<sqlite3> dbp_;
		std::vector<std::unique_ptr<sqlite3_module>> mods_;
		std::vector<std::shared_ptr<function>> funcs_;
		std::function<int(int, std::string, std::string, std::string, std::string)> authorizer_;
		std::function<void(int, sqlite3xx::database&, std::string, int64_t)> update_hook_;
		int timeout_;
		std::function<int(int)> busy_;
		uint64_t error_;
		static int busy_handler(void* arg, int n);
		static int default_auth(void* dptr, int acd, char const* arg1, char const* arg2, char const* arg3, char const* arg4);
		static void default_hook(void* dptr, int op, char const* db, char const* tab, sqlite3_int64 id);
		static int collate_function(void* arg, int len1, void const* pstr1, int len2, void const* pstr2);
		std::shared_ptr<sqlite3_stmt> prepare(std::string_view, std::string*) const;
	};
}
