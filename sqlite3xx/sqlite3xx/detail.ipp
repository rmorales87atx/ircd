#include "database.hpp"
#pragma once

template<typename T>
sqlite3xx::detail::use_value<T> sqlite3xx::use(T const& value, std::optional<index_reference> index)
{
	return detail::use_value<T>{ value, index };
}

template<typename T>
sqlite3xx::detail::into_ref<T> sqlite3xx::into(T& ref, std::optional<index_reference> index)
{
	return detail::into_ref<T>{ ref, index };
}

template<typename T>
sqlite3xx::detail::into_opt<T> sqlite3xx::into(std::optional<T>& ref, std::optional<index_reference> index)
{
	return detail::into_opt<T>{ ref, index };
}

template<typename... Args>
int sqlite3xx::statement::bind_args(int offs, Args&&... args) const
{
	assert(offs > 0);
	int n = offs;
	(bind(n++, std::forward<Args>(args)), ...);
	return n - offs;
}

template<typename ...Args>
inline void sqlite3xx::database::config(int op, Args && ...args) const
{
	if (!dbp_) throw error{ SQLITE_MISUSE, std::nullopt };
	int res = sqlite3_db_config(dbp_.get(), op, args...);
	if (res != SQLITE_OK) throw error{ res, std::nullopt };
}

template<typename ...Args>
int sqlite3xx::database::exec_dml(std::string_view const sql, Args && ...args) const
{
	if (auto stmt = compile(sql); stmt.is_valid()) {
		stmt.bind_args(1, args...);
		if (stmt.step()) return changes();
	}
	return 0;
}

template<typename ...Args>
sqlite3xx::column sqlite3xx::database::exec_scalar(std::string_view const sql, Args && ...args) const
{
	if (auto stmt = compile(sql); stmt.is_valid()) {
		stmt.bind_args(1, args...);
		if (stmt.step()) return stmt.field_value(0);
	}
	return column{};
}

template<typename ...Args>
sqlite3xx::statement sqlite3xx::database::exec_query(std::string_view const sql, Args && ...args) const
{
	if (auto stmt = compile(sql); stmt.is_valid()) {
		stmt.bind_args(1, args...);
		stmt.step();
		return stmt;
	}
	return statement{ *this };
}

template<typename ...Args>
sqlite3xx::statement sqlite3xx::database::compile(std::string_view const sql, Args && ...args) const
{
	if (auto stmt = compile(sql); stmt.is_valid()) {
		stmt.bind_args(1, args...);
		return stmt;
	}
	return statement{ *this };
}

template<typename ...Args>
void sqlite3xx::config(int op, Args && ...args)
{
	int res = sqlite3_config(op, args...);
	if (res != SQLITE_OK) throw error{ res, std::nullopt };
}
