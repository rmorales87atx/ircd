//
// SQLite3++ (sqlite3xx.hpp)
//
// Based on code developed by NeoSmart Technologies: https://github.com/neosmart/CppSQLite
// and Rob Groves <rob.groves@btinternet.com>
//
// Requires C++17 compiler support

#pragma once

namespace sqlite3xx {
	/// The objects in this namespace help to enable the "simplified syntax".
	/// @see database::operator<<
	namespace detail {
		template<typename T>
		struct use_value {
			T const& value_;
			std::optional<index_reference> index;

			use_value(T const& init, std::optional<index_reference> idx)
				: value_{ init }, index{ idx }
			{}
		};

		template<typename T>
		struct into_ref {
			T& ref_;
			std::optional<index_reference> index;

			into_ref(T& ref, std::optional<index_reference> idx)
				: ref_{ ref }, index{ idx }
			{}
		};

		template<typename T>
		struct into_opt {
			std::optional<T>& ref_;
			std::optional<index_reference> index;

			into_opt(std::optional<T>& ref, std::optional<index_reference> idx)
				: ref_{ ref }, index{ idx }
			{}
		};

		struct statement_temporary {
			struct out_info {
				int type;
				void* ptr;
				size_t siz;
				std::optional<index_reference> index;
			};
			bool no_exec;
			statement stmt;
			int bind_count_;
			int column_index_;
			std::vector<out_info> outbuf_;

			statement_temporary(statement&& st);
			~statement_temporary() noexcept(false);
			bool step();
			void copy_into(out_info& dst, sqlite3xx::column& src);

			statement_temporary& operator,(use_value<int> uv);
			statement_temporary& operator,(use_value<unsigned int> uv);
			statement_temporary& operator,(use_value<int64_t> uv);
			statement_temporary& operator,(use_value<uint64_t> uv);
			statement_temporary& operator,(use_value<float> uv);
			statement_temporary& operator,(use_value<double> uv);
			statement_temporary& operator,(use_value<std::string> uv);
			statement_temporary& operator,(use_value<std::string_view> uv);
			statement_temporary& operator,(use_value<char const*> uv);
			statement_temporary& operator,(use_value<std::u16string> uv);
			statement_temporary& operator,(use_value<std::u16string_view> uv);
			statement_temporary& operator,(use_value<char16_t const*> uv);
#if defined(_MSC_VER) || defined(WIN32)
			statement_temporary& operator,(use_value<std::wstring> uv);
			statement_temporary& operator,(use_value<std::wstring_view> uv);
			statement_temporary& operator,(use_value<wchar_t const*> uv);
#endif
			statement_temporary& operator,(into_ref<int> iref);
			statement_temporary& operator,(into_ref<unsigned int> iref);
			statement_temporary& operator,(into_ref<int64_t> uv);
			statement_temporary& operator,(into_ref<uint64_t> uv);
			statement_temporary& operator,(into_ref<float> uv);
			statement_temporary& operator,(into_ref<double> uv);
			statement_temporary& operator,(into_ref<std::string> uv);
			statement_temporary& operator,(into_ref<std::u16string> uv);
#if defined(_MSC_VER) || defined(WIN32)
			statement_temporary& operator,(into_ref<std::wstring> uv);
#endif
			statement_temporary& operator,(into_opt<int> iref);
			statement_temporary& operator,(into_opt<unsigned int> iref);
			statement_temporary& operator,(into_opt<int64_t> uv);
			statement_temporary& operator,(into_opt<uint64_t> uv);
			statement_temporary& operator,(into_opt<double> uv);
		};
	}

	/// Exception thrown when using the "simplified syntax", and the column
	/// being received with into() contains a NULL value.
	class null_column_value : public error {
	public:
		null_column_value(detail::statement_temporary::out_info& dst, statement& stmt);
		std::optional<int> index() const noexcept;
		std::string name() const noexcept;
		char const* what() const noexcept;
	private:
		std::optional<int> index_;
		std::string refname_;
		std::string message_;
	};

	/// @brief This function binds a parameter to a "simplified syntax" SQL query.
	/// @param value The parameter value. This reference must not go out of scope while the query is active.
	/// @param index The parameter index, either a named parameter or an integer value (starting at 1).
	template<typename T>
	detail::use_value<T> use(T const& value, std::optional<index_reference> index = std::nullopt);

	/// @brief Bind an output reference into a "simplified syntax" SQL query.
	/// If the SQL column contains a NULL value, an exception is thrown.
	/// @param ref The variable that will receive data.
	/// @param index The column index, either a named column or an integer value (starting at 0).
	template<typename T>
	detail::into_ref<T> into(T& ref, std::optional<index_reference> index = std::nullopt);

	/// @brief Bind an optional output reference into a "simplified syntax" SQL query.
	/// If the SQL column contains a NULL value, the reference will be set to std::nullopt.
	/// @param ref The variable that will receive data.
	/// @param index The column index, either a named column or an integer value (starting at 0).
	template<typename T>
	detail::into_opt<T> into(std::optional<T>& ref, std::optional<index_reference> index = std::nullopt);

	/// @brief This class is used in a "simplified syntax" SQL query that returns multiple rows.
	/// @remarks
	/// Example of use:
	///		sqlite3xx::database db;
	///		db.open(...);
	///		int64_t id;
	///		std::string name;
	///		sqlite3xx::rowset results = (db << "SELECT id, name FROM employees ORDER BY name");
	///		for (; !results.eof(); results.step()) {
	///			// vars "id" and "name" will auto-populate for each row iteration
	///		}
	class rowset {
	public:
		rowset(detail::statement_temporary&);
		statement& get_statement();
		bool next();
		sqlite3xx::statement* operator->();
	private:
		detail::statement_temporary holder_;
	};
}
