//
// SQLite3++ (sqlite3xx.hpp)
//
// Based on code developed by NeoSmart Technologies: https://github.com/neosmart/CppSQLite
// and Rob Groves <rob.groves@btinternet.com>
//
// Requires C++17 compiler support

#pragma once

namespace sqlite3xx {
	/// @brief Object representing a value in a SQLite column.
	/// 
	/// NOTE: Interfaces for wide strings (std::wstring) are supported
	/// only on MSVC/Win32; this is because sizeof(char16_t)==sizeof(wchar_t)
	class column {
	public:
		column(std::shared_ptr<sqlite3_stmt> vmptr, int col_idx);
		column();
		column(column const&) = delete;
		column(column&&);

		/// @brief Determine value type.
		int type() const;

		/// @brief Determine declared type.
		std::string decl_type() const;

		/// @brief Determine if value is null.
		bool is_null() const;

		/// @brief Return value as an integer.
		std::optional<int> get_int() const;

		/// @brief Return value as a 64-bit integer.
		std::optional<int64_t> get_int64() const;

		/// @brief Return value as an unsigned integer.
		std::optional<unsigned int> get_uint() const;

		/// @brief Return value as an unsigned 64-bit integer.
		std::optional<uint64_t> get_uint64() const;

		/// @brief Return value as a float type.
		std::optional<double> get_double() const;

		/// @brief Return value as an integer.
		int get_int(int def) const;

		/// @brief Return value as a 64-bit integer.
		int64_t get_int64(int64_t def) const;

		/// @brief Return value as an unsigned integer.
		unsigned int get_uint(unsigned int def) const;

		/// @brief Return value as an unsigned 64-bit integer.
		uint64_t get_uint64(uint64_t def) const;

		/// @brief Return value as a float type.
		double get_double(double def) const;

		/// @brief Return value as text.
		std::string get_text() const;

		/// @brief Return value as UTF-16 text.
		std::u16string get_text16() const;

		/// @brief Return value as text.
		char const* c_str() const;

		/// @brief Return value as UTF-16 text.
		char16_t const* c_str16() const;

		/// @brief Return value as text.
		std::string get_text(std::string_view null_value) const;

		/// @brief Return value as UTF-16 text.
		std::u16string get_text16(std::u16string_view null_value) const;

		/// @brief Return value as text.
		char const* c_str(char const* null_value) const;

		/// @brief Return value as UTF-16 text.
		char16_t const* c_str16(char16_t const* null_value) const;

		/// @brief Return value as BLOB data.
		std::vector<char> get_blob() const;

#if defined(_MSC_VER) || defined(WIN32)
		/// @brief Return value as UTF-16 text.
		std::wstring get_textW() const;

		/// @brief Return value as UTF-16 text.
		wchar_t const* wc_str() const;
#endif

		/// @brief Return value as UUID.
		boost::uuids::uuid get_uuid() const;

		/// Operator for stream output
		std::ostream& operator<<(std::ostream& os) const;

#if defined(_MSC_VER) || defined(WIN32)
		/// Operator for stream output
		std::wostream& operator<<(std::wostream& os) const;
#endif

		/// Operator for stream output
		friend std::ostream& operator<<(std::ostream& os, column const& v)
		{
			return v.operator<<(os);
		}

#if defined(_MSC_VER) || defined(WIN32)
		/// Operator for stream output
		friend std::wostream& operator<<(std::wostream& os, column const& v)
		{
			return v.operator<<(os);
		}
#endif

	private:
		std::shared_ptr<sqlite3_stmt> vmptr_;
		int index_;
	};

	/// @brief Compiled SQLite statement.
	/// Statement objects represent both DDL (INSERT/UPDATE/DELETE)
	/// and DML (SELECT) statements.
	class statement {
		friend class database;
	public:
		statement(database const&);
		statement(statement const&);
		statement(statement&&);
		statement& operator=(statement&& ref);
		virtual ~statement();

		/// @brief Construct statement object by compiling the given SQL statement.
		statement(database const&, std::string_view sql_text);

		/// @brief Determine if the statement is still valid (not yet finalized)
		bool is_valid() const;

		/// @brief Determine number of changes affected by a recent execution of this statement.
		int changes();

		/// @brief Determine the number of SQL parameters
		int param_count() const;

		/// @brief Retrieve the name of a parameter
		std::string param_name(int n) const;

		/// @brief Bind a field value to the specified parameter in a compiled statement.
		int bind(index_reference, column const&) const;

		/// @brief Bind a text value to the specified parameter in a compiled statement.
		int bind_text(index_reference, std::string_view const) const;

		/// @brief Bind a text value to the specified parameter in a compiled statement.
		int bind_text(index_reference, std::u16string_view const) const;

#if defined(_MSC_VER) || defined(WIN32)
		/// @brief Bind a text value to the specified parameter in a compiled statement.
		int bind_text(index_reference, std::wstring_view const) const;
#endif

		/// @brief Bind a numeric value to the specified parameter in a compiled statement.
		int bind_int(index_reference, std::optional<int> const) const;

		/// @brief Bind a numeric value to the specified parameter in a compiled statement.
		int bind_int64(index_reference, std::optional<int64_t> const) const;

		/// @brief Bind a numeric value to the specified parameter in a compiled statement.
		int bind_uint64(index_reference, std::optional<uint64_t> const) const;

		/// @brief Bind a numeric value to the specified parameter in a compiled statement.
		int bind_double(index_reference, std::optional<double> const) const;

		/// @brief Bind BLOB data to the specified parameter in a compiled statement.
		int bind_blob(index_reference, std::vector<char> const&) const;

		/// @brief Bind a NULL value to the specified parameter in a compiled statement.
		int bind_null(index_reference) const;

		/** @{
		 *  @brief Convenience bind methods.
		 */

		int bind(index_reference i, std::string_view arg) const { return bind_text(i, arg); }
		int bind(index_reference i, std::u16string_view arg) const { return bind_text(i, arg); }
#if defined(_MSC_VER) || defined(WIN32)
		int bind(index_reference i, std::wstring_view arg) const { return bind_text(i, arg); }
#endif
		int bind(index_reference i, int const arg) const { return bind_int(i, arg); }
		int bind(index_reference i, unsigned int const arg) const { return bind_int(i, arg); }
		int bind(index_reference i, int64_t const arg) const { return bind_int64(i, arg); }
		int bind(index_reference i, uint64_t const arg) const { return bind_uint64(i, arg); }
		int bind(index_reference i, double const arg) const { return bind_double(i, arg); }
		int bind(index_reference i, std::vector<char> const& arg) const { return bind_blob(i, arg); }
		int bind(index_reference i, std::nullopt_t&) const { return bind_null(i); }
		int bind(index_reference i, std::optional<int> arg) const { return arg ? bind_int(i, *arg) : bind_null(i); }
		int bind(index_reference i, std::optional<unsigned int> arg) const { return arg ? bind_int(i, *arg) : bind_null(i); }
		int bind(index_reference i, std::optional<int64_t> arg) const { return arg ? bind_int64(i, *arg) : bind_null(i); }
		int bind(index_reference i, std::optional<uint64_t> arg) const { return arg ? bind_uint64(i, *arg) : bind_null(i); }
		int bind(index_reference i, std::optional<double> arg) const { return arg ? bind_double(i, *arg) : bind_null(i); }

		/** @} */

		/// Bind multiple field values in a compiled statement, starting from the specified offset.
		/// @param offs Offset to begin binding values.
		/// @return The number of values bound.
		template<typename... Args>
		int bind_args(int offs, Args&&... args) const;

		/// @brief Reset the statement object.
		int reset();

		/// @brief Finalize statement object.
		void finalize();

#if defined(SQLITE_ENABLE_COLUMN_METADATA)
		/// @brief Return the database name originated with this statement.
		std::string database_name(int n) const;

		/// @brief Return the table name originated with this statement.
		std::string table_name(int n) const;
#endif

		/// @brief Determine the number of fields in the query.
		int field_count() const;

		/// @brief Locate a field index by name.
		std::optional<int> field_index(std::string_view) const;

		/// @brief Determine field name by column index.
		std::string field_name(int) const;

		/// @brief Return the given field's declared datatype, as a text string.
		std::string field_decltype(index_reference) const;

		/// @brief Return the given field's value datatype.
		std::optional<int> field_data_type(index_reference) const;

		/// @brief Return the given field's current value.
		column field_value(index_reference) const;

		/// @brief Convenience operator to return a field value.
		column operator()(index_reference) const;

		/// @brief Determine if a field is null.
		bool is_field_null(index_reference) const;

		/// @brief Continue execution of statement.
		bool step();

		/// @brief Determine current status.
		bool eof() const;

	private:
		statement(database const&, std::shared_ptr<sqlite3_stmt>);
		database const& db_;
		sqlite3* sql_;
		std::shared_ptr<sqlite3_stmt> vmptr_;
		bool eof_;
		int ncols_;
		std::unordered_map<std::string, int> names_;
		void init_names();
		std::optional<int> get_column(index_reference& ref) const;
		std::optional<int> get_param(index_reference& ref) const;
	};
}
