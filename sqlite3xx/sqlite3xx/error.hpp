//
// SQLite3++ (sqlite3xx.hpp)
//
// Based on code developed by NeoSmart Technologies: https://github.com/neosmart/CppSQLite
// and Rob Groves <rob.groves@btinternet.com>
//
// Requires C++17 compiler support

#pragma once

namespace sqlite3xx {
	/// @brief Exception mask
	/// @see database::set_error_mask, database::get_error_mask
	/// If this bits are set, an exception will be thrown
	/// instead of simply returning an error code
	enum error_mask : uint64_t {
		step = (1 << 0),
		finalize = (1 << 1),
		reset = (1 << 2),
		bind = (1 << 3),
		open = (1 << 4),
		key = (1 << 5),
		exec = (1 << 6),
		prepare = (1 << 7),
		invalid_column = (1 << 8),
		sql_errors = step | finalize | reset | bind | open | key | exec | prepare | invalid_column,
		authorizer = (1 << 9),
		create_function = (1 << 10),
		create_collation = (1 << 26),
		all_db_errors = sql_errors | authorizer | create_function | create_collation,
		session_create = (1 << 11),
		session_attach = (1 << 12),
		session_enable = (1 << 13),
		session_indirect = (1 << 14),
		session_diff = (1 << 15),
		session_changeset = (1 << 16),
		session_patchset = (1 << 17),
		all_session_errors = session_attach | session_enable | session_indirect | session_diff | session_changeset | session_patchset,
		changeset_start = (1 << 18),
		changeset_op = (1 << 19),
		changeset_next = (1 << 20),
		changeset_new = (1 << 21),
		changeset_old = (1 << 22),
		changeset_apply = (1 << 23),
		changeset_concat = (1 << 24),
		changeset_invert = (1 << 25),
		all_changeset_errors = changeset_start | changeset_op | changeset_next | changeset_new | changeset_old | changeset_apply | changeset_concat | changeset_invert,
		everything = all_db_errors | all_session_errors | all_changeset_errors,
	};

	/// @brief SQLite API error
	class error : public std::runtime_error {
	public:
		/// Direct constructor with an error code, and optional extended code.
		error(int err_code, std::optional<int> ext_code);

		/// Direct constructor with only an error message.
		error(std::string_view const);

		/// Constructor used by SQLite3xx methods. Error code is retrieved from sqlite3_errcode().
		error(database const&);

		/// Retrieve error code, if available.
		std::optional<int> code() const noexcept;

		/// Retrieve 'extended' error code, if available.
		std::optional<int> code_ext() const noexcept;

		/// Retrieve error message.
		char const* what() const noexcept;
	private:
		std::optional<int> code_, ext_;
		std::string messg_;
	};
}
