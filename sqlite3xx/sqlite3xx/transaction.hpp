//
// SQLite3++ (sqlite3xx.hpp)
//
// Based on code developed by NeoSmart Technologies: https://github.com/neosmart/CppSQLite
// and Rob Groves <rob.groves@btinternet.com>
//
// Requires C++17 compiler support

#pragma once

namespace sqlite3xx {
	/// @brief Simple RAII wrapper for SAVEPOINT transactions.
	class savepoint {
	public:
		/// Initiate SAVEPOINT on the specified database.
		savepoint(database&, std::string_view name);

		/// Empty savepoint.
		savepoint(database&);

		/// RELEASE the savepoint.
		~savepoint();

		/// Initiate savepoint.
		void start(std::string_view name);

		/// Determine if savepoint is active.
		bool is_active() const;

		/// Release (COMMIT) the savepoint.
		void release();

		/// Rollback the savepoint.
		/// If the database was in auto-commit mode when this SAVEPOINT began,
		/// a "COMMIT" statement will also be executed.
		void rollback();

	private:
		database& db_;
		std::string name_;
		bool autocommit_, active_;
	};

	/// Transaction modes used in the transaction() constructor.
	enum {
		TRANSACTION_DEFERRED = 0,
		TRANSACTION_IMMEDIATE = 1,
		TRANSACTION_EXCLUSIVE = 2,
	};

	/// @brief Simple RAII wrapper for database transactions.
	class transaction {
	public:
		/// Initiate transaction on the specified database.
		/// If the mode is not specified (std::nullopt), it defaults to "DEFERRED".
		transaction(database& db, std::optional<int> mode = std::nullopt);

		~transaction();

		/// Determine if transaction is active.
		bool is_active() const;

		/// Determine transaction behavior.
		int get_mode() const;

		/// End (COMMIT) the transaction.
		void commit();

		/// Abort the transaction.
		void rollback();

	private:
		database& db_;
		int mode_;
		bool active_;
	};
}
