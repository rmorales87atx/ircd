//
// SQLite3++ (sqlite3xx.hpp)
//
// Based on code developed by NeoSmart Technologies: https://github.com/neosmart/CppSQLite
// and Rob Groves <rob.groves@btinternet.com>
//
// Requires C++17 compiler support

#pragma once

namespace sqlite3xx {
	/// @brief Class representing a sqlite3_value object.
	class value {
	public:
		value(std::shared_ptr<sqlite3_value> vptr);
		value(sqlite3_value*);
		value();
		value(value const&);
		value(value&&) noexcept;

		/// @brief Determine value type.
		int type() const;

		/// @brief Determine if value is null.
		bool is_null() const;

		/// @brief Best numeric datatype of the value
		int numeric_type() const;

		/// @brief Return value as an integer.
		std::optional<int> get_int() const;

		/// @brief Return value as a 64-bit integer.
		std::optional<int64_t> get_int64() const;

		/// @brief Return value as an unsigned integer.
		std::optional<unsigned int> get_uint() const;

		/// @brief Return value as an unsigned 64-bit integer.
		std::optional<uint64_t> get_uint64() const;

		/// @brief Return value as a float type.
		std::optional<double> get_double() const;

		/// @brief Return value as an integer.
		int get_int(int def) const;

		/// @brief Return value as a 64-bit integer.
		int64_t get_int64(int64_t def) const;

		/// @brief Return value as an unsigned integer.
		unsigned int get_uint(unsigned int def) const;

		/// @brief Return value as an unsigned 64-bit integer.
		uint64_t get_uint64(uint64_t def) const;

		/// @brief Return value as a float type.
		double get_double(double def) const;

		/// @brief Return value as text.
		std::string get_text() const;

		/// @brief Return value as text.
		std::u16string get_text16() const;

		/// @brief Return value as text.
		char const* c_str() const;

		/// @brief Return value as text.
		char16_t const* c_str16() const;

		/// @brief Return value as BLOB data.
		std::vector<char> get_blob() const;

		/// @brief Operator for stream output.
		std::ostream& operator<<(std::ostream& os) const;

	private:
		std::shared_ptr<sqlite3_value> valptr_;
	};

	/// SQLite context, used by SQL functions
	class context {
	public:
		context(sqlite3_context*);

		/// @brief Retrieve database handle
		database db_handle() const;

		/// @brief Obtain memory buffer for aggregate context objects
		template<typename T>
		T* aggregate(int nbytes) { return reinterpret_cast<T*>(agg_data(nbytes)); }

		/// @brief Assign NULL function result
		void no_result();

		/// @brief Assign BLOB data as function result
		void set_result(std::vector<char> const& bs);

		/// @brief Assign function result
		void set_result(double const);

		/// @brief Assign function result
		void set_result(int const);

		/// @brief Assign function result
		void set_result(int64_t const);

		/// @brief Assign function result
		void set_result(unsigned int const);

		/// @brief Assign function result
		void set_result(uint64_t const);

		/// @brief Assign function result
		void set_result(std::string_view);

		/// @brief Assign function result
		void set_result(std::u16string_view);

		/// @brief Assign function result
		void set_result(value const&);

		/// @brief Assign function error
		void set_error(std::string_view, std::optional<int> code = std::nullopt);

		/// @brief Retrieve userdata
		template<typename T>
		T* user_data() const { return reinterpret_cast<T*>(get_udata()); }

	private:
		sqlite3_context* cptr_;
		void* agg_data(int nbytes);
		void* get_udata() const;
	};

	/// SQLite function information
	struct function {
		std::string name;
		int num_args;
		std::function<void(context&, std::vector<value> const& args)> xFunc, xStep, xInverse;
		std::function<void(context&)> xFinal, xValue;

		function() : num_args{ 0 }
		{}

		function(function const&) = delete;

		function(function&& other)
			: name{ other.name }, num_args{ 0 }
			, xFunc{ std::move(other.xFunc) }, xStep{ std::move(other.xStep) }, xInverse{ std::move(other.xInverse) }
			, xFinal{ std::move(other.xFinal) }, xValue{ std::move(other.xValue) }
		{}
	};
}
