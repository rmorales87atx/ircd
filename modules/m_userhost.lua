--
-- ircd - An IRCv3 Server
-- Copyright © 2019
--
-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, you can obtain one at: http://mozilla.org/MPL/2.0/
--

module = {
	name = "m_userhost",
	description = "Provide USERHOST command",
	api = 1,
}

local RPL_USERHOST = 302

function module.init()
	ircd.add_command('USERHOST', do_userhost)
	ircd.set_numeric(RPL_USERHOST, "%1% :%2%")
end

function do_userhost(cptr, sptr, msg)
	if not sptr:has_flag(CLI_REGISTERED) then
		sptr:send_numeric(ERR_NOTREGISTERED, sptr.nick)
	elseif #msg.args < 1 then
		sptr:send_numeric(ERR_NEEDPARAMS, sptr.nick, msg.command)
	elseif not sptr:has_flag(CLI_REMOTE) then
		-- allow only up to five parameters
		for i=1,math.min(#msg.args, 5) do
			local uptr = ircd.find_user(msg.args[i])
			if uptr ~= nil then
				local status = "+"
				if sptr:has_umode(UMODE_AWAY) then status = "-" end
				sptr:send_numeric(RPL_USERHOST, sptr.nick,
					uptr.nick .. "=" .. status .. uptr.username .. "@" .. uptr.hostname)
			else
				sptr:send_numeric(RPL_USERHOST, sptr.nick, "")
			end
		end
	end
end