--
-- ircd - An IRCv3 Server
-- Copyright © 2020
--
-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, you can obtain one at: http://mozilla.org/MPL/2.0/
--

module = {
	name = "m_kline",
	description = "KLINE command for opers",
	api = 1,
}

function module.init()
	ircd.add_command("KLINE", kline_command)
end

function find_kline(mask)
	local results = ircd.db:exec_scalar("SELECT count(*) FROM [server-bans] WHERE [mask]=?", mask):get_int(0)
	return results ~= 0
end

function kline_command(cptr, sptr, msg)
	if sptr:has_flag(CLI_REMOTE) then
		sptr:send_numeric(ERR_UNKNOWNCOMMAND, sptr.nick, msg.command)
	elseif not sptr:has_flag(CLI_REGISTERED) then
		sptr:send_numeric(ERR_NOTREGISTERED, sptr.nick)
	elseif not sptr:has_umode(UMODE_OPERATOR) and not sptr:has_umode(UMODE_SERVICE) then
		sptr:send_numeric(ERR_NOTOPER, sptr.nick)
	elseif #msg.args < 1 then
		sptr:send_numeric(RPL_INFO, sptr.nick, "KLINE command")
		sptr:send_numeric(RPL_INFO, sptr.nick, "Opers may use this command to manage server bans ('K-lines').")
		sptr:send_numeric(RPL_INFO, sptr.nick, "To view a list of the currently active K-lines: /STATS k")
		sptr:send_numeric(RPL_INFO, sptr.nick, "To view a list of K-lines with expire times: /KLINE EXPIRE")
		sptr:send_numeric(RPL_INFO, sptr.nick, "For detailed information on a particular K-line: /KLINE INFO <mask>")
		sptr:send_numeric(RPL_INFO, sptr.nick, "K-lines can be set on user@host masks and CIDR (e.g., 10.0.0.1/24)")
		sptr:send_numeric(RPL_INFO, sptr.nick, "Add a K-line: KLINE ADD <mask> <minutes> :<reason>")
		sptr:send_numeric(RPL_INFO, sptr.nick, "Remove a K-line: KLINE REM <mask>")
		sptr:send_numeric(RPL_INFO, sptr.nick, "Update K-line: KLINE SET <mask> (EXPIRE|COMMENT) ...")
		sptr:send_numeric(RPL_ENDOFINFO, sptr.nick, "End of INFO")
	elseif same_text(msg.args[1], 'ADD') and #msg.args >= 4 then
		local mask = msg.args[2]
		if ircd.valid_mask(mask) == MASK_INVALID then
			sptr:send_notice("Invalid mask")
			return
		end
		if find_kline(mask) then
			sptr:send_notice("K-LINE already exists for " .. mask)
			return
		end
		local mins = tonumber(msg.args[3]) or 0
		local reason = msg.args[4]
		if mins < 0 then mins = 0 end
		if mins ~= 0 then
			local ts = ircd.timestamp() + (mins * 60)
			local exp = os.date("%c", ts)
			ircd.snomask_log(LOG_OPER, SNO_KILLS, "%1% adding K-LINE for %2%, expiring at %3% (%4%)", sptr.userhost, mask, exp, reason)
			ircd.add_kline_temp(sptr, mask, reason, ts)
		else
			ircd.snomask_log(LOG_OPER, SNO_KILLS, "%1% adding K-LINE for %2% (%3%)", sptr.userhost, mask, reason)
			ircd.add_kline(sptr, mask, reason)
		end
		kline_enforce(mask)
	elseif same_text(msg.args[1], 'REM') and #msg.args >= 2 then
		local mask = msg.args[2]
		if ircd.del_kline(mask) then
			ircd.snomask_log(LOG_OPER, SNO_KILLS, "%1% removing K-LINE for %2%", sptr.userhost, mask)
		else
			sptr:send_notice("No such K-line")
		end
	elseif same_text(msg.args[1], 'INFO') and #msg.args >= 2 then
		local data = ircd.db:exec_query("SELECT _rowid_,* FROM [server-bans] WHERE [mask]=?", msg.args[2])
		if data ~= nil and not data.eof then
			sptr:send_numeric(RPL_INFO, sptr.nick, "KLINE: " .. tostring(data['mask']))
			if not data:is_field_null('created') then
				sptr:send_numeric(RPL_INFO, sptr.nick, "Created: " .. os.date("%c", data['created']:get_uint64()))
			else
				sptr:send_numeric(RPL_INFO, sptr.nick, "Created: <N/A>")
			end
			if not data:is_field_null('source') then
				sptr:send_numeric(RPL_INFO, sptr.nick, "Source: " .. tostring(data['source']))
			else
				sptr:send_numeric(RPL_INFO, sptr.nick, "Source: <N/A>")
			end
			if not data:is_field_null('expire') then
				sptr:send_numeric(RPL_INFO, sptr.nick, "Expiration: " .. os.date("%c", data['expire']:get_uint64()))
			else
				sptr:send_numeric(RPL_INFO, sptr.nick, "Expiration: <none>")
			end
			if not data:is_field_null('comment') then
				sptr:send_numeric(RPL_INFO, sptr.nick, "Comment: " .. tostring(data['comment']))
			end
		end
		sptr:send_numeric(RPL_ENDOFINFO, sptr.nick, "End of INFO")
	elseif same_text(msg.args[1], 'EXPIRE') then
		local data = ircd.db:exec_query("SELECT [mask],[expire],([expire]-[created])/60.0 AS [time],([expire]-timestamp())/60.0 AS [remain] FROM [server-bans] WHERE [expire] IS NOT NULL ORDER BY [expire]")
		while data ~= nil and not data.eof do
			local remain = string.format("%.2f", data['remain']:get_numeric())
			local original = string.format("%.2f", data['time']:get_numeric())
			local exp = os.date("%c", data['expire']:get_uint64())
			sptr:send_numeric(RPL_INFO, sptr.nick, tostring(data['mask']) .. " banned for " .. original .. " minute(s), " .. remain .. " remaining, expires " .. exp)
			data:next()
		end
		sptr:send_numeric(RPL_ENDOFINFO, sptr.nick, "End of INFO")
	elseif #msg.args >= 4 and same_text(msg.args[1], 'SET') and same_text(msg.args[3], 'EXPIRE') then
		local mask = msg.args[2]
		local mins = tonumber(msg.args[4]) or 0
		if mins < 0 then mins = 0 end
		local res = 0
		if mins ~= 0 then
			local ts = ircd.timestamp() + (mins * 60)
			local exp = os.date("%c", ts)
			res = ircd.db:exec_dml("UPDATE [server-bans] SET [created]=timestamp(), [expire]=? WHERE [mask]=?", ts, mask)
			if res ~= 0 then ircd.snomask_log(LOG_OPER, SNO_KILLS, "%1% updating K-LINE for %2%, expiring at %3%", sptr.userhost, mask, exp) end
		else
			res = ircd.db:exec_dml("UPDATE [server-bans] SET [created]=timestamp(), [expire]=NULL WHERE [mask]=?", mask)
			if res ~= 0 then ircd.snomask_log(LOG_OPER, SNO_KILLS, "%1% updating K-LINE for %2%, removed expiration", sptr.userhost, mask, reason) end
		end
		if res == 0 then
			sptr:send_notice("There is no K-line for " .. mask)
		end
	elseif #msg.args >= 4 and same_text(msg.args[1], 'SET') and same_text(msg.args[3], 'COMMENT') then
		local mask = msg.args[2]
		local comment = msg.args[4]
		local res = ircd.db:exec_dml("UPDATE [server-bans] SET [comment]=? WHERE [mask]=?", comment, mask)
		if res ~= 0 then
			sptr:send_notice("K-line comment updated for " .. mask)
		else
			sptr:send_notice("There is no K-line for " .. mask)
		end
	else
		sptr:send_numeric(ERR_UNKNOWN, sptr.nick, msg.command, "Invalid subcommand")
	end
end

function kline_enforce(mask)
	local results = ircd.db:exec_query("SELECT users.EID, (SELECT COUNT(*) FROM [server-bans] WHERE ([type] IN (1, 2) AND [UserHost] GLOB [mask]) OR ([type] IN (3, 4) AND cidr_match([mask], [Address]))) AS kline FROM users")
	while not results.eof do
		if 0 ~= results['kline']:get_int(0) then
			local target = ircd.get_client(results['EID']:get_uuid())
			target:close("K-lined")
		end
		results:step()
	end
end
