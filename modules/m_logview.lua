--
-- ircd - An IRCv3 Server
-- Copyright © 2020
--
-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, you can obtain one at: http://mozilla.org/MPL/2.0/
--

module = {
	name = "m_logview",
	description = "Log file viewing commands",
	api = 1,
}

local RECORD_MAX = 50

function module.init()
	ircd.add_command("LOGVIEW", do_logview)
	ircd.add_command("LOGFIND", do_logfind)
	ircd.add_command("LOGNUM", do_lognum)
	ircd.add_command("INFO", info_command)
end

function info_command(cptr, sptr, msg)
	local messages = {
		["logview"] = {
			"Syntax: LOGVIEW [n]",
			"This command displays up to " .. tostring(RECORD_MAX) .. " of the most recent log entries.",
			"You can specify a limit less than " .. tostring(RECORD_MAX) .. " with the optional parameter 'n'.",
			"For more info about the data format returned by LOGVIEW: /INFO LOG-DISPLAY",
		},
		["logfind"] = {
			"Syntax: LOGFIND <pattern> [n]",
			"This command searches for log entries matching the given glob pattern.",
			"Up to " .. tostring(RECORD_MAX) .. " records will display. You can specify a limit less than " .. tostring(RECORD_MAX) .. " with the optional parameter 'n'.",
			"For more info about the data format returned by LOGFIND: /INFO LOG-DISPLAY",
		},
		["lognum"] = {
			"Syntax: LOGNUM <id> [n]",
			"This command displays all log entries occuring after the specified ID number.",
			"Up to " .. tostring(RECORD_MAX) .. " records will display. You can specify a limit less than " .. tostring(RECORD_MAX) .. " with the optional parameter 'n'.",
			"For more info about the data format returned by LOGFIND: /INFO LOG-DISPLAY",
		},
		["log-display"] = {
			"The LOGVIEW, LOGFIND, and LOGNUM commands return their output using the INFO numerics in the following format:",
			"	371 <nick> :<id> <datestamp> <timestamp> <message>",
			"<id> is a unique number identifying the record.",
			"<datestamp> is the UTC date the record was generated, in YYYY-MM-DD format.",
			"<timestamp> is the UTC time the record was generated, in HH:mm:ss format.",
			"If the <message> contains newlines, the same <id> will generate on a subsequent 371 reply for all lines in the message.",
		},
	}
	local lines = messages[fold_case_nfkc(msg.args[1] or "")]
	if sptr:has_flag(CLI_REGISTERED) and lines ~= nil then
		if not sptr:has_umode(UMODE_OPERATOR) then
			sptr:send_numeric(ERR_NOTOPER, sptr.nick)
			return true
		end
		for i=1,#lines do
			sptr:send_numeric(RPL_INFO, sptr.nick, lines[i])
		end
		sptr:send_numeric(RPL_ENDOFINFO, sptr.nick, "End of /INFO")
		return true
	else
		return false
	end
end

function show_results(sptr, data)
	local empty = data.eof
	local last_id = 0
	while not data.eof do
		local lines = split(tostring(data["Message"]), "\r\n")
		for n=1,#lines do
			local text = tostring(data["ID"]) .. " " .. tostring(data["TS"]) .. " " .. lines[n]
			sptr:write_msg(nil, MSG_TRAILING_ARG, tostring(RPL_INFO), sptr.nick, text)
		end
		last_id = data["ID"]:get_int(0)
		data:step()
	end
	local count_after = ircd.query_log("SELECT count(*) FROM [log_data] WHERE [ID] > " .. tostring(last_id))
	sptr:send_numeric(RPL_ENDOFINFO, sptr.nick, "End of log info (" .. count_after[0]:get_text("0") .. " remaining)")
	return not empty
end

function do_logview(cptr, sptr, msg)
	if not sptr:has_flag(CLI_REGISTERED) then
		sptr:send_numeric(ERR_NOTREGISTERED, sptr.nick)
	elseif not sptr:has_umode(UMODE_OPERATOR) then
		sptr:send_numeric(ERR_NOTOPER, sptr.nick)
	else
		local limit = 30
		if #msg.args >= 1 then
			limit = tonumber(msg.args[1]) or 0
			if limit > RECORD_MAX or limit <= 0 then
				sptr:send_notice("LOGVIEW: the limit you specified is too high, or invalid (%1%)", limit)
				return
			end
		end
		local data = ircd.query_log("SELECT * FROM (SELECT [ID], [TS], [Message] FROM [log_data] ORDER BY [ID] DESC LIMIT " .. limit .. ") ORDER BY [ID]")
		if not show_results(sptr, data) then
			sptr:send_notice("LOGVIEW: no results")
		end
	end
end

function do_logfind(cptr, sptr, msg)
	if not sptr:has_flag(CLI_REGISTERED) then
		sptr:send_numeric(ERR_NOTREGISTERED, sptr.nick)
	elseif not sptr:has_umode(UMODE_OPERATOR) then
		sptr:send_numeric(ERR_NOTOPER, sptr.nick)
	elseif #msg.args < 1 then
		sptr:send_notice("LOGFIND: provide glob parameter")
	else
		local find = sql_escape_text('Q', msg.args[1])
		local limit = RECORD_MAX
		if #msg.args >= 2 then limit = tonumber(msg.args[2]) or 0 end
		if limit <= 0 or limit > RECORD_MAX then limit = RECORD_MAX end
		local data = ircd.query_log("SELECT * FROM (SELECT [ID], [TS], [Message] FROM [log_data] WHERE [Message] GLOB " .. find .. " ORDER BY [ID] DESC) ORDER BY [ID] LIMIT " .. limit)
		if not show_results(sptr, data) then
			sptr:send_notice("LOGFIND: no results")
		end
	end
end

function do_lognum(cptr, sptr, msg)
	if not sptr:has_flag(CLI_REGISTERED) then
		sptr:send_numeric(ERR_NOTREGISTERED, sptr.nick)
	elseif not sptr:has_umode(UMODE_OPERATOR) then
		sptr:send_numeric(ERR_NOTOPER, sptr.nick)
	elseif #msg.args < 1 then
		sptr:send_notice("LOGNUM: provide ID parameter")
	else
		local id = tonumber(msg.args[1]) or 0
		local limit = RECORD_MAX
		if #msg.args >= 2 then limit = tonumber(msg.args[2]) or 0 end
		if limit <= 0 or limit > RECORD_MAX then limit = RECORD_MAX end
		local data = ircd.query_log("SELECT * FROM (SELECT [ID], [TS], [Message] FROM [log_data] WHERE (ID > " .. id .. ") ORDER BY [ID] DESC) ORDER BY [ID] LIMIT " .. limit)
		if not show_results(sptr, data) then
			sptr:send_notice("LOGNUM: no results")
		end
	end
end
