--
-- ircd - An IRCv3 Server
-- Copyright © 2019
--
-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, you can obtain one at: http://mozilla.org/MPL/2.0/
--

module = {
	name = "m_server",
	description = "Server-to-server protocol and management",
	api = 1,
}

local VERSION_SERVER = 2 -- server protocol version

function module.init()
	ircd.add_command("CONNECT", do_connect)
	ircd.add_command("EOB", do_eob)
	ircd.add_command("EID", do_eid)
	ircd.add_command("SCAP", do_scap)
	ircd.add_command("SCHAN", do_schan)
	ircd.add_command("SERVER", do_server)
	ircd.add_command("SID", do_sid)
	ircd.add_command("SJOIN", do_sjoin)
	ircd.add_command("SMASK", do_smask)
	ircd.add_command("SQUIT", do_squit)
	ircd.add_command("SYNC", do_sync)
end

function hooks.on_link_connect(sptr)
	local passwd = ircd.db:exec_scalar("SELECT [password] FROM [server-links] WHERE [name]=?", sptr.nick)
	local info = ircd.info
	sptr:write_msg(nil, MSG_TRAILING_ARG|MSG_NO_PREFIX, "PASS", passwd)
	sptr:write_msg(nil, MSG_TRAILING_ARG|MSG_NO_PREFIX, "SERVER", info.fullname, info.id, info.net_name, VERSION_SERVER, info.description)
	ircd.send_server_caps(sptr)
	sptr:write_msg(nil, MSG_TRAILING_ARG|MSG_NO_PREFIX, "SYNC", ircd.timestamp())
end

function hooks.on_user(sptr)
	local ts_str = tostring(sptr.timestamp)
	-- EID <ID> <nick> <TS> <ident> <host> <ip> <modes> <account> :<gecos>
	if sptr:has_flag(CLI_SERVICE) then
		local type = sptr:get_metadata("service"):get_text()
		ircd.notify_servers(nil, nil, MSG_TRAILING_ARG, "EID", sptr.eid, sptr.nick, ts_str, sptr.username, sptr.hostname, sptr.address, sptr.umodes, type, sptr.gecos)
	else
		ircd.notify_servers(nil, nil, MSG_TRAILING_ARG, "EID", sptr.eid, sptr.nick, ts_str, sptr.username, sptr.hostname, sptr.address, sptr.umodes, sptr.account, sptr.gecos)
	end
end

function do_connect(cptr, sptr, msg)
	-- establish connection to a server
	-- the requested link must be configured with connect{} information
	if not sptr:has_flag(CLI_REGISTERED) then
		sptr:send_numeric(ERR_NOTREGISTERED, sptr.nick)
	elseif not sptr:has_umode(UMODE_OPERATOR) then
		sptr:send_numeric(ERR_NOTOPER, sptr.nick)
	elseif #msg.args < 1 then
		sptr:send_numeric(ERR_NEEDPARAMS, sptr.nick, msg.command)
	elseif #msg.args == 1 and not sptr:has_flag(CLI_REMOTE) then
		if nil ~= ircd.find_server_name(msg.args[1]) then
			sptr:send_notice("Already connected to %1%", msg.args[1])
		elseif not ircd.connect_to_link(sptr, msg.args[1]) then
			sptr:send_notice("Unable to connect %1%", msg.args[1])
		end
	end
end

function do_eob(cptr, sptr, msg)
	-- EOB = end of burst, synchronization is complete
	if not sptr:has_flag(CLI_SERVER) then
		sptr:send_numeric(ERR_UNKNOWNCOMMAND, sptr.nick, msg.command)
	elseif sptr:has_flag(CLI_EOB) then
		sptr:send_numeric(ERR_UNKNOWN, sptr.nick, msg.command, "EOB already received")
	elseif not sptr:has_flag(CLI_SYNC) then
		sptr:send_numeric(ERR_UNKNOWN, sptr.nick, msg.command, "SYNC not received")
	else
		sptr:set_flag(CLI_EOB, true)
		ircd.snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Synchronized with %1% (link complete)", sptr.nick)
	end
end

function do_eid(cptr, sptr, msg)
	-- :<SID> EID <ID> <nick> <TS> <ident> <host> <ip> <modes> <account> :<gecos>
	--            1    2      3    4       5      6    7       8         9
	-- for services, <account> is the service name
	-- <ID> must be a complete entity identifier (EID = SID + UID)
	if not sptr:has_flag(CLI_SERVER) then
		sptr:send_numeric(ERR_UNKNOWNCOMMAND, sptr.nick, msg.command)
	elseif #msg.args < 9 then
		sptr:send_numeric(ERR_NEEDPARAMS, sptr.nick, msg.command)
	else
		-- establish the local client record
		local nptr = ircd.new_client(sptr.session)
		nptr:set_flag(CLI_REMOTE, true)
		nptr:set_flag(CLI_SSL, sptr:has_flag(CLI_SSL))
		nptr.eid = msg.args[1]
		nptr.username = msg.args[4]
		nptr.hostname = msg.args[5]
		nptr.address = msg.args[6]
		nptr.umodes = tonumber(msg.args[7])
		if nptr:has_umode(UMODE_SERVICE) then
			nptr:set_metadata("service", msg.args[8])
		else
			nptr.account = msg.args[8]
		end
		nptr.gecos = msg.args[9]
		-- determine if a nick collision will occur
		local uptr = ircd.find_user(msg.args[2])
		if uptr ~= nil then
			-- collision detected: uptr is the other user
			if uptr.timestamp <= tonumber(msg.args[3]) then
				nptr.nick = nptr.eid
				uptr.nick = msg.args[2]
				if nptr:has_flag(CLI_REMOTE) then nptr.timestamp = ircd.timestamp() end
				ircd.notify_servers(nil, nptr, MSG_TRAILING_ARG, "NICK", nptr.eid, nptr.timestamp, "Nick collision")
			else
				uptr.nick = uptr.eid
				nptr.nick = msg.args[2]
				if uptr:has_flag(CLI_REMOTE) then uptr.timestamp = ircd.timestamp() end
				ircd.notify_servers(nil, uptr, MSG_TRAILING_ARG, "NICK", uptr.eid, uptr.timestamp, "Nick collision")
			end
		else
			-- no collision
			nptr.nick = msg.args[2]
			nptr.timestamp = tonumber(msg.args[3])
		end
		-- all done
		nptr:set_flag(CLI_REGISTERED, true)
		ircd.snomask_log(LOG_USER, SNO_REMOTE_USER_CONN, "Client connecting on %2%: %1%", nptr.userhost, nptr.server)
	end
end

function do_scap(cptr, sptr, msg)
	-- SCAP from a server is used to communicate limits (nicklen, etc.)
	-- store and verify with SYNC command - this is in case someday we
	-- exceed the 15 parameter limit with future additions to server CAP
	if not sptr:has_flag(CLI_SERVER) then
		sptr:send_numeric(ERR_UNKNOWNCOMMAND, sptr.nick, msg.command)
	elseif sptr:has_flag(CLI_EOB) then
		sptr:send_numeric(ERR_UNKNOWN, sptr.nick, msg.command, "Invalid command")
	else
		for i=1,#msg.args do
			local token = split(msg.args[i], "=")
			cptr:set_metadata(token[1], token[2])
		end
	end
end

function do_schan(cptr, sptr, msg)
	if not sptr:has_flag(CLI_SERVER) then
		sptr:send_numeric(ERR_UNKNOWNCOMMAND, sptr.nick, msg.command)
	elseif not sptr:has_flag(CLI_SYNC) then
		sptr:send_numeric(ERR_UNKNOWN, sptr.nick, msg.command, "SYNC not received")
	elseif #msg.args < 8 then
		sptr:send_numeric(ERR_NEEDPARAMS, sptr.nick, msg.command)
	elseif utf8.sub(msg.args[1],1,1) == CHTYPE_LOCAL then
		sptr:send_numeric(ERR_UNKNOWN, sptr.nick, msg.command, "invalid channel type")
		ircd.snomask_log(LOG_DEBUG, SNO_DEBUG, "Server %1% attempted to send local channel %2%", sptr.nick, msg.args[1])
	else
		-- :<SID> SCHAN <name> <ts> <topic-ts> <topic-setter> <modes> <key> <limit> :<topic>
		--              1      2    3          4              5       6     7       8
		local chname = msg.args[1]
		local info = channel.find(chname)
		-- determine if we have a channel collision
		local delta = 0
		if info ~= nil then delta = info.timestamp - tonumber(msg.args[2]) end
		if delta ~= 0 then
			ircd.snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Channel collision on %1% from %2% (TS delta %3%)", chname, sptr.nick, delta)
		end
		-- process channel
		if same_text(msg.args[6], "*") then msg.args[6] = "" end
		if info == nil then
			ircd.db:exec_dml("INSERT INTO [channels] ([Name], [TS], [TopicTime], [TopicSetBy], [Modes], [Key], [Limit], [Topic]) VALUES(?,?,?,?,?,?,?,?)",
				msg.args[1], msg.args[2], msg.args[3], msg.args[4], msg.args[5], msg.args[6], msg.args[7], msg.args[8])
		elseif delta < 0 then
			ircd.db:exec_dml("UPDATE [channels] SET [TS]=?2, [TopicTime]=?3, [TopicSetBy]=?4, [Modes]=?5, [Key]=?6, [Limit]=?7, [Topic]=?8 WHERE [Name]=?1",
				msg.args[1], msg.args[2], msg.args[3], msg.args[4], msg.args[5], msg.args[6], msg.args[7], msg.args[8])
			local members = channel.get_members(chname)
			local chmodes = channel.get_modes(chname)
			local mode_sub = "-" .. utf8.sub(chmodes,2)
			for _,memb in pairs(members) do
				if not memb:has_flag(CLI_REMOTE) then
					memb:write_msg(cptr, MSG_TRAILING_ARG, "TOPIC", chname, msg.args[8])
					memb:write_msg(cptr, MSG_TRAILING_ARG, "MODE", chname, mode_sub)
					memb:write_msg(cptr, MSG_TRAILING_ARG, "MODE", chname, chmodes)
				end
			end
		end
	end
end

function link_ssl_required(name)
	local req = ircd.db:exec_scalar("SELECT [ssl_required] FROM [server-links] WHERE [name]=?", name):get_int(0)
	return req ~= 0
end

function do_server(cptr, sptr, msg)
	-- SERVER <full-name> <SID> <net-name> <version> :<description>
	--        1           2     3          4         5
	if sptr:has_flag(CLI_REGISTERED) then
		sptr:send_numeric(ERR_REGISTERED, sptr.nick)
	elseif #msg.args < 5 then
		sptr:send_numeric(ERR_NEEDPARAMS, sptr.nick, msg.command)
	elseif not sptr:has_metadata("recv_pass") then
		ircd.snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Rejecting link with %1% (invalid password)", msg.args[1])
		sptr:close("Unauthorized")
	elseif same_text(msg.args[2], ircd.info.id) then
		ircd.snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Rejecting link with %1% (SID collision)", msg.args[1])
		sptr:close("Invalid SID")
	elseif nil ~= ircd.find_entity(msg.args[2]) then
		ircd.snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Rejecting link with %1% (SID collision)", msg.args[1])
		sptr:close("Invalid SID")
	elseif not same_text(msg.args[3], ircd.info.net_name) then
		ircd.snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Rejecting link with %1% (network name mismatch)", msg.args[1])
		sptr:close("Network name mismatch")
	elseif tonumber(msg.args[4]) ~= VERSION_SERVER then
		ircd.snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Rejecting link with %1% (version mismatch)", msg.args[1])
		sptr:close("Version mismatch")
	elseif link_ssl_required(msg.args[1]) and not sptr:has_flag(CLI_SSL) then
		ircd.snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Rejecting link with %1% (secure connection required)", msg.args[1])
		sptr:close("Secure connection required")
	elseif accept_server_link(sptr, msg.args[1]) then
		sptr:set_flag(CLI_SERVER, true)
		sptr.nick = msg.args[1]
		sptr.eid = msg.args[2]
		sptr.gecos = msg.args[5]
		ircd.db:exec_dml("INSERT OR REPLACE INTO [link-status] ([name], [status], [last_connect], [last_error], [SID]) VALUES(?, 1, timestamp(), NULL, ?)",
			sptr.nick, sptr.eid)
		ircd.snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Link with %1% established", sptr.nick)
		sptr:write_msg(nil, MSG_NO_PREFIX|MSG_TRAILING_ARG, "SID", ircd.info.fullname, ircd.info.id, ircd.info.description)
		ircd.send_server_caps(sptr)
		sptr:write_msg(nil, 0, "SYNC", ircd.timestamp())
	else
		ircd.snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Rejecting link with %1% (unauthorized)", msg.args[1])
		sptr:close("Unauthorized")
	end
end

function accept_server_link(sptr, name)
	local sql = ircd.db:compile("SELECT COUNT(*) FROM [server-links] WHERE [address] = @address AND [name] = @name AND [password] = @passwd")
	sql:bind_text("@name", name)
	sql:bind_text("@passwd", tostring(sptr:get_metadata("recv_pass")))
	sql:bind_text("@address", sptr.address)
	local res = sql:exec_scalar()
	return 0 ~= res:get_int(0)
end

function do_sid(cptr, sptr, msg)
	if not sptr:has_flag(CLI_SERVER) then
		sptr:send_numeric(ERR_UNKNOWNCOMMAND, sptr.nick, msg.command)
	elseif #msg.args < 2 then
		sptr:send_numeric(ERR_NEEDPARAMS, sptr.nick, msg.command)
	elseif same_text(msg.args[1], ircd.info.fullname) and not same_text(msg.args[2], ircd.info.id) then
		-- this would be a rather obscure error - server sent my name with a different ID ????
		cptr:close("Invalid SID")
	elseif same_text(msg.args[2], ircd.info.id) and not same_text(msg.args[1], ircd.info.fullname) then
		-- server sent my ID with a different name
		cptr:close("Invalid SID")
	else
		local uptr = ircd.find_server_name(msg.args[1])
		if uptr ~= nil and uptr:has_flag(CLI_SERVER) then
			uptr.eid = msg.args[2]
			ircd.db:exec_dml("UPDATE [link-status] SET [SID]=?2 WHERE [name]=?1", msg.args[1], msg.args[2])
			if #msg.args > 2 then uptr.gecos = msg.args[3] end
		end
	end
end

function do_sjoin(cptr, sptr, msg)
	if not sptr:has_flag(CLI_SERVER) then
		sptr:send_numeric(ERR_UNKNOWNCOMMAND, sptr.nick, msg.command)
	elseif #msg.args < 2 then
		sptr:send_numeric(ERR_NEEDPARAMS, sptr.nick, msg.command)
	elseif not sptr:has_flag(CLI_SYNC) then
		ircd.send_numeric(ERR_UNKNOWN, sptr.nick, msg.command, "SYNC not received")
	elseif utf8.sub(msg.args[1],1,1) == CHTYPE_LOCAL then
		ircd.snomask_log(LOG_DEBUG, SNO_DEBUG, "Server %1% attempted to SJOIN local channel %2%", sptr.nick, msg.args[1])
	else
		local chrec = channel.find(msg.args[1])
		if chrec ~= nil then
			sjoin_process(cptr, sptr, chrec, msg.args)
		else
			ircd.snomask_log(LOG_DEBUG, SNO_DEBUG, "Server %1% attempted to SJOIN unknown channel %2%", sptr.nick, msg.args[1])
		end
	end
end

function sjoin_process(cptr, sptr, chrec, args)
	for i=2,#args do
		local arg = args[i]
		local start = utf8.sub(args[i],1,1)
		local modes = {
			["@"] = { "o", channel.set_op, },
			["+"] = { "v", channel.set_voice, },
		}
		local info = modes[start]
		if info ~= nil then
			local name = utf8.sub(arg, 2)
			local uptr = ircd.find_entity(name)
			if uptr ~= nil then
				channel.ins_user(chrec.name, cptr, uptr)
				info[2](chrec.name, uptr, true)
				channel.notify_members(chrec.name, cptr, sptr, 0, "MODE", chrec.name, "+" .. info[1], uptr.nick)
			end
		else
			local uptr = ircd.find_entity(arg)
			if uptr ~= nil then channel.ins_user(chrec.name, cptr, uptr) end
		end
	end
end

function do_smask(cptr, sptr, msg)
	if not sptr:has_flag(CLI_SERVER) then
		sptr:send_numeric(ERR_UNKNOWNCOMMAND, sptr.nick, msg.command)
	elseif #msg.args < 2 then
		sptr:send_numeric(ERR_NEEDPARAMS, sptr.nick, msg.command)
	elseif not sptr:has_flag(CLI_SYNC) then
		ircd.send_numeric(ERR_UNKNOWN, sptr.nick, msg.command, "SYNC not received")
	elseif utf8.sub(msg.args[1],1,1) == CHTYPE_LOCAL then
		ircd.snomask_log(LOG_DEBUG, SNO_DEBUG, "Server %1% attempted to SMASK local channel %2%", sptr.nick, msg.args[1])
	else
		local chrec = channel.find(msg.args[1])
		if chrec ~= nil then
			local types = {
				["b"] = CHLIST_BAN,
				["e"] = CHLIST_EXEMPT,
			}
			if types[msg.args[2]] ~= nil then
				channel.list_add(chrec.name, types[msg.args[2]], msg.args[3], msg.args[4], tonumber(msg.args[5]))
				channel.notify_members(chrec.name, cptr, sptr, 0, "MODE", chrec.name, "+" .. msg.args[2], msg.args[3])
			end
		else
			ircd.snomask_log(LOG_DEBUG, SNO_DEBUG, "Server %1% attempted to SMASK unknown channel %2%", sptr.nick, msg.args[1])
		end
	end
end

function do_squit(cptr, sptr, msg)
	if not sptr:has_flag(CLI_REGISTERED) then
		--sptr:send_numeric(ERR_NOTREGISTERED, sptr.nick)
	elseif not sptr:has_umode(UMODE_OPERATOR) and not sptr:has_flag(CLI_SERVER) then
		sptr:send_numeric(ERR_NOTOPER, sptr.nick)
	elseif #msg.args < 1 then
		sptr:send_numeric(ERR_NEEDPARAMS, sptr.nick, msg.command)
	elseif cptr:has_flag(CLI_SERVER) and (same_text(msg.args[1], ircd.info.fullname) or same_text(msg.args[1], ircd.info.id)) then
		ircd.snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Received SQUIT %1% from %2%", ircd.info.fullname, cptr.nick)
		cptr:close("SQUIT")
	else
		local uptr = nil
		if sptr:has_flag(CLI_SERVER) then
			uptr = ircd.find_entity(msg.args[1])
		else
			uptr = ircd.find_server_name(msg.args[1])
		end
		if uptr ~= nil then
			ircd.snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Received SQUIT %1% from %2%", uptr.nick, sptr.userhost)
			ircd.notify_servers(cptr, sptr, 0, "SQUIT", uptr.eid)
			uptr:close("SQUIT")
		else
			sptr:send_numeric(ERR_NOSUCHSERVER, sptr.nick, msg.args[1])
		end
	end
end

function do_sync(cptr, sptr, msg)
	if not sptr:has_flag(CLI_SERVER) then
		sptr:send_numeric(ERR_UNKNOWNCOMMAND, sptr.nick, msg.command)
		return
	elseif #msg.args < 1 then
		sptr:send_numeric(ERR_NEEDPARAMS, sptr.nick, msg.command)
		return
	elseif sptr:has_flag(CLI_SYNC) then
		ircd.send_numeric(ERR_UNKNOWN, sptr.nick, msg.command, "SYNC already received")
		return
	elseif sptr:has_flag(CLI_EOB) then
		ircd.send_numeric(ERR_UNKNOWN, sptr.nick, msg.command, "SYNC already completed")
		return
	end
	delta = math.abs(ircd.timestamp() - tonumber(msg.args[1]))
	if delta > MAX_TSDELTA then
		ircd.snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Excessive T/S delta from %1%, rejecting link (%2%)", sptr.nick, delta)
		sptr:close("Excess T/S delta")
	elseif not ircd.validate_server_caps(sptr) then
		ircd.snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Rejecting link %1% due to configuration error (limits)", sptr.nick)
		sptr:close("Invalid CAPs")
	else
		sptr:set_flag(CLI_SYNC, true)
		sptr:set_metadata("sync_ts", tonumber(msg.args[1]))
		ircd.snomask_log(LOG_SERVER, SNO_SERVER_LINK, "Synchronizing with %1%", sptr.nick)
		send_sync_data(sptr, "SID", MSG_TRAILING_ARG, "SELECT [Nick], [EID], ifnull([Gecos], '') FROM [servers] ORDER BY [EID]")
		send_sync_data(sptr, "EID", MSG_TRAILING_ARG, "SELECT [EID], [Nick], [TS], [UserName], [Hostname], ifnull([Address], '*'), ifnull([Mode], '0'), ifnull([Account], '*'), ifnull([Gecos], '') FROM [users] ORDER BY [EID]")
		send_sync_data(sptr, "SCHAN", MSG_TRAILING_ARG, "SELECT [Name], [TS], ifnull([TopicTime], '0'), ifnull([TopicSetBy], '*'), [Modes], ifnull([Key], '*'), ifnull([Limit], '0'), [Topic] FROM [channels] WHERE substr([Name],1,1) <> '&'")
		send_sync_data(sptr, "SMASK", 0, "SELECT [ChannelName], choose([ListID], '*', 'b', 'e'), [Mask], [Creator], [TS] FROM [channel-masks] WHERE [ListID]<>" .. CHLIST_INVITE)
		send_sync_data(sptr, "SJOIN", 0, "SELECT [ChannelName], channel_status_text([Status])||[clients].[Nick] FROM [channel-users] JOIN [clients] ON [clients].[EID]=[channel-users].[EID]")
		sptr:write_msg(nil, 0, "EOB")
	end
end

function send_sync_data(sptr, cmd, flags, sql)
	local query = ircd.db:exec_query(sql)
	while query ~= nil and not query.eof do
		sptr:write_msg(nil, flags, cmd, query)
		query:step()
	end
end
