--
-- ircd - An IRCv3 Server
-- Copyright © 2020
--
-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, you can obtain one at: http://mozilla.org/MPL/2.0/
--

module = {
	name = "m_httpget",
	description = "Respond to http scanners",
	api = 1,
}

function module.init()
	ircd.add_command("GET", http_command)
	ircd.add_command("HEAD", http_command)
end

function http_command(cptr, sptr, msg)
	if not sptr:has_flag(CLI_REGISTERED) and not sptr:has_flag(CLI_REMOTE) then
		ircd.snomask_log(LOG_USER, SNO_LOCAL_USER_CONN, "Rejecting connection from %1% (http request)", sptr.userhost)
		sptr:write_msg(nil, MSG_NO_PREFIX, "HTTP/1.1", "400 Not an HTTP server")
		-- add temporary K-line
		local ts = ircd.timestamp() + (60 * 60)
		local exp = os.date("%c", ts)
		ircd.snomask_log(LOG_OPER, SNO_KILLS, "Adding K-LINE for %1%, expiring at %2%", sptr.address, mask, exp)
		ircd.add_kline_temp(sptr, sptr.address, "HTTP scanner", ts)
		-- shut down the socket
		sptr:shutdown()
	else
		return false
	end
end
