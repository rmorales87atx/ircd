--
-- ircd - An IRCv3 Server
-- Copyright © 2019
--
-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, you can obtain one at: http://mozilla.org/MPL/2.0/
--

module = {
	name = "m_setname",
	description = "IRCv3 'SETNAME' capability",
	api = 1,
}

function module.init()
	ircd.add_command("SETNAME", do_setname)
	ircd.add_cap("setname")
end

function do_setname(cptr, sptr, msg)
	-- SETNAME must be allowed even when CAP is not present
	if not sptr:has_flag(CLI_REGISTERED) then
		sptr:send_numeric(ERR_NOTREGISTERED, sptr.nick)
	elseif #msg.args < 1 then
		sptr:send_numeric(ERR_NEEDPARAMS, sptr.nick, msg.command)
	else
		sptr.gecos = left(msg.args[1], MAX_NAMELEN)
		if sptr:has_cap("setname") and not sptr:has_flag(CLI_REMOTE) then
			sptr:write_msg(sptr, MSG_TRAILING_ARG, msg.command, msg.args[1])
		end
		ircd.notify_servers(cptr, sptr, MSG_TRAILING_ARG, msg.command, msg.args[1])
		-- TODO: notify common channels?
	end
end
