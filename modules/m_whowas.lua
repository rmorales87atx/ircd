--
-- ircd - An IRCv3 Server
-- Copyright © 2019
--
-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, you can obtain one at: http://mozilla.org/MPL/2.0/
--

-- Module definition
module = {
	name = "m_monitor",
	description = "WHOWAS user history",
	api = 1,
}

-- Module numerics
local RPL_WHOWASUSER = 314
local RPL_ENDOFWHOWAS = 369
local ERR_WASNOSUCHNICK = 406

-- Module initializer
function module.init()
	ircd.add_command("WHOWAS", do_whowas)
	ircd.add_cap("monitor")
	ircd.set_numeric(RPL_WHOWASUSER, "%1% %2% %3% %4% * :%5%")
	ircd.set_numeric(RPL_ENDOFWHOWAS, "%1% %2% :End of /WHOWAS")
	ircd.set_numeric(ERR_WASNOSUCHNICK, "%1% %2% :There was no such nickname")
	ircd.db:exec_dml([[CREATE TABLE IF NOT EXISTS [user-history] (
		[Nick] TEXT COLLATE NOCASE,
		[UserName] TEXT COLLATE NOCASE,
		[Hostname] TEXT COLLATE NOCASE,
		[Gecos] TEXT,
		[TS] INTEGER,
		[Server] TEXT COLLATE NOCASE,
		[Code] TEXT COLLATE NOCASE,
		PRIMARY KEY([Nick], [TS]));]])
end

-- Module implementation

function hooks.on_user(sptr)
	ircd.db:exec_dml([[INSERT OR REPLACE INTO [user-history]
		SELECT [users].[Nick], [users].[UserName], [users].[Hostname], [users].[Gecos], [users].[TS], [users].[Server], soundex([users].[Nick]) AS [Code]
		FROM [users] WHERE [users].[EID]=?1]], sptr.eid)
end

function hooks.on_quit(sptr)
	ircd.db:exec_dml([[INSERT OR REPLACE INTO [user-history]
		SELECT [users].[Nick], [users].[UserName], [users].[Hostname], [users].[Gecos], [users].[TS], [users].[Server], soundex([users].[Nick]) AS [Code]
		FROM [users] WHERE [users].[EID]=?1]], sptr.eid)
end

function hooks.on_nick(sptr, old_nick)
	ircd.db:exec_dml([[INSERT OR REPLACE INTO [user-history]
		SELECT ?2 AS [Nick], [users].[UserName], [users].[Hostname], [users].[Gecos], [users].[TS], [users].[Server], soundex(?2) AS [Code]
		FROM [users] WHERE [users].[EID]=?1]], sptr.eid, old_nick)
end

function do_whowas(cptr, sptr, msg)
	if not sptr:has_flag(CLI_REGISTERED) then
		sptr:send_numeric(ERR_NOTREGISTERED, sptr.nick)
	elseif #msg.args == 2 and not sptr:has_flag(CLI_REMOTE) and not same_text(msg.args[2], ircd.info.fullname) then
		local servptr = ircd.find_server_name(msg.args[1])
		if servptr ~= nil then
			servptr:write_msg(sptr, MSG_TRAILING_ARG, msg.command, msg.args[1])
		else
			sptr:send_numeric(ERR_NOSUCHSERVER, sptr.nick, msg.args[2])
		end
	else
		local data = ircd.db:exec_query("SELECT [Nick], [UserName], [Hostname], [Gecos] FROM [user-history] WHERE [Code] = soundex(?) ORDER BY [TS] DESC",
			msg.args[1])
		if data == nil or data.eof then
			sptr:send_numeric(ERR_WASNOSUCHNICK, sptr.nick, msg.args[1])
		else
			while not data.eof do
				sptr:send_numeric(RPL_WHOWASUSER, sptr.nick, data[0], data[1], data[2], data[3])
				data:step()
			end
		end
		sptr:send_numeric(RPL_ENDOFWHOWAS, sptr.nick, msg.args[1])
	end
end

