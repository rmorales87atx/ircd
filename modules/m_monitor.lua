--
-- ircd - An IRCv3 Server
-- Copyright © 2019
--
-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, you can obtain one at: http://mozilla.org/MPL/2.0/
--

-- Module definition
module = {
	name = "m_monitor",
	description = "IRCv3 monitor capability",
	api = 1,
}

-- Module numerics
local RPL_MONONLINE = 730
local RPL_MONOFFLINE = 731
local RPL_MONLIST = 732
local RPL_ENDOFMONLIST = 733
local ERR_MONLISTFULL = 734

-- Module initializer
function module.init()
	ircd.add_command("MONITOR", do_monitor)
	ircd.add_cap("monitor")
	ircd.set_numeric(RPL_MONONLINE, "%1% :%2%")
	ircd.set_numeric(RPL_MONOFFLINE, "%1% :%2%")
	ircd.set_numeric(RPL_MONLIST, "%1% :%2%")
	ircd.set_numeric(RPL_ENDOFMONLIST, "%1% :End of MONITOR list")
	ircd.set_numeric(ERR_MONLISTFULL, "%1% :Your MONITOR list is full")
	ircd.db:exec_dml([[CREATE TABLE IF NOT EXISTS [user-watch](
		[EID] TEXT COLLATE BINARY REFERENCES [clients]([EID]) ON DELETE CASCADE ON UPDATE CASCADE,
		[WatchName] TEXT COLLATE NOCASE,
		[WatchFlags] INTEGER,
		PRIMARY KEY([EID],[WatchName]));]])
end

-- Module implementation

function hooks.on_user(sptr)
	local query = ircd.db:exec_query("SELECT [user-watch].[EID] FROM [user-watch] WHERE [user-watch].WatchName=?1", sptr.nick)
	while query ~= nil and not query.eof do
		local uptr = ircd.get_client(query[0]:get_uuid())
		if uptr:has_cap("monitor") then
			uptr:send_numeric(RPL_MONONLINE, "*", sptr.userhost)
		end
		query:next()
	end
end

function hooks.on_quit(sptr)
	local query = ircd.db:exec_query("SELECT [user-watch].[EID] FROM [user-watch] WHERE [user-watch].WatchName=?1", sptr.nick)
	while query ~= nil and not query.eof do
		local uptr = ircd.get_client(query[0]:get_uuid())
		if uptr:has_cap("monitor") then
			uptr:send_numeric(RPL_MONOFFLINE, "*", sptr.userhost)
		end
		query:next()
	end
end

function hooks.on_nick(sptr, old_nick)
	local sql = [[SELECT [user-watch].[EID], 0 AS code FROM [user-watch] WHERE [user-watch].WatchName=?1
				UNION SELECT [user-watch].[EID], 1 AS code FROM [user-watch] WHERE [user-watch].WatchName=?2]]
	local query = ircd.db:exec_query(sql, old_nick, sptr.nick)
	while query ~= nil and not query.eof do
		local uptr = ircd.get_client(query[0]:get_uuid())
		if uptr ~= nil and uptr:has_cap("monitor") then
			local code = query[1]:get_int(0)
			if code == 0 then
				uptr:send_numeric(RPL_MONOFFLINE, "*", old_nick)
			elseif code == 1 then
				uptr:send_numeric(RPL_MONONLINE, "*", sptr.userhost)
			end
		end
		query:next()
	end
end

function do_monitor(cptr, sptr, msg)
	if sptr:has_flag(CLI_REMOTE) or not sptr:has_cap("monitor") then
		sptr:send_numeric(ERR_UNKNOWNCOMMAND, sptr.nick, msg.command)
	elseif not sptr:has_flag(CLI_REGISTERED) then
		sptr:send_numeric(ERR_NOTREGISTERED, sptr.nick)
	elseif #msg.args < 1 then
		sptr:send_numeric(ERR_NEEDPARAMS, sptr.nick, msg.command)
	elseif same_text(msg.args[1], "+") and #msg.args > 1 then
		do_monitor_add(cptr, sptr, msg)
	elseif same_text(msg.args[1], "-") and #msg.args > 1 then
		do_monitor_del(cptr, sptr, msg)
	elseif same_text(msg.args[1], "S") then
		do_monitor_show(cptr, sptr, msg)
	elseif same_text(msg.args[1], "L") then
		do_monitor_list(cptr, sptr, msg)
	elseif same_text(msg.args[1], "C") then
		ircd.db:exec_dml("DELETE FROM [user-watch] WHERE [EID]=?", sptr.eid)
	else
		sptr:send_numeric(ERR_UNKNOWN, sptr.nick, msg.command .. " " .. table.concat(msg.args, " "), "Invalid parameters")
	end
end

function do_monitor_add(cptr, sptr, msg)
	local targets = split(msg.args[2], ",")
	local count = ircd.db:exec_scalar("SELECT COUNT(*) FROM [user-watch] WHERE [EID]=?", sptr.eid):get_int(0)
	if (count + #targets) > MAX_MONITOR then
		sptr:send_numeric(ERR_MONLISTFULL, sptr.nick, MAX_MONITOR, msg.args[2])
	else
		for _,v in pairs(targets) do
			ircd.db:exec_dml("INSERT INTO [user-watch] ([EID], [WatchName]) VALUES(?,?)", sptr.eid, v)
		end
		do_monitor_show(cptr, sptr, nil)
	end
end

function do_monitor_del(cptr, sptr, msg)
	local targets = split(msg.args[2], ",")
	for _,v in pairs(targets) do
		ircd.db:exec_dml("DELETE FROM [user-watch] WHERE [EID]=? AND [WatchName]=?", sptr.eid, v)
	end
end

function do_monitor_show(cptr, sptr, msg)
	local data = ircd.db:exec_query("SELECT [user-watch].WatchName, [clients].[EID] FROM ([user-watch] LEFT JOIN [clients] ON [clients].[Nick]=[user-watch].WatchName) WHERE [user-watch].[EID]=?", sptr.eid)
	while data ~= nil and not data.eof do
		if not data[1].null then
			local uptr = ircd.get_client(data[1]:get_uuid())
			sptr:send_numeric(RPL_MONONLINE, sptr.nick, uptr.userhost)
		else
			sptr:send_numeric(RPL_MONOFFLINE, sptr.nick, data[0])
		end
		data:step()
	end
end

function do_monitor_list(cptr, sptr, msg)
	local data = ircd.db:exec_query("SELECT [WatchName] FROM [user-watch] WHERE [EID]=?", sptr.eid)
	while data ~= nil and not data.eof do
		sptr:send_numeric(RPL_MONLIST, sptr.nick, data[0])
		data:step()
	end
	sptr:send_numeric(RPL_ENDOFMONLIST, sptr.nick)
end
