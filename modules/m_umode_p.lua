--
-- ircd - An IRCv3 Server
-- Copyright © 2019
--
-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, you can obtain one at: http://mozilla.org/MPL/2.0/
--

module = {
	name = "m_umode_p",
	description = "Usermode +g and +p (based on Hybrid's usermode +g)",
	api = 1,
}

local RPL_ACCEPTLIST = 280
local RPL_ACCEPTEND = 281
local ERR_ACCEPTFULL = 456
local ERR_ACCEPTEXIST = 457
local ERR_ACCEPTNOT = 458
local ERR_CANTSENDTOUSER = 531
local MAX_ACCEPT = MAX_MONITOR
local UMODE_PRIVATE = nil -- ignoring all PRIVMSG
local UMODE_PRIVCOMM = nil -- ignoring all PRIVMSG not from a common channel

function module.init()
	ircd.add_command("ACCEPT", do_accept_command)
	ircd.set_numeric(RPL_ACCEPTLIST, "%1% :%2%")
	ircd.set_numeric(RPL_ACCEPTEND, "%1% :End of ACCEPT list")
	ircd.set_numeric(ERR_ACCEPTFULL, "%1% :ACCEPT list is full")
	ircd.set_numeric(ERR_ACCEPTEXIST, "%1% %2% :Already in ACCEPT list")
	ircd.set_numeric(ERR_ACCEPTNOT, "%1% %2% :Not in ACCEPT list")
	ircd.set_numeric(ERR_CANTSENDTOUSER, "%1% %2% :You are not permitted to send private messages to this user")
	-- add usermodes +g and +p
	UMODE_PRIVATE = ircd.add_umode('g', 'private1')
	UMODE_PRIVCOMM = ircd.add_umode('p', 'private2')
	-- init SQL tables
	ircd.db:exec_dml([[CREATE TABLE IF NOT EXISTS [user-accept](
		[EID] TEXT COLLATE BINARY REFERENCES [clients]([EID]) ON DELETE CASCADE ON UPDATE CASCADE,
		[AcceptName] TEXT COLLATE NOCASE,
		PRIMARY KEY([EID],[AcceptName]));

	CREATE TABLE IF NOT EXISTS [accept-notify](
		[EID] TEXT COLLATE BINARY REFERENCES [clients]([EID]) ON DELETE CASCADE ON UPDATE CASCADE,
		[From] TEXT COLLATE BINARY REFERENCES [clients]([EID]) ON DELETE CASCADE ON UPDATE CASCADE,
		[TS] INTEGER NOT NULL DEFAULT 0,
		PRIMARY KEY([EID], [From]));]])
end

function hooks.on_quit(sptr)
	ircd.db:exec_query("DELETE FROM [user-accept] WHERE [AcceptName]=?", sptr.nick)
end

function hooks.on_nick(sptr, old_nick)
	ircd.db:exec_query("DELETE FROM [user-accept] WHERE [AcceptName]=?", old_nick)
end

function hooks.allow_message_user(cptr, sptr, target, command, text)
	if target:has_umode(UMODE_PRIVATE) and not sptr:has_flag(CLI_SERVICE) then
		if not find_accept(target, sptr.nick) then
			sptr:send_numeric(ERR_CANTSENDTOUSER, sptr.nick, target.nick)
			notify_accept(target, sptr)
			return false -- prevent message from sending
		end
		return true -- allow message
	elseif target:has_umode(UMODE_PRIVCOMM) and not sptr:has_flag(CLI_SERVICE) then
		if not find_accept(target, sptr.nick) and not ircd.has_common_channels(target, sptr) then
			sptr:send_numeric(ERR_CANTSENDTOUSER, sptr.nick, target.nick)
			notify_accept(target, sptr)
			return false -- prevent message from sending
		end
		return true -- allow message
	end
	return true -- allow by default
end

local function f_test(bits, mask)
	return (bits & mask) == mask
end

function hooks.before_umode(sptr, cur, prev)
	-- only one of +g or +p can be set
	-- if both are set, +g will overrule
	if f_test(cur, UMODE_PRIVATE) and f_test(cur, UMODE_PRIVCOMM) then
		cur = cur & ~UMODE_PRIVCOMM
	end
	return cur
end

function find_accept(target, who)
	local n = ircd.db:exec_scalar("SELECT COUNT(*) FROM [user-accept] WHERE [EID]=?1 AND [AcceptName]=?2", target.eid, who):get_int(0)
	return n ~= 0
end

function notify_accept(target, from)
	-- limit notices to the target 1x per minute
	local last_delta = ircd.db:exec_scalar("SELECT timestamp()-[TS] FROM [accept-notify] WHERE [EID]=?1 AND [From]=?2", target.eid, from.eid):get_int(0)
	if last_delta == 0 or last_delta > 60 then
		target:send_notice("*** %1% attempted to message you. To accept future messages, use command ACCEPT %2%", from.userhost, from.nick)
	end
	-- update notification TS
	ircd.db:exec_dml("INSERT OR REPLACE INTO [accept-notify] ([EID], [From], [TS]) VALUES(?, ?, timestamp())", target.eid, from.eid)
end

function do_accept_command(cptr, sptr, msg)
	if sptr:has_flag(CLI_REMOTE) then
		return false
	elseif not sptr:has_flag(CLI_REGISTERED) then
		sptr:send_numeric(ERR_NOTREGISTERED, sptr.nick)
	elseif not sptr:has_umode(UMODE_PRIVATE) and not sptr:has_umode(UMODE_PRIVCOMM) then
		sptr:send_notice("You must be in private mode (+g or +p) to use ACCEPT")
	elseif #msg.args < 1 then
		sptr:send_numeric(ERR_NEEDPARAMS, sptr.nick, msg.command)
	elseif same_text(msg.args[1], "*") then
		do_accept_list(sptr)
	else
		local names = split(msg.args[1], ",")
		for _,v in pairs(names) do
			if utf8.sub(v,1,1) == '-' then
				do_accept_del(sptr, utf8.sub(v,2))
			else
				do_accept_add(sptr, v)
			end
		end
	end
end

function do_accept_list(sptr)
	local lines = get_accept_list(sptr)
	for i,v in pairs(lines) do
		if #lines[i] > 0 then sptr:send_numeric(RPL_ACCEPTLIST, sptr.nick, table.concat(v, " ")) end
	end
	sptr:send_numeric(RPL_ACCEPTEND, sptr.nick)
end

function get_accept_list(sptr)
	local names = ircd.db:exec_query("SELECT [AcceptName] FROM [user-accept] WHERE [EID]=?", sptr.eid)
	-- currently allowing 10 names per line
	local n = 1
	local r = 1
	local lines = {{}}
	while names ~= nil and not names.eof do
		if n > 10 then
			r = r + 1
			n = 1
			lines[r] = {}
		end
		table.insert(lines[r], names[0]:get_text())
		names:step()
		n = n + 1
	end
	return lines
end

function do_accept_add(sptr, who)
	if utf8.len(who) == 0 then return end
	local num = ircd.db:exec_scalar("SELECT COUNT(*) FROM [user-accept] WHERE [EID]=?", sptr.eid):get_int(0)
	if (num+1) >= MAX_ACCEPT then
		sptr:send_numeric(ERR_ACCEPTFULL, sptr.nick)
		return
	end
	local uptr = ircd.find_user(who)
	if uptr ~= nil then
		local n = ircd.db:exec_dml("INSERT INTO [user-accept] ([EID], [AcceptName]) VALUES(?,?)", sptr.eid, uptr.nick)
		if n == 0 then
			sptr:send_numeric(ERR_ACCEPTEXIST, sptr.nick, uptr.nick)
		end
	else
		sptr:send_numeric(ERR_NOSUCHNICK, sptr.nick, who)
	end
end

function do_accept_del(sptr, who)
	if utf8.len(who) == 0 then return end
	local num = ircd.db:exec_dml("DELETE FROM [user-accept] WHERE [EID]=?1 AND [AcceptName]=?2", sptr.eid, who)
	if num == 0 then
		sptr:send_numeric(ERR_ACCEPTNOT, sptr.nick, who)
	end
end
