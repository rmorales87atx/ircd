--
-- ircd - An IRCv3 Server
-- Copyright © 2020
--
-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, you can obtain one at: http://mozilla.org/MPL/2.0/
--

module = {
	name = "m_ison",
	description = "ISON command",
	api = 1,
}

local RPL_ISON = 303

function module.init()
	ircd.add_command("ISON", do_ison)
	ircd.set_numeric(RPL_ISON, "%1% :%2%")
end

function do_ison(cptr, sptr, msg)
	if sptr:has_flag(CLI_REMOTE) then
		sptr:send_numeric(ERR_UNKNOWNCOMMAND, sptr.nick, msg.command)
	elseif not sptr:has_flag(CLI_REGISTERED) then
		sptr:send_numeric(ERR_NOTREGISTERED, sptr.nick)
	elseif #msg.args < 1 then
		sptr:send_numeric(ERR_NEEDPARAMS, sptr.nick, msg.command)
	else
		local results = {}
		for i,arg in pairs(msg.args) do
			if i > MAX_TARGETS then break end
			local uptr = ircd.find_user(arg)
			if uptr ~= nil then
				table.insert(results, uptr.nick)
			end
		end
		sptr:send_numeric(RPL_ISON, sptr.nick, table.concat(results, " "))
	end
end
