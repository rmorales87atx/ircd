--
-- ircd - An IRCv3 Server
-- Copyright © 2019
--
-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, you can obtain one at: http://mozilla.org/MPL/2.0/
--

module = {
	name = "m_jupe",
	description = "Allow opers to manage server jupes",
	api = 1,
}

function module.init()
	ircd.add_command("JUPE", do_jupe)
end

function do_jupe(cptr, sptr, msg)
	-- add or remove nick/channel JUPE
	if not sptr:has_flag(CLI_REGISTERED) then
		sptr:send_numeric(ERR_NOTREGISTERED, sptr.nick)
	elseif not sptr:has_umode(UMODE_OPERATOR) and not sptr:has_umode(UMODE_SERVICE) then
		sptr:send_numeric(ERR_NOTOPER, sptr.nick)
	elseif #msg.args < 1 then
		sptr:send_numeric(ERR_NEEDPARAMS, sptr.nick, msg.command)
	elseif same_text(msg.args[1], "ADD") or same_text(msg.args[1], "DEL") then
		local reason = ""
		-- oper can provide a reason as a trailing arg
		if same_text(msg.args[1], "ADD") and (msg.flags & MSG_TRAILING_ARG) ~= 0 then
			local i = #msg.args
			reason = left(msg.args[i], MAX_REASON)
			msg.args:erase(i)
		end
		for i,v in pairs(msg.args) do
			if i > 1 and ircd.valid_nick(v) ~= NAME_ERROR_NONE then
				if same_text(msg.args[1], "ADD") then
					do_jupe_add_nick(sptr, v, reason)
				elseif same_text(msg.args[1], "DEL") then
					do_jupe_del_nick(sptr, v)
				end
			elseif i > 1 and ircd.valid_chan(v) ~= NAME_ERROR_NONE then
				if same_text(msg.args[1], "ADD") then
					do_jupe_add_channel(sptr, v, reason)
				elseif same_text(msg.args[1], "DEL") then
					do_jupe_del_channel(sptr, v)
				end
			end
		end
	end
end

function do_jupe_add_nick(sptr, what, reason)
	local uptr = ircd.find_user(what)
	if uptr ~= nil then uptr:kill("Invalid nickname") end
	ircd.add_jupe(JUPE_NICK, what, sptr, reason, JUPE_STATUS_TEMP)
	ircd.snomask_log(LOG_OPER, SNO_NICK_JUPE, "Received JUPE ADD %1% from %2% (%3%)", what, sptr.userhost, reason)
end

function do_jupe_add_channel(sptr, what, reason)
	channel.kill(what)
	ircd.add_jupe(JUPE_CHANNEL, what, sptr, reason, JUPE_STATUS_TEMP)
	ircd.snomask_log(LOG_OPER, SNO_CHAN_JUPE, "Received JUPE ADD %1% from %2% (%3%)", what, sptr.userhost, reason)
end

function do_jupe_del_nick(sptr, what)
	ircd.del_jupe(JUPE_NICK, what)
	ircd.snomask_log(LOG_OPER, SNO_NICK_JUPE, "Received JUPE DEL %1% from %2% (%3%)", what, sptr.userhost, reason)
end

function do_jupe_del_channel(sptr, what)
	ircd.del_jupe(JUPE_CHANNEL, what)
	ircd.snomask_log(LOG_OPER, SNO_CHAN_JUPE, "Received JUPE DEL %1% from %2% (%3%)", what, sptr.userhost, reason)
end
