# Welcome

This repository hosts a simple IRC server written in modern C++ and [implementing IRCv3 specifications](IRCv3), licensed under [MPL 2.0](https://mozilla.org/en-US/MPL/2.0/). The server also makes extensive use of Lua to implement modules (commands).

The client protocol is largely based on **[RFC 2812](https://tools.ietf.org/html/rfc2812)**. Other influences include:

* https://modern.ircdocs.horse/
* https://defs.ircdocs.horse/
* [ircd-ratbox](https://www.ratbox.org/)

## General Code Prerequisites
* In general, a C++ compiler that supports **C++17** is required; clang 8.0+ and Visual Studio 2019+ are recommended.
* Boost version **1.70**+ (several libraries in use: asio, date_time, regex, spirit, locale, ...)
* OpenSSL
* Lua 5.3 (included in the source tree)

## Building: Windows Platforms
* Use [vcpkg](https://docs.microsoft.com/en-us/cpp/build/vcpkg) to install boost:
	* Run `vcpkg install boost:x64-windows-static`; this will also install OpenSSL.
	* For **32-bit builds** you'll also want to install `boost:x86-windows-static`
	* The project files are written to assume static linking (`/MT`).
	* Make sure you **integrate** using `vcpkg integrate install`.
* No additional steps are needed for the built-in Lua library, it should automatically build and link when the `ircd` project is built.
* The `ircd.exe` binary will generate into a `bin` directory.

## Building: UNIX-like Platforms
* Install boost and OpenSSL development libraries using your OS package manager (such as `apt-get install libboost1.71-dev libssl-dev`).
* If you plan to allow Unicode names, ensure that `boost-locale` is installed with ICU support **and** that your terminal has a UTF-8 locale.
* Before building the server code, build the Lua library:
	- `cd lualib` and `make linux local`; for other than Linux, replace `linux` with [whichever platform is needed](https://www.lua.org/manual/5.3/readme.html) (but keep the `local` part).
* After building `lualib` you can go back to the ircd source and build with CMake:
	- `cmake -S . -B build`
	- `cmake --build build`
* The  `ircd` executable binary will generate into a `bin` directory. You will need to either copy or symlink any desired Lua module files (`*.lua`) as appropriate.

## Running
* If you want to run `ircd` in an alternate directory: copy the `ircd` executable and all module files (`*.lua`) to the desired location.
* Write an appropriate [configuration file](Configuration).
* If you've set up the server to allow Unicode names (`utf8mapping = "rfc7700"`), remember to configure your terminal's locale to UTF-8.
* Launch the `ircd` executable. On UNIX-like platforms `ircd` will `fork()` into the background.
