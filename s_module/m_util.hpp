/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#pragma once

template<typename T>
static void sql_bind_value(sqlite3xx::statement& stmt, int const i, T& v)
{
	if (v.template is<int>()) {
		stmt.bind_int(i, v.template as<int>());
	} else if (v.template is<int64_t>()) {
		stmt.bind_int64(i, v.template as<int64_t>());
	} else if (v.template is<uint64_t>()) {
		stmt.bind_uint64(i, v.template as<uint64_t>());
	} else if (v.template is<double>()) {
		stmt.bind_double(i, v.template as<double>());
	} else if (v.get_type() == sol::type::string) {
		stmt.bind_text(i, ircd::normalize_nfc(v.template as<std::string>()));
	} else if (v.template is<sqlite3xx::column>()) {
		auto& col = v.template as<sqlite3xx::column>();
		if (!col.is_null() && col.type() == SQLITE_INTEGER) {
			stmt.bind_uint64(i, *col.get_uint64());
		} else if (!col.is_null() && col.type() == SQLITE_FLOAT) {
			stmt.bind_double(i, *col.get_double());
		} else if (!col.is_null() && col.type() == SQLITE_TEXT) {
			stmt.bind_text(i, ircd::normalize_nfc(col.get_text()));
		} else {
			stmt.bind_null(i);
		}
	} else {
		stmt.bind_null(i);
	}
}

/** Bind Lua varargs to a SQLite statement object */
static void sql_bind_varargs(sqlite3xx::statement& stmt, sol::variadic_args va)
{
	int i = 1;
	for (auto const& v : va) {
		sql_bind_value(stmt, i++, v);
	}
}

/** Generate value from Lua varargs as a string */
static std::string vararg_to_str(sol::variadic_args::value_type const& v)
{
	if (v.is<sqlite3xx::column>()) {
		auto const& col = v.as<sqlite3xx::column>();
		return col.get_text();
	} else {
		return ircd::normalize_nfc(v.as<std::string>());
	}
}

/** Bind Lua varargs to a boost.format object */
static void varargs_to_format(boost::format& fmt, sol::variadic_args va)
{
	using namespace boost::io;
	fmt.exceptions(all_error_bits ^ (too_many_args_bit | too_few_args_bit));
	for (auto v : va) {
		if (v.is<int>()) {
			fmt% v.as<int>();
		} else if (v.is<unsigned int>()) {
			fmt% v.as<unsigned int>();
		} else if (v.is<int64_t>()) {
			fmt% v.as<int64_t>();
		} else if (v.is<uint64_t>()) {
			fmt% v.as<uint64_t>();
		} else if (v.is<double>()) {
			fmt% v.as<double>();
		} else if (v.get_type() == sol::type::string) {
			fmt% ircd::normalize_nfc(v.as<std::string>());
		} else if (v.is<sqlite3xx::column>()) {
			auto& col = v.as<sqlite3xx::column>();
			if (!col.is_null() && col.type() == SQLITE_INTEGER) {
				fmt% *col.get_uint64();
			} else if (!col.is_null() && col.type() == SQLITE_FLOAT) {
				fmt% *col.get_double();
			} else if (!col.is_null() && col.type() == SQLITE_TEXT) {
				fmt% col.get_text();
			}
		}
	}
}

/** Retrieve pointer type from sol::object - this is so we can allow 'nil' to be passed for pointer parameters */
template<typename T>
T ptr_from_lua(sol::object const& v)
{
	if (v.is<T>()) {
		return T{ v.as<T>() };
	} else {
		return nullptr;
	}
}
