/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"
#include "m_api.hpp"
#include "m_util.hpp"

void ircd::module::init_api_chan()
{
	// channel object
	auto chan_type = lua_.create_named_table("channel");
	chan_type["find"] = &channel::find;
	chan_type["create"] = &channel::create;
	chan_type["type"] = &channel::type;
	chan_type["timestamp"] = &channel::timestamp;
	chan_type["set_topic"] = &channel::set_topic;
	chan_type["has_mode"] = &channel::has_mode;
	chan_type["get_key"] = channel::get_key;
	chan_type["set_key"] = channel::set_key;
	chan_type["get_limit"] = channel::get_limit;
	chan_type["set_limit"] = channel::set_limit;
	chan_type["add_user"] = &channel::add_user;
	chan_type["ins_user"] = &channel::ins_user;
	chan_type["del_user"] = &channel::del_user;
	chan_type["has_user"] = &channel::has_user;
	chan_type["kill"] = &channel::kill;
	chan_type["count"] = &channel::count;
	chan_type["is_op"] = &channel::is_op;
	chan_type["set_op"] = &channel::set_op;
	chan_type["is_voice"] = &channel::is_voice;
	chan_type["set_voice"] = &channel::set_voice;
	chan_type["do_message"] = &channel::do_message;
	chan_type["send_topic"] = &channel::send_topic;
	chan_type["send_names"] = &channel::send_names;
	chan_type["get_modes"] = &channel::get_mode_str;
	/*chan_type["get_list"] = &channel::get_list;*/
	chan_type["list_find"] = &channel::list_find;
	chan_type["list_add"] = &channel::list_add;
	chan_type["list_remove"] = &channel::list_remove;
	chan_type["list_count"] = &channel::list_count;
	chan_type["send_list"] = &channel::send_list;
	chan_type["is_banned"] = &channel::is_banned;
	chan_type["kick"] = &channel::kick;
	chan_type["set_modes"] = [this](std::string chname, sol::object cptr, sol::object sptr, std::string what, sol::variadic_args va) {
		std::vector<std::string> args;
		for (auto const& v : va) {
			args.push_back(vararg_to_str(v));
		}
		channel::set_modes(chname, ptr_from_lua<client_ptr>(cptr), ptr_from_lua<client_ptr>(sptr), what, args);
	};
	chan_type["get_members"] = [this](std::string chname) {
		std::vector<client_ptr> res;
		for (auto member : channel::get_members(chname)) {
			res.push_back(member);
		}
		return res;
	};
	chan_type["notify_members"] = [this](std::string chname, sol::object cpt, sol::object spt, uint32_t flags, std::string cmd, sol::variadic_args va) {
		auto cptr{ ptr_from_lua<client_ptr>(cpt) };
		for (auto member : channel::get_members(chname)) {
			if (member && !member->has_flag(CLI_REMOTE)) member->write_msg(ptr_from_lua<client_ptr>(spt), flags, cmd, va);
		}
	};
}
