/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"
#include "m_api.hpp"
#include "m_util.hpp"

void ircd::module::init_api_client()
{
	// client methods
	auto cli_type = lua_.new_usertype<client>("client");
	cli_type["has_flag"] = &client::has_flag;
	cli_type["set_flag"] = &client::set_flag;
	cli_type["socket"] = sol::property(&client::socket);
	cli_type["session"] = sol::property(&client::session);
	cli_type["uuid"] = sol::property(&client::uuid);
	cli_type["eid"] = sol::property(&client::eid, &client::set_eid);
	cli_type["timestamp"] = sol::property(&client::timestamp, &client::set_timestamp);
	cli_type["address"] = sol::property(&client::address, &client::set_address);
	cli_type["hostname"] = sol::property(&client::hostname, &client::set_hostname);
	cli_type["nick"] = sol::property(&client::nick, &client::set_nick);
	cli_type["username"] = sol::property(&client::username, &client::set_username);
	cli_type["userhost"] = sol::property(&client::userhost);
	cli_type["gecos"] = sol::property(&client::gecos, &client::set_gecos);
	cli_type["account"] = sol::property(&client::account, &client::set_account);
	cli_type["server"] = sol::property(&client::server);
	cli_type["caps"] = sol::property(&client::get_caps, &client::set_caps);
	cli_type["umodes"] = sol::property(&client::get_umodes, &client::set_umodes);
	cli_type["change_snomask"] = &client::change_snomask;
	cli_type["snomask"] = sol::property(&client::raw_snomask);
	cli_type["change_umodes"] = &client::change_umodes;
	cli_type["has_umode"] = &client::has_umode;
	cli_type["get_metadata"] = &client::get_metadata;
	cli_type["has_metadata"] = &client::has_metadata;
	cli_type["is_service"] = &client::is_service;
	cli_type["cap_notify"] = &client::cap_notify;
	cli_type["do_register"] = &client::do_register;
	cli_type["has_snomask"] = &client::has_snomask;
	cli_type["str_snomask"] = &client::get_snomask;
	cli_type.set(sol::meta_function::to_string, &client::userhost);
	cli_type["str_umodes"] = [](client& self) {
		return ircd::umode_str(self.get_umodes());
	};
	cli_type["set_metadata"] = [](client& self, std::string key, sol::object value) {
		self.set_metadata(key, value.as<std::string>());
	};
	cli_type["has_cap"] = [](client& self, std::string name) -> bool {
		auto iter = ircd::cap_names.find(name);
		if (iter != ircd::cap_names.end()) {
			auto info = iter->second;
			return self.has_cap(info.mask);
		} else {
			return false;
		}
	};
	cli_type["set_cap"] = [](client& self, std::string name, bool state) {
		auto iter = ircd::cap_names.find(name);
		if (iter != ircd::cap_names.end()) {
			auto info = iter->second;
			self.set_cap(info.mask, state);
		}
	};
	cli_type["write_msg"] = [](client& self, sol::object sptr, uint32_t flags, std::string cmd, sol::variadic_args va) {
		if (sptr.is<client_ptr>()) {
			self.write_msg(sptr.as<client_ptr>(), flags, cmd, va);
		} else {
			self.write_msg(nullptr, flags, cmd, va);
		}
	};
	cli_type["send_numeric"] = [](client& self, int n, sol::variadic_args va) {
		if (self.has_flag(CLI_REMOTE) && !rpl_allow_remote(n)) return;
		boost::format fmt{ rpl_str(n) };
		varargs_to_format(fmt, va);
		self.write_msg(nullptr, 0, format_str("%03u", n), fmt.str());
	};
	cli_type["send_notice"] = [](client& self, std::string text) {
		self.send_notice(text);
	};
	cli_type["send_notice"] = [](client& self, std::string fspec, sol::variadic_args va) {
		boost::format fmt{ fspec };
		varargs_to_format(fmt, va);
		self.send_notice(fmt.str());
	};
	cli_type["close"] = [](client& self, std::string why) {
		kill_client(self.shared_from_this(), why);
	};
	cli_type["kill"] = [](client& self, sol::object cpt, sol::object spt, std::string why) {
		kill_client(spt.as<client_ptr>(), why);
	};
	cli_type["shutdown"] = [](client& self) {
		if (auto sptr = self.session(); sptr && sptr->socket.is_open()) {
			boost::system::error_code ec;
			sptr->socket.cancel(ec);
			sptr->socket.shutdown(tcp::socket::shutdown_receive, ec);
		}
	};
}
