/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"
#include "m_api.hpp"
#include "m_util.hpp"

void ircd::module::init_api_app()
{
	// Server info object
	auto info_type = lua_.new_usertype<server_info>("server_info");
	info_type["create_time"] = &server_info::create_time;
	info_type["fullname"] = &server_info::fullname;
	info_type["id"] = &server_info::id;
	info_type["description"] = &server_info::description;
	info_type["admin_loc1"] = sol::property([](server_info& si) { return si.admin[0]; });
	info_type["admin_loc2"] = sol::property([](server_info& si) { return si.admin[1]; });
	info_type["admin_email"] = sol::property([](server_info& si) { return si.admin[2]; });
	info_type["max_clients"] = &server_info::max_clients;
	info_type["net_name"] = &server_info::net_name;
	info_type["net_description"] = &server_info::net_description;
	info_type["motd_user_text"] = &server_info::motd_user_text;
	info_type["motd_oper_text"] = &server_info::motd_oper_text;
	// IRC message object
	auto msg_type = lua_.new_usertype<message>("message");
	msg_type["tags"] = &message::tags;
	msg_type["prefix"] = &message::prefix;
	msg_type["command"] = &message::command;
	msg_type["flags"] = &message::flags;
	msg_type["args"] = &message::args;
	// application methods
	auto app_type = lua_.create_named_table("ircd");
	app_type["info"] = &info;
	app_type["db"] = &db;
	app_type["valid_nick"] = &valid_nick;
	app_type["valid_username"] = &valid_username;
	app_type["valid_chan"] = &valid_chan;
	app_type["reload_config"] = &reload_config;
	app_type["num_sessions"] = &num_sessions;
	app_type["num_connections"] = &num_connections;
	app_type["get_client"] = [](boost::uuids::uuid const& uuid) -> client_ptr { return get_client(uuid); };
	app_type["new_client"] = &new_client;
	app_type["find_user"] = &find_user;
	app_type["find_nick"] = &find_nick;
	app_type["find_server_name"] = &find_server_name;
	app_type["find_entity"] = &find_entity;
	app_type["send_isupport"] = &send_isupport;
	app_type["do_message"] = &do_message;
	app_type["add_jupe"] = &add_jupe;
	app_type["del_jupe"] = &del_jupe;
	app_type["get_jupe_status"] = &get_jupe_status;
	app_type["connect_to_link"] = &connect_to_link;
	app_type["send_server_caps"] = &send_server_caps;
	app_type["validate_server_caps"] = &validate_server_caps;
	app_type["add_kline"] = &add_kline;
	app_type["add_kline_temp"] = &add_kline_temp;
	app_type["del_kline"] = &del_kline;
	app_type["is_user_kline"] = &is_user_kline;
	app_type["has_common_channels"] = &has_common_channels;
	app_type["get_common_channels"] = &get_common_channels;
	app_type["query_log"] = &query_log;
	app_type["version_str"] = &ircd::version_str;
	app_type["timestamp"] = &ircd::timestamp;
	app_type["valid_mask"] = &ircd::valid_mask;
	app_type["name_uuid"] = &ircd::name_uuid;
	app_type["time_uuid"] = &ircd::time_uuid;
	app_type["random_uuid"] = &ircd::random_uuid;
	app_type["uuid_string"] = &ircd::uuid_string;
	app_type["uuid_from_str"] = &ircd::uuid_from_str;
	app_type["create_message"] = [](sol::object spt, uint32_t flags, std::string cmd, sol::variadic_args va) {
		auto sptr{ ptr_from_lua<client_ptr>(spt) };
		message msg;
		if (sptr) msg.prefix = sptr->userhost();
		msg.command = normalize_nfkc(cmd);
		msg.flags = flags;
		for (auto const& v : va) {
			msg.args.push_back(vararg_to_str(v));
		}
		if (sptr && sptr->has_flag(CLI_ACCOUNT)) {
			msg.tags["account"] = sptr->account();
		}
		return msg;
	};
	app_type["address_from_str"] = [](std::string what) {
		return asio::ip::address::from_string(what);
	};
	lua_["same_text"] = [](std::string left, std::string right) {
		return ircd::same_text(left, right);
	};
	lua_["normalize_nfkc"] = [](std::string text) {
		return ircd::normalize_nfkc(text);
	};
	lua_["normalize_nfc"] = [](std::string text) {
		return ircd::normalize_nfc(text);
	};
	lua_["fold_case_nfkc"] = [](std::string text) {
		return ircd::fold_case_nfkc(text);
	};
	lua_["fold_case_nfc"] = [](std::string text) {
		return ircd::fold_case_nfc(text);
	};
	lua_["split"] = [](std::string source, std::string delims) {
		std::vector<std::string> tokens;
		ircd::split_text(tokens, source, delims, std::nullopt);
		return tokens;
	};
	app_type["cap_to_str"] = [](uint64_t what) {
		return ircd::cap_to_str(what);
	};
	app_type["cap_from_str"] = [](std::string what) -> std::tuple<uint64_t, std::string> {
		std::string err;
		uint64_t caps = ircd::cap_from_str(what, &err);
		return std::make_tuple(caps, err);
	};
	app_type["caps_implemented"] = []() {
		return ircd::caps_implemented();
	};
	app_type["add_command"] = [this](std::string cname, sol::protected_function handler) {
		if (init_complete_) return;
		commands_[upper_case_nfkc(cname)] = handler;
	};
	app_type["set_numeric"] = [this](int n, std::string format) {
		if (init_complete_) return;
		if (n > 99 && n < 1000) rpl_str_set(n, format);
	};
	app_type["add_cap"] = [this](std::string name) {
		if (init_complete_) return;
		ircd::create_cap(name);
	};
	app_type["add_umode"] = [this](char mode, std::string description) -> sol::lua_value {
		if (init_complete_) return sol::lua_nil;
		auto id = ircd::new_umode(mode, description);
		if (id != 0) {
			return id;
		} else {
			return sol::lua_nil;
		}
	};
	app_type["add_chanmode"] = [this](char mode, char category, std::string label) {
		if (init_complete_) return;
		uint32_t id{ 0 };
		switch (category) {
		case 'A':
			id = ircd::new_chmode(mode, MODE_CAT_A, label);
			break;
		case 'B':
			id = ircd::new_chmode(mode, MODE_CAT_B, label);
			break;
		case 'C':
			id = ircd::new_chmode(mode, MODE_CAT_C, label);
			break;
		case 'D':
			id = ircd::new_chmode(mode, MODE_CAT_D, label);
			break;
		}
		if (id != 0) lua_[label] = id;
	};
	app_type["write_log"] = [this](int category, std::string text) {
		return write_log(category, shared_from_this(), text);
	};
	app_type["write_log"] = [this](int category, std::string spec, sol::variadic_args va) {
		boost::format fmt{ spec };
		varargs_to_format(fmt, va);
		return write_log(category, shared_from_this(), fmt.str());
	};
	app_type["snomask_log"] = [this](int category, uint16_t snomask, std::string text) {
		return snomask_log(category, snomask, shared_from_this(), text);
	};
	app_type["snomask_log"] = [this](int category, uint16_t snomask, std::string spec, sol::variadic_args va) {
		boost::format fmt{ spec };
		varargs_to_format(fmt, va);
		return snomask_log(category, snomask, shared_from_this(), fmt.str());
	};
	app_type["notify_servers"] = [](sol::object cpt, sol::object spt, uint32_t flags, std::string cmd, sol::variadic_args va) {
		message msg;
		auto cptr{ ptr_from_lua<client_ptr>(cpt) };
		auto sptr{ ptr_from_lua<client_ptr>(spt) };
		msg.command = cmd;
		msg.flags = flags;
		for (auto const& v : va) {
			msg.args.push_back(vararg_to_str(v));
		}
		notify_servers(cptr, sptr, msg);
	};
	app_type["notify_snomask"] = [](uint16_t snomask, std::string spec, sol::variadic_args va) {
		boost::format fmt{ spec };
		varargs_to_format(fmt, va);
		notify_snomask(snomask, fmt.str());
	};
	app_type["get_local_users"] = []() {
		std::vector<client_ptr> res;
		for (auto qry = get_local_users(); !qry.eof(); qry.step()) {
			res.push_back(get_client(qry(0).get_uuid()));
		}
		return res;
	};
	app_type["get_server_clients"] = []() {
		std::vector<client_ptr> res;
		for (auto qry = get_server_clients(); !qry.eof(); qry.step()) {
			res.push_back(get_client(qry(0).get_uuid()));
		}
		return res;
	};
	app_type["num_chans"] = [](client_ptr sptr) -> unsigned int {
		return db.exec_scalar("SELECT COUNT(*) FROM [channel-users] WHERE [EID]=?", sptr->eid()).get_uint(0);
	};
}
