/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"
#include "m_api.hpp"
#include "m_util.hpp"

void ircd::module::init_api_sql()
{
	auto col_type = lua_.new_usertype<sqlite3xx::column>("sql_column");
	auto stmt_type = lua_.new_usertype<sqlite3xx::statement>("sql_stmt");
	auto db_type = lua_.new_usertype<sqlite3xx::database>("sql_db");
	lua_["sql_escape_text"] = &sqlite3xx::escape_text;
	col_type["type"] = sol::property(&sqlite3xx::column::type);
	col_type["null"] = sol::property(&sqlite3xx::column::is_null);
	col_type["get_int"] = [](sqlite3xx::column& self, sol::object def) -> std::optional<int> {
		if (def != sol::lua_nil) {
			return self.get_int(def.as<int>());
		} else {
			return self.get_int();
		}
	};
	col_type["get_int64"] = [](sqlite3xx::column& self, sol::object def) -> std::optional<int64_t> {
		if (def != sol::lua_nil) {
			return self.get_int64(def.as<int64_t>());
		} else {
			return self.get_int64();
		}
	};
	col_type["get_uint"] = [](sqlite3xx::column& self, sol::object def) -> std::optional<unsigned int> {
		if (def != sol::lua_nil) {
			return self.get_uint(def.as<unsigned int>());
		} else {
			return self.get_uint();
		}
	};
	col_type["get_uint64"] = [](sqlite3xx::column& self, sol::object def) -> std::optional<uint64_t> {
		if (def != sol::lua_nil) {
			return self.get_uint64(def.as<uint64_t>());
		} else {
			return self.get_uint64();
		}
	};
	col_type["get_numeric"] = [](sqlite3xx::column& self, sol::object def) -> std::optional<double> {
		if (def != sol::lua_nil) {
			return self.get_double(def.as<double>());
		} else {
			return self.get_double();
		}
	};
	col_type["get_uuid"] = [](sqlite3xx::column& self) -> boost::uuids::uuid {
		return self.get_uuid();
	};
	auto const col_tostring = [](sqlite3xx::column& self) -> std::string {
		return self.get_text();
	};
	col_type["get_text"] = col_tostring;
	col_type.set(sol::meta_function::to_string, col_tostring);
	col_type.set(sol::meta_function::concatenation, col_tostring);
	stmt_type["count"] = sol::property(&sqlite3xx::statement::field_count);
	stmt_type["eof"] = sol::property(&sqlite3xx::statement::eof);
	stmt_type["field_index"] = &sqlite3xx::statement::field_index;
	stmt_type["field_name"] = &sqlite3xx::statement::field_name;
	stmt_type["field_decltype"] = &sqlite3xx::statement::field_decltype;
	stmt_type["field_data_type"] = &sqlite3xx::statement::field_data_type;
	stmt_type["field_value"] = &sqlite3xx::statement::field_value;
	stmt_type["is_field_null"] = &sqlite3xx::statement::is_field_null;
	stmt_type["step"] = &sqlite3xx::statement::step;
	stmt_type["finalize"] = &sqlite3xx::statement::finalize;
	stmt_type.set(sol::meta_function::index, &sqlite3xx::statement::field_value);
	stmt_type["param_count"] = &sqlite3xx::statement::param_count;
	stmt_type["param_name"] = &sqlite3xx::statement::param_name;
	stmt_type["bind_text"] = [](sqlite3xx::statement& self, sqlite3xx::index_reference i, std::string text) -> int { return self.bind_text(i, text); };
	stmt_type["bind_int"] = &sqlite3xx::statement::bind_int;
	stmt_type["bind_int64"] = &sqlite3xx::statement::bind_int64;
	stmt_type["bind_uint64"] = &sqlite3xx::statement::bind_uint64;
	stmt_type["bind_numeric"] = &sqlite3xx::statement::bind_double;
	stmt_type["bind_null"] = &sqlite3xx::statement::bind_null;
	stmt_type["reset"] = &sqlite3xx::statement::reset;
	stmt_type.set(sol::meta_function::length, &sqlite3xx::statement::param_count);
	stmt_type.set(sol::meta_function::new_index, [](sqlite3xx::statement& stmt, int n, sol::object val) {
		sql_bind_value(stmt, n, val);
	});
	db_type["table_exists"] = &sqlite3xx::database::table_exists;
	db_type["changes"] = sol::property(&sqlite3xx::database::changes);
	db_type["compile"] = [](sqlite3xx::database& self, std::string text) { return self.compile(text); };
	db_type["last_rowid"] = sol::property(&sqlite3xx::database::last_rowid);
	db_type["busy_timeout"] = sol::property(&sqlite3xx::database::get_busy_timeout, &sqlite3xx::database::set_busy_timeout);
	db_type["version"] = sol::property(&sqlite3xx::database::version);
	db_type["last_error_code"] = sol::property(&sqlite3xx::database::last_error_code);
	db_type["last_error_message"] = sol::property(&sqlite3xx::database::last_error_message);
	db_type["exec_dml"] = [](sqlite3xx::database& self, std::string sql) {
		return self.exec_dml(sql);
	};
	db_type["exec_scalar"] = [](sqlite3xx::database& self, std::string sql, sol::variadic_args va) -> sqlite3xx::column {
		if (auto stmt = self.compile(sql); stmt.is_valid()) {
			sql_bind_varargs(stmt, va);
			if (stmt.step()) {
				return stmt.field_value(0);
			}
		}
		return sqlite3xx::column{};
	};
	db_type["exec_query"] = [](sqlite3xx::database& self, std::string sql, sol::variadic_args va) -> sqlite3xx::statement {
		if (auto stmt = self.compile(sql); stmt.is_valid()) {
			sql_bind_varargs(stmt, va);
			if (stmt.step()) {
				return stmt;
			}
		}
		return sqlite3xx::statement{ self };
	};
	db_type["compile"] = [](sqlite3xx::database& self, std::string sql) -> sqlite3xx::statement {
		return self.compile(sql);
	};
#if defined(SQLITE_ENABLE_SESSION) && defined(SQLITE_ENABLE_PREUPDATE_HOOK)
	db_type["session_available"] = [](sqlite3xx::database&) { return true; };
	auto chgiter_type = lua_.new_usertype<sqlite3xx::changeset::iterator>("sql_changeset_iterator");
	auto chgset_type = lua_.new_usertype<sqlite3xx::changeset>("sql_changeset");
	auto session_type = lua_.new_usertype<sqlite3xx::session>("sql_session");
	db_type["create_session"] = &sqlite3xx::database::create_session;
	chgiter_type["table"] = sol::property(&sqlite3xx::changeset::iterator::table);
	chgiter_type["type"] = sol::property(&sqlite3xx::changeset::iterator::type);
	chgiter_type["ncols"] = sol::property(&sqlite3xx::changeset::iterator::ncols);
	chgiter_type["indirect"] = sol::property(&sqlite3xx::changeset::iterator::get_indirect);
	chgiter_type["eof"] = sol::property(&sqlite3xx::changeset::iterator::eof);
	chgiter_type["new_value"] = &sqlite3xx::changeset::iterator::new_value;
	chgiter_type["old_value"] = &sqlite3xx::changeset::iterator::old_value;
	chgiter_type["next"] = &sqlite3xx::changeset::iterator::next;
	// TODO: set_apply_filter
	// TODO: set_apply_conflict
	chgset_type["start"] = &sqlite3xx::changeset::start;
	chgset_type["apply"] = &sqlite3xx::changeset::apply;
	chgset_type["concat"] = &sqlite3xx::changeset::concat;
	chgset_type["invert"] = &sqlite3xx::changeset::invert;
	chgset_type["size"] = sol::property(&sqlite3xx::changeset::size);
	session_type["enabled"] = sol::property(&sqlite3xx::session::is_enabled, &sqlite3xx::session::set_enabled);
	session_type["empty"] = sol::property(&sqlite3xx::session::empty);
	session_type["indirect"] = sol::property(&sqlite3xx::session::get_indirect, &sqlite3xx::session::set_indirect);
	session_type["attach"] = &sqlite3xx::session::attach;
	session_type["changes"] = &sqlite3xx::session::changes;
	session_type["patchset"] = &sqlite3xx::session::patchset;
	session_type["diff"] = &sqlite3xx::session::diff;
	// TODO: set_table_filter
#else
	db_type["session_available"] = [](sqlite3xx::database&) { return false; };
#endif
}