/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#pragma once

#include <any>

namespace ircd {
	/// Entry in the API enums table.
	struct enum_info {
		/// Lua identifier.
		char const* label;

		/// Value of the enum.
		int value;

		/// API version required.
		int version;
	};

	/// API constants available to Lua modules.
    extern std::vector<enum_info> const api_enums;

	/// Lua debugger script.
	extern char const* LUA_DEBUGGER_SCRIPT;

	/// UTF-8 support functions.
	extern char const* UTF8_SUPPORT_SCRIPT;
};
