/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#pragma once

namespace ircd {
	class module : public std::enable_shared_from_this<module> {
	public:
		module(std::string_view source_file);

		virtual ~module();

		/// Return module name.
		std::string name() const;

		/// Load associated Lua source file and initialize module.
		void load();

		/// Execute command possibly associated with this module.
		bool do_command(client_ptr cptr, client_ptr sptr, message msg);

		/// Call a hook function in this module.
		template<typename ...Args>
		std::optional<sol::protected_function_result> call_hook(std::string_view name, Args&& ...args);

	private:
		sol::state lua_;
		std::string name_, source_;
		int version_;
		bool init_complete_;
		std::map<std::string, sol::protected_function> commands_;
		void init_api();
		void init_api_app();
		void init_api_client();
		void init_api_chan();
		void init_api_sql();
	};

	typedef std::shared_ptr<module> module_ptr;
};
