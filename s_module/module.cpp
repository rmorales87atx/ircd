/*
 * ircd - An IRCv3 Server
 * Copyright � 2018
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, you can obtain one at: http://mozilla.org/MPL/2.0/
 */

#include "app.hpp"
#include "m_api.hpp"

using namespace std::placeholders;

ircd::module::module(std::string_view source_file)
	: source_{ source_file }
	, version_{ 0 }
{
	lua_.open_libraries();
}

ircd::module::~module()
{}

std::string ircd::module::name() const
{
	return name_;
}

void ircd::module::init_api()
{
	// utility functions
	lua_.safe_script(UTF8_SUPPORT_SCRIPT);
	// standard constants
	for (enum_info const& en : api_enums) {
		if (version_ >= en.version) {
			lua_[en.label] = en.value;
		}
	}
	// insert constants for limits
	for (limit_info const* iptr = limit_table; iptr->label; iptr++) {
		lua_[iptr->label] = info.limits[iptr->id];
	}
	init_api_app();
	init_api_client();
	init_api_chan();
	init_api_sql();
}

void ircd::module::load()
{
	using std::string;
	using sol::table;
	using sol::optional;
	init_complete_ = false;
	lua_.open_libraries();
	if (lua_debug()) {
		lua_.require_script("debugger", LUA_DEBUGGER_SCRIPT);
		lua_.safe_script("debugger.auto_where = 2");
	}
	lua_.safe_script("hooks = {}");
	lua_.safe_script_file(source_);
	// validate interface table
	table info = lua_["module"];
	if (!info.valid() || info.empty()) throw std::runtime_error{ "missing interface table" };
	if (!info["name"].valid()) throw std::runtime_error{ "missing name" };
	name_ = info["name"];
	version_ = info["api"].get_or(0);
	if (version_ < 1) throw std::runtime_error{ "invalid API version" };
	// load API methods
	init_api();
	// run module initializer if available
	if (sol::protected_function init = lua_["module"]["init"]; init.valid()) {
		sol::protected_function_result res = init();
		if (!res.valid()) {
			sol::error err = res;
			write_log(LOG_DEBUG, shared_from_this(), "Error in module init: %1%", err.what());
			throw std::runtime_error{ err.what() };
		}
	}
	init_complete_ = true;
}

bool ircd::module::do_command(client_ptr cptr, client_ptr sptr, message msg)
{
	auto iter = commands_.find(msg.command);
	if (iter != commands_.end()) {
		sol::protected_function_result res = iter->second(cptr, sptr, msg);
		if (res.valid()) {
			sol::optional<bool> rval = res;
			return rval.value_or(true);
		} else {
			sol::error err = res;
			write_log(LOG_DEBUG, shared_from_this(), "Error running command '%1%': %2%", msg.command, err.what());
			sptr->send_numeric(ERR_UNKNOWN, sptr->nick(), msg.command, err.what());
		}
		return true;
	}
	return false;
}
